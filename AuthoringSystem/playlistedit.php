<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/6/2017
 * Time: 11:12 PM
 */
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/mediaentrybean.php';
require_once '../wp-content/php/model/playlist.class.php';
require_once '../wp-content/php/model/playlistentry.class.php';
require_once '../wp-content/php/model/mediacategory.class.php';
require_once '../wp-content/php/dao/MediaItemDao.php';
require_once '../wp-content/php/dao/PlaylistEntryDao.php';
require_once '../wp-content/php/dao/PlaylistDao.php';
require_once '../wp-content/php/model/video.class.php';

$con = getMySqliDbConnection();
$playlistDao = new PlaylistDao($con);
$playlistEntryDao = new PlaylistEntryDao($con);
$current_user = wp_get_current_user();
$playlistId = $_POST['playlistid'];
$playlistInfoEdit = $_POST['playlistinfoedit'];
if (!empty($playlistInfoEdit) && $playlistInfoEdit == "1") {
    $playlist = new Playlist();
    $playlist->id = $playlistId;
    $playlist->name = $_POST["playlistname"];
    $playlist->blurb = $_POST["playlistblurb"];
    if (!empty($_POST['makeinvisible']) && $_POST['makeinvisible'] == "on") {
        $playlist->playlistPageInvisible = 1;
    } else {
        $playlist->playlistPageInvisible = 0;
    }
    $playlist = $playlistDao->save($playlist);
    $playlistId = $playlist->id;
}
if (empty($playlistId)) {
    $playlistId =  $_GET["playlistid"];
}
if ( empty($playlistId) ) {
    die("No playlist id");
} else {
    $playlist = $playlistDao->getPlaylistById($playlistId);
}

$mediaIdToAdd = $_POST['mediaidtoadd'];
if (!empty($mediaIdToAdd)) {
    $playlistEntryDao->insert($mediaIdToAdd, $playlist);
}
$con->close();
?>

<head>
    <title>Playlist Edit</title>

    <link rel="stylesheet" type="text/css" href="css/authoring.css">
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">

    <!-- Standard datatables-editor includes -->
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.1/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/css/editor.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/resources/demo.css">
    <style type="text/css" class="init">
    </style>


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
    </script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/js/RowReorder-1.2.3/js/dataTables.rowReorder.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/js/dataTables.editor.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/syntax/shCore.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/demo.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/editor-demo.js">
    </script>

    <!-- End of standard datatables-editor includes -->



    <script type="text/javascript" language="javascript" class="init">

        function openWin(rowData) {
            var x = 10;
            if (typeof rowData == 'undefined') {
                return;
            }
            var plmiId = rowData.playlist_media_item.id;
            var url = "plmiEditor.php?id=" + plmiId;
            window.open(url,"","width="+850+",height="+800+",status=yes,toolbar=no,scrollbars=no,left=100,top=100");
        }

        function openMediaPicker() {
            var url = "mediapickerpopup.php";
            window.open(url,"","width="+850+",height="+800+",status=yes,toolbar=no,scrollbars=no,left=975,top=100");
        }

        function addNewRandomSong() {
            var randomNumberBetween1and600 = Math.floor(Math.random() * 160) + 431;
            alert("add new song id is: " + randomNumberBetween1and600);
            addNewSong(randomNumberBetween1and600)
        }

        function addNewSong(mediaId) {
            document.getElementById("mediaidtoadd").value = mediaId;
            document.getElementById("playlistedit").submit();

        }

        var editor; // use a global for the submit and return data rendering in the examples

        $(document).ready(function() {
            editor = new $.fn.dataTable.Editor( {
                ajax:  '<?=content_url()?>/php/pageservice/playlist-grid-data.php?playlistid=<?=$playlistId?>',
                table: '#example',
                fields: [ {
                    label: 'order:',
                    name: 'playlist_media_item.sequence',
                    message: 'This field can only be edited via click and drag row reordering.'
                }, {
                    label: 'Title:',
                    name:  'media_item.title'
                }, {
                    label: 'Artist:',
                    name:  'media_item.artist'
                }
                ]
            } );

            var table = $('#example').DataTable( {
                dom: 'Bfrtip',
                ajax:  '<?=content_url()?>/php/pageservice/playlist-grid-data.php?playlistid=<?=$playlistId?>',
                columns: [
                    { data: 'playlist_media_item.sequence', className: 'reorder' },
                    { data: 'media_item.title' },
                    { data: 'media_item.artist' }
                ],
                columnDefs: [
                    { orderable: false, targets: [ 1,2 ] }
                ],
                rowReorder: {
                    dataSrc: 'playlist_media_item.sequence',
                    editor:  editor
                },
                select: true,
                buttons: [
                    {
                        text: 'Add To Playlist',
                        action: function ( e, dt, node, config ) {
                            openMediaPicker();
                        }
                    },
                    {
                        text: 'Edit Selected',
                        action: function ( e, dt, node, config ) {
                            var table = $('#example').dataTable().api();
                            var data = table.rows( { selected: true } ).data()[0];
                            openWin(data);
                        }
                    },
                    {
                        text: 'Add New Song',
                        action: function (e, dt, node, config ) {
                            addNewRandomSong();
                        }
                    },
                    { extend: 'remove', editor: editor }
                ]
            } );
        } );



    </script>
</head>

<body>
<br/><br/>
<form action="playlistedit.php" onsubmit="javascript:doSubmit(this)" enctype="multipart/form-data" method="post" id="playlistedit" name="playlistedit"><br/><br/>
    <input type="hidden" id="addnew" name="addnew" value="0";
    <input type="hidden" name="magicquotecheck" value="'" />
    <input type="hidden" name="editrecord" value="<?=$playlistId?>" />
    <input type="hidden" name="mediaidtoadd" id="mediaidtoadd" value="" />
    <input type="hidden" name="playlistid" id="playlistid" value="<?=$playlistId?>" />
    <?php
    if (empty($playlistId)) {
        echo "<b>New Playlist</b>";
    } else {
        echo "<b>PLAYLIST ID #" . $playlistId . "</b>";
    }
    ?>
    <br/><br/>
    <table>
        <tr>
            <td>Name</td>
            <td><?=$playlist->name?></td>
        </tr>
        <tr>
        <tr>
            <td>Guid</td>
            <td><?=$playlist->guid?></td>
        </tr>
        <tr>
            <td>Playlist Blurb</td>
            <td><?=$playlist->blurb?></td>
        </tr>
        <tr>
            <td colspan="2">
                <br/>
                <a href="playlistinfoedit.php?playlistid=<?=$playlistId?>">Edit Playlist Info</a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br/>
                <hr/>
                <br/>
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Order</th>
                        <th>Title</th>
                        <th>Artist</th>
                    </tr>
                    </thead>
                </table>
                <hr/>
            </td>
        </tr>
    </table>
    <br/><br/>
    <a href="playlistmenu.php?playlistid=<?=$playlistId?>">Playlist Menu</a><br/><br/>
    <br/><br/>
    <a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
