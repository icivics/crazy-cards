<?php
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once('report.class.php');

$con = getMySqliDbConnection();

$deleteReportId = $_GET["deletereportid"];
if (!empty($deleteReportId)) {
    $sql = "delete from report where id=" . $deleteReportId;
    $result = mySqli_query_wrapper($con, $sql, "reports delete");
}

$reports = array();
$sql = "select * from report";
$result = mySqli_query_wrapper($con, $sql, "reports");
while($row = mysqli_fetch_assoc($result))
{
	$report = new Report();
	$report->id = $row['id'];
	$report->shortName = $row['short_name'];
	$report->reportName = $row['report_name'];
	array_push($reports, $report);
}

$con->close();

?>
<html>
<head>
<script language="Javascript">
function confirmDelete(reportId) {
	if (confirm("Are you sure?") );
		location.href = "?deletereportid=" + reportId;
}
</script>
</head>

<body>

<?php
	echo "<table border=1>";
	echo "<tr><td>&nbsp;</td><td>Report Name</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
	foreach ($reports as $report)
	{
		echo "<tr>";
		echo "<td><a href=\"sqllookup.php?report=" . $report->shortName . "\">Run</a></td>";
		echo "<td>" . $report->reportName . "</td>";
		echo "<td><a href=\"reportedit.php?mode=edit&reportid=" . $report->id . "\">Edit</a></td>";
		echo "<td><a href=\"javascript:confirmDelete(" . $report->id . ");\">Delete</a></td>";
		echo "</tr>";
	}
	echo "</table>";
	echo "<a href=\"reportedit.php?mode=add\">Add new report</a>";
?>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
