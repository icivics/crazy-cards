<?php
require_once('auth.php');

require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/mediaentrybean.php';
require_once '../wp-content/php/model/mediacategory.class.php';
require_once '../wp-content/php/dao/MediaCategoryDao.php';
require_once '../wp-content/php/dao/MediaItemDao.php';
require_once '../wp-content/php/model/video.class.php';
require_once '../wp-content/php/output/DataTableOutputter.php';

$category = $_GET['category'];
if (empty($category)) {
    $category = "MUSIC";
}
$con = getMySqliDbConnection();
$mediaCategoryDao = new MediaCategoryDao($con);

$mediaCategories = $mediaCategoryDao->getAllMediaCategories();
$myMediaCategory = $mediaCategoryDao->findMediaCategoryByName($mediaCategories, $category);

$dataTableOutputter = new DataTableOutputter();

function checkedValue($ctgname, $categoryToMatch) {
    if (strcasecmp($ctgname, $categoryToMatch) == 0) {
        return "CHECKED";
    } else {
        return "";
    }
}

?>

<!DOCTYPE html>
<html>
	<title>Authoring Song List</title>
	<head>
        <!-- Standard datatables-editor includes -->
        <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.1/css/rowReorder.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/css/editor.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/resources/syntax/shCore.css">
        <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/resources/demo.css">
        <style type="text/css" class="init">
        </style>


        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/js/RowReorder-1.2.3/js/dataTables.rowReorder.js">
        </script>
        <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/js/dataTables.editor.js">
        </script>
        <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/syntax/shCore.js">
        </script>
        <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/demo.js">
        </script>
        <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/editor-demo.js">
        </script>

        <!-- End of standard datatables-editor includes -->

        <script type="text/javascript" language="JavaScript" class="init">

            var editor; // use a global for the submit and return data rendering in the examples

            $(document).ready(function() {
                editor = new $.fn.dataTable.Editor( {
                    ajax:  '<?=content_url()?>/php/pageservice/authoring-media-grid-data.php?category=<?=$category?>',
                    table: '#song-grid',
                    <?=$dataTableOutputter->outputTableFields($myMediaCategory);?>
                    /*fields: [ {
                        label: 'Title:',
                        name:  'title'
                    }, {
                        label: 'Artist:',
                        name:  'artist'
                    }, {
                        label: 'Album:',
                        name:  'album'
                    }, {
                        label: 'Genre:',
                        name:  'genre'
                    }, {
                        label: 'Released:',
                        name:  'released'
                    }, {
                        label: 'Categories:',
                        name:  'categories'
                    }
                    ]
                    */
                } );

                var songGridTable = $('#song-grid').DataTable( {
                    dom: 'Blfrtip',
                    lengthMenu: [ 10, 25, 50, 75, 100 ],
                    ajax:  '<?=content_url()?>/php/pageservice/authoring-media-grid-data.php?category=<?=$category?>',
                    <?=$dataTableOutputter->outputDataColumns($myMediaCategory);?>
                    /*columns: [
                        { data: 'id' },
                        { data: 'title' },
                        { data: 'artist' },
                        { data: 'album' },
                        { data: 'genre' },
                        { data: 'released' },
                        { data: 'categories'}
                    ],*/
                    columnDefs: [
                        {
                            "targets": 0,
                            "visible": false,
                            "searchable": false
                        },
                        {
                            "targets": 1,
                            "render": function ( data, type, row, meta ) {
                                if(type === 'display'){
                                    var title = data;
                                    var id = row.id;
                                    data = '<a href="mediaentry.php?id=' + id + '">' + title + '</a>';
                                }
                                return data;
                            },
                        } ],
                    select: false,
                    buttons: [

                    ]
                } );

            } );


            function newCategory(radioButtonElement) {
                category = radioButtonElement.value;
                location.href = "media-list.php?category=" + category;
            }

            function createNewMediaItem() {
                category = document.querySelector('input[name = "categories"]:checked').value;
                location.href = "mediaentry.php?category=" + category;
            }

        </script>

	</head>
	<body>
		<div class="header"><h1>Media List</h1></div>
		<div class="container">
            <table width="100%">
                <tr>
                    <td valign="top">
                        Categories:
                    </td>
                    <td>
                        <?php
                        $count = 1;
                        foreach ($mediaCategories as $ctg):
                            echo '<input type="radio" name="categories" value="' . $ctg->name . '" ' . checkedValue($ctg->name, $category) . ' onclick="newCategory(this);">' . $ctg->name . '</input>';
                            if ($count == 8) {
                                echo '<br/>';
                            } else {
                                echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                            }
                            $count++;
                        endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr/>
                        <br/><br/>
                    </td>
                </tr>
            </table>
			<table id="song-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
					<thead align="left">
						<tr>
                            <?=$dataTableOutputter->outputTheadHtml($myMediaCategory);?>
						</tr>
					</thead>
			</table>
            <br/><br/>
            <input type="button" value="Add New Media Item" onClick="createNewMediaItem();" />
            <br/><br/>
            <input type="button" value="Main Menu" onClick="location.href='authoringmenu.php'" />
        </div>
    </body>
</html>
