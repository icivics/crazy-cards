<?php
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Database lookup</title>
<?php


$sql = ""; 
$resultsLog = "";
$reportShortName = $_GET["report"];
$submitSqlQuery = $_POST["submitSqlQuery"];


if(!empty($submitSqlQuery) || !empty($reportShortName) ) {
    $con = getMySqliDbConnection();
	if (!empty($reportShortName)) {
		$sql = getReportSql($con, $reportShortName);
	} else {
		$sql = stripslashes($_POST["sql"]);
	}
	//echo "Sql is : " . $sql;
	
	if (strncasecmp($sql, "select", strlen("select")))
	{
		$resultsLog = "Only selects allowed";
	}
	else 
	{
        $result = mySqli_query_wrapper($con, $sql, "Error running sql");
		if ($result != false) {
			$resultsLog = "<table><tr>";
			for($i = 0; $i < mysqli_field_count ($con); $i++) {
			    $field_info = mysqli_fetch_field ($result);
			    $resultsLog .= "<th>{$field_info->name}</th>";
			}
			
			// Print the data
			while($row = mysqli_fetch_row($result)) {
			    $resultsLog .= "<tr>";
			    foreach($row as $_column) {
			        $resultsLog .= "<td>{$_column}</td>";
			    }
			    $resultsLog .= "</tr>";
			}
			
			$resultsLog .= "</table>";	
		}
	}
	mysqli_close($con);
	
}

function getReportSql($con, $reportShortName) {
	$sql = "select * from report where short_name='" . $reportShortName . "'";
	//echo $sql;
	$result = mySqli_query_wrapper($con, $sql, "Error running sql");
	if ($result != false) {
		$row = mysqli_fetch_array($result);
		$returnSql = $row['sql'];
	}
	return $returnSql;
}

?>

</head>

<body>
<form id="form1" name="form1" method="post" action="sqllookup.php">
SQL:<br/><br/>
<textarea name="sql" rows="10" cols="150"><?php echo $sql; ?></textarea>
<br/><br/>
<input type="submit" />
<input type="hidden" name="submitSqlQuery" value="Y" />
</form>
<br/><br/>
<a href="reports.php">Reports Menu</a>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a>
<br/><br/>
Results:<br/><br/>
<?php echo $resultsLog;?>

</body>