<?php
require_once('auth.php');
require_once '../wp-content/php/dbdefs.php';
require_once '../wp-content/php/songbean.php';
require_once '../wp-content/php/mediaentrybean.php';
require_once '../wp-content/php/video.class.php';


$DbLink = mysqli_connect(DBHOST, DBUSER, DBPASSWORD, DBSCHEMA);
if (mysqli_connect_errno($DbLink)) {
    die('Connect Error: ' . mysqli_connect_error());
}

$videos = array();
$sql = "select * from video";
$result = mysqli_query($DbLink, $sql);
while($row = mysqli_fetch_assoc($result))
{
    $video = new Video();
    $video->id = $row['id'];
    $video->topicId = $row['topic_id'];
    $video->selectionId = $row['selection_id'];
    $video->videoId = $row['video_id'];
    $video->url = $row['url'];
    array_push($videos, $video);
}

foreach ($videos as $video) {
    $justTheId = $video->url;
    $label = "embed/";
    $embedPos = strpos($justTheId, $label, 0);
    if ($embedPos != false) {
        $justTheId = substr($justTheId, $embedPos + strlen($label));
        $qpos = strpos($justTheId, "?");
        if ($qpos != false) {
            $justTheId = substr($justTheId, 0, $qpos);
        }
        $sql = "Update video set video_id='" . $justTheId . "' where id='" . $video->id . "'";
        //echo $sql;
        mysqli_query($DbLink,$sql) or die (mysqli_error($DbLink));
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Update Video Codes</title>

</head>
<body>

Update of video codes is done!
<br/><br/>
<a href="utilitiesmenu.php">Utilities Menu</a><br/><br/>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
