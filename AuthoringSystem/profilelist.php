<?php 
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/model/profile.class.php';
require_once '../wp-content/php/util/util.php';

if (isset($_POST["currentProfile"]))
	$currentProfile = $_POST["currentProfile"];

if ($currentProfile == "")
	$currentProfile = $_GET["currentProfile"];
//echo "Current profile is: " . $currentProfile

$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_select_db(DBSCHEMA, $con);

$allProfiles = array();
$sql = "Select p.id, p.shortname, p.displayname, p.owner from profile p";
$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
{
	$profile = new Profile();
	$profile->id = $row['id'];
	$profile->shortName = $row['shortname'];
	$profile->displayName = $row['displayname'];
	$profile->owner = $row['owner'];
	array_push($allProfiles, $profile);
}
  	//foreach ($allProfiles as $profile) {
	//	echo "Profile: " . $profile->id . " " . $profile->shortName;
	//}
if ($currentProfile == "")
	$currentProfile = $allProfiles[0]->shortName;
mysql_close($con);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profiles</title>

<script type="text/javascript">
 </script>

<body>
<form id="form1" name="form1" method="post" action="profileedit.php?mode=edit">
  <input type="hidden" name="checktopicconfigurations" value="1" />
  <select name="currentProfile" id="currentProfile" size="<?php echo count($allProfiles)?>">
  <?php
  	foreach ($allProfiles as $profile) {
		if ($currentProfile == $profile->shortName)
			echo "<option selected value='" . $profile->shortName . "'>" . $profile->shortName . " - " . $profile->displayName . "</option>";
		else
			echo "<option value='" . $profile->shortName . "'>" . $profile->shortName . " - " . $profile->displayName . "</option>";
	}
  ?>
  </select>
<br/><br/>
<input type="submit" value="Edit Selected Profile" />
<br/><br/>
<button onclick="javascript:form1.action='profiletopics.php'">Edit Topics for Selected Profile</button>
<br/><br/>
<button onclick="javascript:form1.action='profileedit.php?mode=add'">Add New Profile</button>
<br/><br/>
<button onclick="javascript:form1.action='profileclone.php'">Clone Selected Profile</button>
<br/><br/>
<button onclick="javascript:form1.action='profiledelete.php'">Delete Selected Profile</button>
<br/><br/>
</form>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>