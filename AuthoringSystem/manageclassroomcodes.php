<?php 
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/profile.class.php';
require_once '../wp-content/php/model/classroomcodebean.class.php';
require_once '../wp-content/php/dao/profilemethods.php';
require_once '../wp-content/php/util/util.php';

$submitToDatabase = $_POST["submittodatabase"];
$profileSubscriberToDelete = $_POST["classroomcodes"];
$con = getMySqliDbConnection();

if ($submitToDatabase == "1") {
	//echo "Classroom code to delete: " . $profileSubscriberToDelete;
	deleteReferencesToProfileSubscriber($profileSubscriberToDelete);
}

$allClassroomCodes = getAllClassroomCodes($con);

mysqli_close($con);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Manage Classroom Codes</title>

<script type="text/javascript">
function handleSubmit() {
	var codeToDelete = document.getElementById('classroomcodes').value;
	if (!codeToDelete) {
		alert("Select a code to delete");
		return false;
	}
	return window.confirm("Are you sure you want to delete this classroom code?");
}

 </script>

<body>
<form id="form1" name="form1" method="post" action="">
	<input type="hidden" id="submittodatabase" name="submittodatabase" value="1" />
	<select id="classroomcodes" name="classroomcodes" size=10>
	<?php 
	foreach ($allClassroomCodes as $classroomCodeBean) {
		echo "<option value='" . $classroomCodeBean->profileSubscriberId . "'>" . $classroomCodeBean->userNiceName . 
			" - " . $classroomCodeBean->classroomCode . " (" . $classroomCodeBean->profileDisplayName . ")</option>";
	}
	
	?>
	</select>
  <br/><br/>
<input type="submit" value="Delete Selected Classroom Code" onclick="return handleSubmit();" />
</form>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>