<?php 
require_once('auth.php');
require_once '../wp-content/php/dbdefs.php';
require_once '../wp-content/php/profile.class.php';
require_once '../wp-content/php/profilemessaging.class.php';
require_once '../wp-content/php/profilemethods.php';
require_once '../wp-content/php/util.php';

$currentMessageType = $_POST["messagetype"];
$submitToDatabase = $_POST["submittodatabase"];
$messageTypeToChange = $_POST["messagetypetochange"];

$welcomeMsg = $_POST["welcomemsg"];

$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_select_db(DBSCHEMA, $con);

$myMoralCompassId = getProfileId("MYMORALCOMPASS");
$top100Id = getProfileId("TOP100");

if ($submitToDatabase == "1") {
	if ($messageTypeToChange == "HOMEPAGE") {
		$sql = "update profile_messaging set welcome_message = '" . my_escape($welcomeMsg, getUnmagicValue()) .
			"' where user_type='VISITOR' and profile_id=" . $myMoralCompassId . " and profile_subscriber_id is null";
	} else if ($messageTypeToChange == "TOP100VISITOR") {
		$sql = "update profile_messaging set welcome_message = '" . my_escape($welcomeMsg, getUnmagicValue()) .
			"' where user_type='TOP100VISITOR' and profile_id=" . $top100Id . " and profile_subscriber_id is null";
	} else {
		$sql = "update profile_messaging set welcome_message = '" . my_escape($welcomeMsg, getUnmagicValue()) .
		"' where user_type='" . $messageTypeToChange . "' and profile_id is null";
	}
	//echo $sql;
	my_mysql_query($sql, "Error updating profile_messaging");
}

$allGenericMessages = array();
$sql = "Select pm.id, pm.user_type, pm.welcome_message, p.shortname from profile_messaging pm left outer join profile p on " .
"pm.profile_id=p.id where pm.profile_subscriber_id is null";
//echo $sql;
$result = mysql_query($sql);
while($row = mysql_fetch_array($result))
{
	$profileMessaging = new ProfileMessaging();
	$profileMessaging->id = $row['id'];
	$profileName = $row['shortname'];
	if (strcasecmp($profileName, "MYMORALCOMPASS") == 0) {
		$profileMessaging->userType = HOMEPAGE;
		$profileMessaging->displayName = $profileName . " HOMEPAGE";
	} else {
		$profileMessaging->userType = $row['user_type'];
		$profileMessaging->displayName = $row['user_type'];
	}
	//echo "in loop userType: " . $profileMessaging->userType . "<br/>";
	$profileMessaging->heading1 = $row['heading1'];
	$profileMessaging->heading2 = $row['heading2'];
	$profileMessaging->welcomeMsg = $row['welcome_message'];
	$allGenericMessages[$profileMessaging->userType] = $profileMessaging;
	//echo "in loop userType: " . $allGenericMessages[$profileMessaging->userType]->userType . "<br/>";
}

if (empty($currentMessageType)) {
	$currentMessageType = $allGenericMessages[VISITOR]->userType;
}
//echo "before findMessageoftype of " . $currentMessageType . "<br/>";
$currentMessage = findMessageOfType($allGenericMessages, $currentMessageType);
//echo "after findMessageoftype currentMessage: " . $currentMessage->welcomeMsg . "<br/>";
mysql_close($con);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome Messages</title>

<script type="text/javascript">
function selectTypeChange(selectObj, currentType) { 
	// get the index of the selected option 
	var idx = selectObj.selectedIndex; 
	// get the value of the selected option 
	var messageType = selectObj.options[idx].value; 
	document.getElementById('messagetype').value = messageType;
	document.getElementById('submittodatabase').value = "0";
	document.getElementById('form1').submit();
}


 </script>

<body>
<form id="form1" name="form1" method="post" action="">
  <input type="hidden" id="messagetype" name="messagetype" value="<?=$currentMessageType?>" />
  <input type="hidden" id="messagetypetochange" name="messagetypetochange" value="<?=$currentMessageType?>" />
  <input type="hidden" id="profileid" name="profileid" value="<?=$profileMessaging->profileId?>" />
  <input type="hidden" id="submittodatabase" name="submittodatabase" value="1" />
  <table>
  <tr>
  <td>User Type: </td>
  <td>
  <select name="currentMessageType" onchange="javascript:selectTypeChange(this, '<?=$currentMessageType?>');">
  <?php
  	foreach ($allGenericMessages as $profileMessaging) {
  		if ($currentMessageType == $profileMessaging->userType) {
			echo "<option value='" . $profileMessaging->userType . "' SELECTED >" . $profileMessaging->displayName . "</option>";
  		} else {
  			echo "<option value='" . $profileMessaging->userType . "'>" . $profileMessaging->displayName . "</option>";
  		}
	}
  ?>
  </select>
  </td>
  </tr>
  <tr>
  <td>Welcome Message: </td>
  <td>
  <TEXTAREA NAME="welcomemsg" class="red-scrollbar" COLS=80 ROWS=12><?=$currentMessage->welcomeMsg ?></TEXTAREA>
  </td>
  </tr>
  </table>
  <br/><br/>
<input type="submit" value="Edit Selected Welcome Message" />
</form>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>