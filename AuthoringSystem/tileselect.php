<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/21/2019
 * Time: 9:55 PM
 */
error_reporting( E_ALL );

require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/tile.class.php';
require_once '../wp-content/php/dao/TileDao.php';

$con = getMySqliDbConnection();
$tileDao = new TileDao($con);
$current_user = wp_get_current_user();

//delete card if the button was clicked
$deleteId = $_GET['deleteId'];
if(isset($deleteId)) {
    $tileDao->deleteTile($deleteId);
}

$arrTiles = $tileDao->getTiles('cardgames'); //there could be a category selector

$con->close();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tile Selections</title>
</head>

<body>
<?php
    echo "<a href='tileeditor.php'>Add New Tile</a><br/><br/><br/>";
    foreach ($arrTiles as $tile) {
        echo "<a href='tileeditor.php?id=" . $tile->id . "'>" . $tile->title . "</a><br/><br/>";
    }
?>

<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>
</html>