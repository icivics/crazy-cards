<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/11/2017
 * Time: 10:12 PM
 */
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/mediaentrybean.php';
require_once '../wp-content/php/model/playlist.class.php';
require_once '../wp-content/php/model/playlistentry.class.php';
require_once '../wp-content/php/dao/MediaItemDao.php';
require_once '../wp-content/php/dao/PlaylistEntryDao.php';
require_once '../wp-content/php/output/MediaOutputter.php';
require_once '../wp-content/php/model/video.class.php';

$con = getMySqliDbConnection();
$playlistEntryDao = new PlaylistEntryDao($con);
$current_user = wp_get_current_user();

// $id as get, when coming in for the first time
$id = $_GET["id"];

function uploadFile($myFile, $myTopic, $selectionType)
{
    //phpinfo();
    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        echo "<p>An error occurred.</p>";
        exit;
    }
    echo "<br/>Name is: " . $myFile["name"] . "<br/>";
    echo "Temp name is: " . $myFile["tmp_name"] . "<br/>";
    echo "Temp dir is: " . ini_get('upload_tmp_dir') . "<br/>";
    //ini_set('upload_tmp_dir', '/home/mcmosber/public_html/tmp');
    echo "After, temp dir is: " . ini_get('upload_tmp_dir') . "\n";
    echo "Max filesize is: " . ini_get('upload_max_filesize') . "\n";
    //ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

    $parts = pathinfo($name);
    $extension = strtolower($parts["extension"]);
    echo "Name: " . $name . "  extension: " . $parts["extension"] . "<br/>";
    $uploadDir = "../wp-content/blogimages/";
    echo "Uploaded dir is: " . $uploadDir;

    if (!file_exists($uploadDir)) {
        mkdir($uploadDir, 0777, true);
    }
    //echo "Target file is: " . $uploadDir . $name . "\n";
    /*
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name);
    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    */
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"],
        $uploadDir . $name);
    if (!$success) {
        echo "<p>Unable to save file.</p>";
        exit;
    }
    // set proper permissions on the new file
    chmod($uploadDir . $name, 0644);
}

// if posting changes, $id will be from the _POST
if(isset($_POST["submitToDatabase"])) {
    $myImageFile = $_FILES["myblogimage"];
    if (!empty($myImageFile["name"]))
    {
        $image = $myImageFile["name"];
        uploadFile($myImageFile, $currentTopic, $selectionType);
    }
    $playlistEntryToSubmit = new PlaylistEntry();
    $playlistEntryToSubmit->id = $_POST["playlistEntryId"];
    $playlistEntryToSubmit->tagline = $_POST["tagline"];
    $playlistEntryToSubmit->blurb = $_POST["blurb"];
    $playlistEntryToSubmit->postDate = $_POST["postdate"];
    $playlistEntryToSubmit->blogImage = $image;
    $playlistEntryDao->update($playlistEntryToSubmit);
    // be sure to sync up what the rest of the page expects $id to be
    $id = $playlistEntryToSubmit->id;
}

// needs to be modified to include the image
$playlistEntry = $playlistEntryDao->getPlaylistEntry($id);

?>
<html>
<head>
    <title>Playlist Entry Editor</title>

    <link rel="stylesheet" type="text/css" href="css/authoring.css">
    <script language="JavaScript">
        function closeWin(){ window.close(); }

        function doSubmit(theForm) {
            theForm.submit();
            //closeWin();
        }

    </script>
</head>

<body>

<table>
    <tr>
        <td width=230>

            <?php echo "<h2>" . $playlistEntry->title . "</h2>";
            echo "<h6>" . htmlForMetadata($playlistEntry) . "</h6>" ;
            echo  htmlForFeaturedThumbnail($playlistEntry);
            ?>
        </td>
        <td width=620>
            <?php
            $className = $resourceCount == 0 ? "visible" : "hidden";
            echo "<div width=580 align='center' id='divResource" . strval($resourceCount+1) . "' class='" . $className . "'>" .
                htmlForVideoId($playlistEntry->videoId, false) . "</div>";
            ?>
        </td>
    </tr>
</table>
<form action="plmiEditor.php" onsubmit="javascript:doSubmit(this)" enctype="multipart/form-data" method="post" id="songentry" name="songentry"><br/><br/>
    <input type="hidden" name="submitToDatabase" value="1"/>
    <input type="hidden" name="playlistEntryId" value="<?=$playlistEntry->id?>"/>
    <table>
        <tr>
            <td>Playlist Tagline</td>
            <td><input type="text" name="tagline" value="<?=$playlistEntry->tagline?>" maxlength="100" size="100" /></td>
        </tr>
        <tr>
            <td>Blurb</td>
            <td><textarea name="blurb" cols="100" rows="10"><?=$playlistEntry->blurb?></textarea></td>
        </tr>
        <tr>
            <td>Post Date</td>
            <td><input type="text" name="postdate" value="<?=$playlistEntry->postDate?>" maxlength="10" size="20" /></td>
        </tr>
        <tr>
            <td>Image</td>
            <td><?php

                if (empty($playlistEntry->blogImage))
                    echo "No image file chosen";
                else
                {
                    $filePath = "../wp-content/blogimages/" . $playlistEntry->blogImage;
                    ?>
                    <a href='javascript:openWin("<?php echo $filePath;?>");'><img src='<?php echo $filePath;?>' width='120' height='90' /><br/>
                        <?php echo $playlistEntry->blogImage;?></a>
                    <?php
                }

                ?>
                <br/>
                <input type="hidden" name="image" value="<?php echo $playlistEntry->blogImage?>" />
                Choose a new file: <input name="myblogimage" type="file" /><br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr/>
                <input type="submit" value="Submit Changes" />
                <input type="button" value="Done" onClick="closeWin()" />
            </td>
        </tr>
    </table>
</form>
</body>
</html>