<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/5/2019
 * Time: 4:59 PM
 */
?>
<div align="center">
<strong>Matching Game Help</strong>
    <div align="left">
        Tap the cards to turn them over<br/>
        Tap the card again to zoom in<br/>
        Find the matching card<br/>
        Look for surprises
    </div>
</div>