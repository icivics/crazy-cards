<?php 
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/model/category.class.php';
require_once '../wp-content/php/model/profile.class.php';
require_once('../wp-content/php/model/topic.class.php');
require_once('../wp-content/php/dao/profilemethods.php');
require_once('../wp-content/php/dao/dbfunctions.php');
require_once '../wp-content/php/util/util.php';

function isUsedInACategory($categoryList, $topictofind)
{
	$found = false;
	foreach ($categoryList as $category)
	{
		foreach ($category->topics as $topic)
		{
			if ($topictofind->shortName == $topic->shortName)
			{
				$found = true;
				break;
			}
		}
	}
	return $found;
}

function foundInArray($id, $arr) 
{
	return in_array($id, $arr);
}

function deleteCategory($categoryId)
{
	//echo "Deleting category id " . $categoryId . "<br/>";
	$sql = "delete from categorytopic where category_id=" . $categoryId;
	$result = mysql_query($sql);
	if ($result != 1)
	{
		echo "Error deleting --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql . "<br/>";
		exit();
	}
	$sql = "delete from category where id=" . $categoryId;
	$result = mysql_query($sql);
	if ($result != 1)
	{
		echo "Error deleting --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql . "<br/>";
		exit();
	}
}
if (isset($_POST["currentProfile"]))
	$currentProfile = $_POST["currentProfile"];

if ($currentProfile == "")
	$currentProfile = $_GET["currentProfile"];
//echo "Current profile is: " . $currentProfile;
$unmagic = 0;
$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_select_db(DBSCHEMA, $con);

$profileId = getProfileId($currentProfile);
if(isset($_POST["submitToDatabase"]))
{
	/*
	echo "<table>";
	foreach ($_POST as $key => $value) {
		echo "<tr>";
		echo "<td>";
		echo $key;
		echo "</td>";
		echo "<td>";
		echo $value;
		echo "</td>";
		echo "</tr>";
	}
	echo "</table>";
	*/
	$categoryCount = 0;
	$arrCategoryIds = array();
	$arrOriginalCategoryIds = array();
	$sql = "select * from category where profile_id=" . $profileId;
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result))
	{
		$id = $row['id'];
		array_push($arrOriginalCategoryIds, $id);
	}	
	
	$arrIds = array();
	$arrNames = array();
	$arrBreadcrumbs = array();
	$categoryCount = 0;
	$elementCount = 0;
	$done = false;
	while (!$done)
	{
		if (isset($_POST["topicvalues" . strval($categoryCount)]))
		{
			$strid = "categoryid" . strval($elementCount);
			if (isset($_POST[$strid]))
			{
				array_push($arrIds, $_POST[$strid]);
				array_push($arrNames, $_POST["categoryname" . strval($elementCount)]);
				array_push($arrBreadcrumbs, $_POST["breadcrumb" . strval($elementCount)]);
				$categoryCount = $categoryCount + 1;
				$elementCount = $elementCount + 1;
			}
			else
			{
				$elementCount = $elementCount + 1;
				//echo "Didn't find element " . $strid;
			}
		}
		else
		{
			//echo "Didn't find element " . "topicvalues" . strval($categoryCount);
			$done = true;
		}
	}
	//echo "arrids: <br/>";
	//var_dump($arrIds);
	// write out all the categories, new or updated
	for ($i=0;$i<count($arrIds);$i++)
	{
		$id = $arrIds[$i];
		$categoryName = $arrNames[$i];
		$breadcrumb = $arrBreadcrumbs[$i];
		if ($id == -1)
			$sql = "insert into category (profile_id, name, breadcrumb) values (" . $profileId . ", '" .
				$categoryName . "', '" . $breadcrumb . "')";
		else 
			$sql = "update category set name='" .  $categoryName . "', breadcrumb='" . $breadcrumb . "' where id=" . $id;
		$result = mysql_query($sql);
		if ($result != 1)
		{
			echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
		if ($id == -1)
			$id = mysql_insert_id();
		array_push($arrCategoryIds, $id);
	}
	$categoryCount = 0;
	while ($categoryCount < count($arrCategoryIds))
	{
		$strid = "topicvalues" . strval($categoryCount);
		$sql = "delete from categorytopic where category_id=" . $arrCategoryIds[$categoryCount];
		$result = mysql_query($sql);
		if ($result != 1)
		{
			echo "Error deleting --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql . "<br/>";
			exit();
		}
		
		if (isset($_POST[$strid]))
		{
			$strtopiclist = $_POST[$strid];
			$myArray = explode(',', $strtopiclist);
			if ($strtopiclist.length > 0 && count($myArray) > 0)
			{
				$tccount = 1;
				foreach ($myArray as $tcid)
				{
					$sql = "insert into categorytopic (category_id, topic_configuration_id, sequence) values ('" . $arrCategoryIds[$categoryCount] .
						"', '" . trim($tcid) . "', '" . $tccount . "')";
					//echo "SQL is " . $sql . "<br/>";
					$result = mysql_query($sql);
					if ($result != 1)
					{
						echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
						echo "SQL is " . $sql;
						exit();
					}
					$tccount = $tccount + 1;
				}
			}
		}
		$categoryCount = $categoryCount + 1;
	}
	//echo "Original category ids: " . $arrOriginalCategoryIds . "<br/>";
	//var_dump($arrOriginalCategoryIds);
	//echo "<br/>";
	//echo "New category ids: ";
	//var_dump($arrCategoryIds);
	//echo "<br/>";
	foreach ($arrOriginalCategoryIds as $id)
	{
		if (!foundInArray($id, $arrCategoryIds))
			deleteCategory($id);
	}
}

if (isset($_POST["checktopicconfigurations"])) {
	updateTopicsForProfile($currentProfile);
}

$allTopics = array();
$allCategories = array();
$allTopics = array();
$currentCategory = null;
mysql_select_db(DBSCHEMA, $con);
if ($currentProfile != "") {
	$sql = "Select * from profile where shortname='" . $currentProfile . "'";
	//echo $sql;
	$result = mysql_query($sql);
	if ($row = mysql_fetch_array($result))
	{
		$profile = new Profile();
		$profile->id = $row['id'];
		$profile->shortName = $row['shortname'];
		$profile->owner = $row['owner'];
		$profile->welcomeMsg = convertUtf8($row['welcomemessage']);
		$editRecord = $profile->id;
	}
	//$sql = "SELECT topic.id, topic.active, topic.sequence, topic.short_name, topic_configuration.id " +
	//	"FROM topic INNER JOIN topic_configuration ON topic.id = topic_configuration.topic_id " +
	//	"where topic_configuration.profile_id='" . $profile->id . "'";
	$sql = "select topic.id as id, topic.active, topic.sequence, topic.short_name, topic.title, topic_configuration.id as tcid " .
		"FROM topic INNER JOIN topic_configuration ON topic.id = topic_configuration.topic_id " .
		"where topic_configuration.profile_id='" . $profile->id . "' order by topic.sequence";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result))
	{
		$topic = new TopicSpec();
		$topic->id = $row['id'];
		$topic->configurationId = $row['tcid'];
		$topic->shortName = $row['short_name'];
		$topic->featured = $row['featured'];
		$topic->title = $row['title'];
		$topic->description = $row['description'];
		//echo "Topic " . $topic->shortName . "  configuration id: " . $topic->configurationId . "<br/>";
		array_push($allTopics, $topic);
	}	
	$allCategories = fetchCategoriesAndTopics($profile->id);
	$currentCategory = $allCategories[0];
}
mysql_close($con);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profile Topics</title>

<script type="text/javascript">

var g_selectedCategory = 0;

function dumpHtmlTags() {
	var all = document.getElementsByTagName("*");
	var st = "";
	for (var i=0, max=all.length; i < max; i++) {
		if (typeof all[i].id != 'undefined' && all[i].id.length > 0)
	     	st = st + all[i].id + "\n";
	}
	//alert("all elements are: " + st);
}


function findIndexWithinCategories(selectedCategory) {
	var ncategories = document.getElementById('allcategoriestable').getElementsByTagName("table").length;
	for (var i=0;i<ncategories;i++) {
		var tbl = document.getElementById('allcategoriestable').getElementsByTagName("table")[i];
		var idtag = tbl.id;
		var elementNumber = idtag.substring("categorytable".length);
		if (elementNumber == selectedCategory)
			return i;
	}
	return -1;
}

function findCategoryIdForIndex(idx) {
	var tbl = document.getElementById('allcategoriestable').getElementsByTagName("table")[idx];
	var elementNumber = -1;
	if (tbl != null) {
		var idtag = tbl.id;
		elementNumber = idtag.substring("categorytable".length);
	}
	return elementNumber;
}
function openWin(selectElement) 
{
	var text = selectElement.options[selectElement.selectedIndex].text;
	var value = selectElement.options[selectElement.selectedIndex].value;
	var url1="tcpopup.php?id=" + text + "&value=" + value;
	//alert("Opening window: " + url1);
	window.open(url1,"","width="+850+",height="+800+",status=yes,toolbar=no,scrollbars=no,left=100,top=100");
}

function moveleft() {
	var categorytableid = "topicsforcategory" + g_selectedCategory;
	var select = document.getElementById(categorytableid);
	if(select.options.length > 0 && select.selectedIndex >= 0) {
		var text = select.options[select.selectedIndex].text;
		var value = select.options[select.selectedIndex].value;
		var targetlist = document.getElementById("alltopics");
		targetlist.options[targetlist.options.length] = new Option(text, value);
		select.remove(select.selectedIndex);
		select.size = select.options.length;
		if (select.size < 5)
			select.size = 5;
	}
}

function moveAllTopicsLeft(selectedCategory) {
	// used prior to deleting category
	var categorytableid = "topicsforcategory" + selectedCategory;
	var select = document.getElementById(categorytableid);
	for (var i=select.options.length-1;i>=0;i--) {
		var text = select.options[i].text;
		var value = select.options[i].value;
		var targetlist = document.getElementById("alltopics");
		targetlist.options[targetlist.options.length] = new Option(text, value);
		select.remove(i);
	}
}
function moveright() {
	var select = document.getElementById("alltopics");
	var categorytableid = "topicsforcategory" + g_selectedCategory;
	var selectedIndexOnLeft = select.selectedIndex;
	if(select.options.length > 0 && select.selectedIndex >= 0) {
		var text = select.options[select.selectedIndex].text;
		var value = select.options[select.selectedIndex].value;
		var targetlist = document.getElementById(categorytableid);
		targetlist.options[targetlist.options.length] = new Option(text, value);
		targetlist.size = targetlist.options.length;
		if (targetlist.size < 5)
			targetlist.size = 5;
		select.remove(select.selectedIndex);
		if (select.options.length > selectedIndexOnLeft)
			select.selectedIndex = selectedIndexOnLeft;
		else if (select.options.length > 0)
			select.selectedIndex = selectedIndexOnLeft - 1;
	}
}

function setState(selectedCategory) {
	var ncategories = document.getElementById('allcategoriestable').getElementsByTagName("table").length;
	g_selectedCategory = selectedCategory;
	var i = 0;
	var count = 0;
	while (count < ncategories) {
		var categoryTable = document.getElementById("categorytable" + i);
		//var categoryTable = document.getElementById('allcategoriestable').getElementsByTagName("table")[i];
		if (categoryTable != null) {
			//alert("setState selectedCategory is: " + selectedCategory + "  categoryTable " + i + " is " + categoryTable.id);
			categoryTable.style.borderColor = (i==selectedCategory ? 'red' : 'black');
			var listbox = document.getElementById("topicsforcategory" + i);
			if (listbox != null && i != selectedCategory)
				listbox.selectedIndex = -1;
			count++;
		}
		i++;
	}
}

function changeAllElementIds(selectedCategory) {
	var done = false;
	//dumpHtmlTags();
	while (!done) {
		var numstr = (selectedCategory + 1).toString();
		var categorytableid = "topicsforcategory" + numstr;
		var element = document.getElementById(categorytableid);
		if (element != null) {
			var lowernumstr = selectedCategory.toString();
			element.id = "topicsforcategory" + lowernumstr;
			var xx = "categoryid" + numstr;
			element = document.getElementById(xx);
			if (element != null) {
				var parent = element.parentNode;
				parent.removeChild(element);
				element.id = "categoryid" + lowernumstr;
				parent.appendChild(element);
				alert("Renaming " + xx + " to " + element.id);
			}
			else
				alert("Not renaming " + xx);
			element = document.getElementById("categorytable" + numstr);
			if (element != null) {
				var parent = element.parentNode;
				parent.removeChild(element);
				element.id = "categorytable" + lowernumstr;
				parent.appendChild(element);
				alert("Renaming " + "categorytable" + numstr + " to " + element.id);
			}
			element = document.getElementById("categoryname" + numstr);
			if (element != null) {
				var parent = element.parentNode;
				parent.removeChild(element);
				element.id = "categoryname" + lowernumstr;
				parent.appendChild(element);
				alert("Renaming " + "categoryname" + numstr + " to " + element.id);
			}
			element = document.getElementById("breadcrumb" + numstr);
			if (element != null) {
				var parent = element.parentNode;
				parent.removeChild(element);
				element.id = "breadcrumb" + lowernumstr;
				parent.appendChild(element);
				alert("Renaming " + "breadcrumb" + numstr + " to " + element.id);
			}
			selectedCategory = selectedCategory + 1;
		}
		else
			done = true;
	}
}

function deleteCategory(selectedCategory) {
    var conf = confirm("Are you sure you want to delete this category?");
    if(conf == true){
         var categorytableid = "topicsforcategory" + selectedCategory;
     	var outertable = document.getElementById('allcategoriestable');
     	var idx = findIndexWithinCategories(selectedCategory);
     	moveAllTopicsLeft(selectedCategory);
     	outertable.deleteRow(idx);
     	//changeAllElementIds(selectedCategory);
     	
     	var ncategories = outertable.childNodes.length;
     	if (idx < ncategories)
         	idx = findCategoryIdForIndex(idx);
     	else if (ncategories > 0)
         	idx = findCategoryIdForIndex(idx-1);
     	if (idx != -1)
     		setState(idx);
	}
}
function addCategoryTable() {
	var outertable = document.getElementById('allcategoriestable');
	var ncategories = outertable.getElementsByTagName("table").length;
	var id;
	if (ncategories == 0)
		nextElementNumber = 0;
	else {
		var idtag = outertable.getElementsByTagName("table")[ncategories-1].id;
		var elementNumber = idtag.substring("categorytable".length);
		nextElementNumber = parseInt(elementNumber) + 1;
		//alert("idtag = " + idtag + "  elementNumber: " + elementNumber + " nextElementNumber: " + nextElementNumber);
	}
	var row = outertable.insertRow(0);
	var col = row.insertCell(0);
	col.innerHTML =
		"<input type='hidden' id='categoryid" + nextElementNumber + "' name='categoryid" + nextElementNumber + "' value='-1' />" + 
		"<td><table width=500 border=2 id='categorytable" + nextElementNumber + "'><tr>" +
		"<td width=150>Category:" +
		"</td>" + 
		"<td width=350>" +
		"<input type='text' id='categoryname" + nextElementNumber + "' name='categoryname" + nextElementNumber + "' onfocus='setState(" + nextElementNumber + ")' size=50 maxlen=50/>" +
		"</td></tr>" + 
		"<tr><td>Breadcrumb:</td>" +
		"<td><input type='text' id='breadcrumb" + nextElementNumber + "' name='breadcrumb" + nextElementNumber + "' onfocus='setState(" + nextElementNumber + ")' size=50 maxlen=50 />" +
		"</td></tr>" + 
		"<tr><td>Topics for Category:<br/>" +
		"<button onclick='javascript:setState(" + nextElementNumber + ");return false;'>Select</button>" +
		"</td>" +
		"<td>" +
			"<select name='topicsforcategory" + nextElementNumber + "' id='topicsforcategory" + nextElementNumber + "' " +
			"onchange='setState(" + nextElementNumber + ")' onclick='setState(" + nextElementNumber + ")' size='5' style='width:340px;'>" +
			  "</select>" +
		"</td></tr>" +
		"</table></td>";
	outertable.appendChild(row);
	setState(nextElementNumber);
}

function handleSubmit() {
	var ncategories = document.getElementById('allcategoriestable').getElementsByTagName("table").length;
	//dumpHtmlTags();
	for (var i=0;i<ncategories;i++) {
		//var listbox = document.getElementById('topicsforcategory' + i);
	
		var listbox = document.getElementById('allcategoriestable').getElementsByTagName("select")[i];
		/*
		if (listbox == null)
			alert("listbox " + i + " is null");
		else
			alert("listbox "  + i + " = " + listbox.id);
		*/
		var noptions = listbox.options.length;
		var strOptions = "";
		for (var j=0;j<noptions;j++) {
			strOptions = strOptions + listbox.options[j].value;
			if (j < noptions -1)
				strOptions = strOptions + ", ";
		}
		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "topicvalues" + i);
		input.setAttribute("value", strOptions);
		//append to form element that you want .
		document.getElementById("profiletopics").appendChild(input);	
	}
}

 </script>

<body onload="javascript:setState(0)">
<form action="profiletopics.php" enctype="multipart/form-data" method="post" name="profiletopics" id="profiletopics" onsubmit="javascript:handleSubmit()"><br/><br/>
<input type="hidden" name="submitToDatabase" value="1" />
<input type="hidden" name="currentProfile" value="<?=$currentProfile?>" />
<table>
<tr>
<td valign=top>Unused topics<br/><br/>
<select name="alltopics" id="alltopics" size="20" style="width:250px;">
  <?php
  	foreach ($allTopics as $topic) {
		if (!isUsedInACategory($allCategories, $topic))
			echo "<option value='" . $topic->configurationId . "'>" . $topic->title . "</option>";
	}
  ?>
  </select>
</td>
<td valign="top">
<button type="button" onclick="javascript:moveright();return false;">==></button>
<br/><br/>
<button type="button" onclick="javascript:moveleft();return false;"><==</button>
<br/><br/>
<button type="button" onclick="javascript:addCategoryTable();return false;">Add New Category</button>
</td>
<td valign="top">
<table id="allcategoriestable">
<?php 
$count = 0;
foreach ($allCategories as $category) : 
$listBoxSize = count($category->topics);
if ($listBoxSize < 5)
	$listBoxSize = 5;
?>
<tr>
<td>
<table width=500 id="categorytable<?=$count?>" border=2 >
<input type="hidden" id="categoryid<?=$count?>" name="categoryid<?=$count?>" value="<?=$category->id?>"/>
<tr>
<td width=150 onfocus="setState(<?=$count?>)" >Category: </td>
<td width=350>
<input type="text" id="categoryname<?=$count?>" name="categoryname<?=$count?>" value="<?=$category->categoryName?>" onfocus="setState(<?=$count?>)"  size=50 maxlen=50/>
</td>
</tr>
<tr>
<td>
Breadcrumb: 
</td>
<td>
<input type="text" id="breadcrumb<?=$count?>" name="breadcrumb<?=$count?>" value="<?=$category->categoryBreadcrumb?>" onfocus="setState(<?=$count?>)" size=50 maxlen=50 />
</td>
</tr>
<tr>
<td>
Topics for Category:<br/>
<button type="button" onclick="javascript:setState(<?=$count?>);deleteCategory(<?=$count?>);return false;">Delete</button>
</td>
<td>
	<select name="topicsforcategory<?=$count?>" id="topicsforcategory<?=$count?>" size="<?=$listBoxSize?>" style="width:340px;"
		onchange='setState(<?=$count?>);' onclick='setState(<?=$count?>);' ondblclick='openWin(this)' >
	  <?php
	  	foreach ($category->topics as $topic) {
			echo "<option value='" . $topic->configurationId . "'>" . $topic->title . "</option>";
		}
	  ?>
	  </select>
</td>
</tr>
</table>

</td>
</tr>
<?php 
$count = $count + 1;
endforeach;  ?>
</table>


</td>
</tr>
</table>
<input type="submit" name="submit" value="submit"/>&nbsp;&nbsp;
<input type="button" value="Go Back" onClick="location.href='profilelist.php?currentProfile=<?=$currentProfile?>'" />
</form>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>