<?php 
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/model/profile.class.php';
require_once '../wp-content/php/dao/profilemethods.php';
require_once '../wp-content/php/util/util.php';

$mode = $_GET["mode"];
if ($mode == "add")
	$currentProfile = "";
else
{
	if (isset($_POST["currentProfile"]))
		$currentProfile = $_POST["currentProfile"];
	
	if ($currentProfile == "")
		$currentProfile = $_GET["currentProfile"];
}
//echo "Current profile is: " . $currentProfile;
$unmagic = 0;
$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_select_db(DBSCHEMA, $con);
$editRecord = -1;
if (isset($_POST["editrecord"]))
{
	$editRecord = $_POST["editrecord"];
}
if(isset($_POST["submitToDatabase"]))
{
	//setMagicQuoteCheck($_POST["magicquotecheck"]);
	$profile = new Profile();
	$profile->id = $editRecord;
	$profile->shortName = $_POST["shortname"];
	$profile->displayName = $_POST["displayname"];
	$profile->owner = $_POST["owner"];
	updateProfile($profile);
	//updateProfileMessaging($profile, null);
	$currentProfile = $profile->shortName;
}
if ($currentProfile != "") {
	$sql = "Select p.id, p.shortname, p.displayname, p.owner " .
		"from profile p " .
		"where p.shortname='" . $currentProfile . "'";
	//echo $sql;
	$result = mysql_query($sql);
	if ($row = mysql_fetch_array($result))
	{
		$profile = new Profile();
		$profile->id = $row['id'];
		$profile->shortName = $row['shortname'];
		$profile->displayName = $row['displayname'];
		$profile->owner = $row['owner'];
		$editRecord = $profile->id;
	}
	else
	{
		$profile= new Profile();
		$profile->id = -1;
		$profile->shortName = "";
		$profile->displayName = "";
		$profile->owner = 0;
		$profile->welcomeMsg = "";
		$editRecord = $profile->id;
	}
}
mysql_close($con);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profiles</title>

<script type="text/javascript">
 </script>

<body>
<form action="profileedit.php" enctype="multipart/form-data" method="post" name="profileedit"><br/><br/>
<input type="hidden" name="magicquotecheck" value="'" />
<input type="hidden" name="editrecord" value="<?=$editRecord?>" />
<table>
<tr>
<td>Short Name</td>
<td><input type="text" name="shortname" value="<?=$profile->shortName?>" maxlength="30" size="30" /></td>
</tr>
<tr>
<tr>
<td>Display Name</td>
<td><input type="text" name="displayname" value="<?=$profile->displayName?>" maxlength="45" size="45" /></td>
</tr>
<tr>
<td>Owner</td>
<td><input type="text" name="owner" value="<?=$profile->owner?>" maxlength="10" size="10" /></td>
</tr>
</table>
<input type="hidden" name="submitToDatabase" value="1" />
<input type="hidden" name="currentProfile" value="<?=$currentProfile?>" />
<input type="submit" name="Submit" value="Submit" />
<input type="button" value="Go Back" onClick="location.href='profilelist.php?currentProfile=<?=$currentProfile?>'" />
</form>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>