<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/6/2017
 * Time: 11:12 PM
 */
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/mediaentrybean.php';
require_once '../wp-content/php/model/playlist.class.php';
require_once '../wp-content/php/model/playlistentry.class.php';
require_once '../wp-content/php/dao/MediaItemDao.php';
require_once '../wp-content/php/dao/PlaylistEntryDao.php';
require_once '../wp-content/php/dao/PlaylistDao.php';
require_once '../wp-content/php/model/video.class.php';

$con = getMySqliDbConnection();
$playlistDao = new PlaylistDao($con);
$current_user = wp_get_current_user();
$playlistId = $_POST['playlistid'];
if (empty($playlistId)) {
    $playlistId =  $_GET["playlistid"];
}
if ( empty($playlistId) ) {
    $playlistId = -1;
}
if ($playlistId > 0) {
    $playlist = $playlistDao->getPlaylistById($playlistId);
}

mysqli_close($con);
?>

<head>
    <title>Playlist Info Edit</title>

    <link rel="stylesheet" type="text/css" href="css/authoring.css">
</head>

<body>
<br/><br/>
<form action="playlistedit.php" onsubmit="javascript:doSubmit(this)" enctype="multipart/form-data" method="post" id="playlistinfoedit" name="playlistinfoedit"><br/><br/>
    <input type="hidden" name="magicquotecheck" value="'" />
    <input type="hidden" name="playlistid" id="playlistid" value="<?=$playlistId?>" />
    <input type="hidden" name="playlistinfoedit" id="playlistinfoedit" value="1" />
    <?php
    if ($playlistId == -1) {
        echo "<b>New Playlist</b>";
    } else {
        echo "<b>PLAYLIST ID #" . $playlistId . "</b>";
    }
    ?>
    <br/><br/>
    <table>
        <tr>
            <td>Name</td>
            <td><input type="text" name="playlistname" value="<?=$playlist->name?>" maxlength="100" size="100" /></td>
        </tr>
        <tr>
        <tr>
            <td>Guid</td>
            <td><?=$playlist->guid?></td>
        </tr>
        <tr>
            <td>Playlist Blurb</td>
            <td><textarea name="playlistblurb" cols="100" rows="3"><?=$playlist->blurb?></textarea></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="checkbox" name="makeinvisible" <?php if ($playlist->playlistPageInvisible) echo "CHECKED"; ?>>Make Playlist Page Invisible</input></td>
        </tr>
        <tr>
            <td colspan="2">
                <br/><br/>
                <input type="submit" value="Submit" />
                &nbsp;&nbsp;
                <input type="button" value="Cancel" onclick="location.href='playlistmenu.php?playlistid=<?=$playlistId?>'" />
            </td>
        </tr>
    </table>
    <br/><br/>
    <a href="playlistmenu.php?playlistid=<?=$playlistId?>">Playlist Menu</a><br/><br/>
    <br/><br/>
    <a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
