<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/19/19
 * Time: 10:12 PM
 */
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/gamecard.class.php';
require_once '../wp-content/php/dao/GameCardDao.php';
require_once '../wp-content/php/util/util.php';


$con = getMySqliDbConnection();
$gameCardDao = new GameCardDao($con);
$current_user = wp_get_current_user();

// $id as get, when coming in for the first time
$id = $_GET["id"];
if (empty($id)) {
    $id = -1;
}
$category = $_GET["category"];

function uploadFile($myFile, $gameCard)
{
    //phpinfo();
    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        echo "<p>An error occurred uploading audio file " . $myFile["name"] . "</p>";
        exit;
    }
    //echo "<br/>Name is: " . $myFile["name"] . "<br/>";
    //echo "Temp name is: " . $myFile["tmp_name"] . "<br/>";
    //echo "Temp dir is: " . ini_get('upload_tmp_dir') . "<br/>";
    //ini_set('upload_tmp_dir', '/home/mcmosber/public_html/tmp');
    //echo "After, temp dir is: " . ini_get('upload_tmp_dir') . "\n";
    //echo "Max filesize is: " . ini_get('upload_max_filesize') . "\n";
    //ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

    $parts = pathinfo($name);
//    $extension = strtolower($parts["extension"]);
    //echo "Name: " . $name . "  extension: " . $parts["extension"] . "<br/>";
    $uploadDir = "./" . $gameCard->category . "/";
    if (strcasecmp($parts["extension"], "mp3") == 0)
        $uploadDir .= "audio/";
    else
        $uploadDir .= "img/";
    //echo "Uploaded dir is: " . $uploadDir;

    if (!file_exists($uploadDir)) {
        mkdir($uploadDir, 0777, true);
    }
    echo "Target file is: " . $uploadDir . $name . "\n";
    /*
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name);
    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    */
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], $uploadDir . $name);
    if (!$success) {
        echo "<p>Unable to save file.</p>";
        exit;
    }
    // set proper permissions on the new file
    chmod($uploadDir . $name, 0644);

    echo "file uploaded successfully";
}

// if posting changes, $id will be from the _POST
if(isset($_POST["submitToDatabase"])) {
    $gameCard = new GameCard();
    $gameCard->id = $_POST["gameCardEntry"];
    $gameCard->category = $_POST["category"];
    $gameCard->breadcrumb = $_POST["breadcrumb"];
    $gameCard->blurb = $_POST["blurb"];
    $gameCard->description = $_POST["description"];
    $image = $_POST["image"];
    $audio = $_POST["audio"];

    $myImageFile = $_FILES["mycardimage"];
    if (!empty($myImageFile["name"]))
    {
        $image = $myImageFile["name"];
        uploadFile($myImageFile, $gameCard);
    }

    $myAudioFile = $_FILES["mycardaudio"];
    if (!empty($myAudioFile["name"]))
    {
        $audio = $myAudioFile["name"];
        uploadFile($myAudioFile, $gameCard);
    }

    $gameCard->img = $image;
    $gameCard->audio = $audio;

    $gameCardDao->upsert($gameCard);
    // be sure to sync up what the rest of the page expects $id to be
    $id = $gameCard->id;
}

if ($id != -1) {
    $gameCard = $gameCardDao->getGameCard($id);
} else {
    $gameCard = new GameCard();
    $gameCard->id = -1;
    $gameCard->category = $category;
}

if (empty($category) && $id == -1) {
    echo "Must have an id or category";
    exit;
}
$con->close();

?>
<html>
<head>
    <title>Game Card Editor</title>

    <link rel="stylesheet" type="text/css" href="css/authoring.css">
    <script language="JavaScript">
        function goBack(category){ location.href="gamecategoryselect.php?currentCategory=" + category; }

        function doSubmit(theForm) {
            theForm.submit();
            //closeWin();
        }

    </script>
</head>

<body>

<form action="gamecardeditor.php" onsubmit="javascript:doSubmit(this)" enctype="multipart/form-data" method="post" id="gamecardentry" name="gamecardentry"><br/><br/>
    <input type="hidden" name="submitToDatabase" value="1"/>
    <input type="hidden" name="gameCardEntry" value="<?=$gameCard->id?>"/>
    <input type="hidden" name="category" value="<?=$gameCard->category?>" />
    <table>
        <tr>
            <td>Category</td>
            <td><?=$gameCard->category?></td>
        </tr>
        <tr>
            <td>Breadcrumb</td>
            <td><input type="text" name="breadcrumb" value="<?=$gameCard->breadcrumb?>" maxlength="100" size="100" /></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><input type="text" name="description" value="<?=$gameCard->description?>" maxlength="100" size="100" /></td>
        </tr>

        <tr>
            <td>Blurb</td>
            <td><textarea name="blurb" cols="100" rows="10"><?=$gameCard->blurb?></textarea></td>
        </tr>
        <tr>
            <td>Image</td>
            <td>
                <?php
                echo "image start<br>";

                if (empty($gameCard->img))
                    echo "No image file chosen";
                else
                {
                    $filePath = assetPath() . "/" . $gameCard->category . "/img/" . $gameCard->img;
                    ?>
                    <img src='<?php echo $filePath;?>' width='120' height='90' /><br/>
                        <?php echo $gameCard->img;?>
                    <?php
                }

                ?>
                <br/>
                <input type="hidden" name="image" value="<?php echo $gameCard->img?>" />
                Choose a new file: <input name="mycardimage" type="file" /><br /><br /><br />
            </td>
        </tr>

        <tr>
            <td>Audio</td>
            <td><?php

                if (empty($gameCard->audio))
                    echo "No audio file chosen";
                else {
                    $filePath = assetPath() . $gameCard->category . "/audio/" . $gameCard->audio;
                    echo '<audio id="player1" controls src="' . $filePath . '"> </audio>';
                }
                echo "<br/>" . $gameCard->audio;
                ?>
                <br/>
                <input type="hidden" name="audio" value="<?php echo $gameCard->audio?>" />
                Choose a new file: <input name="mycardaudio" type="file" /><br />
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <hr/>
                <input type="submit" value="Submit Changes" />
                <input type="button" value="Done" onClick="goBack('<?=$gameCard->category?>')" />
            </td>
        </tr>
    </table>
</form>
</body>
</html>