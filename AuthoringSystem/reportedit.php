<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/13/2019
 * Time: 6:08 AM
 */

require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once('report.class.php');

$reportId = $_GET["reportid"];
if (empty($reportId)) {
    $reportId = $_POST["reportid"];
    if (empty($reportId)) {
        $reportId = -1;
    }
}
$mode = $_GET["mode"];
$con = getMySqliDbConnection();

$dosubmit = $_POST['dosubmit'];
if ($dosubmit == 1) {
    $report = new Report();
    $report->id = $_POST["reportid"];
    $report->reportName = $_POST["reportname"];
    $report->shortName = $_POST["reportshortname"];
    $report->sql = $_POST["reportsql"];
    $doInsert = false;
    if ($reportId == -1) {
        $sql = "insert into report (report_name, short_name, `sql`) " .
            "values ('" . $report->reportName . "', '" . $report->shortName .
            "', '" . $report->sql . "' )";
        $doInsert = true;
    } else {
        $sql = "update report set report_name='" . $report->reportName .
            "', short_name='" . $report->shortName . "', `sql`='" . $report->sql .
            "' where id=" . $report->id;
    }
    echo $sql;
    $result = mySqli_query_wrapper($con, $sql, "report edit");
    if ($doInsert) {
        $reportId = mysqli_insert_id($con);
    }
    $mode = 'edit';
}
if ($mode == 'edit') {
    $sql = "select * from report where id=" . $reportId;
    $result = mySqli_query_wrapper($con, $sql, "report edit");
    if ($row = mysqli_fetch_assoc($result)) {
        $report = new Report();
        $report->id = $row['id'];
        $report->shortName = $row['short_name'];
        $report->reportName = $row['report_name'];
        $report->sql = $row['sql'];
    }
} else {
    $reportId = -1;
}
$con->close();
?>


<html>
<head>

</head>
<body>
<br/><br/>
<form action="reportedit.php" onsubmit="javascript:doSubmit(this)" enctype="multipart/form-data"
      method="post" id="reportedit" name="reportedit"><br/><br/>
    <?php
    if ($mode == 'add') {
        echo "<b>New Report</b>";
    } else {
        echo "<b>Report ID #" . $report->id . "</b>";
    }
    ?>
    <br/><br/>
    <form action="reportedit.php" onsubmit="javascript:doSubmit(this)"
          enctype="multipart/form-data" method="post" id="reportedit" name="reportedit">
        <input type="hidden" name="dosubmit" value="1" />
        <input type="hidden" name="reportid" value="<?=$reportId?>">
    <table>
        <tr>
            <td>Name</td>
            <td><input type="text" name="reportname" value="<?=$report->reportName?>" maxlength="100" size="100" /></td>
        </tr>
        <tr>
        <tr>
            <td>Short Name</td>
            <td><input type="text" name="reportshortname" value="<?=$report->shortName?>" maxlength="20" size="20" /></td>
        </tr>
        <tr>
            <td>SQL</td>
            <td><textarea name="reportsql" cols="100" rows="3"><?=$report->sql?></textarea></td>
        </tr>
        <tr>
            <td colspan="2">
                <br/><br/>
                <input type="submit" value="Submit" />
                &nbsp;&nbsp;
                <input type="button" value="Cancel" onclick="location.href='reports.php'" />
            </td>
        </tr>
    </table>
    <br/><br/>
    </form>
    <a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>