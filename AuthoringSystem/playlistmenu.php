<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/6/2017
 * Time: 11:12 PM
 */
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/mediaentrybean.php';
require_once '../wp-content/php/model/playlist.class.php';
require_once '../wp-content/php/dao/MediaItemDao.php';
require_once '../wp-content/php/dao/PlaylistDao.php';
require_once '../wp-content/php/model/video.class.php';

$con = getMySqliDbConnection();
$playlistDao = new PlaylistDao($con);
$current_user = wp_get_current_user();
$playlistId = $_GET['playlistid'];
if ( 0 == $current_user->ID ) {
    die("Not logged in");
} else {
    $playlistsForUser = $playlistDao->getPlaylistsForOwner($current_user->ID);
}
if (empty($playlistId) || $playlistId <= 0) {
    $playlistId = $playlistsForUser[0]->id;
}
mysqli_close($con);
?>

<head>
    <title>Playlist Selection</title>

    <script language="JavaScript">
        function createNewPlaylist() {
            location.href= "playlistinfoedit.php?playlistid=-1"
        }
        function editPlaylistInfo() {
            var sel = document.getElementById("playlistMenu");
            var i = sel.selectedIndex;
            if (i >= 0) {
                var selectedId = sel.options[i].value;
                location.href = "playlistinfoedit.php?playlistid=" + selectedId;
            }
        }

        function editPlaylistMedia() {
            var sel = document.getElementById("playlistMenu");
            var i = sel.selectedIndex;
            if (i >= 0) {
                var selectedId = sel.options[i].value;
                location.href = "playlistedit.php?playlistid=" + selectedId;
            }
        }
    </script>
    <link rel="stylesheet" type="text/css" href="css/authoring.css">
</head>

<body>
<br/><br/>
<select name="playlistMenu" id="playlistMenu" size="<?php echo count($playlistsForUser)?>">
    <?php
    foreach ($playlistsForUser as $playlist) {
        if ($playlist->id == $playlistId) {
            echo "<option value='" . $playlist->id . "' SELECTED>" . $playlist->name . "</option>";
        } else {
            echo "<option value='" . $playlist->id . "'>" . $playlist->name . "</option>";
        }
    }
    ?>
</select>
<br/><br/>
<input type="button" value="Edit Playlist Info" onClick="editPlaylistInfo()" />
<br/><br/>
<input type="button" value="Add/Edit Media for Playlist" onClick="editPlaylistMedia()" />
<br/><br/>
<input type="button" value="Create New Playlist" onClick="createNewPlaylist()" />
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
