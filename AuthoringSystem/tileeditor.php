<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/19/19
 * Time: 10:12 PM
 */
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/tile.class.php';
require_once '../wp-content/php/dao/TileDao.php';
require_once '../wp-content/php/util/util.php';

//ini_set('display_errors',1);
//error_reporting(E_ALL);

function uploadFile($myFile)
{
    //phpinfo();
    if ($myFile["error"] !== UPLOAD_ERR_OK) {
        echo "<p>An error occurred uploading file " . $myFile["name"] . "</p>";
        exit;
    }

    //echo "<br/>Name is: " . $myFile["name"] . "<br/>";
    //echo "Temp name is: " . $myFile["tmp_name"] . "<br/>";
    //echo "Temp dir is: " . ini_get('upload_tmp_dir') . "<br/>";
    //ini_set('upload_tmp_dir', '/home/mcmosber/public_html/tmp');
    //echo "After, temp dir is: " . ini_get('upload_tmp_dir') . "\n";
    //echo "Max filesize is: " . ini_get('upload_max_filesize') . "\n";

    //ensure a safe filename
    $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

    //$parts = pathinfo($name);
    //$extension = strtolower($parts["extension"]);
    //echo "Name: " . $name . "  extension: " . $parts["extension"] . "<br/>";
    $uploadDir = "./wp-content/themes/compass/img/";
    //echo "Uploaded dir is: " . $uploadDir;

    if (!file_exists($uploadDir)) {
        mkdir($uploadDir, 0777, true);
    }
    //echo "Target file is: " . $uploadDir . $name . "\n";
    /*
    // don't overwrite an existing file
    $i = 0;
    $parts = pathinfo($name);
    while (file_exists(UPLOAD_DIR . $name)) {
        $i++;
        $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
    }
    */
    // preserve file from temporary directory
    $success = move_uploaded_file($myFile["tmp_name"], $uploadDir . $name);
    if (!$success) {
        echo "<p>Unable to save file.</p>";
        exit;
    }
    // set proper permissions on the new file
    chmod($uploadDir . $name, 0644);
}

$con = getMySqliDbConnection();
$tileDao = new TileDao($con);
$current_user = wp_get_current_user();

// $id as get, when coming in for the first time
$id = $_GET["id"];
if (empty($id)) {
    $id = -1;
}

$page = 'cardgames'; //could be changed into a menu

// if posting changes, $id will be from the _POST
if(isset($_POST["submitToDatabase"])) {
    $tile = new Tile();
    $tile->id = $_POST["id"];
    $tile->page = $_POST["page"];
    $tile->title = $_POST["title"];
    $tile->description = $_POST["description"];
    $tile->destination = $_POST["destination"];
    $image = $_POST["image"];
    $tile->sequence = $_POST["sequence"];

    $myImageFile = $_FILES["mycardimage"];
    if (!empty($myImageFile["name"]))
    {
        $image = $myImageFile["name"];
        uploadFile($myImageFile);
    }

    $tile->image = $image;

    $tileDao->upsert($tile);
    // be sure to sync up what the rest of the page expects $id to be
    $id = $tile->id;
}

if ($id != -1) {
    //echo "getting tile";
    $tile = $tileDao->getTile($id, $page);
} else {
    //echo "making new tile";
    $tile = new Tile();
    $tile->id = -1;
    $tile->page = $page;
}

if (empty($page) && $id == -1) {
    echo "Must have an id or page";
    exit;
}
$con->close();

?>
<html>
<head>
    <title>Tile Editor</title>

    <link rel="stylesheet" type="text/css" href="css/authoring.css">
    <script language="JavaScript">
        function goBack(){ location.href="tileselect.php" }

        function doSubmit(theForm) {
            theForm.submit();
            //closeWin();
        }
    </script>
</head>

<body>

<form action="tileeditor.php?id=<?=$tile->id?>" onsubmit="javascript:doSubmit(this)" enctype="multipart/form-data" method="post" id="tileEntry" name="tileEntry"><br/><br/>
    <input type="hidden" name="submitToDatabase" value="1"/>
    <input type="hidden" name="id" value="<?=$tile->id?>"/>
    <input type="hidden" name="page" value="<?=$tile->page?>" />
    <table>
        <tr>
            <td>Page</td>
            <td><?=$tile->page?></td>
        </tr>
        <tr>
            <td>Title</td>
            <td><input type="text" name="title" value="<?=$tile->title?>" maxlength="100" size="100" /></td>
        </tr>

        <tr>
            <td>Description</td>
            <td><input type="text" name="description" value="<?=$tile->description?>" maxlength="150" size="100" /></td>
        </tr>

        <tr>
            <td>Destination</td>
            <td><input name="destination" cols="100" rows="10" value="<?=$tile->destination?>"/></td>
        </tr>
        <tr>
            <td>Image</td>
            <td>
                <?php
                //echo "image start<br>";

                if (empty($tile->image))
                    echo "No image file chosen";
                else
                {
                    $filePath = ABSURL . "wp-content/themes/compass/img/" . $tile->image;
                    ?>
                    <img src='<?php echo $filePath;?>' width='120' height='90' /><br/>
                    <?php echo $tile->image;?>
                    <?php
                }

                ?>
                <br/>
                <input type="hidden" name="image" value="<?php echo $tile->image?>" />
                Choose a new file: <input name="mycardimage" type="file" /><br /><br /><br />
            </td>
        </tr>

        <tr>
            <td>Sequence</td>
            <td>
            <input type="number" name="sequence" value="<?=$tile->sequence?>" min="1" max="5"/>
            </td>
        </tr>

        <tr>
            <td colspan="3">
                <hr/>
                <input type="submit" value="Submit Changes" />
                <input type="button" value="Done" onClick="goBack()" />
                <input type="button" value="Delete" onClick="window.location.href='tileselect.php?deleteId=<?=$tile->id?>';"/>
            </td>
        </tr>
    </table>
</form>
</body>
</html>