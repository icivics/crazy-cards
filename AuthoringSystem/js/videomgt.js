function findChildElement(parentElement, childId) {
    var retElement = null;
    var lstChildren = getAllDescendant(parentElement);

    for (var i = 0; i < lstChildren.length; i++)
    {
        if (lstChildren[i].id == childId)
        {
            retElement = lstChildren[i];
            break;
        }
    }
    return retElement;
}

function getAllDescendant(parentElement, lstChildrenNodes) {
    lstChildrenNodes = lstChildrenNodes ? lstChildrenNodes : [];

    var lstChildren = parentElement.childNodes;

    for (var i = 0; i < lstChildren.length; i++)
    {
        if (lstChildren[i].nodeType == 1) // 1 is 'ELEMENT_NODE'
        {
            lstChildrenNodes.push(lstChildren[i]);
            lstChildrenNodes = getAllDescendant(lstChildren[i], lstChildrenNodes);
        }
    }

    return lstChildrenNodes;
}

function newVideo() {
    for (i=0;i<10;i++) {
        var elementName = "videoentry" + i.toString();
        var element = document.getElementById(elementName);
        if (element.className === "invisible") {
            element.className = "visible";
            videoPlayerElement = document.getElementById("expandedvideoplayer" + i.toString());
            videoPlayerElement.className = "invisible";
            break;
        }
    }
}

function parseVideoCode(videoURL) {
    var codePos = videoURL.indexOf("v=");
    var code = null;
    if (codePos != -1) {
        code = videoURL.substring(codePos + 2);
    } else {
        codePos = videoURL.indexOf("embed/");
        if (codePos != -1) {
            code = videoURL.substring(codePos + 6);
        }
    }
    if (code != null) {
        var qpos = code.indexOf("?");
        if (qpos != -1) {
            code = code.substring(0, qpos);
        }
        var amppos = code.indexOf("&");
        if (amppos != -1) {
            code = code.substring(0, amppos);
        }
    }
    return code;
}

var youtubeTemplate =
    "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/%s?rel=0\" frameborder=\"0\" allowfullscreen></iframe>";
var youtubeImageTemplate =
    "<img src='https://img.youtube.com/vi/%s' border='0' />";

function expandVideo(videoIndex) {
    var parentElement = document.getElementById("videoentry" + videoIndex.toString());
    var buttonElement = findChildElement(parentElement, "expandvideotogglebutton");
    var buttonValue = buttonElement.value;
    var expandedVideoElement = findChildElement(parentElement, "expandedvideo");
    var videoPlayerElement = findChildElement(parentElement, "expandedvideoplayer");
    var imgElement1 = findChildElement(parentElement, "expandedvideoimg_1");
    var imgElement2 = findChildElement(parentElement, "expandedvideoimg_2");
    var imgElement3 = findChildElement(parentElement, "expandedvideoimg_3");

    if (buttonValue === '+') {
        var urlElement = findChildElement(parentElement, "videoURL");
        var urlValue = urlElement.value;
        var code = parseVideoCode(urlValue);
        if (!code || code.length === 0) {
            html = "Video URL is invalid";
        } else {
            html = youtubeTemplate;
            html = html.replace("%s", code);
            htmlImg1 = youtubeImageTemplate.replace("%s", code + "/1.jpg");
            imgElement1.innerHTML = htmlImg1;
            imgElement1.className = 'visible';
            htmlImg2 = youtubeImageTemplate.replace("%s", code + "/2.jpg");
            imgElement2.innerHTML = htmlImg2;
            imgElement2.className = 'visible';
            htmlImg3 = youtubeImageTemplate.replace("%s", code + "/3.jpg");
            imgElement3.innerHTML = htmlImg3;
            imgElement3.className = 'visible';
        }
        videoPlayerElement.innerHTML = html;
        expandedVideoElement.className = 'visible';
        videoPlayerElement.className = 'visible';

        buttonElement.value = '-';
    } else {
        html = "placeholder";
        videoPlayerElement.innerHTML = html;
        videoPlayerElement.className = 'invisible';
        imgElement1.innerHTML = "placeholder";
        imgElement1.className = 'invisible';
        imgElement2.innerHTML = "placeholder";
        imgElement2.className = 'invisible';
        imgElement3.innerHTML = "placeholder";
        imgElement3.className = 'invisible';
        buttonElement.value = '+';
    }
}

function deleteElementById(elementId) {
    var elem = document.getElementById(elementId);
    var newElement = elem.cloneNode(true);
    newElement.className = 'invisible';
    parentElement = elem.parentElement;
    parentElement.removeChild(elem);
    newId = findHighestVideoId() + 1;
    newElement.id = "videoentry" + newId.toString();
    parentElement.appendChild(newElement);
}

function deleteVideo(videoIndex) {
    if (confirm('Delete this video, are you sure?')) {
        deleteElementById("videoentry" + videoIndex.toString());
    } else {
        // Do nothing!
    }
}

function createHiddenElement(theForm, key, value) {
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = key;
    input.value = value;
    theForm.appendChild(input);
    return input;
}

function doSubmit(theForm) {
    divEntryElements = document.getElementsByName("videoentrydiv");
    count = 0;
    for (var i=0; i < divEntryElements.length; i++) {
        divEntryElement = divEntryElements[i];
        if (divEntryElement.className === 'visible') {
            urlElement = findChildElement(divEntryElement, "videoURL");
            notesElement = findChildElement(divEntryElement, "videoNotes");
            newElement = createHiddenElement(theForm, "videourl" + count.toString(), urlElement.value);
            newElement2 = createHiddenElement(theForm, "videocode" + count.toString(), parseVideoCode(urlElement.value));
            newElement3 = createHiddenElement(theForm, "videonotes" + count.toString(), notesElement.value);
            count++;
        }
    }
    theForm.submit();
}

function addNew() {
    document.getElementById("addnew").value = "1";
    document.getElementById("submitToDatabase").value = "0";
    doSubmit(document.getElementById("mediaentry"));
}
