<?php
require_once('auth.php');
require_once '../wp-content/php/dbdefs.php';
require_once '../wp-content/php/songbean.php';
require_once '../wp-content/php/mediaentrybean.php';
require_once '../wp-content/php/video.class.php';

$DbLink = mysqli_connect(DBHOST, DBUSER, DBPASSWORD, DBSCHEMA);
if (mysqli_connect_errno($DbLink)) {
    die('Connect Error: ' . mysqli_connect_error());
}

$originalSongs = array();
$sql = "select t.id, t.title, s.article_text from topic t join selection s on t.id=s.topic where t.topic_type='SONG' and s.type=1";
$result = mysqli_query($DbLink, $sql);
while($row = mysqli_fetch_assoc($result))
{
    $mediaEntryBean = new MediaEntryBean();
    $mediaEntryBean->originalId = $row['id'];
    $songTitle = $row['title'];
    $songTitle = str_replace("\'", "'", $songTitle);
    $mediaEntryBean->title = mysqli_real_escape_string($DbLink, $songTitle);
    $introText = $row['article_text'];
    $mediaEntryBean->album = mysqli_real_escape_string($DbLink, parseString($introText, "Album:"));
    $mediaEntryBean->artist = mysqli_real_escape_string($DbLink, parseString($introText, "Artist:"));
    $mediaEntryBean->released = mysqli_real_escape_string($DbLink, parseString($introText, "Released:"));
    array_push($originalSongs, $mediaEntryBean);
}

foreach ($originalSongs as $mediaEntryBean) {
    $sql = "insert into song (title, artist, album, released) values ('" . $mediaEntryBean->title .
        "', '" . $mediaEntryBean->artist . "', '" . $mediaEntryBean->album . "', '" . $mediaEntryBean->released . "')";
    mysqli_query($DbLink,$sql) or die (mysqli_error($DbLink));
    $mediaEntryBean->id =  mysqli_insert_id($DbLink);

}

$videos = array();
$sql = "select * from video";
$result = mysqli_query($DbLink, $sql);
while($row = mysqli_fetch_assoc($result))
{
    $video = new Video();
    $video->id = $row['id'];
    $video->topicId = $row['topic_id'];
    $video->selectionId = $row['selection_id'];
    $video->videoId = $row['video_id'];
    array_push($videos, $video);
}

foreach ($videos as $video) {
    if (($mediaEntryBean = videoIsFound($originalSongs, $video)) != null) {
        $sql = "insert into songvideoxref (song_id, video_id) values (" . $mediaEntryBean->id . "," . $video->id . ")";
        mysqli_query($DbLink,$sql) or die (mysqli_error($DbLink));
    }
}

mysqli_close($DbLink);

function videoIsFound($originalSongs, $video) {
    foreach ($originalSongs as $mediaEntryBean) {
        if ($mediaEntryBean->originalId == $video->topicId) {
            return $mediaEntryBean;
        }
    }
    return null;
}

function parseString($origString, $label) {
    $pos = strpos($origString, $label) + strlen($label);
    $endpos = strpos($origString, "\r", $pos);
    $targetString = null;
    if ($pos != false) {
        if ($endpos != false) {
            $targetString = trim(substr($origString, $pos, $endpos - $pos));
        } else {
            $targetString = trim(substr($origString, $pos));
        }
    }
    return $targetString;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Migrate Songs</title>

</head>
<body>

Migrating of songs is done!
<br/><br/>
<a href="utilitiesmenu.php">Utilities Menu</a><br/><br/>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>