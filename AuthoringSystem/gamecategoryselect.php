<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/21/2019
 * Time: 9:55 PM
 */
error_reporting( E_ALL );

require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/gamecard.class.php';
require_once '../wp-content/php/dao/GameCardDao.php';

$currentCategory = "";
if (isset($_POST["currentCategory"]))
    $currentCategory = $_POST["currentCategory"];

if ($currentCategory == "")
    $currentCategory = $_GET["currentCategory"];

$con = getMySqliDbConnection();
$gameCardDao = new GameCardDao($con);
$current_user = wp_get_current_user();

$arrCategories = $gameCardDao->getAllCategories();

if (!empty($currentCategory) ) {
    $thisCategory = array();
    array_push($thisCategory, $currentCategory);
    $arrGameCards = $gameCardDao->getAllGameCards($thisCategory);
}

$con->close();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Game Category Selections</title>
</head>
<body>
    <form id="form1" name="form1" method="post" action="">
        <label>Topic
            <select name="currentCategory" id="currentCategory" onchange="this.form.submit()">
                <?php
                if ($currentCategory == "")
                    echo "<option selected value=''>Select a category</option>";
                else
                    echo "<option value=''>Select a category</option>";
                $count = 1;
                foreach ($arrCategories as $category) {
                    if ($currentCategory == $category)
                        echo "<option selected value='" . $category . "'>" . $category . "</option>";
                    else
                        echo "<option value='" . $category . "'>" . $category . "</option>";
                    $count++;
                }
                ?>
            </select>
        </label>
    </form>
    <?php
        if (!empty($currentCategory)) {
            echo "<a href='gamecardeditor.php?id=-1&category=" . $currentCategory . "'>Add New Card</a><br/><br/><br/>";
            foreach ($arrGameCards as $gameCard) {
                echo "<a href='gamecardeditor.php?id=" . $gameCard->id . "'>" . $gameCard->breadcrumb . "</a><br/><br/>";
            }
        }
    ?>

    <br/><br/>
    <a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>
</html>