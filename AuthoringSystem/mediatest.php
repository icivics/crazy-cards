<?php
require_once('auth.php');
require_once '../wp-content/php/dbdefs.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Media Lookup</title>
<?php

function tokenizeLink($strLink)
{
	$strLink = str_replace("<", "&lt;", $strLink);
	$strLink = str_replace(">", "&gt;", $strLink);
	return $strLink;
}

$topic = "";
$keywords = ""; 
$resultsLog = "";
if(isset($_POST["submitToDatabase"]))
{
	$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
	if (!$con)
	{
		die('Could not connect: ' . mysql_error());
	}
	
	mysql_select_db(DBSCHEMA, $con);
	$topic = stripslashes($_POST["topic"]);
	$keywords = stripslashes($_POST["keywords"]);
	
	// need to adjust this line of sql
	$sql = "select * from selection where type=3 or type=4";
	$result = mysql_query($sql);
	if ($result == false)
	{
		$resultsLog = mysql_error();
	}
	else
	{
		$resultsLog = "<table><tr>";
		for($i = 0; $i < mysql_num_fields($result); $i++) {
		    $field_info = mysql_fetch_field($result, $i);
		    $resultsLog .= "<th>{$field_info->name}</th>";
		}
		
		// Print the data
		while($row = mysql_fetch_row($result)) {
		    $resultsLog .= "<tr>";
		    foreach($row as $_column) {
		        $resultsLog .= "<td>" . tokenizeLink($_column) . "</td>";
		    }
		    $resultsLog .= "</tr>";
		}
		
		$resultsLog .= "</table>";	
	}
	mysql_close($con);
	
}

?>

</head>

<body>
<form id="form1" name="form1" method="post" action="">
Topic:<br/><br/>
<input type="text" name="topic" value="<?php echo $topic; ?>"><br/><br/>
Key Words:<br/><br/>
<input type="text" name="keywords" value="<?php echo $keywords; ?>">
<br/><br/>
<input type="submit" />
<input type="hidden" name="submitToDatabase" value="Y" />
</form>
<br/><br/>
Results:<br/><br/>
<?php echo $resultsLog;?>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>