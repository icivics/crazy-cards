<?php
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Authoring Menu</title>

</head>
<body>
<h1>Authoring Menu</h1>
Product is <?=PRODUCT?><br/><br/>
<?php if (PRODUCT == 'MyMoralCompass'): ?>
<a href="">Topics Overview NO LONGER OPERATIONAL (Add/delete topic, change topic metadata)</a>
<br/><br/>
<a href="topicselections.php?type=STORY">Edit Topics</a>
<br/><br/>
<a href="topicselections.php?type=FIGURE">Edit Historical Figures</a>
<br/><br/>
<a href="profilelist.php">Add/edit Profile</a>
<br/><br/>
<a href="welcomemessagelist.php">Edit Welcome Messages</a>
<br/><br/>
<a href="manageclassroomcodes.php">Manage Classroom Codes</a>
<br/><br/>
<?php endif;?>

<?php if (PRODUCT == 'Top100'): ?>
<a href="topicselections.php?type=SONG">Edit Songs</a>
<br/><br/>
<?php endif;?>

<a href="reports.php">Reports</a>
<br/><br/>
<?php if (PRODUCT == 'Top100'): ?>
<a href="media-list.php">Media Entry and Edit</a>
<br/><br/>
<a href="playlistmenu.php">Create and Edit Playlists</a>
<br/><br/>
<a href="utilitiesmenu.php">Utilities</a>
<br/><br/>
<?php endif;?>
<?php if (PRODUCT != 'MyMoralCompass'): ?>
<a href="gamecategoryselect.php">Add/edit Game Cards</a>
<?php endif;?>
<br><br>
<a href="tileselect.php">Add/edit Menu Tiles</a>
</body>