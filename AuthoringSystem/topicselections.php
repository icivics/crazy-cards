<?php 
//require_once('auth.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Topic Selections</title>
<?php

$currentTopic = "";
$deleteSelection = "";
$currentType = "";
$currentTopic = "";

if (isset($_POST["currentTopic"]))
	$currentTopic = $_POST["currentTopic"];

if ($currentTopic == "" && isset($_GET["currentTopic"]))
	$currentTopic = $_GET["currentTopic"];
	
if (isset($_POST["type"]))
	$currentType = $_POST["type"];

if ($currentType == "")
	$currentType = $_GET["type"];
	
if (isset($_GET["deleteSelection"]))
	$deleteSelection = $_GET["deleteSelection"];
	
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/topic.class.php';
require_once '../wp-content/php/model/topicselection.class.php';
require_once '../wp-content/php/model/topicdocument.class.php';
require_once '../wp-content/php/dao/topicdocumentmethods.php';
require_once '../wp-content/php/dao/dbfunctions.php';

$con = getMySqliDbConnection();

if (!empty($deleteSelection))
{
	$sql = "Delete from selection where id=" . $deleteSelection;
	$result = mySqli_query_wrapper($con, $sql, "topicselections.php delete");
	if ($result != 1)
	{
		echo "Error deleting selection --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
	}
	
}

// adding the game topic cause there's no way to edit the game topic yet
$sql = "Select * from topic where topic_type='" . $currentType . "' or topic_type='GAME' order by title";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "topicselections.php select");
$numberOfTopics = 0;
	while($row = mysqli_fetch_assoc($result))
	  {
	  	$atopic = new topicSpec();
		$atopic->id = $row['id'];
		$atopic->sequence = $row['sequence'];
		$atopic->shortName = $row['short_name'];
		$atopic->title = $row['title'];
		$topics[$numberOfTopics] = $atopic;
		$numberOfTopics = $numberOfTopics + 1;
	  }

if ($currentTopic != '')
{
	$sql="SELECT s.id, s.type, s.sequence, s.article_text, s.thumbnail_image, " .
        "s.graphic_image, s.breadcrumb, s.video_embed_code FROM selection s inner join topic t " .
        " on s.topic=t.id WHERE t.short_name = '".$currentTopic."' order by s.id";
	//echo $sql;
	
	$result = mySqli_query_wrapper($con, $sql, "topicselections select");
	$numberOfPages = 0;
	  //echo "<table>";
	while($row = mysqli_fetch_assoc($result))
	  {
		$aSelection = new Selection();
		$aSelection->id = $row['id'];
		$aSelection->type = $row['type'];
		if (empty($row['sequence']))
			$aSelection->sequence = '';
		else
			$aSelection->sequence = $row['sequence'];
		$aSelection->article = nl2br($row['article_text']);
		$aSelection->thumbnail = $row['thumbnail_image'];
		$aSelection->image = $row['graphic_image'];
		$aSelection->breadcrumb = nl2br($row['breadcrumb']);
		$aSelection->videoEmbedCode = $row['video_embed_code'];

		$topicSelections[$numberOfPages] = $aSelection;
		$numberOfPages = $numberOfPages + 1;
		/*
	  echo "<tr>";
	  echo "<td>" . $row['type'] . "</td>";
	  echo "<td>" . $row[2] . "</td>";
	  echo "<td>" . $row['thumbnail_image'] . "</td>";
	  echo "<td>" . $row['title'] . "</td>";
	  echo "</tr>";
	  */
		if ($aSelection->type == Selection::DOCUMENT) {
		  	$topicDocument = fetchSelectionDocument($con, $aSelection->id);
		  	$aSelection->image = $topicDocument->file;
		  	$aSelection->breadcrumb = $topicDocument->name;
		}
	  }
	//echo "</table>";
}  

$con->close();

//$topicSelections[0]->printTopicLabels();
//exit();
?>
<script language="Javascript">
function confirmDelete(topic, selection) {
	if (confirm("Are you sure?") );
		location.href = "?currentTopic=" + topic + "&deleteSelection=" + selection;
}
</script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <label>Topic
  <select name="currentTopic" id="currentTopic" onchange="this.form.submit()">
  <?php
  	if ($currentTopic == "")
		echo "<option selected value=''>Select a topic</option>";
	else
		echo "<option value=''>Select a topic</option>";
	$count = 1;
  	foreach ($topics as $topic) {
		if ($currentTopic == $topic->shortName)
			echo "<option selected value='" . $topic->shortName . "'>" . $count . ". " . $topic->title . "</option>";
		else
			echo "<option value='" . $topic->shortName . "'>" . $count . ". " . $topic->title . "</option>";
		$count++;
	}
  ?>
  </select>
  </label>
</form>
<?php
	if ($currentTopic != '')
	{
		echo "<table border=1>";
		echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>Type</td><td>Sequence</td><td>Thumbnail</td><td>Image</td><td>Breadcrumb</td><td>Video Embed</td></tr>";
		foreach ($topicSelections as $selection)
		{
			echo "<tr>";
			echo "<td><a href='edittopicselection.php?currentTopic=" . $currentTopic . "&record=" . $selection->id . "&mode=edit&type=" . 
				$currentType . "'>Edit</a></td>";
			echo "<td><a href='javascript:confirmDelete(\"" . $currentTopic . "\", " . $selection->id . ");'>Delete</a></td>";
			echo "<td>" . $selection->topicLabel() . "</td>";
			echo "<td>" . $selection->sequence . "</td>";
			echo "<td>" . $selection->thumbnail . "</td>";
			echo "<td>" . $selection->image . "</td>";
			echo "<td>" . $selection->breadcrumb . "</td>";
			echo "<td>" . $selection->tokenizeLink($selection->videoEmbedCode) . "</td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<a href='edittopicselection.php?currentTopic=" . $currentTopic . "&mode=add&type=" . $currentType . "'>Add new selection</a>";
	}
?>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
