<?php 
require_once('auth.php');
require_once '../wp-content/php/dbdefs.php';
require_once '../wp-content/php/category.class.php';
require_once '../wp-content/php/profile.class.php';
require_once('../wp-content/php/topic.class.php');
require_once '../wp-content/php/topicconfiguration.class.php';
require_once '../wp-content/php/tcmeta.class.php';
require_once('../wp-content/php/topicselection.class.php'); 
require_once('../wp-content/php/topicdocument.class.php');
require_once('../wp-content/php/topicconfigurationdocument.class.php');
require_once('../wp-content/php/topicdocumentmethods.php');
require_once('../wp-content/php/profilemethods.php');
require_once('../wp-content/php/dbfunctions.php');
require_once '../wp-content/php/util.php';

function uploadFile($myFile, $myTopic, $myProfile)
{
	//phpinfo();
	
	if ($myFile["error"] !== UPLOAD_ERR_OK) {
		echo "<p>An error occurred.</p>";
		exit;
	}
	
	//echo "<br/>Name is: " . $myFile["name"] . "<br/>";
	//echo "Temp name is: " . $myFile["tmp_name"] . "<br/>";
	//echo "Temp dir is: " . ini_get('upload_tmp_dir') . "<br/>";
	//ini_set('upload_tmp_dir', '/home/mcmosber/public_html/tmp');
	//echo "After, temp dir is: " . ini_get('upload_tmp_dir') . "\n";
	//echo "Max filesize is: " . ini_get('upload_max_filesize') . "\n";
	// ensure a safe filename
	$name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

	$uploadDir = "../wp-content/docs/" . $myTopic . "/" . $myProfile . "/";
	
	echo "Upload dir: " . $uploadDir . "<br/>";
	if (!file_exists($uploadDir)) {
		echo "creating directory <br/>";
	    mkdir($uploadDir, 0777, true);
	}	
	/*
	 // don't overwrite an existing file
	$i = 0;
	$parts = pathinfo($name);
	while (file_exists(UPLOAD_DIR . $name)) {
	$i++;
	$name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
	}
	*/
	// preserve file from temporary directory
	$success = move_uploaded_file($myFile["tmp_name"],
			$uploadDir . $name);
	if (!$success) {
		echo "<p>Unable to save file.</p>";
		exit;
	}
	// set proper permissions on the new file
	chmod($uploadDir . $name, 0644);
}

$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_select_db(DBSCHEMA, $con);

$tcid = $_GET["id"];
if (isset($_POST["tcrecord"]))
	$tcrecord = $_POST["tcrecord"];
else
	$tcrecord = $_GET["value"];

/*
echo "<table>";
foreach ($_POST as $key => $value) {
	echo "<tr>";
	echo "<td>";
	echo $key;
	echo "</td>";
	echo "<td>";
	echo $value;
	echo "</td>";
	echo "</tr>";
}
echo "</table>";
*/

$topicId = getTopicIdFromTopicConfiguration($tcrecord);

if(isset($_POST["submitToDatabase"]))
{
	$fields = array();
	$fieldValues = array();
	$dataTypes = array();
	
	$profileName = getProfileNameForTopicConfiguration($tcrecord);
	$topicName = getTopicNameFromTopicConfiguration($tcrecord);
	
	// shared documents
	$done = false;
	$sharedDocCount = 1;
	$sharedDocs = array();
	while (!$done) {
		$docRecord = $_POST["tdrecord" . $sharedDocCount];
		if ($docRecord == null || empty($docRecord)) {
			$done = true;
		} else  {
			$checkboxvalue = $_POST["shareddocvisible" . $sharedDocCount];
			$visible = ($checkboxvalue == "on" ? 1 : 0);
			updateTopicConfigurationDocument($tcrecord, $docRecord, $visible, null);
		}
		$sharedDocCount++;
	}
	// private documents
	$origDocString1 = $_POST["origdoc1"];
	$origDocString2 = $_POST["origdoc2"];
	$origDocString3 = $_POST["origdoc3"];
	//echo "<br/>originalDocString1: " . $origDocString1;
	
	$doc1Object = new TopicDocument($origDocString1);
	$doc1Object->name = $_POST["doc1text"];
	$doc2Object = new TopicDocument($origDocString2);
	$doc2Object->name = $_POST["doc2text"];
	$doc3Object = new TopicDocument($origDocString3);
	$doc3Object->name = $_POST["doc3text"];
	//echo "<br/>$doc1Object name: " . $doc1Object->filename;
	$doc1Object->url = $_POST["documentURL1"];
	$doc2Object->url = $_POST["documentURL2"];
	$doc3Object->url = $_POST["documentURL3"];
	
	$mydoc1file = $_FILES["mydoc1file"];
	$mydoc2file = $_FILES["mydoc2file"];
	$mydoc3file = $_FILES["mydoc3file"];
	//echo "<br/>$mydoc1file: " . $mydoc1file;
	
	
	if (!empty($mydoc1file["name"]))
	{
		uploadFile($mydoc1file, $topicName, $profileName);
		$doc1Object->file = $mydoc1file["name"];
	}
	
	if (!empty($mydoc2file["name"]))
	{
		$doc2file = $mydoc2file["name"];
		uploadFile($mydoc2file, $topicName, $profileName);
		$doc2Object->file = $mydoc2file["name"];
	}
	
	if (!empty($mydoc3file["name"]))
	{
		$doc3file = $mydoc3file["name"];
		uploadFile($mydoc3file, $topicName, $profileName);
		$doc3Object->file = $mydoc3file["name"];
	}
	
	$lstTcMeta = array();
	$tcMeta = new tcMeta();
	
	$tcMeta->field = "show_instructions";
	$tcMeta->fieldValue = $_POST["showinstructions"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	if (!empty($_POST["instructions"])) {
		$tcMeta = new tcMeta();
		$tcMeta->field = "instructions";
		$tcMeta->fieldValue = $_POST["instructions"];
		$tcMeta->fieldType = "text";
		array_push($lstTcMeta, $tcMeta);
	}
		
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_author_analysis";
	$tcMeta->fieldValue = $_POST["showauthoranalysis"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_author_scorecard";
	$tcMeta->fieldValue = $_POST["showauthorscorecard"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_user_scorecard";
	$tcMeta->fieldValue = $_POST["showuserscorecard"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_generic_scorecard";
	$tcMeta->fieldValue = $_POST["showgenericscorecard"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_comments";
	$tcMeta->fieldValue = $_POST["showcomments"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_login_controls";
	$tcMeta->fieldValue = $_POST["showLoginControls"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_dbqs";
	$tcMeta->fieldValue = $_POST["showdbqs"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	/*
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_topic_documents";
	$tcMeta->fieldValue = $_POST["showTopicDocuments"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	
	$tcMeta = new tcMeta();
	$tcMeta->field = "show_config_documents";
	$tcMeta->fieldValue = $_POST["showConfigDocuments"];
	$tcMeta->fieldType = "boolean";
	array_push($lstTcMeta, $tcMeta);
	*/
	if (!empty($_POST["dbq1"])) {
		$tcMeta = new tcMeta();
		$tcMeta->field = "dbq1";
		$tcMeta->fieldValue = $_POST["dbq1"];
		$tcMeta->fieldType = "text";
		array_push($lstTcMeta, $tcMeta);
	}
	
	if (!empty($_POST["dbq2"])) {
		$tcMeta = new tcMeta();
		$tcMeta->field = "dbq2";
		$tcMeta->fieldValue = $_POST["dbq2"];
		$tcMeta->fieldType = "text";
		array_push($lstTcMeta, $tcMeta);
	}
	
	if (!empty($_POST["dbq3"])) {
		$tcMeta = new tcMeta();
		$tcMeta->field = "dbq3";
		$tcMeta->fieldValue = $_POST["dbq3"];
		$tcMeta->fieldType = "text";
		array_push($lstTcMeta, $tcMeta);
	}
	
	$doc1FullString = $doc1Object->getFullString();
	if (!empty($doc1FullString)) {
		$doc = new TopicDocument($doc1FullString);
		$doc->url = $doc1Object->url;
		$checkboxvalue = $_POST["privatedocvisible1"];
		$doc = updateTopicDocument($doc, $topicId, $tcrecord);
		$doc->visible = ($checkboxvalue == "on" ? 1 : 0);
		updateTopicConfigurationDocument($tcrecord, $doc->id, $doc->visible, null);
	}
		
	$doc2FullString = $doc2Object->getFullString();
	if (!empty($doc2FullString)) {
		$doc = new TopicDocument($doc2FullString);
		$doc->url = $doc2Object->url;
		$checkboxvalue = $_POST["privatedocvisible2"];
		$doc = updateTopicDocument($doc, $topicId, $tcrecord);
		$doc->visible = ($checkboxvalue == "on" ? 1 : 0);
		updateTopicConfigurationDocument($tcrecord, $doc->id, $doc->visible, null);
	}
		
	$doc3FullString = $doc3Object->getFullString();
	if (!empty($doc3FullString)) {
		$doc = new TopicDocument($doc3FullString);
		$doc->url = $doc3Object->url;
		$checkboxvalue = $_POST["privatedocvisible3"];
		$doc = updateTopicDocument($doc, $topicId, $tcrecord);
		$doc->visible = ($checkboxvalue == "on" ? 1 : 0);
		updateTopicConfigurationDocument($tcrecord, $doc->id, $doc->visible, null);
	}
		
	updateTopicConfiguration($tcrecord, $lstTcMeta, null);
	/*	
	$sql = "delete from topic_configuration_meta where topic_configuration_id=" . $tcrecord;
	$result = mysql_query($sql);
	if ($result != 1)
	{
		echo "Error delete topic configuration meta --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
		exit();
	}
	
	
	for ($i=0;$i<count($fields);$i++)
	{
		$sql = "insert into topic_configuration_meta (topic_configuration_id, meta_key, meta_value) values (" . $tcrecord . ", '" .
				$fields[$i] . "', '";
		if ($dataTypes[$i] == "text")
			$sql = $sql . $fieldValues[$i] . "')";
		else
			$sql = $sql . ($fieldValues[$i] == "on" ? "true" : "false") . "')";
		//echo "Executing sql: " . $sql . "<br/>";  
		$result = mysql_query($sql);
		if ($result != 1)
		{
			echo "Error inserting score --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
	}
	*/
}
$topicAllDocuments = fetchDocumentsForTopicConfiguration($tcrecord, false, null);
$topicSharedDocuments = getSubsetOfDocuments($topicAllDocuments, 1);
$topicPrivateDocuments = getSubsetOfDocuments($topicAllDocuments, 0);
$topicUnlinkedSharedDocuments = fetchUnlinkedSharedDocuments($tcrecord, $topicId);
$topicSharedDocuments = array_merge($topicSharedDocuments, $topicUnlinkedSharedDocuments);
 
$topicConfiguration = fetchTopicConfiguration($tcrecord, null);
$configDoc1 = new TopicDocument("");
if (count($topicPrivateDocuments) > 0) {
	$configDoc1->name = $topicPrivateDocuments[0]->name;
	$configDoc1->file = $topicPrivateDocuments[0]->file;
	$configDoc1->url = $topicPrivateDocuments[0]->url;
	$configDoc1->visible = $topicPrivateDocuments[0]->visible;
}
$configDoc2 = new TopicDocument("");
if (count($topicPrivateDocuments) > 1) {
	$configDoc2->name = $topicPrivateDocuments[1]->name;
	$configDoc2->file = $topicPrivateDocuments[1]->file;
	$configDoc2->url = $topicPrivateDocuments[1]->url;
	$configDoc2->visible = $topicPrivateDocuments[1]->visible;
}
$configDoc3 = new TopicDocument("");
if (count($topicPrivateDocuments) > 2) {
	$configDoc3->name = $topicPrivateDocuments[2]->name;
	$configDoc3->file = $topicPrivateDocuments[2]->file;
	$configDoc3->url = $topicPrivateDocuments[2]->url;
	$configDoc3->visible = $topicPrivateDocuments[2]->visible;
}

?>
<html><head><title>Topic Congfiguration</title></head>
<style type="text/css">
textarea.orange-scrollbar {scrollbar-base-color:orange;}
textarea.red-scrollbar {scrollbar-base-color:red;}

.hidden {
  display: none;
}
</style>

<script type="text/javascript">
function closeWin(){ window.close(); }

function makeVisible(tag, vis) {
	var element = document.getElementById(tag);
	if (element)
	{
		if (vis)
			element.className = "visible";
			//element.style.display = "block";	
		else
			element.className = "hidden";
			//element.style.display = "none";		
	}
}

function instructionsClicked(cb)
{
	makeVisible("divinstructions", cb.checked);
}

function dbqsClicked(cb)
{
	makeVisible("divdbq1", cb.checked);
	makeVisible("divdbq2", cb.checked);
	makeVisible("divdbq3", cb.checked);
}

function docsClicked(cb)
{
	makeVisible("divdoc1", cb.checked);
	makeVisible("divdoc2", cb.checked);
	makeVisible("divdoc3", cb.checked);
}

function initializeVisibility()
{
	var element = document.getElementById("showinstructions");
	if (element)
		makeVisible("divinstructions", element.checked);
	element = document.getElementById("showdbqs");
	if (element)
	{
		makeVisible("divdbq1", element.checked);
		makeVisible("divdbq2", element.checked);
		makeVisible("divdbq3", element.checked);
	}
	element = document.getElementById("showConfigDocuments");
	if (element)
	{
		makeVisible("divdoc1", element.checked);
		makeVisible("divdoc2", element.checked);
		makeVisible("divdoc3", element.checked);
	}
}
</script>

<body onload="initializeVisibility();">
<form action="tcpopup.php" enctype="multipart/form-data" method="post" name="tcpopup" id="tcpopup" onsubmit="javascript:handleSubmit()">
<input type="hidden" name="submitToDatabase" value="1" />
<input type="hidden" name="tcrecord" value="<?=$tcrecord?>" />
<input type="hidden" name="origdoc1" value="<?=$configDoc1->getFullString();?>" />
<input type="hidden" name="origdoc2" value="<?=$configDoc2->getFullString();?>" />
<input type="hidden" name="origdoc3" value="<?=$configDoc3->getFullString();?>" />
<input type="checkbox" id="showinstructions" name="showinstructions" <?php if ($topicConfiguration->showInstructions) echo "checked"?> onclick="instructionsClicked(this);">Show Instructions</input>
<div id="divinstructions">
Instructions:
<br/>
<TEXTAREA NAME="instructions" class="red-scrollbar" COLS=80 ROWS=3><?=$topicConfiguration->instructions?></TEXTAREA>
</div>
<br/>
<table padding="10" border="2">
<tr>
<td valign="top">
<input type="checkbox" name="showauthoranalysis" <?php if ($topicConfiguration->showAuthorAnalysis) echo "checked"?>>Show Author Analysis</input>
<br/>
<input type="checkbox" name="showauthorscorecard" <?php if ($topicConfiguration->showAuthorScorecard) echo "checked"?>>Show Author Scorecard</input>
<br/>
<input type="checkbox" name="showuserscorecard" <?php if ($topicConfiguration->showUserScorecard) echo "checked"?>>Show User Scorecard</input>
<br/>
<input type="checkbox" name="showgenericscorecard" <?php if ($topicConfiguration->showGenericScorecard) echo "checked"?>>Show Generic Scorecard</input>
<br/>
<input type="checkbox" name="showcomments" <?php if ($topicConfiguration->showComments) echo "checked"?>>Show Comments</input>
<br/>
<input type="checkbox" id="showdbqs" name="showdbqs" <?php if ($topicConfiguration->showDbqs) echo "checked"?> onclick="dbqsClicked(this);">Show DBQs</input>
<br/>
<input type="checkbox" name="showLoginControls" <?php if ($topicConfiguration->showLoginControls) echo "checked"?>>Show Login Controls</input>
<br/>
<div id="divdbq1">
DBQ 1:
<br/>
<TEXTAREA NAME="dbq1" class="red-scrollbar" COLS=80 ROWS=3><?=$topicConfiguration->dbq1?></TEXTAREA>
</div>
<div id="divdbq2">
DBQ 2:
<br/>
<TEXTAREA NAME="dbq2" class="red-scrollbar" COLS=80 ROWS=3><?=$topicConfiguration->dbq2?></TEXTAREA>
</div>
<div id="divdbq3">
DBQ 3:
<br/>
<TEXTAREA NAME="dbq3" class="red-scrollbar" COLS=80 ROWS=3><?=$topicConfiguration->dbq3?></TEXTAREA>
</div>
</td>
<td valign="top">
Shared documents - check to make <br/>
visible on your page::
<br/><br/>
<?php 
$sharedDocCount = 1;
foreach ($topicSharedDocuments as $doc) { ?>
<input type="checkbox" name="shareddocvisible<?=$sharedDocCount?>" 
	<?php if ($topicSharedDocuments[$sharedDocCount-1]->visible) echo "checked"; echo ">" . 
	$topicSharedDocuments[$sharedDocCount-1]->name ?></input>
<input type="hidden" name="tdrecord<?=$sharedDocCount?>" value="<?=$doc->id?>" />
<br/>

<?php $sharedDocCount++;
}?>

</td>
</tr>
</table>
<br/>
These will be private documents, visible only on this version of the topic.  For each, upload a document or enter a URL.
FOR NOW, DON'T USE THIS FEATURE.
</br><br/>
<div id="divdoc1">
<table>
<tr>
<td colspan="2">
<input type="checkbox" name="privatedocvisible1" 
	<?php if ($configDoc1->visible) echo "checked";?>>Private Document 1</input>
</td>
</tr>
<tr>
<td>Description</td>
<td><input type="text" name="doc1text" maxlength="60" size="60" value="<?=$configDoc1->name ?>"/></td>
</tr>
<tr>
<td>Document</td>
<td>
<?php 

	if (empty($configDoc1->file))
		echo "No document file chosen";
	else {
		echo $configDoc1->file;
	}
	
	?>
	<br/>
 <input type="hidden" name="doc1" value="" />
 Choose a new file: <input name="mydoc1file" type="file" /><br />

</td>
</tr>
<tr>
<td>URL</td>
<td><input type="text" name="documentURL1" maxlen="100" size="100" value="<?php echo $configDoc1->url ?>" />
</td>
</tr>
</table>
</div>
<div id="divdoc2">
<table>
<tr>
<td colspan="2">
<input type="checkbox" name="privatedocvisible2" 
	<?php if ($configDoc2->visible) echo "checked";?>>Private Document 2</input>
</td>
</tr>
<tr>
<td>Description</td>
<td><input type="text" name="doc2text" maxlength="60" size="60"  value="<?=$configDoc2->name ?>"/></td>
</tr>
<tr>
<td>Document</td>
<td>
<?php 

	if (empty($configDoc2->file))
		echo "No document file chosen";
	else {
		echo $configDoc2->file;
	}
	
	?>
	<br/>
 <input type="hidden" name="doc2" value="" />
 Choose a new file: <input name="mydoc2file" type="file" /><br />
</td>
</tr>
<tr>
<td>URL</td>
<td><input type="text" name="documentURL2" maxlen="100" size="100" value="<?php echo $configDoc2->url ?>" />
</td>
</tr>
</table>
</div>
<div id="divdoc3">
<table>
<tr>
<td colspan="2">
<input type="checkbox" name="privatedocvisible3" 
	<?php if ($configDoc3->visible) echo "checked";?>>Private Document 3</input>
</td>
</tr>
<tr>
<td>Description</td>
<td><input type="text" name="doc3text" maxlength="60" size="60" value="<?=$configDoc3->name ?>"/></td>
</tr>
<tr>
<td>Document</td>
<td>
<?php 

	if (empty($configDoc3->file))
		echo "No document file chosen";
	else {
		echo $configDoc3->file;
	}
	
	?>
	<br/>
 <input type="hidden" name="doc3" value="" />
 Choose a new file: <input name="mydoc3file" type="file" /><br />

</td>
</tr>
<tr>
<td>URL</td>
<td><input type="text" name="documentURL3" maxlen="100" size="100" value="<?php echo $configDoc3->url ?>" />
</td>
</tr>
</table>
</div>
<center>
<input type="button" value="Close" onclick="closeWin()" /> 
<input type="submit" name="Save" value="Save" /> 
</center>
</form>
</body>
</html>