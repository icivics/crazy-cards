<?php
require_once('auth.php');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>Media Picker</title>

    <!-- Standard datatables-editor includes -->
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.1/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/css/editor.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="../Datatables-Editor-1.6.5/resources/demo.css">
    <style type="text/css" class="init">
    </style>


    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
    </script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js">
    </script>
    <!--<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/rowreorder/1.2.1/js/dataTables.rowReorder.min.js">
    </script>
    -->
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/js/RowReorder-1.2.3/js/dataTables.rowReorder.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/js/dataTables.editor.min.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/syntax/shCore.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/demo.js">
    </script>
    <script type="text/javascript" language="javascript" src="../Datatables-Editor-1.6.5/resources/editor-demo.js">
    </script>

    <!-- End of standard datatables-editor includes -->


    <script type="text/javascript" language="javascript" class="init">

        var editor; // use a global for the submit and return data rendering in the examples

        $(document).ready(function() {
            editor = new $.fn.dataTable.Editor( {
                ajax:  '<?=content_url()?>/php/pageservice/media-picker-grid-data.php',
                table: '#mediaPicker',
                fields: [ {
                    label: 'Title:',
                    name:  'title'
                }, {
                    label: 'Artist:',
                    name:  'artist'
                }
                ]
            } );

            var mediaPickerTable = $('#mediaPicker').DataTable( {
                dom: 'Bfrtip',
                ajax:  '<?=content_url()?>/php/pageservice/media-picker-grid-data.php',
                columns: [
                    {
                        "data": null,
                        "defaultContent": "<button>Select</button>"
                    },
                    { data: 'title' },
                    { data: 'artist' }
                ],
                select: true,
                buttons: [

                ]
            } );
            $('#mediaPicker tbody').on( 'click', 'button', function () {
                var data = mediaPickerTable.row( $(this).parents('tr') ).data();
                window.opener.addNewSong(data.id);
            } );
        } );



    </script>
  </head>
  <body class="dt-example">
     <div class="container">
        <section>
    <div class="demo-html"></div>
        <table id="mediaPicker" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th></th>
                <th>Title</th>
                <th>Artist</th>
            </tr>
        </thead>
        </table>

        </section>
     </div>
  </body>
</html>