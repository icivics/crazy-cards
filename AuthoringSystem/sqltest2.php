<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 9/25/2018
 * Time: 4:13 PM
 */

define('DBHOST', 'localhost');
define('DBUSER', 'mattieoz');
define('DBPASSWORD', 'B0ze123)+');
define('DBSCHEMA', 'top100b_datastore');

class Tile {
    public $id;
    public $page;
    public $title;
    public $description;
    public $destination;
    public $image;
    public $sequence;
}

function getMySqliDbConnection() {
    $con = mysqli_connect(DBHOST, DBUSER, DBPASSWORD, DBSCHEMA);
    if (mysqli_connect_errno($con)) {
        die('Could not connect: ' . mysqli_connect_error());
    }
    return $con;
}

function mySqli_query_wrapper($con, $sql, $message) {
    $result = mysqli_query ($con, $sql );
    if ($result == false) {
        echo "<br/>" . $message . "<br/> --- Result = " . $result . "<br/>error: " . mysql_error ();
        echo "<br/>SQL is " . $sql;
        exit ();
    }
    return $result;
}

$page = "top100";
$con = getMySqliDbConnection();
$sql = "select id, page, title, description, destination, image, sequence from tile where page='" . $page . "' order by sequence";
$result = mySqli_query_wrapper($con, $sql, "Error fetching tiles");
$tiles = array();
while ($row = mysqli_fetch_assoc($result)) {
    $tile = new Tile();
    $tile->id = $row['id'];
    $tile->page = $row['page'];
    $tile->title = $row['title'];
    $tile->description = $row['description'];
    $tile->destination = $row['destination'];
    $tile->image = $row['image'];
    $tile->sequence = $row['sequence'];
    array_push($tiles, $tile);
}
mysqli_close($con);

echo "<table>";
foreach ($tiles as $tile) {
    echo "<tr>";
    echo "<td>" . $tile->id . "</td>";
    echo "<td>" . $tile->page . "</td>";
    echo "<td>" . $tile->title . "</td>";
    echo "<td>" . $tile->description . "</td>";
    echo "</tr>";
}
echo "</table>";

?>