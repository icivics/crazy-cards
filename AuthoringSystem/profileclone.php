<?php 
require_once('auth.php');
require_once '../wp-content/php/dbdefs.php';
require_once '../wp-content/php/profile.class.php';
require_once '../wp-content/php/categorybean.class.php';
require_once '../wp-content/php/categorytopicbean.class.php';
require_once '../wp-content/php/topicconfigurationbean.class.php';
require_once '../wp-content/php/topicconfigurationdocument.class.php';
require_once '../wp-content/php/tcmetabean.class.php';
require_once '../wp-content/php/util.php';

function insertTcBean($tcBean) {
	$sql = "Insert into topic_configuration (topic_id, profile_id) values (" . $tcBean->topicId . 
		", " . $tcBean->profileId . ")";
	return insertRow($sql);
}

function insertTcDoc($tcDoc) {
	$sql = "Insert into topic_configuration_document (topic_configuration_id, topic_document_id, visible) " .
			"values (" . $tcDoc->topicConfigurationId . ", " . $tcDoc->topicDocumentId . ", " . $tcDoc->visible . ")";
	//echo "<br/>Ready to nsert sql is: " . $sql;
	return insertRow($sql);
}

function insertTcMeta($tcMetaBean) {
	$sql = "Insert into topic_configuration_meta (topic_configuration_id, meta_key, meta_value) " .
			"values (" . $tcMetaBean->topicConfigurationId . ", '" . $tcMetaBean->metaKey . "', '" . $tcMetaBean->metaValue . "')";
	//echo "<br/>Ready to nsert sql is: " . $sql;
	return insertRow($sql);
}

function insertCategoryBean($categoryBean) {
	$sql = "Insert into category (profile_id, sequence, name, breadcrumb, usetimeline, timelinestart, timelineend) " .
		"values (" . $categoryBean->profileId . ", " . $categoryBean->sequence . ", '" .
		$categoryBean->categoryName . "', '" . $categoryBean->categoryBreadcrumb . "', '" .
		$categoryBean->useTimeline . "', '" . $categoryBean->timelineStart . "', '" .
		$categoryBean->timelineEnd . "')";
	return insertRow($sql);
}

function insertCategoryTopic($ctBean) {
	$sql = "Insert into categorytopic (category_id, topic_configuration_id, sequence) " .
		"values (" . $ctBean->categoryId . ", " . $ctBean->topicConfigurationId . ", " . $ctBean->sequence . ")";
	return insertRow($sql);
}

function insertRow($sql) {
	//echo "<br/>Insert sql is: " . $sql;
	$result = mysql_query($sql);
	if ($result != 1) {
		echo "Error inserting selection --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
		exit();
	}
	$newId = mysql_insert_id();
	return $newId;
}

function createInClauseForTcConfigs($tcConfigsArray) {
	$oldIds = array();
	foreach ($tcConfigsArray as $tcConfig) {
		array_push($oldIds, $tcConfig->oldId);
	}
	$inclause = "IN (" . rtrim(implode(',', $oldIds), ',') . ")";
	return $inclause;
}

function createInClauseForCategories($categoriesArray) {
	$oldIds = array();
	foreach ($categoriesArray as $category) {
		array_push($oldIds, $category->oldId);
	}
	$inclause = "IN (" . rtrim(implode(',', $oldIds), ',') . ")";
	return $inclause;
}

function lookupNewConfigId($tcConfigsArray, $oldId) {
	foreach ($tcConfigsArray as $tcBean) {
		if ($tcBean->oldId == $oldId) {
			return $tcBean->newId;
		}
	}
	return -1;
}

function lookupNewCategoryId($categoryArray, $oldId) {
	foreach ($categoryArray as $categoryBean) {
		if ($categoryBean->oldId == $oldId) {
			return $categoryBean->newId;
		}
	}
	return -1;
}

$topicConfigsForCurrentProfile = array();
$categoriesForCurrentProfile = array();

if (isset($_POST["currentProfile"]))
	$currentProfile = $_POST["currentProfile"];

if ($currentProfile == "")
	$currentProfile = $_GET["currentProfile"];

$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
if (!$con)
{
	die('Could not connect: ' . mysql_error());
}
mysql_select_db(DBSCHEMA, $con);

$sql = "Select * from profile where shortname='" . $currentProfile . "'";
//echo $sql;
$result = mysql_query($sql);
if ($row = mysql_fetch_array($result))
{
	$oldProfile = new Profile();
	$oldProfile->id = $row['id'];
	$oldProfile->shortName = $row['shortname'];
	$oldProfile->owner = $row['owner'];
	$oldProfile->name1 = $row['name1'];
	$oldProfile->name2 = $row['name2'];
	$oldProfile->welcomeMsg = convertUtf8($row['welcomemessage']);
	$oldProfile->featuredTopic = $row['featuredtopic'];
	$oldProfile->teacherLogin = $row['teacherlogin'];
} else {
	die("Error loading old profile");
}

if(isset($_POST["submitToDatabase"]))
{
	$profile = new Profile();
	$profile->shortName = $_POST["shortname"];
	$profile->owner = $_POST["owner"];
	$profile->name1 = $_POST["name1"];
	$profile->name2 = $_POST["name2"];
	$profile->welcomeMsg = $oldProfile->welcomeMsg;
	$profile->featuredTopic = $oldProfile->featuredTopic;
	$profile->teacherLogin = $oldProfile->teacherLogin;
	$sql = "Insert into profile (shortname, owner, name1, name2, welcomemessage, featuredtopic, teacherlogin) values('" .
				$profile->shortName . "', '" . $profile->owner . "', '" .  
				my_escape($profile->name1, $unmagic) . "', '" . 
				my_escape($profile->name2, $unmagic) . "', '" . 
				my_escape($profile->welcomeMsg, $unmagic) . "', '" . 
				$profile->featuredTopic . "', '" . $profile->teacherLogin . "')";
	$result = mysql_query($sql);
	if ($result != 1)
	{
		echo "Error inserting selection --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
		exit();
	}
	$newProfileId = mysql_insert_id();
	
	$sql = "Select * from topic_configuration where profile_id='" . $oldProfile->id . "'";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$tcBean = new TopicConfigurationBean();
		$tcBean->oldId = $row['id'];
		$tcBean->topicId = $row['topic_id'];
		$tcBean->profileId = $newProfileId;
		$newTcId = insertTcBean($tcBean);
		$tcBean->newId = $newTcId;
		array_push($topicConfigsForCurrentProfile, $tcBean);
	}
	
	$inclause = createInClauseForTcConfigs($topicConfigsForCurrentProfile);
	$sql = "Select * from topic_configuration_document where topic_configuration_id " . $inclause;
	echo "<br/>sql for topicDocument: " . $sql;
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$tcDoc = new TopicConfigurationDocument();
		$tcDoc->id = $row['id'];
		$tcDoc->topicConfigurationId = $row['topic_configuration_id'];
		$tcDoc->topicDocumentId = $row['topic_document_id'];
		$tcDoc->visible = $row['visible'];
		// lookup the new id for the clone
		$tcDoc->topicConfigurationId = lookupNewConfigId($topicConfigsForCurrentProfile, $tcDoc->topicConfigurationId);
		
		insertTcDoc($tcDoc);
	}
	$sql = "Select * from topic_configuration_meta where topic_configuration_id " . $inclause;
	echo "<br/>sql for topic meta: " . $sql;
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$tcMeta = new TcMetaBean();
		$tcMeta->id = $row['id'];
		$tcMeta->topicConfigurationId = $row['topic_configuration_id'];
		$tcMeta->metaKey = $row['meta_key'];
		$tcMeta->metaValue = $row['meta_value'];
		// lookup the new id for the clone
		$tcMeta->topicConfigurationId = lookupNewConfigId($topicConfigsForCurrentProfile, $tcMeta->topicConfigurationId);
		
		insertTcMeta($tcMeta);
	}
	
	$sql = "Select * from category where profile_id = '" . $oldProfile->id . "'";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$categoryBean = new CategoryBean();
		$categoryBean->oldId = $row['id'];
		$categoryBean->profileId = $newProfileId;
		$categoryBean->sequence = $row['sequence'];
		$categoryBean->categoryName = $row['name'];
		$categoryBean->categoryBreadcrumb = $row['breadcrumb'];
		$categoryBean->useTimeline = $row['usetimeline'];
		$categoryBean->timelineStart = $row['timelinestart'];
		$categoryBean->timelineEnd = $row['timelineend'];
		$newCategoryId = insertCategoryBean($categoryBean);
		$categoryBean->newId = $newCategoryId;
		array_push($categoriesForCurrentProfile, $categoryBean);
	}
	$inclause = createInClauseForCategories($categoriesForCurrentProfile);
	$sql = "Select * from categorytopic where category_id " . $inclause;
	echo "<br/>sql for categorytopic: " . $sql;
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
		$ctBean = new CategoryTopicBean();
		$ctBean->id = $row['id'];
		$ctBean->categoryId = $row['category_id'];
		$ctBean->topicConfigurationId = $row['topic_configuration_id'];
		$ctBean->sequence = $row['sequence'];
		// lookup the new id for the clone
		$ctBean->topicConfigurationId = lookupNewConfigId($topicConfigsForCurrentProfile, $ctBean->topicConfigurationId);
		$ctBean->categoryId = lookupNewCategoryId($categoriesForCurrentProfile, $ctBean->categoryId);
	
		insertCategoryTopic($ctBean);
	}
	
	
	echo "<br/>Clone completed";
}
mysql_close($con);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profiles</title>

<script type="text/javascript">
 </script>

<body>
<br/><br/>Cloning <?=$currentProfile?>
<form action="profileclone.php" enctype="multipart/form-data" method="post" name="profileedit"><br/><br/>
<table>
<tr>
<td>Short Name</td>
<td><input type="text" name="shortname" value="" maxlength="30" size="30" /></td>
</tr>
<tr>
<td>Owner</td>
<td><input type="text" name="owner" value="<?=$oldProfile->owner?>" maxlength="10" size="10" /></td>
</tr>
<tr>
<td>Name 1</td>
<td><input type="text" name="name1" value="<?=$oldProfile->name1?>" maxlength="30" size="30" /></td>
</tr>
<tr>
<td>Name 2</td>
<td><input type="text" name="name2" value="<?=$oldProfile->name2?>" maxlength="30" size="30" /></td>
</tr>
</table>
<input type="hidden" name="submitToDatabase" value="1" />
<input type="hidden" name="currentProfile" value="<?=$currentProfile?>" />
<input type="submit" name="Submit" value="Submit" />
<input type="button" value="Go Back" onClick="location.href='profilelist.php?currentProfile=<?=$currentProfile?>'" />
</form>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>
</body>