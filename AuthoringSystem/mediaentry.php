<?php
require_once('auth.php');
require_once '../wp-content/php/db/dbdefs.php';
require_once '../wp-content/php/util/mysqliutil.php';
require_once '../wp-content/php/model/mediaentrybean.php';
require_once '../wp-content/php/model/mediacategory.class.php';
require_once '../wp-content/php/dao/MediaCategoryDao.php';
require_once '../wp-content/php/dao/MediaItemDao.php';
require_once '../wp-content/php/model/video.class.php';

$editRecord = -1;
if (isset($_POST["editrecord"]))
{
    $editRecord = $_POST["editrecord"];
    $id = $editRecord;
}
$startingCategory = $_GET["category"];
if (empty($startingCategory)) {
    $startingCategory = $_POST["categories"];
}
if (empty($startingCategory)) {
    $startingCategory = "Music";
}
$forceAddNewSong = "0";
if (isset($_POST["addnew"])) {
    $forceAddNewSong = $_POST["addnew"];
}
$con = getMySqliDbConnection();
$mediaItemDao = new MediaItemDao($con);
$mediaCategoryDao = new MediaCategoryDao($con);

$mediaCategories = $mediaCategoryDao->getAllMediaCategories();

$submitToDatabase = $_POST["submitToDatabase"];
$videos = array();
if(isset($submitToDatabase) && $submitToDatabase == "1") {
    $dbMedia = new MediaEntryBean();
    $dbMedia->id = $editRecord;
    $dbMedia->title = $_POST['title'];
    $dbMedia->artist = $_POST['artist'];
    $dbMedia->album = $_POST['album'];
    $dbMedia->genre = $_POST['genre'];
    $dbMedia->released = $_POST['released'];
    $dbMedia->tvShow = $_POST['tv_show'];
    $dbMedia->movie = $_POST['movie'];
    $dbMedia->event = $_POST['event'];
    $dbMedia->product = $_POST['product'];
    $dbMedia->subject = $_POST['subject'];

    gatherMediaCategories($dbMedia, $mediaCategories, $mediaCategoryDao);
    $mediaItemDao->saveMediaItem($dbMedia);

    $done = false;
    $videoIndex = 0;
    while (!$done) {
        if (isset($_POST["videourl" . strval($videoIndex)])) {
            $video = new Video();
            $video->url = $_POST["videourl" . $videoIndex];
            $video->videoId = $_POST["videocode" . $videoIndex];
            $video->breadcrumb = $_POST["videonotes" . $videoIndex];
            $video->category = 'MUSIC';
            array_push($videos, $video);
            $videoIndex++;
        } else {
            $done = true;
        }
    }
    foreach ($videos as $video) {
        $video = $mediaItemDao->findAndSaveIfNecessary($video);
    }
    $mediaItemDao->updateVideoXrefs($dbMedia, $videos);
    $id = $dbMedia->id;
}

$invalidMessage = "";
if (empty($id)) {
    $id = $_GET["id"];
}
if ($forceAddNewSong == 1) {
    $id = null;
}
if (!empty($id)) {
    $mediaEntryBean = $mediaItemDao->getMediaInfoById($id);
    if ($mediaEntryBean == null) {
        $invalidMessage = "No song found with id of " . $id;
    }
    $startingCategory = getPrimaryMediaCategoryName($mediaEntryBean);
} else {
    $mediaEntryBean = new MediaEntryBean();
    unset($videos);
    $videos = array();
    $mediaCategory = $mediaCategoryDao->findMediaCategoryByName($mediaCategories, $startingCategory);
    array_push($mediaEntryBean->arrMediaCategories, $mediaCategory);
}
$arrDivs = array();
foreach ($mediaCategories as $mediaCategory) {
    foreach ($mediaCategory->arrFields as $field) {
        if (!in_array($field, $arrDivs)) {
            $arrDivs[$field] = mapMediaEntryValue($mediaEntryBean, $field);
            //array_push($arrDivs, $field);
        }
    }
}

$con->close();

function mapMediaEntryValue($mediaEntryBean, $field) {
    switch ($field) {
        case "title":
            return $mediaEntryBean->title;
        case "artist":
            return $mediaEntryBean->artist;
        case "album":
            return $mediaEntryBean->album;
        case "genre":
            return $mediaEntryBean->genre;
        case "released":
            return $mediaEntryBean->released;
        case "event":
            return $mediaEntryBean->event;
        case "tv_show":
            return $mediaEntryBean->tvShow;
        case "movie":
            return $mediaEntryBean->movie;
        case "product":
            return $mediaEntryBean->product;
        case "subject":
            return $mediaEntryBean->subject;
        default:
            return "";
    }
}

function lookupMediaCategory($categoryId, $arrMediaCategories) {
    foreach ($arrMediaCategories as $mediaCategory) {
        if ($mediaCategory->id == $categoryId) {
            return $mediaCategory;
        }
    }
    return null;
}

function gatherMediaCategories($dbMediaItem, $mediaCategories, $mediaCategoryDao) {
    $maxCategories = 20;
    $dbMediaItem->arrMediaCategories = array();
    $categoryName = $_POST['categories'];
    $mediaCategory = $mediaCategoryDao->findMediaCategoryByName($mediaCategories, $categoryName);
    if ($mediaCategory != null) {
        array_push($dbMediaItem->arrMediaCategories, $mediaCategory);
    }
}

function checkedValue($category, $mediaEntryBean) {
    foreach ($mediaEntryBean->arrMediaCategories as $mediaCategory) {
        if ($mediaCategory->id == $category->id) {
            return "CHECKED";
        }
    }
    return "";
}

function getPrimaryMediaCategoryName($mediaEntryBean) {
    $mediaCategory = $mediaEntryBean->arrMediaCategories[0];
    return $mediaCategory->name;
}
?>
<head>
    <title>Authoring Song Entry</title>
    <script type="text/javascript" language="javascript" src="js/videomgt.js"></script>

    <script language="Javascript">

    var categoryFields = [];

    window.onload = function() {
        var startingNumberOfVideos = <?=count($videos)?>;
        var category= "<?=$startingCategory?>";
        for (i=0;i<startingNumberOfVideos;i++) {
            videoDivElement = document.getElementById("videoentry" + i.toString());
            videoPlayerElement = findChildElement(videoDivElement, "expandedvideoplayer" )
            if (videoPlayerElement != null) {
                videoPlayerElement.className = "invisible";
            }
        }
        loadUpFields();
        showElementsForCategory(category);
    }

    function loadUpFields() {
        var count = 0;
        <?php
            foreach ($mediaCategories as $mediaCategory) {
                foreach ($mediaCategory->arrFields as $field) {
                ?>
                    categoryFields[count] = {};
                    categoryFields[count].category = "<?=$mediaCategory->name ?>";
                    categoryFields[count].field = "<?=$field?>";
                    count++;
                <?php
                }
            }
        ?>
    }

    function getFieldsForCategory(category) {
        fields = [];
        count = 0;
        for (i=0;i<categoryFields.length;i++) {
            if (categoryFields[i].category == category) {
                fields[count++] = categoryFields[i].field;
            }
        }
        return fields;
    }

    function newCategory(element) {
        category = document.querySelector('input[name = "categories"]:checked').value;
        showElementsForCategory(category);
    }

    function showElementsForCategory(category) {
        fields = getFieldsForCategory(category);
        var allDivCategories = document.getElementsByName("divcategory");
        for (i = 0; i < allDivCategories.length; i++) {
            divelement = allDivCategories[i];
            tag = divelement.id.substring(4);
            if (fields.includes(tag)) {
                divelement.className = 'visible';
            } else {
                divelement.className = 'invisible';
            }
        }
        //alert("current category: " + category);
    }

    function checkAndSubmit() {
        var done = false;
        var count = 1;
        var categoriesOk = false;
        while (!done) {
            element = document.getElementById("category" + count);
            if (element == null) {
                done = true;
            } else if (element.checked == true) {
                categoriesOk = true;
                done = true;
            }
            count++;
        }
        if (categoriesOk == false) {
            alert("You must select at least 1 category.");
            return false;
        } else {
            theForm = document.getElementById("mediaentry");
            return doSubmit(theForm);
        }
    }

    function backToMediaList() {
        category = document.querySelector('input[name = "categories"]:checked').value;
        location.href = "media-list.php?category=" + category;

    }

</script>

    <link rel="stylesheet" type="text/css" href="css/authoring.css">
</head>

<body>
<?php
if (!empty($invalidMessage)) {
    echo $invalidMessage;
} else {
    ?>
<form action="mediaentry.php" enctype="multipart/form-data" method="post" id="mediaentry" name="mediaentry"><br/><br/>
<input type="hidden" id="addnew" name="addnew" value="0";
<input type="hidden" name="magicquotecheck" value="'" />
<input type="hidden" name="editrecord" value="<?=$id?>" />
<input type="hidden" name="submitToDatabase" id="submitToDatabase" value="1" />
<?php
if (empty($id)) {
    echo "<b>NEW MEDIA ITEM</b>";
} else {
    echo "<b>MEDIA ID #" . $id . "</b>";
}
?>
    <br/><br/>
<table>
<tr>
    <td valign="top">Categories</td>
    <td>
    <?php
        $count = 1;
        foreach ($mediaCategories as $category):
            echo '<input type="radio" id="category' . $count . '" name="categories" value="' . $category->name . '" ' . checkedValue($category, $mediaEntryBean) .
                ' onclick="javascript:newCategory(this);" >' . $category->name . ' </input>';
            $count++;
        endforeach; ?>
    <br/><br/>
    </td>
</tr>
</table>

    <?php
    foreach ($arrDivs as $key => $value) {
        echo "<div id='div_" . $key . "' name='divcategory'>";
        echo "<table cellpadding='5'>";
        echo "<tr>";
        echo "<td width='75' align='right' >" . $key . "</td>";
        echo '<td><input type="text" name="' . $key . '" value="' . $value . '" maxlength="100" size="100" /></td>';
        echo "</tr>";
        echo "</table>";
        echo "</div>";
    }
    ?>
</table>
<br/>
Vidoes (<?=count($mediaEntryBean->arrVideos)?>)&nbsp;&nbsp;
    <input type="button" name="AddNewVideo" value="Add New" onclick="javascript:newVideo()" />
    <div id="videotable">
    <table>
        <tr>
            <td>
    <?php
    $maxVideos = 10;
    $numberOfVideos = count($mediaEntryBean->arrVideos);
    for ($i=0;$i<$maxVideos;$i++) {
        if ($i < $numberOfVideos) {
            $video = $mediaEntryBean->arrVideos[$i];
            $url = $video->url;
            $notes = $video->breadcrumb;
        } else {
            $video = null;
            $url = "";
            $notes = "";
        }
    ?>
    <div id="videoentry<?=$i?>" name="videoentrydiv" class="<?=($video == null ? 'invisible' : 'visible')?>">
        <table>
        <tr>
            <td><button style="background-color:transparent; border-color:transparent;" onclick="javascript:deleteVideo(<?=$i?>)">
                    <img src="images/x-button.png" border="0" />
                </button>
            </td>
            <td><input type='button' id="expandvideotogglebutton" name='expandVideoToggleButton' value='+' onclick="javascript:expandVideo(<?=$i?>)"/></td>
            <td><label for="videoURL">Video URL:</label></td>
            <td><input type="text" id="videoURL" name="videoURL" value="<?=$url?>" maxlength="80" size="80"/></td>
            <td><img src="images/checkmark-xxl.png" border="0" </td>
        </tr>
        <tr>
            <td colspan="3" align="right"><label for="videoNotes">Video Notes:</label></td>
            <td colspan="2"><textarea id="videoNotes" rows="4" cols="100"><?=$notes?></textarea></td>
        </tr>
        <div id="expandedvideo" name="expandedvideo" class="invisible">
            <table>
                <tr>
                    <td id="expandedvideoplayer" name="expandedvideoplayer" colspan="3">
                        &nbsp;
                    </td>
                <tr>
                    <td id="expandedvideoimg_1" align="center">
                       &nbsp;
                    </td>
                    <td id="expandedvideoimg_2" align="center">
                        &nbsp;
                    </td>
                    <td id="expandedvideoimg_3" align="center">
                        &nbsp;
                    </td>
                </tr>

                </tr>
            </table>
        </div>
        </table>
    </div>
<?php
    }
    ?>
</td>
</tr>
</table>
</div>
<br/><br/>
<input type="button" name="Submit" value="Submit" onclick="javascript:checkAndSubmit()" />
<br/><br/>
<input type="button" name="AddNew" value="Create New Media Item" onclick="javascript:addNew()" />

</form>
<?php
}

?>
<br/><br/>
<a href="javascript:backToMediaList();">Media List</a><br/><br/>
<br/><br/>
<a href="authoringmenu.php">Main Menu</a><br/><br/>

</body>
</html>
