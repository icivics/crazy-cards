<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'card_game_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'b1tByb1tByt3ByByt3' );

/** MySQL hostname */
define( 'DB_HOST', 'analytics.ckm2zc2drvhd.us-east-1.rds.amazonaws.com:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'e68a00a65380b83cf2cdb53e63d09938017f0975f5f927a3fd13548ad8d84066');
define('SECURE_AUTH_KEY', '3608b42e73f939105e989f25e9c582e28f6898c0d024fb5c007b5f1272bbd64c');
define('LOGGED_IN_KEY', 'a2d1f87c40b63d71f0079982d56dc342d14b5b99ce176e0ac3382934fdc67b96');
define('NONCE_KEY', '7d65e0a1a571f853accfe80b6f9828e1a9c755aadc10c1478d8989a1511b360a');
define('AUTH_SALT', 'f0fc98c3f1f75eb97cee72db050fed757f4837bb09e5b6958d83ce68681c8483');
define('SECURE_AUTH_SALT', '2a229a9041902623c86f279817efe1de4c29a933ca597d872d7a2a14fb355c02');
define('LOGGED_IN_SALT', 'add3541f72c9c4142607b46c944ecb9ce0b0ba94fc9790f83de103b108e78974');
define('NONCE_SALT', '47f47060c10648737e612650be0e9307d14ba5b1cfb01be9deea98165e690dd6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

define('FS_METHOD', 'direct');

/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','http://example.com');
 *  define('WP_SITEURL','http://example.com');
 *
*/

if ( defined( 'WP_CLI' ) ) {
    $_SERVER['HTTP_HOST'] = 'localhost';
}

define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/');
define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/');


/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

define('WP_TEMP_DIR', '/opt/bitnami/apps/wordpress/tmp');


//  Disable pingback.ping xmlrpc method to prevent Wordpress from participating in DDoS attacks
//  More info at: https://docs.bitnami.com/general/apps/wordpress/troubleshooting/xmlrpc-and-pingback/

if ( !defined( 'WP_CLI' ) ) {
    // remove x-pingback HTTP header
    add_filter('wp_headers', function($headers) {
        unset($headers['X-Pingback']);
        return $headers;
    });
    // disable pingbacks
    add_filter( 'xmlrpc_methods', function( $methods ) {
            unset( $methods['pingback.ping'] );
            return $methods;
    });
    add_filter( 'auto_update_translation', '__return_false' );
}


/***** CUSTOM *******/

if ( !defined('ABSURL'))

    define('ABSURL', 'http://54.226.201.211/');

if ( !defined('PERMALINKBASE'))
    define('PERMALINKBASE', 'http://54.226.201.211/index.php/');

if (! defined('MORALCOMPASSURL'))
    define('MORALCOMPASSURL', 'http://my-moral-compass.com/');

define( 'WP_DEBUG', true );