<?php 
require_once(ABSPATH . 'wp-content/php/standardincludes.php');
$con = getMySqliDbConnection();

// initialize all of state managed variables
$myTitle = get_the_title();

if (PRODUCT == 'PoliticalFootball' || PRODUCT == 'Top100') {
	mmc_initCurrentStateAndProfile(PRODUCT);
	$profileName = mmc_getCurrentState()->getProfileName();
} else {
	mmc_initCurrentState();
	$profileName = mmc_getCurrentState()->getProfileName();
}

insertPageView($con, get_the_title(), "PAGE", "PAGES", $profileName, mmc_getCurrentState()->getClassroomCode());

mysqli_close($con);
?>

<?php get_header() ?>

<div class="container1 clearfix">
    <div class="content left">
        <?php if (have_posts()) while (have_posts()) : the_post(); ?>
            <article class=article-content>

                <div class=article-text>
                    <?php
                    echo "Page events here";
                    the_content();
                    ?>
                </div>

            </article>
        <?php endwhile; ?>

    </div>

</div>
<?php get_footer() ?>