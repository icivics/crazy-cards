<?php 
require_once(ABSPATH . 'wp-content/php/pageservice/corecontributorform.php');

?>

<?php get_header() ?>

<div class="container1 clearfix">

    <div class=clearfix>
        <h6 class="head-title left">The Final Countdown</h6>
    </div>
        <section class=top-content>
     
            <article id="<?php the_ID() ?>" class="home-article serif">
                <?php
                	$post = get_post();
                	$welcomeMsg = $post->post_content;
					$welcomeMsg = str_replace("@BLOGINFO", get_bloginfo('url'), $welcomeMsg);
					$welcomeMsg = str_replace("@SITEURL", ABSURL, $welcomeMsg);
					$welcomeMsg = str_replace("@SONGLISTGAME", $htmlForSongTeasers, $welcomeMsg);
					$welcomeMsg = str_replace("@AFTERTOP10TEASERS", $htmlForAfterTop10Teasers, $welcomeMsg);
					$welcomeMsg = str_replace("@CURRENTSONGLISTLINK", $htmlForCurrentSongGroupingLink, $welcomeMsg);
					$welcomeMsg = str_replace("@VISIBLESONGLISTLINKS", $htmlForNonCurrentVisibleSongGroupingLinks, $welcomeMsg);
					$welcomeMsg = str_replace("@INVISIBLESONGLISTLINKS", $htmlForNonCurrentNonVisibleSongGroupingLinks, $welcomeMsg);
					$welcomeMsg = str_replace("@ALLSONGLISTLINK", $htmlForAllSongListLink, $welcomeMsg);
                	echo $welcomeMsg;
                ?>
             </article>
        </section>
  <div id="modalsignup">
	<form action="<?=get_bloginfo('url')?>/signup-thank-you" method="post" name="signup">
		<input type="hidden" name="magicquotecheck" value="'" />
		<div id="options">
			<table cellpadding="2">
			<tr>
			<td align="right">
			<label for="emailaddress">Email Address:</label>
			</td>
			<td>
			<input type="text" name="email"  value="<?=$existingMember->emailAddress?>" maxlength="45" size="52" />
			</td>
            </tr>
            <tr>
            <td colspan="2">
                <a href="javascript:lookupEmail('email')">Look Me Up - Enter your email address if you've been here before, and you can add to your existing list.</a>
            </td>
			</tr>
			<tr>
			<td align="right" width=140>
			<label for="firstname">First Name:</label>
			</td>
			<td>
			<input type="text" name="firstname" value="<?=$existingMember->firstName?>" maxlength="45" size="52" />
			</td>
			</tr>
			<tr>
			<td align="right">
			<label for="lastname">Last Name:</label>
			</td>
			<td>
			<input type="text" name="lastname" value="<?=$existingMember->lastName?>" maxlength="45" size="52" />
			</td>
			</tr>
			<?php if ($showNotFoundMessage): ?>
			<tr>
			<td colspan=2>
			Email address <b><?=$existingContributor?></b> is not found.
			</td>
			</tr>
			<?php endif; ?>
			</table>
			<hr/>
			<?php 
			$numberOfSongs = count($existingSongs);
			$maxSongs = 20;
			for ($i=0;$i<$maxSongs;$i++) {
				if ($i < $numberOfSongs) {
					$songContribution = $existingSongs[$i];
				} else {
					$songContribution = null;
				}
			?>
				<div id="songentry<?=$i?>" class="<?=($songContribution != null ? 'visible' : 'hidden')?>">
				<table>
				<tr>
				<td align="right">
				<label for="songtitle<?=$i?>">Song Title:</label>
				</td>
				<td>
				<input type="text" name="songtitle<?=$i?>" value="<?=($songContribution != null ? $songContribution->title : '')?>" maxlength="45" size="52" />
				</td>
				</tr>
				<tr>
				<td align="right">
				<label for="songartist<?=$i?>">Song Artist:</label>
				</td>
				<td>
				<input type="text" name="songartist<?=$i?>" value="<?=($songContribution != null ? $songContribution->artist : '')?>" maxlength="45" size="52" />
				</td>
				</tr>
				<tr>
				<td align="right">
				<label for="videoURL<?=$i?>">(Optional) Youtube URL:</label>
				</td>
				<td>
				<input type="text" name="videoURL<?=$i?>" value="<?=($songContribution != null ? $songContribution->videoURL : '')?>" maxlength="45" size="52" />
				</td>
				</tr>
				</table>
				<hr/>
				</div>
			<?php } ?>
		</div>
		<br/>
        <div class="btn clearfix" align="right">
            <?php if ($numberOfSongs < $maxSongs): ?>
                <button type="button" class="favorite-songs-button left" onclick="javascript:addNewSong()">Add another</button>&nbsp;&nbsp;&nbsp;
            <?php endif;?>
            <button type="button" class="favorite-songs-submit"  onclick="javascript:validatetop100signup()">Submit</button>&nbsp;
            <button type="button" class="favorite-songs-button"  onclick="javascript:linktotop100mainpage('<?=bloginfo('url')?>')">Cancel</button>
		</div>
        <br/><br/>
	</form>
  </div>
</div>
<?php get_footer() ?>