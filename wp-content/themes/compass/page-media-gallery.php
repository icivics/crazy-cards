<?php 

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include(ABSPATH . 'wp-content/php/pageservice/mediagallery.php');

mmc_getCurrentState()->setUseQuoteOfTheDay(false);
?>

<?php get_header() ?>
<div class="container clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Media Gallery</b></h6>
    </div>
    
    <?php get_sidebar(); ?>
    
    <div>
    <br/>
		<form method='POST' action="">
			Category: <input type="text" id="category" name="categoryEntry" value=""/><br/>
			Key words: <input type="text" id="keywords" name="keywordsEntry" value=""/><br/>
			Bread crumbs: <input type="text" id="breadcrumbs" name="breadcrumbsEntry" value=""/><br/>
			<input type="submit" name="Submit" value="Submit" /><br/><br/>
		</form>    
    </div>
        
        <section class=resources>
            <h6 class=padded-title>RESOURCES (<?php echo $numberOfResources ?>)</h6>

            	<?php
					for ($i=1; $i<=20; $i++)
					{
						if ($i <= $numberOfResources)
							echo "<div align='center' id='divResource" . $i . 
							"' class='hidden'><div align='right'><a href='javascript:setResource(-1)'><img src='" . 
							ABSURL . "wp-content/themes/compass/img/close-button.png'/></a></div><br/>" . $resourceHtml[$i-1] . "</div>";
						else
							echo "<div id='divResource" . $i . "' class='hidden'></div>";
					}
				
				?>
            <div class=media-area-gallery id="divResourceThumbnails">
                <ul class="media-list-gallery clearfix">
                <?php
				$resourceIndex = 0;
				$maxColumns = 4;
				echo "<table>";
				$i = 0;
				while ($i<$numberOfResources) {
					if (($i % $maxColumns) == 0) {
						echo "<tr>";
					}
					$thumbnailIndex = $i + 1;
					$selectionIndex = $i;
					echo "<td width=160>";
					echo "<div id='divThumbnail" . $thumbnailIndex . "' class='visible'>";
					echo "<a href='javascript:setResource(" . $i . ", \"media-area-gallery\")'><img src='" . ABSURL . "wp-content/images/" . 
						$resourceSelections[$selectionIndex]->topicShortName . "/" . $resourceSelections[$selectionIndex]->thumbnailImage . 
						"' width='120' height='90' alt='' /></a>";
					echo "<br/>" . $resourceSelections[$selectionIndex]->breadcrumb;
					echo "<br/><hr>From: <b>" . $resourceSelections[$selectionIndex]->topicShortName . "</b>";
					echo "</div>";
					echo "</td>";
					$i++;
					if (($i % $maxColumns) == 0) {
						echo "</tr>";
					}
				}
				echo "</table>";
				?>
				</ul>
            </div>
        </section>
                
        

</div>

<?php get_footer() ?>