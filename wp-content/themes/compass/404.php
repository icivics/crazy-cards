<?php 
echo "404";
exit();
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/corewheretonext.php');

?>

<?php get_header() ?>
<div class="container1 clearfix">

    <div class=clearfix>
        <h6 class="head-title left">404 Error Not Found</b></h6>
    </div>

    <div class="content left">
    
        <article class=article-text>
	        <h4>Sorry, the page you requested could not be found.<br/>
	        Choose one of the quick links below.</b></h4>
			<br/>
        </article>
        </div>

</div><?php get_footer() ?>