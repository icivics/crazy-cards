<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');
$con = getMySqliDbConnection();

// initialize all of state managed variables
$myTitle = get_the_title();
mmc_initCurrentStateAndProfile(PRODUCT);

insertPageView($con, get_the_title(), "PAGE", "PAGES");

mysqli_close($con);
?>

<?php get_header() ?>

<div class="container1 clearfix">
    <div class="content left">
        <?php if (have_posts()) while (have_posts()) : the_post(); ?>
            <article class=article-content>

                <div class=article-text>
                    <?php
                    the_content();
                    ?>
                </div>

            </article>
        <?php endwhile; ?>

    </div>

</div>
<?php get_footer() ?>