<?php 
require_once(ABSPATH . 'wp-content/php/standardincludes.php');
$con = getDbConnection();

// initialize all of state managed variables
mmc_initCurrentState();

$profileName = mmc_getCurrentState()->getProfileName();

insertPageView(get_the_title(), "PAGE", "PAGES", $profileName, mmc_getCurrentState()->getClassroomCode());

mysql_close($con);
?>

<?php get_header() ?>
<div class="container clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Contact My Moral Compass</b></h6>
    </div>
    
    <?php //get_sidebar(); ?>
    
    <div class="content left">
    
        <article class=article-text>
        <?php if ($returnToPage == "main"): ?>
			<a href="<?php bloginfo('url') ?>">Return to main menu</a>
		<?php else: ?>    	
			<a href="/index.php/<?php echo $returnToPage ?>">Return to <b><?php echo $returnToPageTitle ?></a>
		<?php endif ?>
		<br/><br/>

		<?php 
			echo "Thanks for your message!";
		?>
        </article>
        </div>

</div><?php get_footer() ?>