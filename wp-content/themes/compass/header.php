<?php

global $template;
global $title;
global $secondaryTitle;
global $description;
global $g_profileSubscriber;
global $fullProfile;
global $theBackgroundImage;
global $autoplay;
global $seq;
global $useDataTable;
global $useMatchingGameEngine;
global $updaterURL;
global $gameResultId;
global $category;
global $cheat;
global $runningScoreKept;
global $gameSessionId;
global $gameSession;
global $myMediaCategory;
global $dataTableOutputter;
global $gameCardHolders;
global $arrGameCategoryMetadata;
global $newGame;
global $mediaPage;
global $mediaPickerPage;

function cssClass($tag) {
    global $useMatchingGameEngine;
    if (PRODUCT == 'PoliticalFootball') {
        if ($tag == "site-header") {
            if ($useMatchingGameEngine) {
                return "site-header-matching-game";
            } else {
                return "site-header-political-football";
            }
        }
    } else if (PRODUCT == 'Top100') {
        if ($tag == "site-header") {
            if ($useMatchingGameEngine) {
                return "site-header-matching-game";
            } else {
                return "site-header-education";
            }
        }
        else if ($tag == "logo")
            return "logo-top100";
/*    } else if (PRODUCT == 'CardGames') {
        if ($tag == "site-header") {
            if ($useMatchingGameEngine) {
                return "site-header-matching-game";
            } else {
                return "site-header-education";
            }
        }
        else if ($tag == "logo")
            return "logo-top100";*/
    } else {
        if ($tag == "site-header")
            return "site-header-education";
        else if ($tag == "logo")
            return "logo-education";
    }
}

function hasBackgroundImage() {
}

function backgroundImage() {
	global $theBackgroundImage;
	return $theBackgroundImage;
}

?>

<!DOCTYPE xhtml>
<?php //echo "header time!"?>
<html <?php language_attributes(); ?> class=no-js>
<?php //echo "the journey of a thousand lines of code";?>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--        <link rel=pingback href="--><?php //bloginfo( 'pingback_url' ); ?><!--"/>-->
        <?php //echo "very";?>
        <link rel=profile href="http://gmpg.org/xfn/11"/>
        <meta name=viewport content="width=device-width"/>
        <title>
        <?php
            if ( is_home() || is_front_page() ) {
                echo "front page";
            }
            elseif ( is_search() ) { bloginfo('name'); echo ' | Results for: ' . wp_specialchars($s); }
            elseif ( is_404() ) { bloginfo('name'); echo ' | Not found'; }
//            elseif (mmc_getCurrentState()->isTop100() ) {
//            	echo "Matt's Top 100";
//            	if (!empty($title)) {
//            	    echo " - " . $title;
//                }
//            }
            elseif (empty($title))  { bloginfo('name'); wp_title(' | '); }
            else { bloginfo('name'); echo " | " . trim($title); }
        ?>
        </title>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php if (!empty($description)):
        	echo "<meta name=\"description\" content=\"" . $description . "\"/>";
        	endif ?>
        <?php
            if (is_singular() && get_option('thread_comments'))
                wp_enqueue_script('comment-reply'); 

            wp_head();
        ?>

        <?php //echo "good job. you've started to make your way through the page";?>

        <link rel=stylesheet href="<?php bloginfo('stylesheet_url'); ?>"/>
        <link rel=stylesheet href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <?php if ($autoplay == 1): ?>
        <script type="text/javascript" src="http://www.youtube.com/player_api"></script>
        <?php endif;?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/js/functions.js" type="text/javascript"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/youtubesetup.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

        <!--<script src="<?php //echo get_template_directory_uri(); ?>/js/jquery.modal.js" type="text/javascript"></script>-->
        <!--<script src="/js/jquery.modal.js" type="text/javascript"></script>-->

        <script type="text/javascript">
        	function initNoBackground() {
        	    initResource(1);
                setupVideos();
                //initializeAudioPlayers();
            }

       </script>
        <!--[if lt IE 8]> -->
        <!-- <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script> -->
        <!-- <![endif]-->
    	<?php if ($autoplay == 1):
    		include_once(ABSPATH . 'wp-content/php/youtubejs.php');
    	endif; ?>

        <?php //echo "it's a long journey,"?>

        <?php if ($useMatchingGameEngine == 1): ?>
            <script type="text/javascript" language="JavaScript" class="init">
                var gameResultId;
                var updaterURL;
                var category;
                var cheat;
                var boards;
                var totalPoints;
                var averagePoints;
                var soundOn = 1;
                var gameSessionId;
                var newGame;

                $(document).ready(function() {
                    gameResultId = <?=$gameResultId?>;
                    updaterURL = "<?=$updaterURL?>";
                    category = "<?=$category?>";
                    cheat = "<?=$cheat?>";
                    gameSessionId = "<?=$gameSessionId?>";
                    newGame = <?=$newGame?>;
                    if (gameSessionId != -1) {
                        boards = <?=$gameSession->numberOfCompletedBoards?>;
                        totalPoints = <?=$gameSession->totalPoints?>;
                        averagePoints = <?=$gameSession->averagePoints?>;
                    }
                    valueForSound = readCookie("soundon");
                    soundOn = (valueForSound == null ? 1 : valueForSound);
                });
            </script>
            <script src="<?php echo get_template_directory_uri(); ?>/js/matchinggame.js" type="text/javascript"></script>
            <?php
            $maxCards = 12;
            for ($i=1;$i<=$maxCards;$i++) {
                if ($gameCardHolders[$i-1]->displayMode == 0) {
                    echo "<link rel='preload' href='" . assetPath() . $gameCardHolders[$i-1]->category . "/img-2-1/" .
                        $gameCardHolders[$i - 1]->img . "' as='image' />";
                    echo "<link rel='preload' href='" . assetPath() . $gameCardHolders[$i-1]->category . "/img-4-3/" .
                        $gameCardHolders[$i - 1]->img . "' as='image' />";
                }
            }
            ?>
        <?php endif; ?>

        <?php if ($useDataTable == 1): ?>
            <!-- Standard datatables-editor includes -->
            <!--<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico"> -->
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.1/css/buttons.dataTables.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.1/css/rowReorder.dataTables.min.css">
            <link rel="stylesheet" type="text/css" href="<?=home_url()?>/Datatables-Editor-1.6.5/css/editor.dataTables.min.css">
            <link rel="stylesheet" type="text/css" href="<?=home_url()?>/Datatables-Editor-1.6.5/resources/syntax/shCore.css">
            <link rel="stylesheet" type="text/css" href="<?=home_url()?>/Datatables-Editor-1.6.5/resources/demo.css">
            <style type="text/css" class="init">
            </style>


            <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
            </script>
            <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js">
            </script>
            <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.1/js/dataTables.buttons.min.js">
            </script>
            <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js">
            </script>
            <script type="text/javascript" language="javascript" src="<?=home_url()?>/Datatables-Editor-1.6.5/js/RowReorder-1.2.3/js/dataTables.rowReorder.js">
            </script>
            <script type="text/javascript" language="javascript" src="<?=home_url()?>/Datatables-Editor-1.6.5/js/dataTables.editor.min.js">
            </script>
            <script type="text/javascript" language="javascript" src="<?=home_url()?>/Datatables-Editor-1.6.5/resources/syntax/shCore.js">
            </script>
            <script type="text/javascript" language="javascript" src="<?=home_url()?>/Datatables-Editor-1.6.5/resources/demo.js">
            </script>
            <script type="text/javascript" language="javascript" src="<?=home_url()?>/Datatables-Editor-1.6.5/resources/editor-demo.js">
            </script>

            <!-- End of standard datatables-editor includes -->
            <script type="text/javascript" language="JavaScript" class="init">

                var editor; // use a global for the submit and return data rendering in the examples

                $(document).ready(function() {
                    editor = new $.fn.dataTable.Editor( {
                        ajax:  '<?=content_url()?>/php/pageservice/song-grid-data.php?category=<?=$category?>',
                        table: '#song-grid',
                        <?=$dataTableOutputter->outputTableFields($myMediaCategory);?>
                    } );

                    var songGridTable = $('#song-grid').DataTable( {
                        dom: 'Blfrtip',
                        lengthMenu: [ 10, 25, 50, 75, 100 ],
                        order: [[ 1, "asc" ]],
                        ajax:  '<?=content_url()?>/php/pageservice/song-grid-data.php?category=<?=$category?>',
                        <?=$dataTableOutputter->outputDataColumns($myMediaCategory);?>
                        columnDefs: [
                            {
                                "targets": 0,
                                "visible": false,
                                "searchable": false
                            },
                            {
                                "targets": 1,
                                "render": function ( data, type, row, meta ) {
                                    if(type === 'display'){
                                        var title = data;
                                        var id = row.id;
                                        data = '<a href="<?=$mediaPage?>?id=' + id + '&category=<?=$category?>">' + title + '</a>';
                                    }
                                    return data;
                                },                        } ],
                        select: false,
                        buttons: [

                        ]
                    } );
                } );


            </script>

        <?php endif; ?>
    </head>

    <?php //echo "but you made it to the end of the head";?>
    <?php 
    $hasBackgroundImage = 0;
    echo "<body onload='initNoBackground()' ";
    body_class('body-gray-background');
    echo ">";
    ?>
    
        <div class="wrapper container">
<!--            --><?php //echo "hi"?>
<!--            --><?php //echo "mmc_getCurrentState is: " . (string)mmc_getCurrentState() ?>
<!--            --><?php //echo "got here" ?>
            <!-- header image -->
            <?php if(!$useMatchingGameEngine): ?>
            <header class="<?=cssClass("site-header")?>">
                <?php if (mmc_getCurrentState()->isTop100()): ?>
                        <a href="<?=homeURL(mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getProfileSubsriberId()) ?>">
                            <div class="homelink-education left">
                                Matt's Top 100
                                <?php if (!empty($secondaryTitle)) {
                                    echo "<br/>" . $secondaryTitle;
                                } ?>
                            </div>
                            <div class="homelink-education-small left">
                                 no second line
                            </div>
                        </a>
                    <?php elseif (mmc_getCurrentState()->isCardGames()): ?>
                        <a href="<?=PERMALINKBASE?>">
                            <img class="img-responsive"
                                 src="<?=assetPath()?>/img/bg-header-card-games.jpg" />
                        </a>
                    <?php elseif (mmc_getCurrentState()->isPoliticalFootball()): ?>
                        <a href="<?=PERMALINKBASE?>">
                            <img class="img-responsive"
                                src="<?=ABSURL?>/wp-content/themes/compass/img/bg-header-political-football2.jpg" />
                        </a>
                    <?php elseif (mmc_getCurrentState()->isMyMoralCompassEducators()): ?>
                        <a href="<?=homeURL(mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getProfileSubsriberId()) ?>">
                            <div class="homelink-education left">
                                <?=getProfileHeading(1, mmc_getCurrentState()->getProfile(), mmc_getCurrentState()->getPersona())?>
                            </div>
                            <div class="homelink-education-small left">
                                <?=getProfileHeading(2, mmc_getCurrentState()->getProfile(), mmc_getCurrentState()->getPersona())?>
                            </div>
                        </a>
                         link to home
                        <div class="homelink-education right">
                            <a href="<?php echo get_bloginfo('url') . '?profile=MYMORALCOMPASS' ?>"
                               class="<?=cssClass("logo")?> ir left">
                                <?php bloginfo('name') ?>
                            </a>
                        </div>
                    <?php else: ?>
                        <a href="<?=homeURL(mmc_getCurrentState()->getProfileName(),
                            mmc_getCurrentState()->getProfileSubsriberId()) ?>"
                           class="<?=cssClass("logo")?> ir left">
                            <?php bloginfo('name') ?>
                        </a>
                        <?php
                        echo "<nav class='main-menu-container right'>";
                        if (is_single()) :
                            wp_nav_menu( array( 'container' => '', 'theme_location' => 'article-menu', 'depth' => 1 ) );
                        else :
                            wp_nav_menu( array( 'container' => '', 'theme_location' => 'main-menu', 'depth' => 1 ) );
                        echo "</nav>";
                        ?>
                        <div class="little-search-box">
                            <form role="search" method="post" id="searchform"
                                  action="<?=get_bloginfo('url') . '/'?>search-results-2">
                                <input type=submit value="Search" class=left>
                                <input type=text name=searchstring id=searchstring placeholder="Enter Keyword" class=left>
                            </form>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
<!--                --><?php //if (!$useMatchingGameEngine && !mmc_getCurrentState()->isMyMoralCompass()) : ?>
<!--                <div class="hidden-xs">-->
<!--                <a href="--><?//=MORALCOMPASSURL?><!--/?profile=MYMORALCOMPASS" class="--><?//=cssClass("logo")?><!-- ir right">-->
<!--                    --><?php //bloginfo('name') ?>
<!--                </a>-->
<!--                </div>-->
<!--                --><?php //endif; ?>
<!--                --><?php //else: ?>
<!--                <img class="img-responsive" src=--><?//=assetPath()?><!--."img/bg-header-card-games.jpg"/>-->
            </header>
            <?php endif; ?>


            <div class="row container">