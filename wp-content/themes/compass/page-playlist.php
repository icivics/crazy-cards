<?php 
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/coreplaylist.php');

?>

<?php get_header() ?>
<div class="container1 clearfix">

    <section class=sources>
        <div class=padded-title>
            <h4 ><a href="<?=get_bloginfo('url')?>/top100">
                    <-- Return to Top 100 Main Page</a></h4>
        </div>
    </section>
    <?php
    if ($playlist == null) {
        echo "No playlist found for GUID: " . $guid;
    } else {
        //echo ("<p class='songlisttable'>");
        foreach ($playlist->arrPlaylistEntries as $playlistEntry) {
            echo "<div class='tile-wrapper col-md-4 col-sm-6'>";
            echo "<a name='MEDIA_ID_" . $playlistEntry->mediaItemId . "'/>";
            echo "<div class='thumbnail playlist-thumbnail'>";
            echo $playlistEntryOutputter->outputPlaylistEntry($playlistEntry, $playlist);
            echo "</div>";
            echo "</div>";
        }
        //echo "</p>";
        echo "<hr/>";

    }

    ?>

</div>
<?php get_footer() ?>