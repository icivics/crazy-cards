<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/10/2017
 * Time: 8:58 AM
 */

require_once(ABSPATH . 'wp-content/php/pageservice/coresongpicker.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// php flag to render a bunch of js in the header
$useDataTable = 1;
$mediaPickerPage = "political-media";
$mediaPage = "one-video";

?>
<?php get_header() ?>

<script language="JavaScript">
    function changeCategory() {
        var selector = document.getElementById("categoryselector");
        var category = selector.options[selector.selectedIndex].value;
        location.href = "<?=PERMALINKBASE . $mediaPickerPage?>?category=" + category;
    }

</script>
<div class="container1 clearfix">


    <h6 ><a href="<?=PERMALINKBASE?>politicalfootball">
            <-- Return to Political Football Main Page</a></h6>


    <h6>
        Select a category: &nbsp;&nbsp;
        <select id="categoryselector" onchange="changeCategory()">
            <option value="music" <?=selectedIfNecessary("music", $category)?> >Music</option>
            <option value="comedy" <?=selectedIfNecessary("comedy", $category)?> >Comedy</option>
            <option value="movies" <?=selectedIfNecessary("movies", $category)?> >Movies</option>
            <option value="tv" <?=selectedIfNecessary("tv", $category)?>>TV Shows</option>
        </select>
        <br/><br/>
    </h6>

    <table id="song-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
        <thead align="left">
        <tr>
            <?=$dataTableOutputter->outputTheadHtml($myMediaCategory);?>
        </tr>
        </thead>
    </table>
    <br/><br/>

</div>
<?php get_footer() ?>