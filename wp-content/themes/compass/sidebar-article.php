<aside class="sidebar article-sidebar left">
    <?php global $template; //echo "template is: " . $template; exit(); 
			global $current_user;
      		global $currentStudentId;
      		global $configId;
      		global $title;
      		global $topic;
			global $topicConfiguration;	
			global $numberOfAnalysis;
			global $numberOfScores;
			global $topicDocuments;
            global $topicDocumentsAll;
            global $topicDocumentsVisible;
			global $g_currentUserSubscriber;
			global $g_visibleInstructions;

            $current_user = wp_get_current_user();
			?>
	
	<?php //echo "blog info is: " . get_bloginfo('url');?>
	
	<?php if (true)://if ($template == "MORALCOMPASS") : ?>
	<section class=article-index>
    	<p align="right">
    	</p>
    	
	<?php if (mmc_getCurrentState()->isMyMoralCompass()):?>
		<?php if (mmc_getCurrentState()->getHasVisitedEducatorPage()): ?> 
			<section class="body-educators-controls-article">
	        <?php 
			if (mmc_getCurrentState()->getClassroomCodeObject()->status == SUBSCRIBER_INVALID ):
	        	echo "Invalid classroom code: " . mmc_getCurrentState()->getClassroomCode() . "<br/>";
	        	endif; 
	        
	        if (mmc_getCurrentState()->getClassroomCodeObject()->status == SUBSCRIBER_INVALID ||
	        		mmc_getCurrentState()->isEmptyClassroomCode() ):
	        ?>
			<br/>
			Students, enter classroom code here:
			<form method='POST' action="">
				<input type="text" id="classroom-code-entry-small" name="classroomCodeEntry" value=""/>
				<input type="submit" class="classroom-code-submit-small" name="Submit" value="Submit" />
			</form>
			<?php 
			else:
				echo "You are using classroom code <b>" . mmc_getCurrentState()->getClassroomCode() . "</b><br/>";
				echo "<a href='" . homeURL('ALL', mmc_getCurrentState()->getClassroomCode()) . "'>Go there now</a>";
			endif;
			?>
	</section>
	<br/>
	<?php endif; ?>
	
	<?php else:?>
		<?php if (mmc_getCurrentState()->viewAsStudent == "1"): ?>
		<section class="body-studentview-controls-article">
			<form method='POST' id="formviewteacher" name="formviewteacher" action="">
			Viewing as student.
            <input type="hidden" name="viewasstudent" value="0">
			<a href="javascript:document.formviewteacher.submit();">Return to teacher view</a>
            </form>
		</section>
		<br/>
		<?php endif; ?>
	
		<section class="body-educators-controls-article">
		<?php if (mmc_getCurrentState()->getPersona() == VISITOR):?>
            <?php if (mmc_getCurrentState()->getClassroomCodeObject()->status == SUBSCRIBER_INVALID): ?>
            	Invalid classroom code: <?=mmc_getCurrentState()->getClassroomCode() ?> <br/><br/>
            <?php
	            endif; 
            ?>
			<br/>
			Students, enter classroom code <br/>here:
			<form method='POST' action="">
				<input type="text" id="classroom-code-entry-small" name="classroomCodeEntry" value=""/>
				<input type="submit" class="classroom-code-submit-small" name="Submit" value="Submit" />
			</form>
			<br/><br/>
            Teachers, customize activities for your students:<br/>
			<a href="<?php echo wp_login_url( $_SERVER['REQUEST_URI'] ); ?>" >Log In</a>
			&nbsp;or&nbsp;
			<a href="<?php echo wp_registration_url() . "&redirect_to=" . urlencode ($_SERVER['REQUEST_URI']) ; ?>" >Register</a>
			<br/><br/>
		<?php elseif (mmc_getCurrentState()->getPersona() == LOGGEDINUSER):?>
			Welcome, <?=$current_user->display_name?><br/><br/>
			<form method='POST' action="">
				<input type="hidden" name="subscribeUser" value="1"/>
				<input type="submit" class="classroom-code-submit-small" name="GetClassroomCode" value="Get a classroom code" />
			</form>
				<br/>
				<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
		<?php elseif (mmc_getCurrentState()->getPersona() == STUDENT ||
				mmc_getCurrentState()->viewAsStudent == "1"):?>
		    You are viewing this activity with classroom code <b><?=mmc_getCurrentState()->getClassroomCode()?></b>
		    <br/><br/>
		    and Teacher,  <?=mmc_getCurrentState()->getClassroomCodeObject()->associatedUser->display_name ?>
		    <?php if (mmc_getCurrentState()->viewAsStudent != "1"): ?>
		    <br/><br/>
		    <a href="<?=sameURL(mmc_getCurrentState()->getProfileName(), null, mmc_getCurrentState()->topicConfigId);?>">Enter a different classroom code</a>
		    <?php endif;?>
		<?php elseif (mmc_getCurrentState()->getPersona() == TEACHER):?>
			Welcome, <?=$current_user->display_name?><br/><br/>
		    Share classroom code <b><?=mmc_getCurrentState()->getClassroomCode()?></b><br/>
		    with your students
		    <br/><br/>
            </p>
            <a href="#modal2" rel="modal:open">Teacher Options</a>
            <br/><br/>
            <form method='POST' id="formviewstudent" name="formviewstudent" action="">
            <input type="hidden" name="viewasstudent" value="1">
			<a href="javascript:document.formviewstudent.submit();">View as your students <br/>will see it<br/><br/></a>
            </form>
			<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
		<?php endif; ?>
    </section>
    <br/>
    <?php endif; ?>

	<?php if (mmc_getCurrentState()->isMyMoralCompass()):?>
    <br/><br/>
    <h6><a href="<?php echo PERMALINKBASE ?>whoswho-game">
    <b>Whos Who Page</b></a>
	<h6 >CONTENTS</h6>
        <ol>
            <li><a href="#">Introduction/Article</a></li>
            <li><a href="#resources">Resources</a></li>
            <?php if ($topicConfiguration->showAuthorAnalysis && $numberOfAnalysis > 0) : ?>
            <li><a href="#analysis">Analysis</a></li>
            <?php endif;?>
            <?php if ($numberOfScores > 0 && (($topicConfiguration->showUserScorecard && is_user_logged_in()) || $topicConfiguration->showAuthorScorecard ||
            		$topicConfiguration->showGenericScorecard)) : ?>
            <li><a href="#scorecard">Scorecard</a></li>
            <?php endif;?>
            <?php if (count($topicDocumentsVisible) > 0) : ?>
            <li><a href="#documents">Documents</a></li>
            <?php endif;?>
            <?php if ($topicConfiguration->showComments) : ?>
            <li><a href="#comments">Comments</a></li>
            <?php endif;?>
            <li><a href="#bibliography">Bibliography and Links</a></li>
        </ol>
    </section>
    <?php endif; ?>
    
    <?php if ($topicConfiguration->showLoginControls) : ?>
	<section class=login-area>
		<?php if (!is_user_logged_in()) : ?>
            <p class="controls no-bottom clearfix">
				<a href="<?php echo wp_login_url( $_SERVER['REQUEST_URI'] ); ?>" >Log In</a>
				<br/>
				<!-- Was doing register -->
				<!-- <a class=register-left href="php echo ABSURL ?>wp-signup.php">(or Register to get started)</a> -->
				<!-- php echo wp_register('(or ', ' to get started)'); ?>  -->
            </p>
		<?php else: ?>
            <p class="loggedin-status-left no-bottom clearfix">
            	<b>Logged in as <?php echo $current_user->display_name; ?> </b>
            </p>
            <p class="controls no-bottom clearfix">
				<br/>
				<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
				<!-- <a href="<?php echo wp_logout_url(); ?>" >Log out</a> -->
            </p>
         <?php endif; ?>
    </section>
    <?php endif; ?>
    <?php	global $userScoreCardItems;
    		if ($userScoreCardItems != null) :
    ?>
    
    <?php if ($topicConfiguration->showUserScorecard && is_user_logged_in()) :?>
    <section class=your-scorecard>
        <h6 class=padded-title>MY SCORECARD</h6>
	    <?php 
	    	global $topic;
	    	global $userScoresVisibleForTopic; 
	    ?>
	            <p class="controls no-bottom clearfix">
	                <button id=expand-scoreboard2 class="expand left" onclick="javascript:setState(4)">Add/Change Grades</button>
	                <br/><br/>
	                <?php if ($userScoresVisibleForTopic >= 2): ?>
	                <p class="sidebar-message">There are <?php echo $userScoresVisibleForTopic?> other users who have entered grades.<br/></p>
	                <a href="/index.php/user-grades?topic=<?php echo $topic; ?>"><button id=see-others-grades class="expand left">See Other Users' Grades</button></a>
	                <?php endif; ?>
	                </p>
        <table cellpadding=0 cellspacing=0 class="score-table no-bottom">
		<?php 
		/*
			global $userScoreCardItems;
			foreach ($userScoreCardItems as $scoreCardItem)
			{
				echo "<tr>";
				echo "<th class=left-header>" . $scoreCardItem->who . "</th>";
				echo "<td class=grade>" . "<div class='" . $scoreCardItem->scoreToClass() . "'>" . $scoreCardItem->scoreToGrade() . "</div></td>";
				echo "</tr>";
				echo "<tr>";
				if ($scoreCardItem->id != -1 && !empty($scoreCardItem->description))
					echo "<td colspan=2 class='left-description'><div class=description-text>" . $scoreCardItem->description . "</div></td>";
				else
					echo "<td colspan=2 class='left-description'><div class=description-text>&nbsp;</div></td>";
				
				echo "</tr>";
			}
			*/
		?>
		</table>
    </section>
    <?php endif; //$topicConfiguration->showUserScorecard ?>
    <?php endif; // $userScoreCardItems != null?>
    <?php endif; // $template == "MORALCOMPASS"?>
    
    
    <?php if ($template == "WRITING") : ?>
		<?php if (!is_user_logged_in()) : ?>
            <p class="controls no-bottom clearfix">
				<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
				<br/>
				<a class=register-left href="<?php echo ABSURL ?>wp-signup.php">(or Register to get started)</a>
				<!-- <?php echo wp_register('(or ', ' to get started)'); ?>  -->
            </p>
		<?php else: ?>
            <p class="loggedin-status-left no-bottom clearfix">
            	<b>Logged in as <?php echo $current_user->display_name; ?> </b>
            </p>
            <p class="controls no-bottom clearfix">
				<br/>
				<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
				<!-- <a href="<?php echo wp_logout_url(); ?>" >Log out</a> -->
            </p>
         <?php endif; ?>
		<?php if (loggedInAsTeacher()) : ?>
		<br/><br/>
		 <h2><?php _e('students'); ?></h2>
		   <form action="" method="post">
		   <?php wp_dropdown_users(array('name' => 'student_user_id', 'selected' => $currentStudentId, 'include' => '5,6')); ?>
		   <input type="submit" name="submit" value="view" />
		   </form>
		<?php endif; ?>
		<br/>
		
		<h2><a href="educational-product">Return to educational product main page</a></h2>
     <?php endif; ?>
     <?php if ($g_profile == "MYMORALCOMPASS"): ?>
			<form role="subscribe" method="post" class="mini-standout clearfix" id="subscribeform" action="/index.php/subscribe" 
				onsubmit="return validateEmail()">
				Subscribe to my-moral-compass.com<br/><br/>
                <input type=text name=email id=email placeholder="Enter email" class=left>
                <input type=submit value=Submit class=left>
                <input type=hidden name="returntopage" value="<?php echo $topic ?>">
                <input type=hidden name="returntopagetitle" value="<?php echo $title ?>">
                </form>
     <?php endif; ?>

  <div id="modal2" style="display:none;">
	<div class="header">
		<h3>Teacher Options</h3>
	</div>
	<form action="<?=get_bloginfo('url')?>/handleteacheroptions" method="post" name="teacheroptionsform">
		<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']?>" />
		<input type="hidden" name="submitteacheroptionsform" value="1" />
		<input type="hidden" name="configid" value="<?=$configId?>" />
		<input type="hidden" name="subscriberid" value="<?=$g_profileSubscriber->subscriberId?>" />
		<input type="hidden" name="profile" value="<?=$g_profile?>" />
		
		<div id="options">
			Viewing Options<br/><a href='javascript:toggleVisibility("options", "documents")'>Go to Documents</a><br/><br/>
			<div class="cb">
				<label for="showinstructions"><input type="checkbox" name="showinstructions" id="showinstructions" 
				<?php if ($topicConfiguration->showInstructions): 
					echo "CHECKED"; endif;?>>Show Instructions</input></label>
			</div>
			<div class="txt">
				<label for="instructions">Instructions:</label><br/>
				<textarea rows="5" cols="80" name="instructions" id="instructions"><?php $str = convertBRtoCR($g_visibleInstructions); echo $str; ?></textarea>
			</div>
			<div class="cb">
				<label for="authoranalysis"><input type="checkbox" name="authoranalysis" id="authoranalysis" 
				<?php if ($topicConfiguration->showAuthorAnalysis): 
					echo "CHECKED"; endif;?>>Show Author Analysis</input></label>
			</div>
			<div class="cb">
				<label for="authorscorecard"><input type="checkbox" name="authorscorecard" id="authorscorecard" 
				<?php if ($topicConfiguration->showAuthorScorecard): 
					echo "CHECKED"; endif;?>>Show Author Scorecard</input></label>
			</div>
			<div class="cb">
				<label for="genericscorecard"><input type="checkbox" name="genericscorecard" id="genericscorecard" 
				<?php if ($topicConfiguration->showGenericScorecard): 
					echo "CHECKED"; endif;?>>Show Generic Scorecard</input></label>
			</div>
		</div>
		<div id="documents" class="hidden">
			Viewing Documents<br/>
			<a href='javascript:toggleVisibility("documents", "options")'>Go to Options</a><br/><br/>
			<?=generateHtmlForDocuments($topicDocumentsAll); ?>
		</div>
		<div class="btn clearfix">
			<a class="close" href="#" rel="modal:close" onclick="javascript:document.teacheroptionsform.submit();">Submit</a>
			<a class="close cancel" href="#" rel="modal:close">Cancel</a>
		</div>
	</form>
  </div>

</aside>