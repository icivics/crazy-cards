<?php 
require_once(ABSPATH . 'wp-content/php/pageservice/signupthankyou.php');

?>

<?php get_header() ?>

<div class="container clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Signup Thank You</h6>
    </div>
        <section class=top-content>
     
            <article id="<?php the_ID() ?>" class="home-article serif">
            <h4>
                <?php
                	$post = get_post();
                	$welcomeMsg = $post->post_content;
                	//$welcomeMsg = str_replace("@F", featuredTopicHtml($featuredTopic, $featuredImage), $welcomeMsg);
					$welcomeMsg = str_replace("@BLOGINFO", get_bloginfo('url'), $welcomeMsg);
					$welcomeMsg = str_replace("@SITEURL", ABSURL, $welcomeMsg);
					$welcomeMsg = str_replace("@SONGLISTGAME", $htmlForSongTeasers, $welcomeMsg);
					$welcomeMsg = str_replace("@CURRENTSONGLISTLINK", $htmlForCurrentSongGroupingLink, $welcomeMsg);
					$welcomeMsg = str_replace("@VISIBLESONGLISTLINKS", $htmlForNonCurrentVisibleSongGroupingLinks, $welcomeMsg);
                    $welcomeMsg = str_replace("@INVISIBLESONGLISTLINKS", $htmlForNonCurrentNonVisibleSongGroupingLinks, $welcomeMsg);
                    $welcomeMsg = str_replace("@CONTRIBUTORFORMLINK", $htmlForContributorFormLink, $welcomeMsg);
                	echo $welcomeMsg;
                ?>
             </h4>   
             </article>
        </section>
</div>        
<?php get_footer() ?>

