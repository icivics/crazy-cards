<?php 
//error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/coregamedone.php');

global $gameSession;
?>
<?php get_header() ?>

<?php $name = "";
if (strcasecmp($category, "songoftheweek") == 0) {
    $name = "the Song of the Week Game";
} else if (strcasecmp($category, "icivicsgifs") == 0) {
    $name = "the iCivics GIFs Game";
} else if (strcasecmp($category, "trump") == 0) {
    $name = "the Trump Game";
} else {
    $name = "Disorder in the Court!";
}?>

<div class="container1 clearfix">
    <div class="row">
        <div class="col-md-13 thumbnail" align="center">
            <!--<img class="img-responsive" style="width: 100%" src="/wp-content/themes/compass/img/PoliticalFootballCombo.jpg"/>-->

            <br/>
                <h1>Good Job! You earned <?=$gameSession->totalPoints?> points! To learn more about the iCivics Argument Wars Supreme Court cases, go to the
                    <a href="https://www.icivics.org/games/argument-wars" target="_blank">game</a>.<br></h1>

               <h2> Want to
            <a href="matching-game?category=<?=$category?>">play again?</a></div></h2>
            </div>
    <div class="row">
        <div class="game-done-score thumbnail col-md-6">
            Number of Completed Boards: <?=$gameSession->numberOfCompletedBoards?><br/>
            Total Points: <?=$gameSession->totalPoints?><br/>
            Average per Board: <?=$gameSession->averagePoints?>
        </div>
        <div class="game-done-info thumbnail col-md-6">
            <h5>If you like <?=$name?>, see other great games from iCivics:</h5>
            <br/>
            <a href="https://www.icivics.org/games/win-white-house" target="_blank" >Win the White House</a><br/><br/>
            <a href="https://www.icivics.org/games/do-i-have-right" target="_blank" >Do I Have A Right</a><br/><br/>
            <a href="https://www.icivics.org/games/race-to-ratify" target="_blank" >Race to Ratify</a><br/><br/>
        </div>
    </div>
</div>

<?php get_footer() ?>