<?php 
// Report simple running errors

//echo "Error reporting level: " . error_reporting();
echo "In single";
exit();
include(ABSPATH . 'wp-content/php/pageservice/coreevents.php');
include( ABSPATH . 'wp-admin/includes/plugin.php' );
?>

<?php get_header() ?>



<?php 
//echo "Error reporting level: " . error_reporting();
global $topicConfiguration;
global $g_visibleInstructions;

?>
<div class="container1 clearfix">

    <div class=clearfix>
        <h6 class="head-title left"><?php echo $title ?></h6>
    	<div class="print-only">
    	<div class="delete-no">
        <h5 class="right"><?php echo "Author: " . $authorDisplayName ?></h5>
        </div></div>
    </div>
    
    <div id="divFullArticleOnly" class="hidden">
        <div id="divArticleText" class="article-text-big" style="overflow:auto;height:700px;">
        <a href="javascript:setState(1)">Close Full Article</a>
        <br/><br/>
        <?php
			if (!empty($topicSelections[$articleSelection]->article)) {
				if ($featuredGraphicSelection != -1) :
					echo "<figure class='article-thumb right'>";
					echo $featuredGraphicHtml;
					echo "</figure>";
				endif;
				echo "<p align='left'>" . $topicSelections[$introSelection]->article . "</p>";
                   echo "<p align='left'>" . $topicSelections[$articleSelection]->article . "</p>";
               }
            else {
    			if (have_posts()) {
    				while (have_posts()) { 
    					the_post();
    					the_content();
    				}
    			}
            	//$post = get_post();
                //$articleText = $post->post_content;
                //echo $articleText;
            }
        ?>
        </div><!-- divArticleText -->
    </div><!-- divFullArticleOnly -->
    
    
    
	<div id="divFullPage">
	
    <?php get_sidebar('article'); ?>

    <div class="content left">
    	<div class="print-only">
    	<div class="delete-no">
    	<article class=article-content>
    	<?php if (have_posts()) while (have_posts()) : the_post(); ?>
        
        	<?php if (mmc_getCurrentState()->getPersona() != VISITOR && $topicConfiguration->showInstructions == true) :?>
			<div class="print-no">        	
                <a name="instructions"></a>
        	<div id="divInstructions" class="article-text">
                <h6>INSTRUCTIONS</h6>
                <div id="divInstructionsText" class="instructions-area">
                <?php
                	// TODO this is where custom instructions will land
					//echo "<p align='left'>" . $topicSelections[$instructionsSelection]->article . "</p>";
					echo $g_visibleInstructions;
				?>
				</div>
				<br/><br/>
			</div>
			</div>
			<?php endif ?>

			<?php if ((mmc_getCurrentState()->getPersona() == TEACHER || mmc_getCurrentState()->getPersona() == LOGGEDINUSER) 
					&& !mmc_getCurrentState()->getViewAsStudent() && count($topicDocumentsVisible) > 0) : ?>
			<div class="print-no">
                <a name="documents"></a>
			<?=generateHtmlForDocumentsMainPage($topicDocumentsVisible, mmc_getCurrentState()->getPersona()); ?>
			</div>
			<?php endif ?>


            <a name="intro-article"></a>
			<div class="print-no">
            <div id="divIntro" class="article-text">
            <table cellpadding=2>
            <tr>
            <td>&nbsp;
            </td>
            <td>
                <?php
            	if ($featuredGraphicSelection != -1) : ?>
            	<figure class="article-thumb right">
                <?php echo $featuredGraphicHtml; ?>
                <figcaption>
                    <?php //echo $topicSelections[$featuredGraphicSelection]->article; ?>
                </figcaption>
            	</figure>
            	<?php endif;

                echo "<p align='left'>";
				echo $topicSelections[$introSelection]->article;
				echo "</p>";
				echo "<a href='javascript:setState(2)'>View Full Article</a><br/><br/>";
	            if(function_exists('pf_show_link'))
	            {
	            	echo pf_show_link();
	            }
				
                ?>
            </td>
            </tr>
            </table>
            </div>
            </div>
            
            <div id="divFullArticle" class="hidden">
                <div id="divArticleText" class="article-text" style="overflow:auto;height:600px;">
	            <table cellpadding=2>
	            <tr>
	            <td align="center">&nbsp;
	            </td>
            	<td>
	            <?php
					if (!empty($topicSelections[$articleSelection]->article)) {
						if ($featuredGraphicSelection != -1) :
							echo "<figure class='article-thumb right'>";
							echo $featuredGraphicHtml;
							echo "</figure>";
						endif;
						echo "<p align='left'>" . $topicSelections[$introSelection]->article . "</p>";
	                    echo "<p align='left'>" . $topicSelections[$articleSelection]->article . "</p>";
	                }
	                else {
	                	the_content();
	                }
                ?>
	            </td>
	            </tr>
	            </table>
                </div><!-- divArticleText -->
            </div><!-- divFullArticle -->
			</article>
			</div> <!--  end of delete-no div -->
			</div> <!-- end of print-only div -->
        <?php endwhile; ?>
        
		<a name="resources"></a> 
        <section class=resources>
            <h6 class=padded-title>RESOURCES (<?php echo $numberOfResources ?>)</h6>

            	<?php
					for ($i=1; $i<=8; $i++)
					{
						if ($i <= $numberOfResources)
							echo "<div align='center' id='divResource" . $i . 
							"' class='hidden'><div align='right'><a href='javascript:setResource(-1)'><img src='" . 
							ABSURL . "wp-content/themes/compass/img/close-button.png'/></a></div><br/>" . $resourceHtml[$i-1] . "</div>";
						else
							echo "<div id='divResource" . $i . "' class='hidden'></div>";
					}
				
				?>
            <div class=media-area id="divResourceThumbnails">
                <?php 
					if ($numberOfResources > 3)
					{
						echo "<div id='prevbutton' class='visible'>";
						echo "<a href='javascript:setVisibleThumbnails(-1, " . $numberOfResources . ");' class='arrow previous'></a>";
						echo "</div>";
					}
				?>
				
                <ul class="media-list clearfix">
                <?php
				$resourceIndex = 0;
				for ($i=0; $i<$numberOfResources; $i++)
				{
					$thumbnailIndex = $i + 1;
					$selectionIndex = $resourceSelections[$i];
					if ($i >= 3)
						echo "<div id='divThumbnail" . $thumbnailIndex . "' class='hidden'>";
					else
						echo "<div id='divThumbnail" . $thumbnailIndex . "' class='visible'>";
                    echo "<li class=left>";
                    echo "<figure>";
					echo "<a href='javascript:setResource(" . $i . ")'><img src='" . ABSURL . "wp-content/images/" . $topic . "/" . $topicSelections[$selectionIndex]->thumbnail . 
						"' width='120' height='90' alt='' /></a>";
                    echo "<figcaption>";
					echo $topicSelections[$selectionIndex]->breadcrumb;
                    echo "</figcaption></figure></li></div>";
				}
				?>
                </ul>
                <?php 
					if ($numberOfResources > 3)
					{
						echo "<div id='prevbutton' class='visible'>";
						echo "<a href='javascript:setVisibleThumbnails(1, " . $numberOfResources . ");' class='arrow next'></a>";
						echo "</div>";
					}
				?>
            </div>
        </section>
        
    <?php if ($topicConfiguration->showDbqs && $numberOfWritingPrompts > 0 ) : ?>
            
        <h6 class=padded-title>WRITING (<?php echo $numberOfWritingPrompts ?>)
        <?php 
        	if (!is_user_logged_in())
        		echo "    <b>You must be logged in to activate the writing panel</b>";
        	else if (loggedInAsTeacher() && !empty($currentStudentId))
        	{
        		$user_info = get_userdata($currentStudentId);
        		echo "   <b>You are viewing writing from \"" . $user_info->user_login . "\"</b>";
        	}
        ?>
        </h6>
        
        <div id="divWriting" class=scores>
			<form action="" method="post" name="EditMyWriting">
            <table cellpadding=0 cellspacing=0 class="score-table no-bottom">
	    	<?php
				$iUserWritingCount = 1;
				for ($i=0;$i<$numberOfWritingPrompts;$i++)
        		{
					echo "<p class=clearfix>";
					echo "<tr>";
					echo "<th class=header>" . $topicConfiguration->getDbq($i) . "</th>";
					echo "</tr>";
					echo "<tr>";
					if (!is_user_logged_in() || loggedInAsTeacher())
						echo "<td class=description-text><TEXTAREA readonly='readonly' name='mywriting_text" . $iUserWritingCount . "' COLS=78 ROWS=8></TEXTAREA></td>";
					else
						echo "<td class=description-text><TEXTAREA name='mywriting_text" . $iUserWritingCount . "' COLS=78 ROWS=8></TEXTAREA></td>";
					echo "</tr>";
					echo "<input type='hidden' name='mywriting_id" . $iUserWritingCount . "' value='" . $userWritingItem->id . "'>";
					echo "<input type='hidden' name='mywriting_selectionid" . $iUserWritingCount . "' value='" . $userWritingItem->selectionId . "'>";
					echo "</p>";
					$iUserWritingCount = $iUserWritingCount + 1;
					/*
        			$userWritingItemIndex = findUserWritingIndexForSelection($topicSelections[$writingPromptSelections[$i]]->id);
        			$userWritingItem = $userWritingItems[$userWritingItemIndex];
					echo "<p class=clearfix>";
					echo "<tr>";
					echo "<th class=header>" . $topicSelections[$writingPromptSelections[$i]]->article . "</th>";
					echo "</tr>";
					echo "<tr>";
					if (!is_user_logged_in() || loggedInAsTeacher())
						echo "<td class=description-text><TEXTAREA readonly='readonly' name='mywriting_text" . $iUserWritingCount . "' COLS=78 ROWS=8>" . $userWritingItem->text . "</TEXTAREA></td>";
					else
						echo "<td class=description-text><TEXTAREA name='mywriting_text" . $iUserWritingCount . "' COLS=78 ROWS=8>" . $userWritingItem->text . "</TEXTAREA></td>";
					echo "</tr>";
					echo "<input type='hidden' name='mywriting_id" . $iUserWritingCount . "' value='" . $userWritingItem->id . "'>";
					echo "<input type='hidden' name='mywriting_selectionid" . $iUserWritingCount . "' value='" . $userWritingItem->selectionId . "'>";
					echo "</p>";
					$iUserWritingCount = $iUserWritingCount + 1;
					*/
        		}
        	?>
		    <tr>
		    <td >
		    <input type="submit" value="Submit" />
		    <input type="button" value="Cancel" /></td>
		    </tr>
        	</table>
			<input type="hidden" name="submitMyWriting" value="1" />
        	</form>
        </div>
    
    <?php endif; ?>

	<a name="scorecard"/>
    <?php if ($topicConfiguration->showUserScorecard && $userScoreCardItems != null && is_user_logged_in()) :	?>
	
    <div id="divMyScoreCard" class="hidden">
    <a name="myscorecard"></a> 
    <section class=myscorecard>
	<form action="" method="post" name="EditMyScoreCard">
    <h6 class="featured-title padded-title">MY SCORECARD</h6>
    
        <div class=scores>
            <table cellpadding=0 cellspacing=0 class="score-table no-bottom">
            <tr>
            <td colspan="3">
            <?php 
            $publicValue = $userScoreCardItems[0]->ispublic;
            if ($publicValue == 1)
            	echo "<input type='checkbox' name='public' value='public' checked />";
            else
            	echo "<input type='checkbox' name='public' value='public' />";
            echo "&nbsp;Allow others to see my scorecard";
            	?>
		    </td>
		    </tr>
            <?php
			$iScorecardCount = 1;
			foreach ($userScoreCardItems as $scoreCardItem)
			{
				echo "<p class=clearfix>";
				echo "<tr>";
				echo "<th class=header>" . $scoreCardItem->who . "</th>";
				echo "<td class=grade>";
				echo "<select name='myscorecard_userrating" . $iScorecardCount . "' id=grade class=left>";
				//echo "<select name='grade[]' id=grade class=left>";
				echo outputGradeOptions($scoreCardItem->score);
				echo "</select>";
				echo "</td>";
				echo "<td class=description-text><TEXTAREA name='myscorecard_description" . $iScorecardCount . "' COLS=53 ROWS=2>" . $scoreCardItem->description . "</TEXTAREA></td>";
				echo "</tr>";
				echo "<input type='hidden' name='myscorecard_scoreid" . $iScorecardCount . "' value='" . $scoreCardItem->originalScoreId . "'>";
				echo "<input type='hidden' name='myscorecard_record" . $iScorecardCount . "' value='" . $scoreCardItem->id . "'>";
				echo "</p";
				$iScorecardCount = $iScorecardCount + 1;
			}
	?>
    <tr>
    <td colspan="3">
    <br/>
    <input type="submit" value="Submit" />
    <input type="button" value="Cancel" onClick="javascript:setState(1);" />
    </td>
    </tr>
    </table>
    </div>
	<input type="hidden" name="submitMyScorecard" value="1" />
    </form>
	</section>
	</div>
	<?php //elseif ($numberOfScores > 0) : 
	elseif ($topicConfiguration->showGenericScorecard && $scoreCardItems != null):
	?>
    <div id="divGeneralScoreCard">
    <section class=generalscorecard>
    <h6 class="featured-title padded-title">MORAL COMPASS SCORECARD</h6>

            <div id="generalscores" class=scores>
            <table cellpadding=0 cellspacing=0 class="score-table no-bottom">
		    <?php

			foreach ($scoreCardItems as $scoreCardItem)
			{
				echo "<tr>";
				echo "<th class=header>" . $scoreCardItem->who . "</th>";
				echo "<td class=grade>" . "<div class='letter c'>??</div></td>";
				echo "<td class=description><div class=description-text>&nbsp;</div></td>";
				echo "</tr>";
			}
	
		    ?>
            </table>    
            </div>
    
    
	</section>
	</div>
	<?php endif ?>

	<?php if ($topicConfiguration->showAuthorAnalysis && $numberOfAnalysis > 0) : ?>
		<a name="analysis"></a>
        <section class=scorecard>
        	<div id="divAuthorAnalysisHeader">
	           	<h6 class="featured-title padded-title"><a href='javascript:setAuthorAnalysisVisible(true)'>
	           	Show <?php echo $authorFirstName ?>&rsquo;S ANALYSIS</a></h6>
        	</div>
           	<div id="divAuthorAnalysis" class="hidden">
           	<div class="print-only">
           	<div class="delete-no">
           		<div id="divAuthorAnalysisTop">
	           	<h6 class="featured-title padded-title"><?php echo $authorFirstName ?>&rsquo;s ANALYSIS
            	<a href='javascript:setState(1)'>
            	<div class="print-no">
            	<img class="align-right" src='<?php echo ABSURL ?>wp-content/themes/compass/img/close-button.png'/></a>
				</div><!-- print-no -->
				</h6>
				</div><!-- divAuthorAnalysisTop -->
				
            	<div id="divAnalysisText" class="article-text article-content" style="overflow:auto;width:550px;height:398px;">
            	<table width="100%" cellpadding=2>
	            <tr>
	            <td align="center">
	            <br/>
	            <br/>
	            </td>
            	<td>
				<?php
                    echo "<p align='left'>" . $topicSelections[$analysisSelections[0]]->article . "</p>";
                ?>
   	            </td>
	            </tr>
	            </table>
                
				</div><!-- divAnalysisText -->
			</div><!-- delete-no -->
			</div><!-- div print-only -->
            </div><!-- divAuthorAnalysis -->
        </section>
	<?php endif   ?>
	

	<?php if ($topicConfiguration->showAuthorScorecard && $scoreCardItems != null) : ?>
	
        <div id=your-score-area class=article-text></div>
        <section class=scorecard>
        	<div id="divAuthorScoreCardHeader">
	           	<h6 class="featured-title padded-title"><a href='javascript:setAuthorScoreCardVisible(true)'>
	           	Show <?php echo $authorFirstName ?>&rsquo;S SCORECARD</a></h6>
        	</div>
           	<div id="divAuthorScoreCard" class="hidden">
	           	<h6 class="featured-title padded-title"><?php echo $authorFirstName ?>&rsquo;S SCORECARD
	           	<a href='javascript:setAuthorScoreCardVisible(false)'>
	           	<img class="align-right" src='<?php echo ABSURL ?>wp-content/themes/compass/img/close-button.png'/></a></h6>
        		<br/>
            
	            <div id="authorscores" class=scores>
	                <table cellpadding=0 cellspacing=0 class="score-table no-bottom">
			    <?php
	
				foreach ($scoreCardItems as $scoreCardItem)
				{
					echo "<tr>";
					echo "<th class=header>" . $scoreCardItem->who . "</th>";
					echo "<td class=grade>" . "<div class='" . $scoreCardItem->scoreToClass() . "'>" . $scoreCardItem->scoreToGrade() . "</div></td>";
					echo "<td class=description><div class=description-text>" . $scoreCardItem->description . "</div></td>";
					echo "</tr>";
				}
	
			    ?>
			    <!--
	                    <tr>
	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
	                        <td class=caption>
	                            These are the scores given by the author of this site, <?php the_author_meta('display_name') ?>.<br>
	                            <a href="#" title="What score would you give?">What score would you give?</a> 
	                        </td>
	                    </tr>
	              -->
	                </table>    
	            </div>
        	</div>
            
        </section>
	<?php endif ?>

		<?php if ((mmc_getCurrentState()->getPersona() == STUDENT || mmc_getCurrentState()->getViewAsStudent()) 
				&& count($topicDocumentsVisible) > 0) : ?>
		<div class="print-no">
		<a name="documents"/>
		<?=generateHtmlForDocumentsMainPage($topicDocumentsVisible, STUDENT); ?>
		</div>
		<?php endif ?>



	<?php if ($topicConfiguration->showComments) : ?>
		<a name="comments"/>
        <?php comments_template(); ?>
	<?php endif ?>
		<a name="bibliography"/>
        <?php if (get_post_meta(get_the_ID(), '_compass_sources')) : ?>
        <section class=sources>
            <div class=padded-title>
                <h6 class=underline-title>BIBLIOGRAPHY AND LINKS</h6>
                <?php echo wpautop(get_post_meta(get_the_ID(), '_compass_sources', true)); ?>
            </div>
        </section>
        <?php endif; ?>
        <section class=sources>
            <div class=padded-title>
                <h6 class=underline-title>BIBLIOGRAPHY AND LINKS</h6>
                <?php echo $topicSelections[$linksSelection]->article; ?>
            </div>
        </section>
    </div>
	</div>
</div>
<?php get_footer() ?>