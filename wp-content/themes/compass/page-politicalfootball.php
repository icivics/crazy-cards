<?php 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once(ABSPATH . 'wp-content/php/pageservice/corepoliticalfootball.php');

?>

<?php get_header() ?>

    <div class="container1 clearfix tile-grid">

        <section class=top-content>

            <article id="<?php the_ID() ?>" class="home-article serif">
                <?php
                $post = get_post();
                $postMessage = $post->post_content;
                $postMessage = filterStandardTokens($postMessage);

                $postMessage = str_replace("@TILES", $tileOutputter->createHtmlForTiles(), $postMessage);

                /*
                $postMessage = str_replace("@SONGLISTGAME", $htmlForSongTeasers, $postMessage);
                $postMessage = str_replace("@TOP3TEASERS", $htmlForTop3Teasers, $postMessage);
                $postMessage = str_replace("@AFTERTOP10TEASERS", $htmlForAfterTop10Teasers, $postMessage);
                $postMessage = str_replace("@CURRENTSONGLISTLINK", $htmlForCurrentSongGroupingLink, $postMessage);
                $postMessage = str_replace("@VISIBLESONGLISTLINKS", $htmlForNonCurrentVisibleSongGroupingLinks, $postMessage);
                $postMessage = str_replace("@INVISIBLESONGLISTLINKS", $htmlForNonCurrentNonVisibleSongGroupingLinks, $postMessage);
                $postMessage = str_replace("@ALLSONGLISTLINK", $htmlForAllSongListLink, $postMessage);
                $postMessage = str_replace("@SONGOFTHEWEEKLINK",
                    $playlistEntryOutputter->outputPlaylistEntry($songOfTheWeekEntry, $songOfTheWeekPlaylist), $postMessage);
                $postMessage = str_replace("@SONGOFTHEWEEKTEXTLINK",
                    $playlistEntryOutputter->outputPlaylistTextLink($songOfTheWeekPlaylist, "Song of the Week"), $postMessage);
                */
                echo $postMessage;
                ?>
            </article>
        </section>


    </div>
<?php get_footer() ?>