<?php 
include(ABSPATH . 'wp-content/php/pageservice/corefigures.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
global $topicSelections;
global $g_profile;
global $g_profileSubscriber;
?>

<?php get_header() ?>
<div class="container1 clearfix">
    <div class=clearfix>
        <h6 class="head-title left">Who's Who Historical Figures</h6>
    </div>
        <section class=sources>
            <div class=padded-title>
                <h4 ><a href="<?=PERMALINKBASE?>whoswho-game">
                <-- Return to Who's Who Page</a></h4>
                <h2><?php echo $title ?></h2>
            </div>
        </section>       
<table cellpadding="30">
<tr>
<td width=300>

                <?php
            	if ($featuredGraphicSelection != -1) : ?>
                <?php echo $featuredGraphicHtml; ?>
            	<?php endif;?>
</td>
<td width=400>
<?php 
echo "<div align='center' id='divResource1' class='visible'>" . $resourceHtml[0] . "</div>";
?>
</td>
</tr>
</table>
		<br/>		
        <section class=sources>
            <div class=padded-title>
                <h4 class=underline-title>Notes about <?php echo $title ?></h4>
                <h6><?php echo $topicSelections[$articleSelection]->article; ?></h6>
            </div>
        </section>    
		<br/>
        <section class=sources>
            <div class=padded-title>
                <h6 class=underline-title>LINKS</h6>
                <?php echo $topicSelections[$linksSelection]->article; ?>
            </div>
        </section>      
		<br/>
</div><?php get_footer() ?>