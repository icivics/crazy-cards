<?php 
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/coresonglist.php');

?>

<?php get_header() ?>
<div class="container1 clearfix">

        <h6 ><a href="<?=PERMALINKBASE?>top100?grouping=<?=$songListGrouping->id?>">
                <-- Return to Top 100 Main Page</a></h6>
    <?php  
   		$i = 0;
   		while ($i < count($arrSongLinkHtml)):
            echo "<div class='tile-wrapper col-md-6 col-sm-6'>";
            echo "<div class='thumbnail countdown-thumbnail'>";
            echo "<div class='row'>";
            echo $arrAnchorHtml[$i];
            echo "<div class='col-md-4 col-sm-4 col-xs-4'>";
   			echo $arrFeaturedGraphicHtml[$i];
            echo "</div>";
            echo "<div class='col-md-8 col-sm-8 col-xs-8'>";
   			echo $arrSongLinkHtml[$i++];
            echo "</div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
   		endwhile;
   	?>

</div>
<?php get_footer() ?>