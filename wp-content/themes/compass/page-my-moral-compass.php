<?php 

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include(ABSPATH . 'wp-content/php/pageservice/corecategories.php');
include( ABSPATH . 'wp-admin/includes/plugin.php' );

global $fullProfile;

?>
<?php get_header() ?>

<div class="container1 clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Index Of Articles</h6>
    </div>

    <div class="col-md-6">
        <?php get_sidebar(); ?>
    </div>
    <!--<div class="col-md-6 content left">-->
    <div class="col-md-6">
    	<p align="right">
	    <?php //if (is_plugin_active("simple-facebook-connect/sfc.php"))
	    	//sfc_like_button(array('url'=>'https://www.facebook.com/MyMoralCompass')); ?>
    	</p>
        <section class=top-content>
     
            <article id="<?php the_ID() ?>" class="home-article serif">
                <?php
                	if (mmc_getCurrentState()->isMyMoralCompass()) {
                		$persona = VISITOR;
                		$welcomeMsg = getProfileWelcomeMessage(mmc_getCurrentState()->getProfile(), $persona, true);
                	} else {
                		// student vs. teacher logic
                		$persona = mmc_getCurrentState()->persona;
                		if ($persona == TEACHER && mmc_getCurrentState()->getViewAsStudent() == "1") {
                			$persona = STUDENT;
                		}
                		$welcomeMsg = getProfileWelcomeMessage(mmc_getCurrentState()->getProfile(), $persona, true);
                	}
                	$welcomeMsg = str_replace("@F", featuredTopicHtml($featuredTopic, $featuredImage), $welcomeMsg);
					$welcomeMsg = str_replace("@BLOGINFO", get_bloginfo('url'), $welcomeMsg);
                	echo $welcomeMsg;
                ?>
        </section>
    <?php if (strcasecmp($g_profile, "MYMORALCOMPASS") == 0) : ?>
        <section class=search-box>

            <h6 class=padded-title>Subscribe to my-moral-compass.com</h6>
			<form role="subscribe" method="post" class="search-form standout clearfix" id="subscribeform" action="/index.php/subscribe" 
				onsubmit="return validateEmail()">
				Enter your email address and receive a notification when new content is available.<br/><br/>
                <input type=text name=email id=email placeholder="Enter email" class=left>
                <input type=submit value=Submit class=left>
                <input type=hidden name="returntopage" value="main">
            </form>

        </section>
        <section class=search-box>
        <p align=center>
        Vote for My Moral Compass on History Websites Top 100!<br/><br/>
			<a href="http://www.historywebsites.com/in.php?site=1364910683" target="_blank">
			<img src="http://www.historywebsites.com/images/vote_image1.gif" title="Enter History Websites Top 100 and Vote for this site !!!" border="0"></a>
        </p>
        </section>
    <?php endif ?>
	<!--     
        <section class=author-box>

            <h6 class="padded-title featured-title">ABOUT THE AUTHOR: <strong><?php the_author_meta('display_name', 1) ?></strong></h6>
            <div class=standout>
                <p class=no-bottom>
                    <?php the_author_meta('description', 1) ?>
                </p>
            </div>

        </section>
        
    -->
     </div>
</div>

<?php get_footer() ?>