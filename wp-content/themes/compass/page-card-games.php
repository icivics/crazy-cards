<?php
    //echo "part 1";
    require_once(ABSPATH . 'wp-content/php/pageservice/corecardgames.php');
    //echo "\npart 2"; ?>
    <?php get_header();
    //echo "\npart 2.25";?>

    <div class="container1 clearfix tile-grid">

        <section class=top-content>

            <article class="home-article serif">
                <?php
                $post = get_post();
                $postMessage = $post->post_content;
                //get the post message from the wordpress page editor

                //$postMessage = filterStandardTokens($postMessage);
                $postMessage = str_replace("@TILES", $tileOutputter->createHtmlForTiles(), $postMessage);
                //use the tile outputter to get the HTML for the tiles

                echo $postMessage;

                ?>
            </article>
        </section>


    </div>
<?php get_footer() ?>