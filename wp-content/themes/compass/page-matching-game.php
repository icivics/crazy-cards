<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

//echo "ABSPATH: " . ABSPATH;
require_once(ABSPATH . 'wp-content/php/pageservice/corematchinggame.php');
global $category;
?>

<?php get_header() ?>

<?php $columnsForBootstrap = "col-md-3 col-sm-3 col-xs-4"; ?>

    <div id="gameboardwindow" class="container1 clearfix">
        <div class="basement-projects">
            <h6 ><a href="<?=get_bloginfo('url')?>/basement-projects">
                <-- More from Matt Osber</a></h6>
        </div>
        <div class="row game-header">
            <div class="col-md-1 col-sm-1 col-xs-1">
                <a href="<?=PERMALINKBASE?>">
                    <img src="<?=assetPath()?>/img/home30x30.png" />
                </a>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-3">
                <button id="settingsButton" class="settings-button">Settings</button>
                <?php if ($debug == "1"): ?>
                    <span id="dbg_w">w</span>&nbsp;<span id="dbg_h">h</span>&nbsp;<span id="dbg_ratio">r</span>
                <?php endif; ?>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <?php if ($debug == "1"): ?>
                    <span id="dbg_winw">w</span>&nbsp;<span id="dbg_winh">h</span>
                <?php endif; ?>
                <div id="game-title" class="game-title"><?=getGameTitle($category)?></div>
                <div id="wagervalue" class="bonus-wager hidden">
                    Bonus Wager:
                </div>
                <div id="wageringindicator" class="wageringindicator hidden">
                    Wagering&nbsp;
                    <button id="showbonusdialog" class="showdialogbutton">Show Dialog</button>
                </div>
            </div>
            <div id="scorecard" class="col-md-4 col-sm-4 col-xs-2">
                Points: 0
            </div>

            <div id="gamesounds" class="hidden">
                <audio id="correctplayer" src="<?=assetPath()?>/audio/correct.mp3"></audio>
                <audio id="incorrectplayer" src="<?=assetPath()?>/audio/incorrect.mp3" preload="auto"></audio>
                <audio id="cheerplayer" src="<?=assetPath()?>/audio/cheer.mp3" preload="auto"></audio>
                <audio id="bonusplayer" src="<?=assetPath()?>/audio/jeopardydailydouble.mp3" preload="auto"></audio>
                <audio id="zoominplayer" src="<?=assetPath()?>/audio/zoomin.mp3" preload="auto"></audio>
            </div>
        </div>
        <div class="row">
            <?php
                $maxCards = 12;
                //$updaterURL = content_url() . '/php/pageservice/gameresultupdater.php';
            $cardBack = findValueInArray($arrGameCategoryMetadata, 'card_back');

            for ($i=1;$i<=$maxCards;$i++) {
                echo "<div id='closed_" . $i . "' class='tile-wrapper " . $columnsForBootstrap . "'>";
                echo "<div class='thumbnail gamecard-thumbnail gamecard-thumbnail-dimensions' gamecardindex='" . ($i-1) . "'>";
                //echo "<a href='javascript:turnovercard(\"" . $updaterURL . "\", " . $gameResultId . ", " .$i . ")'>";
                if ($cheat == "1") {
                    echo "<span class='gamecard-thumbnail-cheat'>";
                    echo $gameCardHolders[$i - 1]->id;
                    if ($gameCardHolders[$i-1]->displayMode == 0) {
                        echo " *";
                    }
                    if ($gameCardHolders[$i - 1]->bonus == 1) {
                        echo "<br/>BONUS";
                    }
                    echo "</span>";
                } else {
                    echo "<img class='aligncenter' src='" . assetPath() . "img/" . $cardBack . "'/>";
                }
                //echo "</a>";
                echo "</div>";
                echo "</div>";

                echo "<div id='open_" . $i . "' class='hidden tile-wrapper " . $columnsForBootstrap . "' gamecardid='" . $gameCardHolders[$i-1]->id . "' bonus='" .
                    $gameCardHolders[$i-1]->bonus . "'>"; ?>

                <?php
//                echo "welcome message: " . $welcomeMessage;
//                echo "\nfront side: " . $frontSide;
//                echo "\nback side: " . $backSide;

                //check metadata to see what goes on the front and the back, and display the items accordingly.
                $frontSide = findValueInArray($arrGameCategoryMetadata, 'front_side');
                $backSide = findValueInArray($arrGameCategoryMetadata, 'back_side');

                if ($gameCardHolders[$i-1]->displayMode == 0) { //front side
                    if ($frontSide == 'description') {
                        echo "<div class='thumbnail gamecard-thumbnail " . $category . "-thumbnail-description gamecard-thumbnail-dimensions' gamecardindex='" . ($i - 1) . "'>";
                        echo "<div id='gamecard-thumbnail' gamecardindex='" . $i . "'>" . $gameCardHolders[$i - 1]->description . "</div></div>";
                    } else
                    if ($frontSide == 'breadcrumb') {
                        echo "<div class='thumbnail gamecard-thumbnail " . $category . "-thumbnail-breadcrumb gamecard-thumbnail-dimensions' gamecardindex='" . ($i - 1) . "'>";
                        echo $gameCardHolders[$i - 1]->breadcrumb . "</div>";
                    }
                    else if ($frontSide == 'image') {
                        echo "<div class='thumbnail gamecard-thumbnail gamecard-thumbnail-dimensions' gamecardindex='" . ($i - 1) . "'>";
                        echo "<img id='open_img_" . $i . "' class='alignleft' src='" . assetPath() . $gameCardHolders[$i-1]->category .
                            "/img-4-3/" . $gameCardHolders[$i - 1]->img . "' /></div>";
                    }
                } else { //displayMode == 1, i.e. the back side
                    if ($backSide == 'description') {
                        echo "<div class='thumbnail gamecard-thumbnail " . $category . "-thumbnail-description gamecard-thumbnail-dimensions' gamecardindex='" . ($i - 1) . "'>";
                        echo "<div id='gamecard-thumbnail' gamecardindex='" . $i . "'>" . $gameCardHolders[$i - 1]->description . "</div></div>";
                    } else if ($backSide == 'breadcrumb') {
                        echo "<div class='thumbnail gamecard-thumbnail " . $category . "-thumbnail-breadcrumb gamecard-thumbnail-dimensions' gamecardindex='" . ($i - 1) . "'>";
                        echo  $gameCardHolders[$i - 1]->breadcrumb . "</div>";
                    } else if ($backSide == 'image') {
                        echo "<div class='thumbnail gamecard-thumbnail gamecard-thumbnail-dimensions' gamecardindex='" . ($i - 1) . "'>";
                        echo "<img id='open_img_" . $i . "' class='alignleft' src='" . assetPath() . $gameCardHolders[$i-1]->category .
                            "/img-4-3/" . $gameCardHolders[$i - 1]->img . "' /></div>";
                    }
                }

                echo "</div>"; //closing <div id=open_i ...>

                echo "<div id='done_" . $i . "' class='hidden tile-wrapper " . $columnsForBootstrap . "'>";
                echo "<div class='thumbnail gamecard-thumbnail gamecard-thumbnail-dimensions'>";
                echo "</div>";
                echo "</div>";
            }
            ?>

        </div>
</div>
    <?php $bonusMessage = findValueInArray($arrGameCategoryMetadata, 'bonus_message');?>
    <div id="dialog-bonus" title="Confirm Wager" class="hidden">
        <div id="wagerhide" class="wagerheading">
            <p><button id="hidedialog">Hide This Dialog</button></p>
        </div>
<!--        <div id="wagerimage" class="wagerimage">-->
<!--            <img class="aligncenter" src="--><?//=assetPath()?><!----><?//=$category?><!--/img/BonusPic.jpg" width="80%" height="50%" />-->
<!--        </div>-->
        <p><?=$bonusMessage?></p><br>
        <p class="validateTips wagerlabel">How much do you want to wager?</p>
        <p id="wagermax">Maximum 1000</p>
        <div id="wagersettings" class="wagersettings">
            <span id="dialogwagervalue">100</span>
            <button id="plusbutton">+</button>
            <button id="minusbutton">-</button>
        </div>
    </div>

    <div id="dialog-settings" title="Settings" class="hidden">
        <p class="settings-message">Change settings for the matching game.  Press save when done.<br/>
        <p class="validateTips sound-label"><input id="sound-toggle" type="checkbox"/>
            <label for="sound-toggle">Sound On?</label></p>
        <a href="game-help" target="_new"><br/>Click here for more help.</a>
    </div>

    <div id="dialog-gameover" title="Board Completed!" class="hidden">
        <div class="row game-over-report">
            <div class="col-md-6 col-sm-6 col-xs-6">
                This board:
                <div id="standard-points">Standard: <span id="standard-points-score">&nbsp;</span></div>
                <div id="bonus-points">Bonus: <span id="bonus-points-score">&nbsp;</span></div>
                <div id="total-points"><br/>Total: <span id="total-points-score">&nbsp;</span></div>
                <br/><br/>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div id="number-of-boards">Total Boards: <span id="running-total-boards">&nbsp;</span></div>
                <div id="running-total-points"><br/>Total Points: <span id="running-total-points-score">&nbsp;</span></div>
                <div id="running-average-points"><br/>Average Points: <span id="running-average-points-score">&nbsp;</span></div>
            </div>
        </div>

    </div>

    <div id="dialog-splash" title=<?=getGameTitle($category)?> class="hidden">
        <?php
        $blurbCssClass = findValueInArray($arrGameCategoryMetadata, 'blurb_style');
        $welcomeMessage = findValueInArray($arrGameCategoryMetadata, 'welcome_message');
        $welcomeImage = findValueInArray($arrGameCategoryMetadata, 'welcome_image');

        //        echo "front side: " . $frontSide;
//        echo "back side: " . $backSide;
//        echo "welcome message: " . $welcomeMessage;
        $gameName = getGameTitle($category);
        if ($blurbCssClass == null) {
            $blurbCssClass = 'gamecard-blurb';
        }
        echo "<div id='game_card_full_1'>";
        echo "<div class='row'>";
        echo "<div id='gamecard-splash-img' class='col-xs-6'>";
        echo "<img class='aligncenter' width='70%' height='50%' src='" . assetPath() . $category .
            "/img-4-3/" . $welcomeImage . "' />";
        echo "</div>";
        echo "</div>";
        echo "<div id='gamecard-splash-info' class='col-xs-6 gamecard-playing'>";
        echo "<div class='gamecard-breadcrumb'>" . "The " . $gameName . "<br/></div>";
        echo "<div class='" . $blurbCssClass . "'>" .
            $welcomeMessage
            . "<br/></div>";
        if ($category="cases") {
            echo "<div class='cases-message'>1. Click any two cards to see the back side of them. <br>
                  2. If the two cards match, you get 100 points. <br>
                  3. If they don’t match, they will flip back over. <br>
                  4. Remember what was on the cards and where they were on the screen. <br>
                  5. The game is over when all the cards have been matched.</div>";

        }
        echo "</div>";
        echo "</div>";
        echo "</div>";
        ?>
        <audio id="openingplayer" src="<?=assetPath()?>/audio/correct.mp3"></audio>
    </div>

    <div id="dialog-playgamecard" title="Successful Match!">
        <?php
        $blurbCssClass = findValueInArray($arrGameCategoryMetadata, 'blurb_style');
        if ($blurbCssClass == null) {
            $blurbCssClass = 'gamecard-blurb';
        }
        foreach ($allGameCards as $gameCard) {
            echo "<div id='game_card_full_" . $gameCard->id . "' class='hidden'>";
            echo "<div class='row'>";
            echo "<div id='gamecard-playing-img_" . $gameCard->id . "' class='col-xs-6'>";
            echo "<div id='bonus-award-" . $gameCard->id . "' class='bonus-award hidden'>";
            echo "&nbsp;";
            echo "</div>";
            echo "<img class='aligncenter' width='80%' src='" . assetPath() . $gameCard->category .
                "/img/" . $gameCard->img . "' />";
            echo "</div>";
            echo "<div id='gamecard-playing-info_" . $gameCard->id . "' class='col-xs-6 gamecard-playing'>";
            echo "<div class='gamecard-breadcrumb'>" . $gameCard->breadcrumb . "<br/></div>";
            echo "<div class='" . $blurbCssClass . "'>" . $gameCard->blurb . "<br/></div>";
            echo "</div>";
            echo "<audio class='gamecardaudio' id='player_" . $gameCard->id . "' preload='auto' src='" . assetPath() . $gameCard->category . "/audio/" .
                $gameCard->audio . "' />";
            echo "<div style='border: 1px solid #000000 ; padding: 8px ;'>";
            echo "Sorry, your browser does not support the audio tag used in this demo.";
            echo "</div>";
            echo "</audio>";
            echo "</div>";
            echo "</div>";
        }
        ?>

    </div>


<!--zoom-in dialog-->
    <div id='dialog-showgamecardpart'>
    <?php

        //for each card, find which side it is, then use the metadata to determine what goes on that side,
        //and display it on the card. the "content-type" attribute tells the javascript what type of content
        //is going on the card, which affects how it should be styled

        for ($i=1;$i<=$maxCards;$i++) {
            if ($gameCardHolders[$i-1]->displayMode == 0) {
                if ($frontSide == 'description') {
                    echo "<div id='game_card_content_" . $i . "' class='hidden gamecard-part' gamecardid='" .
                        $gameCardHolders[$i - 1]->id . "' content-type=description" . ">";
                    echo $gameCardHolders[$i - 1]->description;
                    echo "</div>";
                } else if ($frontSide == 'breadcrumb') {
                    echo "<div id='game_card_content_" . $i . "' class='hidden gamecard-part' gamecardid='" .
                        $gameCardHolders[$i-1]->id . "' content-type=breadcrumb" . ">";
                    echo $gameCardHolders[$i - 1]->breadcrumb;
                    echo "</div>";
                } else if ($frontSide == 'image') {
                    echo "<div id='game_card_content_" . $i . "' class='hidden gamecard-part' gamecardid='" .
                        $gameCardHolders[$i-1]->id . "' content-type=image" . ">";
                    echo "<img id='dlg_open_img_" . $i . "' class=aligncenter' width='95%' src='" . assetPath() .
                        $gameCardHolders[$i-1]->category . "/img-4-3/" . $gameCardHolders[$i - 1]->img . "' />";
                    echo "</div>";
                }

            } else {
                if ($backSide == 'description') {
                    echo "<div id='game_card_content_" . $i . "' class='hidden gamecard-part' gamecardid='" .
                        $gameCardHolders[$i - 1]->id . "' content-type=description" . ">";
                    echo $gameCardHolders[$i - 1]->description;
                    echo "</div>";
                } else if ($backSide == 'breadcrumb') {
                    echo "<div id='game_card_content_" . $i . "' class='hidden gamecard-part' gamecardid='" .
                        $gameCardHolders[$i - 1]->id . "' content-type=breadcrumb" . ">";
                    echo $gameCardHolders[$i - 1]->breadcrumb;
                    echo "</div>";
                } else if ($backSide == 'image') {
                    echo "<div id='game_card_content_" . $i . "' class='hidden gamecard-part' gamecardid='" .
                        $gameCardHolders[$i-1]->id . "' content-type=image" . ">";
                    echo "<img id='dlg_open_img_" . $i . "' class=aligncenter' width='95%' src='" . assetPath() .
                        $gameCardHolders[$i-1]->category . "/img-4-3/" . $gameCardHolders[$i - 1]->img . "' />";
                    echo "</div>";
                }
            }
        }
        ?>
    </div>

<?php get_footer() ?>