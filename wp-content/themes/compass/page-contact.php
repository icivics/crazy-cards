<?php 
require_once(ABSPATH . 'wp-content/php/standardincludes.php');
$con = getDbConnection();

// initialize all of state managed variables
mmc_initCurrentState();

$profileName = mmc_getCurrentState()->getProfileName();

insertPageView(get_the_title(), "PAGE", "PAGES", $profileName, mmc_getCurrentState()->getClassroomCode());

mysql_close($con);
?>

<?php get_header() ?>
<div class="container clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Contact My Moral Compass</b></h6>
    </div>
    
    <?php //get_sidebar(); ?>
    
    <div class="content left">
        <?php 
        if (have_posts()) { 
        	while (have_posts()) { 
        		the_post();
        	}
		}
		?>
    <article class=article-text>
		<a href="<?php bloginfo('url') ?>">Return to main menu</a>
        
        <section class=search-box>
			<form role="subscribe" method="post" class="search-form clearfix" id="subscribeform" action="/index.php/contact-done" 
				onsubmit="return validateEmail()">
				<table cellpadding="10">
				<tr>
				<td>Name</td>
				<td><input type=text name=username id=usernamename class=left></td>
				</tr>
				<tr>
				<td>Email</td>
				<td><input type=text name=email id=email class=left>
				</td>
				</tr>
				<tr>
				<td>Subject</td>
				<td><input type=text name=subject id=subject class=left></td>
				</tr>
				<tr>
				<td>Message</td>
				<td><textarea rows="4" cols="50" name="message" id="message" class="left"></textarea></td>
				</tr>
				</table>
                <input type=submit value=Submit class=left>
                <input type=hidden name="returntopage" value="main">
                <input type=hidden name="profile" value="MYMORALCOMPASS">
            </form>

        </section>
  		</article>
        </div>

</div>
<?php get_footer() ?>
