<?php 
include(ABSPATH . 'wp-content/php/pageservice/coreonesong.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
global $topicSelections;
global $autoplay;
global $seq;

$mediaPickerPage = "political-media";

?>
<?php get_header() ?>

<div class="container1 clearfix">
        <section class=sources>
            <div class=padded-title>
                <?php if (!empty($playlist) && !$playlist->playlistPageInvisible): ?>
                <h4><a href="<?=get_bloginfo('url')?>/playlist?guid=<?=$playlist->guid . $playlistEntryAnchor ?>">
                        <-- <?=$playlist->name?> Page
                    </a></h4>
                <?php else: ?>
                <h4 ><a href="<?=PERMALINKBASE . $mediaPickerPage?>?category=<?=$category?>">
                <-- Browse Media</a></h4>
                <?php endif;?>
            </div>
        </section>       

<div class="row">
    <div class="col-md-3">
<?php
        if ($playlistEntry != null) {
            echo "<div class='songoftheweeklabel'>" . $playlistEntryOutputter->outputPlaylistEntryTagline($playlistEntry, $playlist) . "</div>";
            echo "<h3>" . $mediaEntryBean->title . "</h3>";
            echo "<h6>" . htmlForMetadata($playlistEntry) . "</h6>" ;
            echo  htmlForFeaturedThumbnail($playlistEntry);
        } else {
            echo "<h2>" . $mediaEntryBean->title . "</h2>";
            echo "<h6>" . htmlForMetadata($mediaEntryBean) . "</h6>" ;
            echo  htmlForFeaturedThumbnail($mediaEntryBean);
        }
        echo "<br/><br/>"
?>
    </div>
    <div class="col-md-9">
<?php
$resourceCount = 0;
$extraBreadcrumb = null;
while ($resourceCount < count($mediaEntryBean->arrVideos)):
    $visibleClassName = $resourceCount == 0 ? "visible" : "hidden";
	echo "<div class='embed-responsive embed-responsive-16by9 " . $visibleClassName . "' id='divResource" . strval($resourceCount+1) . "'>" .
        htmlForVideo($mediaEntryBean->arrVideos[$resourceCount], false) . "</div>";
	if ($resourceCount == 0 && !empty($mediaEntryBean->arrVideos[$resourceCount]->breadcrumb)) {
        $extraBreadcrumb = $mediaEntryBean->arrVideos[$resourceCount]->breadcrumb;
    }
	$resourceCount++;
endwhile;?>
    </div>
</div>
<?php
    $blurb = null;
    if ($playlistEntry != null):
        $blurb = $playlistEntry->blurb;
    else:
        $blurb = $extraBreadcrumb;
    endif;
    if (!empty($blurb)):
?>
    <div class="row">
        <div class="col-md-12">
            <br/>
            <br/>
            <section class=sources>
                <div class=padded-title>
                    <h4 class=underline-title>Notes</h4>
                    <h6><?php echo convertCRtoBR($blurb); ?></h6>
                </div>
            </section>
        </div>
    </div>
<?php else: ?>
    &nbsp;
<?php endif; ?>

<?php
if (count($mediaEntryBean->arrVideos) > 1):
    ?>
    <div class="row">
    <div class="col-md-12">
    <br/>
    <br/>
    <section class=sources>
        <div class=padded-title>
            <h4 class=underline-title>All Resources</h4>
        </div>
    </section>
    </div>
    </div>
    <div class="row more-resources">
        <?php
        for ($i=0;$i<count($mediaEntryBean->arrVideos);$i++):
            $video = $mediaEntryBean->arrVideos[$i];
            ?>
            <div class="col-md-3 col-sm-3 col-xs-6" align="center">
                <?=htmlForVideoThumbnail($video, $i) ?>
            </div>
        <?php endfor;
        ?>
    </div>
<?php endif;?>

</div><?php get_footer() ?>