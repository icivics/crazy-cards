
<aside class="sidebar articles-index left">
<?php 
	global $lstAllCategories;
	global $g_profile;
	global $g_profileSubscriber;
	global $g_currentUserSubscriber;
	global $fullProfile;
	global $quoteoftheday;
	
	$current_user = wp_get_current_user();
?>	
	<?php if (mmc_getCurrentState()->isMyMoralCompass() && !mmc_getCurrentState()->isMyMoralCompassEducators()): ?>
         <?php 
         	if (mmc_getCurrentState()->getUseQuoteOfTheDay()):
				echo "<section class='body-mmc-controls-menu'>";
	         	echo "<u>Quote of the Day</u><br/>" . $quoteoftheday->quotetext . "<br/><p>--" . $quoteoftheday->author . "</p>";
	         	echo "</section>";
         	endif;
	     ?>
		<br/>
		<?php if (mmc_getCurrentState()->getHasVisitedEducatorPage()): ?> 
			<section class="body-educators-controls-menu">
			        <?php 
			       	if (mmc_getCurrentState()->getClassroomCodeObject()->status == SUBSCRIBER_INVALID):
		          		echo "Invalid classroom code: " . mmc_getCurrentState()->getClassroomCode() . "<br/>";
			        endif; 
			        
			        if (mmc_getCurrentState()->getClassroomCodeObject()->status == SUBSCRIBER_INVALID ||
			        		mmc_getCurrentState()->isEmptyClassroomCode() ):
		            ?>
					<br/>
					Students, enter classroom code here:
					<form method='POST' action="">
						<input type="text" name="classroomCodeEntry" value=""/>
						<input type="submit" class="classroom-code-submit" name="Submit" value="Submit" />
					</form>
					<?php 
					else:
						echo "You are using classroom code <b>" . mmc_getCurrentState()->getClassroomCode() . "</b><br/>";
						echo "<a href='" . homeURL('ALL', mmc_getCurrentState()->getClassroomCode()) . "'>Go there now</a>";
					endif;
					?>
			</section>
			<br/>
		<?php endif; ?>
	<?php else: ?>
		<?php if (mmc_getCurrentState()->viewAsStudent == "1"): ?>
		<section class="body-studentview-controls-menu">
			<form method='POST' id="formviewteacher" name="formviewteacher" action="">
			Viewing as student.
            <input type="hidden" id="viewasstudent" name="viewasstudent" value="0">
			<a href="javascript:document.formviewteacher.submit();">Return to teacher view</a>
            </form>
		</section>
		<br/>
		<?php endif; ?>
		<section class="body-educators-controls-menu">
		<?php if (mmc_getCurrentState()->getPersona() == VISITOR): ?>
            <?php 
            	if (mmc_getCurrentState()->getClassroomCodeObject()->status == SUBSCRIBER_INVALID):
            		echo "Invalid classroom code: " . mmc_getCurrentState()->getClassroomCode() . "<br/>";
	            endif; 
            ?>
			<br/>
			Students, enter classroom code here:
			<form method='POST' action="">
				<input type="text" id="classroom-code-entry" name="classroomCodeEntry" value=""/>
				<input type="submit" class="classroom-code-submit" name="Submit" value="Submit" />
			</form>
			<br/><br/>
            Teachers, customize activities for your students:<br/>
			<a href="<?php echo wp_login_url( $_SERVER['REQUEST_URI'] ); ?>" >Log In</a>
			&nbsp;or&nbsp;
			<a href="<?php echo wp_registration_url() . "&redirect_to=" . urlencode ($_SERVER['REQUEST_URI']) ; ?>" >Register</a>
			<br/><br/>
		<?php elseif (mmc_getCurrentState()->getPersona() == LOGGEDINUSER):?>
			Welcome, <?=$current_user->display_name?><br/><br/>
			<form method='POST' action="">
				<input type="hidden" name="subscribeUser" value="1"/>
				<input type="submit" class="classroom-code-submit" name="GetClassroomCode" value="Get a classroom code" />
			</form>
				<br/>
				<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
		<?php elseif (mmc_getCurrentState()->getPersona() == STUDENT ||
				mmc_getCurrentState()->viewAsStudent == "1"):?>
		    You are viewing this activity with <br/>classroom code <b><?=mmc_getCurrentState()->getClassroomCode() ?></b>
		    <br/><br/>
		    and Teacher, <?=mmc_getCurrentState()->getClassroomCodeObject()->associatedUser->display_name ?>
		    <?php if (mmc_getCurrentState()->viewAsStudent != "1"): ?>
		    <br/><br/>
		    <a href="<?=sameURL(mmc_getCurrentState()->getProfileName(), null, null);?>">Enter a different classroom code</a>
		    <?php endif; ?>
		<?php elseif (mmc_getCurrentState()->getPersona() == TEACHER):?>
			Welcome, <?=$current_user->display_name ?><br/><br/>
		    Share classroom code <b><?=mmc_getCurrentState()->getClassroomCode()?></b><br/>
		    with your students
		    <br/><br/>
            </p>
            <a href="#modal2" rel="modal:open">Teacher Options</a>
            <br/><br/>
            <form method='POST' id="formviewstudent" name="formviewstudent" action="">
            <input type="hidden" name="viewasstudent" value="1">
			<a href="javascript:document.formviewstudent.submit();">View as your students will see it</a>
            </form>
			<br/>
			<?php wp_loginout($_SERVER['REQUEST_URI']); ?>
		<?php endif; ?>
    </section>
    <br/>
<?php
	// endif for $g_profile != MYMORALCOMPASS
	endif;
	if ($lstAllCategories != null)
	{
		for ($i=0;$i<count($lstAllCategories);$i++)
		{
			$category = $lstAllCategories[$i];
			$openclass = (getCategoryOpenState($i) ? "" : "hidden");
			$closedclass = (getCategoryOpenState($i) ? "hidden" : "");
		?>
	    <section class=article-group>
	    	<div id="categoryclosed<?=$i?>" class="<?=$closedclass?>">
	    	<a href="javascript:setCategoryState(<?=$i?>, 1);" title="Click to see all topics in this category">
	    	<table border=0><tr valign="middle">
	    	<td valign="middle">
	    	<img src='<?php echo ABSURL ?>wp-content/themes/compass/img/closedindicatorbig3.png'/>
	    	</td>
	    	<td align="left">
	        <h6 class="group-title"><?php echo $category->categoryName?></h6><br/>
	        </td>
	        </tr>
	        </table>
	        </a>
	        </div>
	    	<div id="categoryopen<?=$i?>" class="<?=$openclass?>">
			<a href="javascript:setCategoryState(<?=$i?>, 0);">
	    	<table border=0><tr valign="middle">
	    	<td valign="middle">
	        <img src='<?php echo ABSURL ?>wp-content/themes/compass/img/openindicatorbig3.png'/>
	        </td>
	        <td>
	        <h6 class="group-title"><?php echo $category->categoryName ?></h6>
	        </td>
	        </tr>
	        </table>
	        </a>
	        <ul class=group-list>
	        <?php foreach ($category->topics as $topic)
	        {
	        ?>
	            <li><a href="<?php echo $topic->shortName . "?configid=" . $topic->configurationId .
	            	subscriberString($g_profileSubscriber->subscriberId)?>" 
	            	title="<?php echo $topic->title ?>" data-excerpt="<?php echo $topic->description?>"><?php echo $topic->title ?></a></li>
	        <?php
	        }
	        ?>
	        </ul>
	        </div>
	    </section>
	<?php
		}
	} 
	?>
  <div id="modal2" style="display:none;">
	<div class="header">
		<h3>Teacher Global Options</h3>
	</div>
	<form action="<?=get_bloginfo('url')?>/handleteacheroptions" method="post" name="teacheroptionsform">
		<input type="hidden" name="redirect_to" value="<?=$_SERVER['REQUEST_URI']?>" />
		<input type="hidden" name="submitteacherprofileform" value="1" />
		<input type="hidden" name="subscriberid" value="<?=$g_profileSubscriber->subscriberId?>" />
		<input type="hidden" name="profile" value="<?=$g_profile?>" />
		
		<div id="options">
			<div class="txt">
				<label for="heading1">Heading Line 1:</label><br/>
				<input type="text" name="heading1" value="<?=getProfileHeading(1, $fullProfile, STUDENT)?>" maxlength="30" />
			</div>
			<div class="txt">
				<label for="heading2">Heading Line 2:</label><br/>
				<input type="text" name="heading2" value="<?=getProfileHeading(2, $fullProfile, STUDENT)?>"  maxlength="30"  />
			</div>
			<div class="txt">
				<label for="welcomemsg">Student Welcome Message:</label><br/>
				<textarea rows="12" cols="80" name="welcomemsg" 
				id="welcomemsg"><?=getProfileWelcomeMessage($fullProfile, STUDENT, false)?></textarea>
			</div>
		</div>
		<div class="btn clearfix">
			<a class="close" href="#" rel="modal:close" onclick="javascript:document.teacheroptionsform.submit();">Submit</a>
			<a class="close cancel" href="#" rel="modal:close">Cancel</a>
		</div>
	</form>
  </div>

</aside>