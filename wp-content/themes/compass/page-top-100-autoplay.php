<?php 
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/coretop100autoplay.php');

?>

<?php get_header() ?>

<div class="container clearfix">

    <div class=clearfix>
        <h6 class="head-title left">The Final Countdown</h6>
    </div>
        <section class=top-content>
     
            <article id="<?php the_ID() ?>" class="home-article serif">
                <?php
                	$post = get_post();
                	$welcomeMsg = $post->post_content;
                	$welcomeMsg = str_replace("@F", featuredTopicHtml($featuredTopic, $featuredImage), $welcomeMsg);
					$welcomeMsg = str_replace("@BLOGINFO", get_bloginfo('url'), $welcomeMsg);
					$welcomeMsg = str_replace("@SITEURL", ABSURL, $welcomeMsg);
					$welcomeMsg = str_replace("@SONGLISTGAME", $htmlForSongTeasers, $welcomeMsg);
					$welcomeMsg = str_replace("@CURRENTSONGLISTLINK", $htmlForCurrentSongGroupingLink, $welcomeMsg);
					$welcomeMsg = str_replace("@ALLSONGLISTLINKS", $htmlForNonCurrentSongGroupingLinks, $welcomeMsg);
                	echo $welcomeMsg;
                ?>
             </article>
        </section>

</div>
<?php get_footer() ?>