<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/26/2019
 * Time: 7:25 AM
 */

require_once(ABSPATH . 'wp-content/php/db/dbdefs.php');

function preserve_qs() {
    if (empty($_SERVER['QUERY_STRING']) && strpos($_SERVER['REQUEST_URI'], "?") === false) {
        return "";
    }
    return "?" . $_SERVER['QUERY_STRING'];
}


$redirectTo = "Location: " . FRONTPAGE . preserve_qs();
header($redirectTo);

?>

