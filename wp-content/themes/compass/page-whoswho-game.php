<?php 
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/corecollagegame.php');

?>

<?php get_header() ?>
<div class="container1 clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Who's Who Page</b></h6>
    </div>
    
    <?php //get_sidebar(); ?>
    
    <h5><b>Below are some of the important historical figures that appear throughout the site.  Who do you recognize?  <br/><br/>Click on each image
    to read about each individual and the topics that they appear in.</b></h5>
    <table cellpadding="10">
    <tr>
    <?php 
    $i = 0;
    while ($i<$numberOfResources) {
		echo ("<td>" . $resourceHtml[$i] . "</td>");
		$i++;
		if (($i % 5) == 0 && $i < $numberOfResources) {
			echo ("</tr><tr>");
		}
	}
    
    ?>
    </tr>
    </table>

</div>
<?php get_footer() ?>