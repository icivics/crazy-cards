</div><!-- /.row -->

<?php
global $g_profile;
global $useMatchingGameEngine;
?>
<?php if (!$useMatchingGameEngine) : ?>
<footer class=site-footer>
                <nav class="footer-menu-container right">
                <?php
                        if (mmc_getCurrentState()->isMyMoralCompass()) :
                            wp_nav_menu( array( 'container' => '', 'theme_location' => 'footer-menu', 'depth' => 1 ) );
                        elseif (mmc_getCurrentState()->isTop100()) :
                            wp_nav_menu( array( 'container' => '', 'theme_location' => 'top-100-footer-menu', 'depth' => 1 ) );
                        elseif (mmc_getCurrentState()->isPoliticalFootball()) :
                            wp_nav_menu( array( 'container' => '', 'theme_location' => 'footer-menu', 'depth' => 1 ) );
                        else :
                            wp_nav_menu( array( 'container' => '', 'theme_location' => 'educator-footer-menu', 'depth' => 1 ) );
                        endif;
                ?>
                </nav>
            </footer>
<?php endif; ?>

        </div>
    
        <?php wp_footer(); ?>
    </body>
</html>