<?php 

error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/coregame.php');

?>

<?php get_header() ?>

<div class="container1 clearfix">
    <div class="row">
        <div class="col-md-12">
            <div align="center">
                <h1>The Matching Game</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class='tile-wrapper col-md-12'>
            <div class="row">

                <div class="col-md-6 game-intro-text">
                    Welcome to the newest of my basement projects.  This is a simple matching game where you turn
                    cards over and try to find their match.  Concentration - for those old enough to remember.
                    <br/><br/>
                    Click or tap the cards to find their matches.
                    <br/><br/>
                    And look for surprises ...
                </div>
                <!--<div class="col-md-6 game-intro-text">
                    Click or tap the cards to find their matches.
                    <br/><br/>
                    And look for surprises ...
                </div>-->
                <div class="game-intro-button col-md-6">
                    <a href="matching-game?category=trump">
                        <img width="70%" border="1" src="/wp-content/themes/compass/img/PlayTrumpGame.png"/>
                    </a>
                    <br/><br/>
                    <a href="matching-game?category=songoftheweek">
                        <img width="70%" border="1" src="/wp-content/themes/compass/img/PlaySongOfTheWeekGame.png"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>