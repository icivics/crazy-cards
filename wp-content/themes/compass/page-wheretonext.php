<?php 

error_reporting(E_ERROR | E_WARNING | E_PARSE);

require_once(ABSPATH . 'wp-content/php/pageservice/corewheretonext.php');

?>

<?php get_header() ?>
<div class="container clearfix">

    <div class=clearfix>
        <h6 class="head-title left">Choose one of the quick links below.</b></h6>
    </div>

    <div class="content left">
    
        <article class=article-text>
			Choose this link if you have a classroom code or you just completed registration.
			<h4><a href="<?php bloginfo('url') ?>?profile=ALL">Educator's Home Page</a></h4>
			<br/>
			Choose this link if you are still exploring the educator's page.
			<h4><a href="<?php bloginfo('url') ?>/for-educators/?profile=MYMORALCOMPASS">Educator's Info Page</a></h4>
			<br/>
        	Choose this link if you want to explore the main My Moral Compass site.
        	<h4><a href="<?php bloginfo('url') ?>?profile=MYMORALCOMPASS">My Moral Compass Home Page</a></h4>
			<br/>
        </article>
        </div>

</div><?php get_footer() ?>