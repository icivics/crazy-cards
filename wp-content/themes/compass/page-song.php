<?php 
include(ABSPATH . 'wp-content/php/pageservice/coresong.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
global $topicSelections;
global $autoplay;
global $seq;
?>

<?php get_header() ?>

<div class="container1 clearfix">

        <section class=sources>
            <div class=padded-title>
            	<?php if ($grouping == 0) : ?>
                <h4 ><a href="<?=PERMALINKBASE?>songlist?grouping=ALL<?=$songListAnchor?>">
                <-- Return to Countdown Page</a></h4>
            	<?php elseif (empty($currentGrouping)) : ?>
                <h4 ><a href="<?=PERMALINKBASE?>top100">
                <-- Return to Top 100 Main Page</a></h4>
                <?php else: ?>
                <h4 ><a href="<?=PERMALINKBASE?>songlist?grouping=<?=$currentGrouping->id?>">
                <-- Return to <?=$currentGrouping->first?> - <?=$currentGrouping->last?> Countdown Page</a></h4>
                <?php endif ?>
               
            </div>
        </section>

    <div class="row">
    <div class="col-md-3">
        <?php echo "<h2>" . $countdownString .  "<br/><br/>" . $title . "</h2>";
            echo "<h6>" .  $topicSelections[$metadataSelection]->article . "</h6>" ;
            echo "<h2><a href='" . createNextURL($autoplay, $seq, $topic) . "'>Next --></a></h2>";
        ?>
    </div>
        <div class="col-md-9">
            <?php
            $resourceCount = 0;
            while ($resourceCount < count($resourceHtml)):
                $visibleClassName = $resourceCount == 0 ? "visible" : "hidden";
                echo "<div class='embed-responsive embed-responsive-16by9 " .
                    $visibleClassName . "' id='divResource" . strval($resourceCount+1) . "'>" .
                    $resourceHtml[$resourceCount] . "</div>";
                $resourceCount++;
            endwhile;?>
        </div>
    </div>

    <?php if (!empty($articleText)): ?>
        <div class="row">
            <div class="col-md-12">
                <br/>
                <br/>
                <section class=sources>
                    <div class=padded-title>
                        <h4 class=underline-title>Notes</h4>
                        <h6><?php echo $articleText;//echo convertCRtoBR($articleText); ?></h6>
                    </div>
                </section>
            </div>
        </div>
    <?php else: ?>
        &nbsp;
    <?php endif; ?>

    <?php
    if (count($resourceThumbnailHtml) > 1):
        ?>
        <div class="row">
            <div class="col-md-12">
                <br/>
                <br/>
                <section class=sources>
                    <div class=padded-title>
                        <h4 class=underline-title>Additional Resources</h4>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($resourceThumbnailHtml as $thumbnailHtml): ?>
                <div class="col-md-3" align="center">
                    <?=$thumbnailHtml ?>
                </div>
            <?php endforeach;
            ?>
        </div>
    <?php endif;?>
    <?php if (!empty($topicSelections[$linksSelection]->article)): ?>
        <div class="row">
            <div class="col-md-12">
                <br/>
                <br/>
                <section class=sources>
                    <div class=padded-title>
                        <h4 class=underline-title>Links</h4>
                    </div>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="padded-title">
                    <?php echo "<h6>" . $topicSelections[$linksSelection]->article . "</h6>"; ?>
                </div>
            </div>
        </div>
    <?php else: ?>
        &nbsp;
    <?php endif;?>


</div><?php get_footer() ?>