<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 3/16/2019
 * Time: 8:34 PM
 */
include(ABSPATH . 'wp-content/php/pageservice/coremusicblog.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE);

global $latestEntry;
global $entry3MonthsAgo;
global $entry6MonthsAgo;
global $entry1YearAgo;
?>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script language="javascript">
            var expanded = 0;
            function setOpenOrClosed() {
                expanded = (expanded == 1 ? 0 : 1);
                element = document.getElementById("blog-scrolling-window");
                if (element != null) {
                    if (expanded == 1) {
                        element.style.top = "284px";
                    } else {
                        element.style.top = "176px";
                    }
                }
            }

            function ensureUnexpanded() {
                expanded = 0;
                element = document.getElementById("blog-scrolling-window");
                if (element != null) {
                    element.style.top = "176";
                }

            }

            var jmediaquery = window.matchMedia("(min-width: 768px)");

            jmediaquery.addListener(handleOrientationChange);
            handleOrientationChange(jmediaquery);

            function handleOrientationChange(jmediaquery) {
                if (jmediaquery.matches) {
                    // orientation changed
                    ensureUnexpanded();
                }
            }
        </script>
    </head>
<?php get_header() ?>

    <div class="container-blog-nav clearfix">


     <section class=sources>
         <div class="navbar-header">
             <button id="nav-expand-button" type="button" onclick="setOpenOrClosed()" class="btn little-menu navbar-toggle collapsed"
                     data-toggle="collapse" data-target="#extranav" aria-expanded="false">
                 <img src="<?=ABSURL?>wp-content/themes/compass/img/MenuCollapsed.jpg" /></button>
         </div>
         <div class="collapse navbar-collapse row blog-nav" id="extranav">
             <ul class="nav navbar-nav primary-menu">
                 <div class="col-md-4 col-sm-4 col-xs-6 blog-nav-item" align="center">
                    <li><a href="#<?=$latestEntry?>" >Latest Entry</a></li>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-6 blog-nav-item" align="center">
                     <li><a href="#<?=$entry3MonthsAgo?>" >3 Months Ago</a></li>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-6 blog-nav-item" align="center">
                    <li><a href="#<?=$entry6MonthsAgo?>" >6 Months Ago</a></li>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-6 blog-nav-item" align="center">
                     <li><a href="#<?=$entry1YearAgo?>" >1 Year Ago</a></li>
                 </div>
                 <div class="col-md-4 col-sm-4 col-xs-6 blog-nav-item" align="center">
                     <li><a href="javascript:randomtag();" ">Random</a></li>
                 </div>
             </ul>

         </div>
    </section>
    </div>

    <div class="container1">

    <div id="blog-scrolling-window" class="container2 row blog-wrapper">
        <?php
        $filePath = ABSURL . "wp-content/blogimages/";

        $itemCount = 1;
        foreach ($playlist->arrPlaylistEntries as $entry):
            echo "<div class='blog-entry-wrapper col-md-12 '>";
            echo "<div class='blog-entry-label'><a id='dateanchortag' name='" . $entry->postDate . "'></a>" .
                $playlistEntryOutputter->outputBlogEntryTagline($entry, $playlist) . "</div>";

            echo "<div class='vplayer embed-responsive' data-v=" . $entry->videoId . " data-i=" . $filePath . $entry->blogImage . ">";
            echo "<div class='plybtn'></div>";
            echo "</div>";

            echo "<div class='blog-entry-blurb'>";
            echo $playlistEntryOutputter->outputBlogEntryBlurb($entry->blurb);
            echo "</div>";

            echo "<hr/>";
            echo "</div>";
            $itemCount++;
        endforeach;
        ?>

    </div>
    </div>
<?php //get_footer() ?>