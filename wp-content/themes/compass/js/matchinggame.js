$( function() {


    const BONUS_STATE_NONE = 0;
    const BONUS_STATE_GETTING_INPUT = 1;
    const BONUS_STATE_GETTING_INPUT_DIALOG_HIDDEN = 2;
    const BONUS_STATE_GUESSING = 3;
    const BONUS_STATE_USED = 4;

    var openstates = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var gameCardActive = -1;
    var gameCardZoomed = -1;

    var points = 0;
    var correctGuesses = 0;
    var incorrectGuesses = 0;
    var bonusPoints = 0;

    var currentMediaPlayer = null;
    var lastCardPlayed = false;
    var bonusState = BONUS_STATE_NONE;
    var bonusCardWager = 100;
    var bonusCardPassedOn = false;


    // initialize
    function initializeSetup() {
        window.addEventListener("resize", resizeGrid);
        resizeGrid();
        if (newGame == 1) {
            displaySplash();
            playSound("openingsound");
        }
    }

    function resizeGrid() {
        var w = window.innerWidth;
        var h = window.innerHeight;
        setElementText('dbg_winw', "w" + w);
        setElementText('dbg_winh', "h" + h);
        if (w >= 768) {
            return;
        }
        var undesiredClass, desiredClass;
        if (w > h) {
            undesiredClass = 'col-xs-4';
            desiredClass = 'col-xs-3';
        } else {
            undesiredClass = 'col-xs-3';
            desiredClass = 'col-xs-4';
        }
        var testElement = document.getElementById('closed_1');
        var colsOk = testElement.classList.contains(desiredClass);
        if (!colsOk) {
            var maxCards = 12;
            for (i = 1; i <= maxCards; i++) {
                var closedElement = document.getElementById('closed_' + i);
                closedElement.classList.remove(undesiredClass);
                closedElement.classList.add(desiredClass);
                var openElement = document.getElementById('open_' + i);
                openElement.classList.remove(undesiredClass);
                openElement.classList.add(desiredClass);
                var doneElement = document.getElementById('done_' + i);
                doneElement.classList.remove(undesiredClass);
                doneElement.classList.add(desiredClass);
            }
        }
    }

// general purpose
    function howManyCardsOpen() {
        var count = 0;
        for (i=0;i<openstates.length;i++) {
            if (openstates[i] == 1) {
                count++;
            }
        }
        return count;
    }

    function makeElementVisible(tag, vis) {
        var element = document.getElementById(tag);
        if (element)
        {
            if (vis) {
                element.classList.remove("hidden");
                element.classList.add("visible");
            }
            else {
                element.classList.remove("visible");
                element.classList.add("hidden");
            }
        }
    }

    function replaceImageIfNecessary(tag, cardIndex, ratio1) {
        var element = document.getElementById(tag + cardIndex);
        if (element != null) {

            var imgsrc = element.getAttribute("src");
            setElementText("dbg_ratio", ratio1);
            if (ratio1 == '4-3') {
                imgsrc = imgsrc.replace('3-1', '4-3');
                imgsrc = imgsrc.replace('2-1', '4-3');
                element.setAttribute("src", imgsrc);
            } else if (ratio1 == '2-1') {
                imgsrc = imgsrc.replace('4-3', '2-1');
                imgsrc = imgsrc.replace('3-1', '2-1');
                element.setAttribute("src", imgsrc);
            } else {
                imgsrc = imgsrc.replace('4-3', '3-1');
                imgsrc = imgsrc.replace('2-1', '3-1');
                element.setAttribute("src", imgsrc);
            }

        }
    }
    function setElementText(elementid, txt) {
        var element = document.getElementById(elementid);
        if (element != null) {
            element.innerHTML = txt;
        }
    }

    function getGameboardWidth() {
        return document.getElementById("gameboardwindow").offsetWidth;
    }

    function getGameboardHeight() {
        return document.getElementById("gameboardwindow").offsetHeight;
    }

    function getGameboardRecommendedPicRatio() {
        var w = getGameboardWidth();
        var h = getGameboardHeight();
        return getRecommendedPicRatio(w, h);
    }

    function getRecommendedPicRatio(w, h) {
        if (h > (w + 70)) {
            return "4-3";
        } else if (w > 768) {
            return "2-1";
        } else {
            var ratio1 = 4 / 3;
            var ratio2 = 6 / 4;
            var ratio3 = w / h;
            if (ratio3 > ratio2) {
                return "3-1";
            } else if (ratio3 > ratio1) {
                return "2-1";
            } else {
                return "4-3";
            }
        }
    }

    function dbgDisplayWindowDimensions() {
        var w = getGameboardWidth();
        var h = getGameboardHeight();
        setElementText('dbg_w', "w" + w);
        setElementText('dbg_h', "h" + h);
    }



    function turnovercard(cardIndex) {
        var cardsOpen = howManyCardsOpen();
        if (cardsOpen == 2) {
            return;
        }
        dbgDisplayWindowDimensions();

        if (openstates[cardIndex-1] == 1) {
            // card already open
            playSound("zoomin");
            showGameCardPart(cardIndex);
        }
        if (bonusState == BONUS_STATE_GETTING_INPUT || bonusState == BONUS_STATE_GETTING_INPUT_DIALOG_HIDDEN) {
            return;
        }
        if (openstates[cardIndex-1] == 0) {
            makeElementVisible("closed_" + cardIndex, false);
            replaceImageIfNecessary('open_img_', cardIndex, getGameboardRecommendedPicRatio());
            makeElementVisible("open_" + cardIndex, true);
            openstates[cardIndex-1] = 1;
            cardsOpen++;
        }

        if (cardsOpen == 1) {
            checkIfBonus();
        }
        if (cardsOpen == 2) {
            correctGameCard = checkIfCorrect();
            if (correctGameCard == -1) {
                // wrong
                incorrectGuesses++;
                playSound("incorrectsound");
                updateScorecard();
                setTimeout(reset, 1500);
                if (bonusState == BONUS_STATE_GUESSING) {
                    bonusPoints -= bonusCardWager;
                    updateScorecard();
                    bonusState = BONUS_STATE_USED;
                }
                updateGameScore(updaterURL, gameResultId);
                return;
            } else {
                correctGuesses++;
                points += 100;
                lightUpMatchingGameCards(cardIndex, true);
                if (bonusState == BONUS_STATE_GUESSING) {
                    bonusPoints += bonusCardWager;
                    bonusState = BONUS_STATE_USED;
                    var elementId = "bonus-award-" + correctGameCard;
                    makeElementVisible(elementId, true);
                    setElementText(elementId, "Bonus Points: " + bonusPoints);
                    playSound("cheersound");
                    /*setTimeout(function() {
                        makeElementVisible("bonus-award", true);
                        element = document.getElementById("bonus-award");
                        if (element != null) {
                            element.innerHTML = "Bonus Points: " + bonusPoints;
                        }
                    }, 1500);*/
                } else {
                    playSound("correctsound");
                }
                updateScorecard();
                updateGameScore(updaterURL, gameResultId)
                setTimeout(openGameCard, 1000, correctGameCard);
                //setTimeout(resetCards, 4000);
            }
            resetBonus();
        }
    }

    function checkIfCorrect() {
        srcMatch = -1;
        destMatch = -1;
        srcCardIndex = -1;
        destCardIndex = -1;
        for (i=0;i<openstates.length;i++) {
            if (openstates[i] == 1) {
                idx = i + 1;
                tag = "open_" + idx.toString();
                element = document.getElementById(tag);
                if (srcMatch == -1) {
                    srcMatch = element.getAttribute("gamecardid");
                    srcCardIndex = i;
                } else {
                    destMatch = element.getAttribute("gamecardid");
                    destCardIndex = i;
                }
            }
        }
        if (srcMatch == destMatch) {
            openstates[srcCardIndex] = 2;
            openstates[destCardIndex] = 2;
            return srcMatch;
        } else {
            return -1;
        }
    }

    function getCardIndexFromElement(element) {
        var id = element.id;
        var idx = -1;
        if (id.indexOf("open_") == 0) {
            idx = id.substring(5);
        }
        return idx;
    }

    function lightUpMatchingGameCards(whichGameCard, matched) {
        var openElement = document.getElementById("open_" + whichGameCard);
        var gameCardId = openElement.getAttribute("gamecardid");
        var arrElements = document.querySelectorAll('[gamecardid="' + gameCardId + '"]');
        for (i=0;i<arrElements.length;i++) {
            var cardIndex = getCardIndexFromElement(arrElements[i]);
            if (cardIndex != -1) {
                var fizzleOut = false;
                var arrChildNodes = arrElements[i].childNodes;
                for (j = 0; j < arrChildNodes.length; j++) {
                    var element = arrChildNodes[j];
                    if (element != null && (typeof element.classList !== 'undefined')) {
                        element.classList.remove("gamecard-thumbnail");
                        element.classList.add("gamecard-thumbnail-matched");
                        fizzleOut = true;
                    }
                }
                if (fizzleOut) {
                    setTimeout(fizzleOutCard, 4000, cardIndex);
                }
            }
        }
    }

    function updateGameScore() {
        var complete = (isGameOver() ? 1 : 0);
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                //document.getElementById("txtHint").innerHTML=this.responseText;
            }
        }
        fullURL = updaterURL + "?gameresultid=" + gameResultId + "&points=" + points + "&complete=" + complete;
        xmlhttp.open("GET", fullURL,true);
        xmlhttp.send();
    }
    function updateScorecard() {
        var element = document.getElementById("scorecard");
        if (element != null) {
            if (isGameOver()) {
                newBonusPoints = (20 - incorrectGuesses) * 100;
                bonusPoints += newBonusPoints;
                points += bonusPoints;
                element.innerHTML = "Final Score: " + points;
            } else {
                element.innerHTML = "Points: " + (points+bonusPoints);
            }
        }
    }


// playing and managing elements

// play a short sound
    function playSound(soundId) {
        if (soundOn == 0) {
            return;
        }
        var tag = "";
        if (soundId == "openingsound") {
            tag = "openingplayer";
        } else if (soundId == "correctsound") {
            tag = "correctplayer";
        } else if (soundId == "incorrectsound") {
            tag = "incorrectplayer";
        } else if (soundId == "cheersound") {
            tag = "cheerplayer";
        } else if (soundId == "bonussound") {
            tag = "bonusplayer";
        } else if (soundId == "zoomin") {
            tag = "zoominplayer";
        }
        var plr = document.getElementById(tag);
        if (plr != null) {
            plr.play();
        }
    }

// play one of the game card elements (audio)
    function playMedia(tag) {
        if (soundOn == 0) {
            return;
        }
        var plr = document.getElementById(tag);
        if (plr != null) {
            currentMediaPlayer = plr;
            currentMediaPlayer.play();
        }
    }

// stop playing current game card element
    function stopMediaPlayer() {
        if (currentMediaPlayer != null) {
            currentMediaPlayer.pause();
            currentMediaPlayer = null;
        }
    }

    function showGameCardPart(gameCardId) {
        if (gameCardZoomed != -1) {
            makeElementVisible("game_card_content_" + gameCardZoomed, false);
        }
        gameCardZoomed = gameCardId;
        makeElementVisible("game_card_content_" + gameCardZoomed, true);
        var w =(window.innerWidth > 600 ? 500 : window.innerWidth - 100);
        var h  = (window.innerHeight > 600 ? 500 : window.innerHeight - 100);
        replaceImageIfNecessary('dlg_open_img_', gameCardId, getRecommendedPicRatio(w, h));

        //find the html element associated with the zoomed-in game card
        var dialog = document.querySelector("#dialog-showgamecardpart");
        var part = dialog.querySelectorAll('#game_card_content_' + gameCardId);

        //alert("Game card id: " + gameCardId);
        //alert("Number of items matching it: " + part.length);
        //alert("Side: " + part[0].getAttribute("card-side"));

        //get the "card-side" attribute; i.e. is it the front or the back?
        var contentType = part[0].getAttribute("content-type");
        //lert("Content type: " + contentType);

        //add the css class for the correct side of the card
        if (contentType === 'description') {
                $("#dialog-showgamecardpart").addClass(category + "-gamecard-description");
        } else if (contentType === 'breadcrumb') {
                $("#dialog-showgamecardpart").addClass(category + "-gamecard-breadcrumb");
        }

        $("#dialog-showgamecardpart").dialog("option", "width", w );
        $("#dialog-showgamecardpart").dialog("option", "height", h );
        dialog_showgamecardpart.dialog("open");
    }

    // transition to playing back game card
    function openGameCard(gameCardId) {
        stopMediaPlayer();
        if (gameCardActive != -1) {
            makeElementVisible("game_card_full_" + gameCardActive, false);
        }
        gameCardActive = gameCardId;
        makeElementVisible("game_card_full_" + gameCardActive, true);
        playMedia("player_" + gameCardActive);

        var w =(window.innerWidth > 600 ? 500 : window.innerWidth - 100);
        var h  = (window.innerHeight > 600 ? 500 : window.innerHeight - 100);
        var colValue = "";
        if (h < 420 && w > h) {
            desiredColValue = "col-xs-6";
            undesiredColValue = "col-xs-12";
        } else {
            desiredColValue = "col-xs-12";
            undesiredColValue = "col-xs-6";
        }
        element = document.getElementById("gamecard-playing-img_" + gameCardId);
        element.classList.remove(undesiredColValue);
        element.classList.add(desiredColValue);

        element = document.getElementById("gamecard-playing-info_" + gameCardId);
        element.classList.remove(undesiredColValue);
        element.classList.add(desiredColValue);

        dialog_playgamecard.dialog( "option", "width", w );
        dialog_playgamecard.dialog( "option", "height", h );
        $("#dialog-playgamecard").dialog("option", "width", w );
        $("#dialog-playgamecard").dialog("option", "height", h );

        dialog_playgamecard.dialog("open");
    }
// resetting game elements
    function resetHeader(bDefaultPanel) {
        if (gameCardActive != -1) {
            makeElementVisible("game_card_full_" + gameCardActive, false);
            makeElementVisible("top_panel_default", bDefaultPanel);
            gameCardActive = -1;
        } else {
            makeElementVisible("top_panel_default", bDefaultPanel);
        }
        makeElementVisible("top_panel_bonus", false);
    }

    function resetCards(allowResetOfOne) {
        var cardsOpen = howManyCardsOpen();
        var allowClose = (allowResetOfOne || cardsOpen != 1);
        for (i=0;i<openstates.length;i++) {
            if (openstates[i] == 2) {
                var idx = i + 1;
                makeElementVisible("closed_" + idx.toString(), false);
                makeElementVisible("open_" + idx.toString(), false);
                makeElementVisible("done_" + idx.toString(), true);
            }
            else if (openstates[i] == 1 && allowClose) {
                var idx = i + 1;
                makeElementVisible("closed_" + idx.toString(), true);
                makeElementVisible("open_" + idx.toString(), false);
                openstates[i] = 0;
            }
        }
    }

    function fizzleOutCard(cardIndex) {
        if (openstates[cardIndex-1] == 2) {
            makeElementVisible("closed_" + cardIndex.toString(), false);
            makeElementVisible("open_" + cardIndex.toString(), false);
            makeElementVisible("done_" + cardIndex.toString(), true);
        }
    }
// settings
    function showSettingsDialog() {
        var h  = (window.innerHeight > 400 ? 300 : window.innerHeight - 100);
        var w =(window.innerWidth > 500 ? 400 : window.innerWidth - 100);
        dialog_settings.dialog( "option", "width", w );
        dialog_settings.dialog( "option", "height", h );
        makeElementVisible("dialog-settings", true);
        dialog_settings.dialog("open");
    }

    function initializeSettingsElements() {
        element = document.getElementById("sound-toggle");
        element.checked = (soundOn == 1 ? true : false);
    }

    function saveSettings() {
        element = document.getElementById("sound-toggle");
        soundOn = (element.checked ? 1 : 0);
        createCookie("soundon", soundOn);
    }

// splash screen
    function displaySplash() {
        var h  = (window.innerHeight > 600 ? 500 : window.innerHeight - 100);
        var w =(window.innerWidth > 600 ? 500 : window.innerWidth - 100);
        var colValue = "";
        if (h < 420 && w > h) {
            desiredColValue = "col-xs-6";
            undesiredColValue = "col-xs-12";
        } else {
            desiredColValue = "col-xs-12";
            undesiredColValue = "col-xs-6";
        }
        element = document.getElementById("gamecard-splash-img");
        element.classList.remove(undesiredColValue);
        element.classList.add(desiredColValue);

        element = document.getElementById("gamecard-splash-info");
        element.classList.remove(undesiredColValue);
        element.classList.add(desiredColValue);

        dialog_splash.dialog( "option", "width", w );
        dialog_splash.dialog( "option", "height", h );
        //$("#dialog-splash").dialog("option", "width", w );
        //$("#dialog-splash").dialog("option", "height", h );
        makeElementVisible("dialog-splash", true);
        dialog_splash.dialog("open");
    }



// bonus card
    function initializeBonusElements() {
        makeElementVisible("wagerimage", (window.innerHeight > 600));
        element = document.getElementById("wagermax");
        if (element != null) {
            if (bonusCardPassedOn == true) {
                element.innerHTML = "Maximum: 200";
            } else {
                element.innerHTML = "Maximum: 1000";
            }
        }
    }

    function checkIfBonus() {
        if (bonusState == BONUS_STATE_USED) {
            return;
        }
        for (i=0;i<openstates.length;i++) {
            if (openstates[i] == 1) {
                idx = i + 1;
                tag = "open_" + idx.toString();
                element = document.getElementById(tag);
                bonus = element.getAttribute("bonus");
                if (bonus == "1") {
                    playSound("bonussound");
                    bonusCardWager = 100;
                    bonusState = BONUS_STATE_GETTING_INPUT;
                    setTimeout(showBonus, 1000);
                }
            }
        }
    }


    function passOnBonus() {
        stopMediaPlayer();
        resetHeader(true);
        resetCards(true);
        resetBonus();
        bonusCardPassedOn = true;
    }

    function hideBonusDialog() {
        bonusState = BONUS_STATE_GETTING_INPUT_DIALOG_HIDDEN;
        dialog_bonus.dialog( "close" );
        setWagerElementText()
    }

    function unhideBonusDialog() {
        bonusState = BONUS_STATE_GETTING_INPUT;
        dialog_bonus.dialog( "open" );
        setWagerElementText()
    }


    function confirmBonusWager() {
        bonusState = BONUS_STATE_GUESSING;
        setWagerElementText();
    }

    function bumpWager(byValue) {
        var min = 100;
        var max = (bonusCardPassedOn ? 200 : 1000);
        var newValue = bonusCardWager + byValue;
        if (newValue >= min && newValue <= max) {
            bonusCardWager = newValue;
            setWagerValueInDialog();
        }
    }

    function setWagerValueInDialog() {
        var element = document.getElementById("dialogwagervalue");
        if (element != null) {
            element.innerHTML = bonusCardWager;
        }
    }

    function setWagerElementText() {
        var element = document.getElementById("wagervalue");
        if (element != null) {
            if (bonusState == BONUS_STATE_GETTING_INPUT_DIALOG_HIDDEN) {
                makeElementVisible("game-title", false);
                makeElementVisible("wageringindicator", true);
            } else if (bonusState == BONUS_STATE_GUESSING) {
                makeElementVisible("game-title", false);
                makeElementVisible("wagervalue", true);
                element.innerHTML = "Bonus Wager: " + bonusCardWager;
            } else {
                makeElementVisible("game-title", true);
                makeElementVisible("wagervalue", false);
                makeElementVisible("wageringindicator", false);
            }
        }
    }

    function resetBonus() {
        if (bonusState != BONUS_STATE_USED) {
            bonusState = BONUS_STATE_NONE;
        }
        setWagerElementText();
    }
    function showBonus() {
        var h  = (window.innerHeight > 600 ? 500 : window.innerHeight - 100);
        var w =(window.innerWidth > 600 ? 500 : window.innerWidth - 100);
        dialog_bonus.dialog( "option", "width", w );
        dialog_bonus.dialog( "option", "height", h );
        makeElementVisible("dialog-bonus", true);
        dialog_bonus.dialog("open");
    }

    // complete reset
    function reset() {
        stopMediaPlayer();
        resetHeader(true);
        resetCards(false);
        resetBonus();
    }

    function isGameOver() {
        for (i=0;i<openstates.length;i++) {
            if (openstates[i] != 2) {
                return false;
            }
        }
        return true;
    }

    function checkForGameOver() {
        //alert("check for game over");
        if (correctGuesses == 6) {
            lastCardPlayed = true;
        }
        if (isGameOver()) {
            transitionToGameOver();
        }
    }

    function transitionToGameOver() {
        //alert("Correct guesses: " + correctGuesses);
        //alert("Last card played: " + lastCardPlayed);
        if (lastCardPlayed) {
            //alert("gem ovr")
            playSound("correctsound");
            setTimeout(function() {
                playSound("cheersound");
            }, 1000);
            setRunningScore();
            makeElementVisible("dialog-gameover", true);
            populateBoardScore();
            var h  = (window.innerHeight > 600 ? 500 : window.innerHeight - 100);
            var w =(window.innerWidth > 600 ? 500 : window.innerWidth - 100);
            dialog_gameover.dialog( "option", "width", w );
            dialog_gameover.dialog( "option", "height", h );
            dialog_gameover.dialog("open");
        }
    }

    function populateBoardScore() {
        var element = document.getElementById("total-points-score");
        if (element != null) {
            element.innerHTML = points;
        }
        element = document.getElementById("standard-points-score");
        if (element != null) {
            element.innerHTML = points - bonusPoints;
        }
        element = document.getElementById("bonus-points-score");
        if (element != null) {
            element.innerHTML = bonusPoints;
        }


        var element = document.getElementById("running-total-boards");
        if (element != null) {
            element.innerHTML = boards;
        }
        var element = document.getElementById("running-total-points-score");
        if (element != null) {
            element.innerHTML = totalPoints;
        }
        var element = document.getElementById("running-average-points-score");
        if (element != null) {
            element.innerHTML = averagePoints;
        }
    }

    function setRunningScore() {
        boards = boards + 1;
        totalPoints += points;
        averagePoints = totalPoints / boards;
    }

    dialog_playgamecard = $( "#dialog-playgamecard" ).dialog({
        autoOpen: false,
        //height:(window.innerHeight > 600 ? 500 : window.innerHeight - 100),
        //width: (window.innerWidth > 600 ? 500 : window.innerWidth - 100),
        modal: true,
        open: function (event, ui) {
            $('#dialog-playgamecard').css('overflow', 'hidden'); //this line does the actual hiding
        },
        buttons: {
            Dismiss: function() {
                reset();
                checkForGameOver();
                dialog_playgamecard.dialog( "close" );
            }
        },
        close: function() {
            reset();
        }
    });
    dialog_showgamecardpart = $("#dialog-showgamecardpart" ).dialog({
        autoOpen: false,
        //height:(window.innerHeight > 600 ? 500 : window.innerHeight - 100),
        //width: (window.innerWidth > 600 ? 500 : window.innerWidth - 100),
        modal: true,
        create: function (event, ui) {
            $(".ui-widget-header").hide();
        },
        open: function (event, ui) {
            var w = $('#dialog-showgamecardpart').data('width');
            var h = $('#dialog-showgamecardpart').data('height');
            $('#dialog-showgamecardpart').css('overflow', 'hidden'); //this line does the actual hiding
        },
        //buttons: {
        //    Dismiss: function() {
        //        dialog_showgamecardpart.dialog( "close" );
        //    }
        //},
        close: function() {
            //this is necessary in order to reset styles based on the side of the card the user was looking at previously--
            //otherwise if the user looks at the front side of one card then the back side of another, the styles
            //from the front side will remain.
            document.querySelector("#dialog-showgamecardpart").classList.remove(category + "-gamecard-breadcrumb");
            document.querySelector("#dialog-showgamecardpart").classList.remove(category + "-gamecard-description");
        }
    });

    dialog_gameover = $( "#dialog-gameover" ).dialog({
        autoOpen: false,
        height:"auto",
        width: (window.innerWidth > 600 ? 500 : window.innerWidth - 100),
        modal: true,
        open: function (event, ui) {
            $('#dialog-gameover').css('overflow', 'hidden'); //this line does the actual hiding
            $('#dialog-gameover').dialog( "option", "position", ['middle',20] );
        },
        buttons: {
            "New Board" : function() {
                var hrefvalue = "matching-game?category=" + category;
                if (cheat == 1) {
                    hrefvalue += "&cheat=1";
                }
                hrefvalue += "&gamesessionid=" + gameSessionId;
                dialog_gameover.dialog( "close" );
                location.href = hrefvalue;
            },
            Done: function() {
                reset();
                dialog_gameover.dialog( "close" );
                location.href="game-done?gamesessionid=" + gameSessionId + "&category=" + category;
            }
        },
        close: function() {
            reset();
        }
    });



    dialog_bonus = $( "#dialog-bonus" ).dialog({
        autoOpen: false,
        modal: true,
        dialogClass: "dialog-no-titlebar",
        open: function (event, ui) {
            //$('#dialog-bonus').css('overflow', 'hidden'); //this line does the actual hiding
            $('#dialog-bonus').dialog( "option", "position", ['middle',20] );
            initializeBonusElements();
        },
        buttons: {
            "Confirm wager":function() {
                confirmBonusWager();
                dialog_bonus.dialog( "close" );
            },

            "Pass": function() {
                dialog_bonus.dialog( "close" );
            }
        },
        close: function() {
            if (bonusState != BONUS_STATE_GUESSING && bonusState != BONUS_STATE_GETTING_INPUT_DIALOG_HIDDEN) {
                passOnBonus();
            }
        }
    });

    dialog_settings = $( "#dialog-settings" ).dialog({
        autoOpen: false,
        modal: true,
        dialogClass: "dialog-no-titlebar",
        open: function (event, ui) {
            //$('#dialog-bonus').css('overflow', 'hidden'); //this line does the actual hiding
            $('#dialog-settings').dialog( "option", "position", ['middle',20] );
            initializeSettingsElements();
        },
        buttons: {
            "Save":function() {
                saveSettings();
                dialog_settings.dialog( "close" );
            },

            "Cancel": function() {
                dialog_settings.dialog( "close" );
            }
        },
        close: function() {
        }
    });

    dialog_splash = $( "#dialog-splash" ).dialog({
        autoOpen: false,
        modal: true,
        dialogClass: "dialog-no-titlebar",
        open: function (event, ui) {
            //$('#dialog-bonus').css('overflow', 'hidden'); //this line does the actual hiding
            $('#dialog-splash').css('overflow', 'hidden'); //this line does the actual hiding
            $('#dialog-splash').dialog( "option", "position", ['middle',20] );
        },
        buttons: {
            "Play":function() {
                dialog_splash.dialog( "close" );
            }
        },
        close: function() {
        }
    });


    $( ".gamecard-thumbnail" ).click( function() {
        var cardIndex = this.getAttribute("gamecardindex");
        cardIndex++;

        turnovercard(cardIndex);
    });

    $("#dialog-showgamecardpart").click(function() {
        dialog_showgamecardpart.dialog("close");
    });


    $("#settingsButton").click(function() {
        showSettingsDialog();
    });

    $("#showbonusdialog").click(function() {
        unhideBonusDialog();
    });

    $( ".reset-button" ).click(function() {
        reset();
    });

    $( "#confirmwager" ).button().on( "click", function() {
        confirmWager();
    });

    $( "#passonbonus" ).button().on( "click", function() {
        passOnBonus();
    });

    $( "#hidedialog" ).button().on( "click", function() {
        hideBonusDialog();
    });


    $( "#plusbutton" ).button().on( "click", function() {
        bumpWager(100);
    });

    $( "#minusbutton" ).button().on( "click", function() {
        bumpWager(-100);
    });




    initializeSetup();
} );
