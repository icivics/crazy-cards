
var divCurrentlyPlayingVideo = "";
var thumbnailOffset = 1;
var categoriesOpenState = 1;

function looksLikeEmail(str) {
    var lastAtPos = str.lastIndexOf('@');
    var lastDotPos = str.lastIndexOf('.');
    return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
}

function validateEmail() {
	var str = document.forms["subscribeform"]["email"].value;
	if (looksLikeEmail(str)) {
		return true;
	}
	else {
		alert("Please enter a valid email address.");
		return false;
	}
		
}

function validatetop100signup() {
	var firstName = document.forms["signup"]["firstname"].value;
	var lastName = document.forms["signup"]["lastname"].value;
    var email = document.forms["signup"]["email"].value;
	if (firstName.length == 0 || lastName.length == 0 || email.length == 0) {
		alert("Please enter a valid first name, last name, and email.");
	} else if (!looksLikeEmail(email)) {
        alert("Please enter a valid email address.");
	} else {
		document.signup.submit();
	}
}

function linktotop100mainpage(bloginfo) {
	location.href= bloginfo + "/top100";
}

function lookupEmail(elementName) {
	emailAddress = document.getElementsByName(elementName)[0].value;
	if (looksLikeEmail(emailAddress)) {
		location.href = "?existingcontributor=" + emailAddress;
	} else {
        alert("Please enter a valid email address.");
	}
}

function addNewSong() {
	for (i=0;i<20;i++) {
		element = document.getElementById("songentry" + i.toString());
		if (element.className == "hidden") {
			element.className = "visible";
			break;
		}
	}
}
//function setBackgroundImage() {
//	alert("here we are");
//	$('body').css('background-image', 'url("img/full-backgroundimage2.jpg")');
//}

function makeVisible(tag, vis, className) {
	var element = document.getElementById(tag);
	if (element)
	{
		if (vis)
		{
			if (className != null)
				element.className = className;
			else if (tag.substring(3, 11) == "Resource")
				element.className = "media-area";
			else
				element.className = "visible";
			//element.style.display = "block";	
		}
		else
			element.className = "hidden";
			//element.style.display = "none";		
	}
}

function toggleVisibility(invistag, vistag) {
	makeVisible(invistag, false, null);
	makeVisible(vistag, true, null);
}
function setCategoryState(categoryIndex, open) {
	var bitvalue = 0;
	if (open == 1) {
		makeVisible("categoryopen" + categoryIndex, true);
		makeVisible("categoryclosed" + categoryIndex, false);
		bitvalue = Math.pow(2, categoryIndex);
		categoriesOpenState = categoriesOpenState | bitvalue;
		createCookie("CategoriesOpenState", categoriesOpenState, null);
	}
	else {
		makeVisible("categoryopen" + categoryIndex, false);
		makeVisible("categoryclosed" + categoryIndex, true);
		bitvalue = ~Math.pow(2, categoryIndex);
		categoriesOpenState = categoriesOpenState & bitvalue;
		createCookie("CategoriesOpenState", categoriesOpenState, null);
	}
}

function initializeAllCategoriesOpenState() {
	categoriesOpenState = readCookie("CategoriesOpenState");
	if (categoriesOpenState == null) {
		categoriesOpenState = 1;
	}
}

function isCategoryOpen(categoryindex) {
	var operand = Math.pow(2, categoryindex);
	var isOpen = ((categoriesOpenState & operand) != 0);
	alert("isCategoryOpen categoryIndex:" + categoryindex + " isopen = " + isOpen);
	return isOpen;
}

function getOpenClass(categoryIndex) {
	var isOpen = isCategoryOpen(categoryIndex);
	alert("getOpenClass categoryIndex:" + categoryindex + " isopen = " + isOpen);
	if (isOpen)
		document.write("");
	else
		document.write("hidden");
}

function getClosedClass(categoryIndex) {
	var isOpen = isCategoryOpen(categoryIndex);
	if (isOpen)
		document.write("hidden");
	else
		document.write("");
	
}
function setAuthorAnalysisVisible(visible) {
	if (visible)
	{
		makeVisible("divAuthorAnalysis", true);
		makeVisible("divAuthorAnalysisHeader", false);
	}
	else
	{
		makeVisible("divAuthorAnalysis", false);
		makeVisible("divAuthorAnalysisHeader", true);
	}
}


function setAuthorScoreCardVisible(visible) {
	if (visible)
	{
		makeVisible("divAuthorScoreCard", true);
		makeVisible("divAuthorScoreCardHeader", false);
	}
	else
	{
		makeVisible("divAuthorScoreCard", false);
		makeVisible("divAuthorScoreCardHeader", true);
	}
}

function setState(param) {
	for (i=1;i<=8;i++)
	{	
		var rsrcName = "divResource" + i;
		makeVisible(rsrcName, false);
	}
	if (param == 1)
	{
		makeVisible("divFullPage", true);
		makeVisible("divFullArticleOnly", false);
		location.href = "#";
	}
	else if (param == 2)
	{
		makeVisible("divFullPage", false);
		makeVisible("divFullArticleOnly", true);
		location.href = "#";
	}
	else if (param == 3)
	{
		location.href = "#scorecard";
	}
	else if (param == 4)
	{
		// do nothing
	}
	else if (param == 5)
	{
		location.href = "#resources";
	}
	else if (param == 6)
	{
		location.href = "#links";
	}
}

function initResource(rsrc) {
    var rsrcName = "divResource" + rsrc.toString();
    divCurrentlyPlayingVideo = rsrcName;
}

function setResource(rsrc, className) {
	var divRsrc = (rsrc + 1);
	//alert("divRsrc = " + divRsrc);
	//alert("divCurrentlyPlayingVideo is: " + divCurrentlyPlayingVideo);
	setState(5);
	//makeVisible("divIntro", true);
	//makeVisible("divFullArticle", false);
	//makeVisible("divAuthorScoreCard", true);
	//makeVisible("divMyScoreCard", false);
	//makeVisible("divResourceThumbnails", true);
	//makeVisible("divLinks", true);
	
	if (divCurrentlyPlayingVideo != "")
	{
		toggleVideo(divCurrentlyPlayingVideo, 'hide');
		divCurrentlyPlayingVideo = "";
	}
	for (i=1;i<=8;i++)
	{	
		var rsrcName = "divResource" + i;
		if (i == divRsrc)
		{
			makeVisible(rsrcName, true, className);
			divCurrentlyPlayingVideo = rsrcName;
		}
		else
			makeVisible(rsrcName, false);
	}
}

// attempt to delay video to fix IE8 problem, didn't work
function setResourceXXXXX(rsrc) {
	setTimeout("setResourceActual(" + (rsrc + 1) + ")", 1500);
} 

function setVisibleThumbnails(increment, maxvalue)
{
	if (thumbnailOffset + increment < 1)
		return;
	if (thumbnailOffset + increment + 2 > maxvalue)
		return;
	thumbnailOffset = thumbnailOffset + increment;
	for (i=1;i<=8;i++)
	{
		var thumbnailName = "divThumbnail" + i;
		if (i < thumbnailOffset || i > thumbnailOffset + 2)
			makeVisible(thumbnailName, false);
		else
			makeVisible(thumbnailName, true);
	}
}

function toggleVideo(videoDivId, state)
{
    var div = document.getElementById(videoDivId);
	if (div.getElementsByTagName("iframe")[0] == undefined)
		return;
    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
	//div.style.display = state == 'hide' ? 'none' : '';
	func = (state == 'hide' ? 'pauseVideo' : 'playVideo');
	iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
	//alert("videoDivId: " + videoDivId + " state: " + state);
	//var myPlayer = document.getElementById('player1');
	//myPlayer.stopVideo();
	/*
    var div = document.getElementById(videoDivId);
	if (div.getElementsByTagName("iframe")[0] == undefined)
		return;
    var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
	//div.style.display = state == 'hide' ? 'none' : '';
	func = (state == 'hide' ? 'pauseVideo' : 'playVideo');
	alert("ready to call postmessage, func: " + func);
	iframe.postMessage('{"event":"command","func":"' + func + '","args":""}','*');
	*/
}

var videoPlayingIndex = 0;

function flipImageAndVideo(itemIdx) {
	if (videoPlayingIndex != -1) {
		var playingVideoDivName = "divResource" + videoPlayingIndex;
		var playingImageName = "divblogimage" + videoPlayingIndex;
		toggleVideo(playingVideoDivName, 'hide');
		makeVisible(playingVideoDivName, false);
		makeVisible(playingImageName, true, 'blog-image-wrapper visible');
	}
	var imageDivName = "divblogimage" + itemIdx;
	var videoDivName = "divResource" + itemIdx;
	makeVisible(imageDivName, false);
	makeVisible(videoDivName, true, 'embed-responsive embed-responsive-16by9 visible');
	videoPlayingIndex = itemIdx;
}

function randomtag() {
	var elements = document.getElementsByTagName("a");
	var tags = [];
	for (i=0;i<elements.length;i++) {
		var element = elements[i];
		if (element.id == 'dateanchortag') {
			tags.push(element.name);
		}
	}
	sz = tags.length;
    var min=0;
    var max=sz - 1;
    var random = Math.floor(Math.random() * (+max - +min)) + +min;
	location.href = '#' + tags[random];
}

function createCookie(name,value,days) {
    if (days != null) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
