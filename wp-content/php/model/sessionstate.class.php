<?php 
class SessionState {
	public $persona; // VISITOR, LOGGEDINUSER, STUDENT, TEACHER
	public $con; // connection to use
	
	// this is only going to get and fill in what it can get from get and post headers
	// no database calls
	public function initCurrentSessionState() {

	}
	public function getForcedProfile() {
	}

	public function setForcedProfile($profile) {
	}

	public function getProfile() {
	}

	public function isCardGames() {
		return (strcasecmp(PRODUCT, "CARDGAMES") == 0);
	}
	public function isMyMoralCompass() {
		//return (PRODUCT == 'MyMoralCompass' && strcasecmp($this->profile->shortName, 'MYMORALCOMPASS') == 0);
		return (strcasecmp(PRODUCT, "MYMORALCOMPASS") == 0);
	}

	public function isMyMoralCompassEducators() {
		$x = 7;
		return (strcasecmp(PRODUCT, "MYMORALCOMPASS") == 0 &&
			$this->profile != null &&
			$this->profile->shortName != null &&
			strcasecmp($this->profile->shortName, 'MYMORALCOMPASS') != 0);
	}
	
	public function isTop100() {
		return PRODUCT == 'Top100';
	}

	public function isPoliticalFootball() {
		return PRODUCT == 'PoliticalFootball';
	}
	
	public function getPersona() {
		return $this->persona;
	}
	
	public function getSubscribeCurrentUser() {
		return $this->subscribeCurrentUser;
	}
	
	public function getViewAsStudent() {
		return $this->viewAsStudent;
	}
	
	public function getTopicConfigId() {
		return $this->topicConfigId;
	}
	
	public function getTopicName() {
		return $this->topicName;
	}
	
	public function getHasVisitedEducatorPage() {
		return $this->hasVisitedEducatorPage;
	}
	
	public function getAssociatedUserId() {
		$associatedUserId = getAssociatedUserFromClassroomCode($this->classroomCodeObject->classroomCode);
		return $associatedUserId;
	}

	public function getConnection() {
		return $this->con;
	}

	public function setConnection($con) {
		$this->con = $con;
	}
	
	public function setPersona() {
		if (is_user_logged_in()) {
			$this->persona = LOGGEDINUSER;
		} else {
			$this->persona = VISITOR;
		}
	}
    public function setTopicAndConfig($con) {
    }
}


?>
