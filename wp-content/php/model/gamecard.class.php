<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:31 AM
 */

class GameCard {
    public $id;
    public $category;
    public $audio;
    public $img;
    public $breadcrumb;
    public $blurb;
    public $displayMode;
    public $bonus;
    public $description;
}