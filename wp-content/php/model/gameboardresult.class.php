<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:31 AM
 */

class GameBoardResult {
    public $id;
    public $gameSessionId;
    public $category;
    public $timeStarted;
    public $timeLastGuess;
    public $points;
    public $complete;
    public $cheat;
    public $username;
}