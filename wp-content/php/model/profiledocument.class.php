<?php
class ProfileDocument
{
	public $profile;
	public $file;
	public $breadcrumb;
	
	public function getFullString() {
		if (empty($this->file) && empty($this->breadcrumb))
			return "";
		else
			return $this->file . "|" . $this->breadcrumb;
	}
	
	public function build($aFile, $aBreadcrumb) {
		$this->file = $aFile;
		$this->breadcrumb = $aBreadcrumb;
	}
	
	public function __construct($aLoadedString) {
		if (!empty($aLoadedString)) {
			$pieces = explode("|", $aLoadedString);
			$this->file = $pieces[0];
			$this->breadcrumb = $pieces[1];
		}
		else {
			$this->file = "";
			$this->breadcrumb = "";
		}
			
	}
}

?>