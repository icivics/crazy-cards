<?php
class Video {
	public $id;
	public $selectionId;
	public $topicId;
	public $topicShortName;
	public $videoId;
	public $name;
	public $category;
	public $keywords;
	public $url;
	public $breadcrumb;
	public $videoEmbedCode;
	public $thumbnailImage;
	public $description;
}

?>