<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:28 AM
 */

class PlaylistEntry extends MediaEntryBean {
    public $mediaItemId;
    public $tagline;
    public $blurb;
    public $postDate;
    public $sequence;
    public $videoId;
    public $thumbnailImage;
    public $blogImage;
}