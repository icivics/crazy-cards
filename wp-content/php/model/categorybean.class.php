<?php
class CategoryBean
{
	public $oldId;
	public $newId;
	public $profileId;
	public $sequence;
	public $categoryName;
	public $categoryBreadcrumb;
	public $useTimeline;
	public $timelineStart;
	public $timelineEnd;
}
?>