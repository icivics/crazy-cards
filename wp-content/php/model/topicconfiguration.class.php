<?php
class topicConfiguration
{
	public $id;
	public $topicId;
	public $showInstructions;
	public $showAuthorAnalysis;
	public $showAuthorScorecard;
	public $showUserScorecard;
	public $showGenericScorecard;
	public $showComments;
	public $showDbqs;
	public $showLoginControls;
	public $showTopicDocuments;
	public $showConfigDocuments;
	public $instructions;
	public $instructionsCustomized;
	public $dbq1;
	public $dbq2;
	public $dbq3;
	public $doc1;
	public $doc2;
	public $doc3;
	
	public function getNumberOfDbqs() {
		$count = 0;
		if ($this->showDbqs == false)
			return 0;
		if (!empty($this->dbq1))
			$count = $count + 1;
		if (!empty($this->dbq2))
			$count = $count + 1;
		if (!empty($this->dbq3))
			$count = $count + 1;
		return $count;
	}
	
	public function getDbq($dbqIndex) {
		if ($dbqIndex == 0)
			return $this->dbq1;
		else if ($dbqIndex == 1)
			return $this->dbq2;
		else if ($dbqIndex == 2)
			return $this->dbq3;
		else
			return "";
	}
}

?>