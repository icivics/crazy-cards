<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/23/2017
 * Time: 8:44 AM
 */
class MediaCategory {
    public $id;
    public $name;
    public $description;
    public $arrFields;

    function __construct() {
        $this->arrFields = array();
    }
}