<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 9/1/2018
 * Time: 9:26 PM
 */
class Tile {
    public $id;
    public $page;
    public $title;
    public $description;
    public $destination;
    public $image;
    public $sequence;
}

?>