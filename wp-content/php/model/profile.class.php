<?php
class Profile
{
	public $id;
	public $shortName;
	public $displayName;
	public $owner;
	public $arrMessaging;

	public function isMyMoralCompass() {
		return (strcasecmp($this->shortName, 'MYMORALCOMPASS') == 0);
	}
	
}

?>