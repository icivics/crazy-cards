<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:31 AM
 */

class GameSession {
    public $id;
    public $username;
    public $timeStarted;
    public $numberOfCompletedBoards;
    public $totalPoints;
    public $averagePoints;
    public $arrGameBoardResults;

    public function recalc() {
        $this->numberOfCompletedBoards = 0;
        $this->totalPoints = 0;
        foreach ($this->arrGameBoardResults as $gameBoardResult) {
            if ($gameBoardResult->complete == 1) {
                $this->numberOfCompletedBoards += 1;
                $this->totalPoints += $gameBoardResult->points;
            }
        }
        if ($this->numberOfCompletedBoards > 0) {
            $this->averagePoints = intdiv ( $this->totalPoints, $this->numberOfCompletedBoards);
        } else {
            $this->averagePoints = 0;
        }
    }
}