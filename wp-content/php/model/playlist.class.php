<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:31 AM
 */

class Playlist {
    public $id;
    public $guid;
    public $ownerId;
    public $name;
    public $blurb;
    public $playlistPageInvisible;
    public $createdOn;
    public $createdBy;
    public $updatedOn;
    public $updatedBy;
    public $arrPlaylistEntries;

    function __construct() {
        $this->arrPlaylistEntries = array();
    }
}