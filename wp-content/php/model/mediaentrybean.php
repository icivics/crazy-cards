<?php

class MediaEntryBean {
    public $id;
    public $originalId;
    public $title;
    public $artist;
    public $album;
    public $genre;
    public $released;
    public $songfactslink;
    public $links;
    public $event;
    public $tvShow;
    public $movie;
    public $product;
    public $subject;
    public $arrVideos;
    public $arrMediaCategories;

    function __construct() {
        $this->arrVideos = array();
        $this->arrMediaCategories = array();
    }

    function mapMediaEntryValue($field) {
        switch ($field) {
            case "title":
                return $this->title;
            case "artist":
                return $this->artist;
            case "album":
                return $this->album;
            case "genre":
                return $this->genre;
            case "released":
                return $this->released;
            case "event":
                return $this->event;
            case "tv_show":
                return $this->tvShow;
            case "movie":
                return $this->movie;
            case "product":
                return $this->product;
            case "subject":
                return $this->subject;
            default:
                return "";
        }
    }
}
?>