<?php
class Category
{
	public $id;
	public $categoryName;
	public $categoryBreadcrumb;
	public $useTimeline;
	public $timelineStart;
	public $timelineEnd;
	public $topics = array();
}
?>