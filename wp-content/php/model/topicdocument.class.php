<?php
class TopicDocument
{
	public $id;
	public $profile;
	public $name;
	public $file;
	public $url;
	public $visible;
	public $shared;
	public $priority;
	public $subscriberId;
	
	public function getFullString() {
		if (empty($this->file) && empty($this->name))
			return "";
		else
			return $this->file . "|" . $this->name;
	}
	
	public function build($aFile, $aBreadcrumb) {
		$this->file = $aFile;
		$this->name = $aBreadcrumb;
	}
	
	public function __construct($aLoadedString) {
		if (!empty($aLoadedString)) {
			$pieces = explode("|", $aLoadedString);
			$this->file = $pieces[0];
			$this->name = $pieces[1];
		}
		else {
			$this->file = "";
			$this->name = "";
		}
			
	}
	
	public function dump() {
		echo "id: " . $this->id . "<br/>";
		echo "profile: " . $this->profile . "<br/>";
		echo "file: " . $this->file . "<br/>";
		echo "name: " . $this->name . "<br/>";
		echo "shared: " . $this->shared . "<br/>";
		echo "visible: " . $this->visible . "<br/>";
		echo "priority: " . $this->priority . "<br/>";
	}
	
}
	
	
?>