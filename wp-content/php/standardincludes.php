
<?php
require_once(ABSPATH . 'wp-content/php/db/dbdefs.php');

//echo "\nProduct: " . PRODUCT;

if (PRODUCT == 'Top100') {
    require_once(ABSPATH . 'wp-content/php/top100StandardIncludes.php');
} else if (PRODUCT == 'PoliticalFootball') {
    require_once(ABSPATH . 'wp-content/php/polfootballStandardIncludes.php');
} else if (PRODUCT == 'MyMoralCompass') {
    require_once(ABSPATH . 'wp-content/php/moralCompassStandardIncludes.php');
} else if (PRODUCT == 'CardGames') {
    //echo "\nrequiring";
    require_once(ABSPATH . 'wp-content/php/cardgamesStandardIncludes.php');
}

?>