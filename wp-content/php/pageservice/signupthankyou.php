<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');
$con = getMySqliDbConnection();

// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");
$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

$unmagic = 0;
if (isset($_POST["magicquotecheck"]))
{
	$magicQuoteCheck = $_POST["magicquotecheck"];
	//echo "Value of magicQuoteCheck is: " . $magicQuoteCheck;
	$positn = strpos($magicQuoteCheck, "'");
	//echo "Position of quote is " . $positn;
	if ($positn == 1)
		$unmagic = 1;
		//echo "Value of unmagic is: " . $unmagic;
}
$email = my_escape($_POST["email"], $unmagic);
$firstName = my_escape($_POST["firstname"], $unmagic);
$lastName = my_escape($_POST["lastname"], $unmagic);
$comments = my_escape($_POST["comments"], $unmagic);
$member = new Member();
$member->emailAddress = $email;
$member->firstName = $firstName;
$member->lastName = $lastName;
$member->comments = $comments;
$member->category = "TOP100";
$htmlForContributorFormLink = get_bloginfo('url') . "/contributor-form?existingcontributor=" . $member->emailAddress;

$songContributions = array();
for ($i=0;$i<20;$i++) {
	$label = "songtitle" . strval($i);
	$title = my_escape($_POST[$label], $unmagic);
	if (empty($title) ) {
		break;
	}
	$songContribution = new SongContribution();
	$songContribution->title = $title;
	$label2 = "songartist" . strval($i);
	$songContribution->artist = my_escape($_POST[$label2], $unmagic);
	$label3 = "videoURL" . strval($i);
	$songContribution->videoURL = my_escape($_POST[$label3], $unmagic);
	array_push($songContributions, $songContribution);
}

updateSongContributions($con, $member, $songContributions);

insertPageView($con, get_the_title(), "SIGNUPTHANKYOU", "PAGES", $g_profile, null);
$subject = 'New Top 100 contribution';

$message = "A user " . $email . " added or updated contributions for top 100.";

//send email to admin
//wp_mail( 'mattosber@gmail.com', $subject, $message);


$result = wp_mail( 'mattosber@gmail.com', $subject, $message);
//$sql = "select * from subscription_new where email='" . $email . "' and category='TOP100'";

//echo $sql;

/*
$result = mysql_query($sql);
if ($row = mysql_fetch_array($result)) {
	$alreadySubscribed = true;
}
else
	{
		$alreadySubscribed = false;
		$sql = "insert into subscription_new (email, first_name, last_name, comments, category) values ('" . $email . "','" .
			$firstName . "','" . $lastName . "','" . $comments . "','" . "TOP100" . "')";
		$result = mysql_query($sql);
		if ($result != 1)
		{
			echo "Error subscribing --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
		$subject = 'New Top 100 signup';

		$message = "A new user " . $email . " is now signed up for top 100.";

		//send email to admin
		//wp_mail( 'mattosber@gmail.com', $subject, $message);


		$result = wp_mail( 'mattosber@gmail.com', $subject, $message);
	}
*/



mysqli_close($con);



?>