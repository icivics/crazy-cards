<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();

// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");

$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

insertPageView($con, get_the_title(), "SONGLIST", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$grouping = $_GET["grouping"];

$allSongs = array();
$arrSongLinkHtml = array();
$arrFeaturedGraphicHtml = array();
$arrAnchorHtml = array();

$top10Songs = array();
$arrTop10SongLinkHtml = array();
$arrTop10FeaturedGraphicHtml = array();

$allSongListGroupings = getAllSongListGroupings($con);
$songListGrouping = new SongListGrouping();
$songListGrouping->id = 0;
$songListGrouping->first = 1;
$songListGrouping->last = 100;
/*
if (empty($grouping)) {
	$songListGrouping = findCurrentSongListGrouping($allSongListGroupings);
} else if ($grouping != "ALL") {
	$songListGrouping = findSongListGroupingById($grouping, $allSongListGroupings);
	// unless they are an editor, don't let them see non-visible groupings
	if (!$songListGrouping->visible && !loggedInUserCanEdit() ) {
		$songListGrouping = null;
	}
	if ($songListGrouping == null) {
		$songListGrouping = findCurrentSongListGrouping($allSongListGroupings);
	}
} else {
	$songListGrouping = new SongListGrouping();
	$songListGrouping->id = 0;
	$songListGrouping->first = 11;
	$songListGrouping->last = 100;
}


if ($grouping == "ALL") {
	$top10Songs =  getSongsAndFeaturedGraphics($con, 1, 10, false, false);
	//shuffle($top10Songs);
	foreach ($top10Songs as $song) {
        if ($song->sequence > 0) {
            $numberId = $song->sequence;
        } else if ($song->sequence == -101) {
            $numberId = "???";
        } else if ($song->sequence == -102) {
            $numberId = "TOP 10";
        }
        $top10LinkHtml =
				"<a href='" . get_bloginfo('url') . "/song?topic=" . $song->shortName . "&grouping=" . $songListGrouping->id
				. "'>" . $numberId . ". " . stripslashes($song->name) .
				"</a><br/>" . $song->metadata;
		array_push($arrTop10SongLinkHtml, $top10LinkHtml);
		$top10featuredGraphicHtml =
			"<a href='" . get_bloginfo('url') . "/song?topic=" . $song->shortName  . "&grouping=" . $songListGrouping->id
			. "'><img src='" . content_url() . "/images/" . $song->shortName . "/" . $song->featuredGraphic . "'/>"
				. "</a>";
		array_push($arrTop10FeaturedGraphicHtml, $top10featuredGraphicHtml);
	}
}
*/
$others = false;
$allSongs = getSongsAndFeaturedGraphics($con, $songListGrouping->first, $songListGrouping->last, $others, false);
foreach ($allSongs as $song) {
	if ($song->sequence > 0) {
		$numberId = $song->sequence;
	} else if ($song->sequence == -101) {
		$numberId = "???";
	} else if ($song->sequence == -102) {
		$numberId = "TOP 10";
	}
	$anchorHtml = "<a name='NUMBER" . $numberId . "'/>";
	array_push($arrAnchorHtml, $anchorHtml);
	$songLinkHtml =
	"<div class='countdown-title'><a href='" . PERMALINKBASE . "song?topic=" . $song->shortName . "&grouping=" . $songListGrouping->id
		. "'>" . $numberId . ". " . stripslashes($song->name) . 
	"</a></div><br/>" .
    "<div class='countdown-metadata'>" . $song->metadata . "</div>";
	array_push($arrSongLinkHtml, $songLinkHtml);
	$featuredGraphicHtml =
		"<div class='countdown-image'><a href='" . PERMALINKBASE . "song?topic=" . $song->shortName  . "&grouping=" . $songListGrouping->id
		. "'><img src='" . content_url() . "/images/" . $song->shortName . "/" . $song->featuredGraphic . "'/>"
		. "</a></div>";
	array_push($arrFeaturedGraphicHtml, $featuredGraphicHtml);
}


$title = "The Top 100";

$con->close();

?>