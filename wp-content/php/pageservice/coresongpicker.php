<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$mediaItemDao = new MediaItemDao($con);
// initialize all of state managed variables
mmc_initCurrentStateAndProfile(PRODUCT);

$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;
$mediaItemCount = $mediaItemDao->mediaItemCount("MUSIC");

$category = $_GET['category'];
if (empty($category)) {
    $category = "MUSIC";
}
$mediaCategoryDao = new MediaCategoryDao($con);
$dataTableOutputter = new DataTableOutputter();

$mediaCategories = $mediaCategoryDao->getAllMediaCategories();
$myMediaCategory = $mediaCategoryDao->findMediaCategoryByName($mediaCategories, $category);

insertPageView($con, get_the_title(), "SONGPICKER", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

//$title = "Matt's Top 100";

mysqli_close($con);

function selectedIfNecessary($selectionCategory, $chosenCategory) {
    if (strcasecmp($selectionCategory, $chosenCategory) == 0) {
        return "SELECTED";
    } else {
        return "";
    }
}

?>