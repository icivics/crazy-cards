<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();

mmc_initCurrentStateWithCon($con);
//$fullProfile = mmc_getCurrentState()->getProfile();
$fullProfile = getCurrentFullProfile($con, mmc_getCurrentState()->getProfile()->shortName, null);

$topic="CollageGame";

$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

insertPageView($con, get_the_title(), "GAME", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());



$sql = "Select * from topic where topic.short_name = '".$topic ."'";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "fetch topic");
$row = mysqli_fetch_assoc($result);
$title = $row['title'];

$topicSelections = fetchTopicSelections($con, $topic);
$topicSelections = narrowDownHistoricalFiguresBasedOnProfile($con, $topicSelections, $fullProfile);

$selectionIndex = 0;
$numberOfSelections = count($topicSelections);
$numberOfResources = 0;
$articleSelection = -1;

$resourceSelections = Array();
$resourceTags = Array();
$resourceHtml = Array();
$origResourceHtml = Array();
while ($selectionIndex < count($topicSelections) )
{
	$aSelection = $topicSelections[$selectionIndex];
	switch ($aSelection->type)
	{
		case Selection::FEATUREDGRAPHIC:
			$resourceTags[$numberOfResources] = $aSelection->article;
			$origResourceHtml[$numberOfResources] = 
				"<a href='" . PERMALINKBASE . "historical-figures?topic=" . $resourceTags[$numberOfResources] .
				"&" . profileAndSubscriberString($g_profile, $g_profileSubscriber->subscriberId) . "'>" .
				"<img class='aligncenter wp-post-image' src='" . ABSURL . "wp-content/images/" . $topic . "/" . $aSelection->image . "' border='0' width='150' height='150' />" .
				"</a>";
			// no longer using the breadcrumb for this purpose
			//if ($aSelection->breadcrumb != null && !empty($aSelection->breadcrumb) )
			//{
			//	$origResourceHtml[$numberOfResources] = $origResourceHtml[$numberOfResources] . "<br/><div align='left'>" . $aSelection->breadcrumb . "</div>";
			//}
			$numberOfResources = $numberOfResources + 1;
			break;
		default:
//			echo "other type of selection: " . $aSelection->type . "<br/>";
			break;
	}
	$selectionIndex = $selectionIndex + 1;
}


$whoswhoOrder = mmc_getCurrentState()->getWhoswhoOrder();
$whoswhoProfile = mmc_getCurrentState()->getWhoswhoProfile();
if (!empty($whoswhoOrder) && !empty($whoswhoProfile)) {
	//echo "whoswhoorder not empty";
	// add in matching of profile
	$resourceSelections = explode(',', $whoswhoOrder);
} else {
	//echo "whoswhoorder IS empty";
	for ($i=0;$i<$numberOfResources;$i++) {
		$resourceSelections[$i] = $i;
	}
	shuffle($resourceSelections);
	$strSelections = implode(',', $resourceSelections);
	mmc_getCurrentState()->setWhosWhoOrder($strSelections);
	mmc_getCurrentState()->setWhosWhoProfile(mmc_getCurrentState()->getProfileName());
}

for ($i=0;$i<$numberOfResources && $i < 20;$i++) {
	$resourceHtml[$i] = $origResourceHtml[$resourceSelections[$i]];
}

$con->close();

?>