<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$playlistDao = new PlaylistDao($con);
$tileDao = new TileDao($con);
// initialize all of state managed variables
mmc_initCurrentStateAndProfile(PRODUCT);
$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

$tiles = $tileDao->getTilesForPage("politicalfootball");

$tileOutputter = new TileOutputter($tiles);

insertPageView($con, get_the_title(), "POLFOOTBALLMAIN", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$title = "The Political Football";

mysqli_close($con);

?>