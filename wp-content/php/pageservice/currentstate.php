<?php 
global $g_currentState;

function  mmc_getCurrentState() {
	global $g_currentState;
	return $g_currentState;
}

function mmc_initCurrentState() {
	global $g_currentState;
	$g_currentState = new SessionState();
	$g_currentState->initCurrentSessionState();
}

function mmc_initCurrentStateWithCon($con) {
    global $g_currentState;
    $g_currentState = new SessionState();
    $g_currentState->setConnection($con);
    $g_currentState->initCurrentSessionState();
}


function mmc_initCurrentStateAndProfile($forcedProfile) {
	global $g_currentState;
	$g_currentState = new SessionState();
	if (!empty($forcedProfile)) {
		$g_currentState->setForcedProfile($forcedProfile);
	}
	$g_currentState->initCurrentSessionState();
}

?>