<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();

// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");
$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

$existingContributor = $_GET["existingcontributor"];
$existingMember = null;
$showNotFoundMessage = false;

if (!empty($existingContributor)) {
	$existingMember = getMember($con, $existingContributor);
}
if ($existingMember != null) {
	$existingSongs = getSongContributions($con, $existingContributor);
} else {
	$existingSongs = array();
	for ($i=0;$i<5;$i++) {
		$songContribution = new SongContribution();
		array_push($existingSongs, $songContribution);
	}
}
if (!empty($existingContributor) && $existingMember == null) {
	$showNotFoundMessage = true;
}

insertPageView($con, get_the_title(), "CONTRIBUTORFORM", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$title = "Contribute to Top 100";

mysqli_close($con);

?>