<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/6/2019
 * Time: 7:32 AM
 */
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$current_user = wp_get_current_user();
$gameSession = null;


$con = getMySqliDbConnection();
$gameResultDao = new GameResultDao($con);

$gameSessionId = $_GET["gamesessionid"];
if (empty($gameSessionId)) {
    $gameSessionId = -1;
} else {
    $gameSession = $gameResultDao->fetchGameSession($gameSessionId);
    $gameSession->recalc();
}

$category = $_GET["category"];
if (empty($category)) {
    $category = (PRODUCT == "PoliticalFootball" ? "trump" : "songoftheweek");
}

mmc_initCurrentStateAndProfile(PRODUCT);

// set up board in two divs - one to show their score, and one to wrap up and market

$con->close();
?>