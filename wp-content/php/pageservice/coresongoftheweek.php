<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/24/2017
 * Time: 11:55 AM
 */

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$playlistEntryDao = new PlaylistEntryDao($con);
$playlistDao = new PlaylistDao($con);

$id = $_GET['id'];
$playlist = $playlistDao->getPlaylistByName("Song of the Week");
if ($playlist != null) {
    if (empty($id)) {
        $songOfTheWeekEntry = null;
        foreach ($playlist->arrPlaylistEntries as $entry) {
            if ($songOfTheWeekEntry == null || $songOfTheWeekEntry->sequence < $entry->sequence) {
                $songOfTheWeekEntry = $entry;
            }
        }
        $redirectTo = "one-song?guid=" . $playlist->guid . "&id=" . $songOfTheWeekEntry->mediaItemId;
    } elseif (!empty($id) && $playlistEntryDao->playlistEntryExists($playlist->id, $id)) {
        $redirectTo = "one-song?guid=" . $playlist->guid . "&id=" . $id;
    } else {
        $redirectTo = "playlist?guid=" . $playlist->guid;
    }
} else {
    $redirectTo = "song-picker";
}
header('Location: '.$redirectTo);

?>