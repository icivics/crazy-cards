<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../../Datatables-Editor-1.6.5/php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

$category = $_GET['category'];
if (empty($category)) {
    $category = 'MUSIC';
}

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'media_item' )
    ->field(
        Field::inst( 'id' ),
        Field::inst( 'title' ),
        Field::inst( 'artist' ),
        Field::inst( 'album' ),
        Field::inst( 'genre' ),
        Field::inst('released'),
        Field::inst('tv_show'),
        Field::inst('movie'),
        Field::inst('event'),
        Field::inst('product'),
        Field::inst('subject'),
        Field::inst('categories')
    )
    ->where(function ( $q ) use ( $category ) {
        if (strcasecmp($category, 'all') != 0) {
            $q->where('categories', '%' . $category . '%', 'LIKE');
        }
    } )
    ->process($_POST)
    ->json();