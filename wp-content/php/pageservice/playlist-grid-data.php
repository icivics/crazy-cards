<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../../Datatables-Editor-1.6.5/php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Options,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

$playlistId = $_GET['playlistid'];

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'playlist_media_item' )
    ->field(
        Field::inst( 'playlist_media_item.id' ),
        Field::inst( 'playlist_media_item.playlist_id' ),
        Field::inst( 'playlist_media_item.sequence' )->validator( 'Validate::numeric' ),
        Field::inst( 'media_item.title' ),
        Field::inst( 'media_item.artist' )
    )
    ->leftJoin( 'media_item',     'playlist_media_item.media_item_id','=', 'media_item.id' )
    ->where('playlist_media_item.playlist_id', $playlistId)
    ->process($_POST)
    ->json();