<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$playlistDao = new PlaylistDao($con);
$playlistEntryOutputter = new PlaylistEntryOutputter();

// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");

$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;
$guid =  $_GET['guid'];

insertPageView($con, "Playlist", "SONG", $guid, mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$playlist = $playlistDao->getPlaylistByGuid($guid);
$title = $playlist->name;

mysqli_close($con);

$playlistEntryOutputter = new PlaylistEntryOutputter();

?>