<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();

// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");
$topic = $_GET["topic"];
$grouping = $_GET["grouping"];
$autoplay = $_GET["autoplay"];
$seq = $_GET["seq"];

$featuredGraphicSelection = -1;
$featuredGraphicHtml = "";

insertPageView($con, get_the_title(), "SONG", $topic, mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$sql = "Select * from topic where topic.topic_type='SONG' and topic.short_name = '".$topic ."'";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "fetching topic");
$row = mysqli_fetch_assoc($result);
$numberInCountdown = intval($row['sequence']);
$songListAnchor = null;
if ($numberInCountdown == -2) {
	$countdownString = "<img src='" . ABSURL . "wp-content/images/Top10.jpg'/>";
} elseif ($numberInCountdown == -1) {
	$countdownString = "???";
} elseif ($numberInCountdown == 0) {
	$countdownString = "";
} else {
	// fix this horror show soon, number is
	$numberInCountdown -= 100;
	$countdownString = "#" . strval($numberInCountdown);
    $songListAnchor = "#NUMBER" . $numberInCountdown;
}
$allSongListGroupings = getAllSongListGroupings($con);
if (!empty($grouping)) {
	$currentGrouping = findSongListGroupingById($grouping, $allSongListGroupings);
} else {
	$currentGrouping = findSongListGroupingByNumber($numberInCountdown, $allSongListGroupings);
}

$title = stripslashes($row['title']);
//echo "title: " . $title . "  description: " . $description;
if (!empty($title)) {
	$topicSelections = fetchTopicSelections($con, $topic);
	$selectionIndex = 0;
	$numberOfSelections = count($topicSelections);
	$numberOfResources = 0;
	$articleSelection = -1;
	while ($selectionIndex < count($topicSelections))
	{
		$aSelection = $topicSelections[$selectionIndex];
		switch ($aSelection->type)
		{
			case Selection::FEATUREDGRAPHIC:
				$featuredGraphicSelection = $selectionIndex;
				$featuredGraphicHtml = "<img src='" . content_url() . "/images/" . $topic . "/" .
					$aSelection->image . "' border='0' width='120' height='90' />";
				$featuredGraphicHtml = str_replace("\"", "\\\"", $featuredGraphicHtml);
				break;
			case Selection::INTRO:
				$metadataSelection = $selectionIndex;
				break;
			case Selection::ARTICLE:
				$articleSelection = $selectionIndex;
				$articleText = $topicSelections[$articleSelection]->article;
				$articleText = str_replace("@TOP10LINK/", get_bloginfo('url') . "/song?grouping=" . $currentGrouping->id . "&topic=", $articleText);
				$articleText = str_replace("@TOP10IMAGE", "<img src='" . content_url() . "/images/Top10.jpg'/>", $articleText);
				break;
			case Selection::VIDEO:
				$resourceSelections[$numberOfResources] = $selectionIndex;
				if (empty($mainResourceHtml)) {
					$mainResourceHtml = fixupVideoEmbed($aSelection->videoEmbedCode);
				}
				$resourceThumbnailHtml[$numberOfResources] = 
					"<figure>" .
					"<a href='javascript:setResource(" . strval($numberOfResources) . ", \"visible\")'><img src='" . content_url() . "/images/" . $topic . "/" .
					$topicSelections[$selectionIndex]->thumbnail .
					"' width='120' height='90' alt='' /></a>" .
					"<figcaption>" .
					$topicSelections[$selectionIndex]->breadcrumb .
					"</figcaption></figure>";
				
				
				$resourceHtml[$numberOfResources] = fixupVideoEmbed($aSelection->videoEmbedCode);
				if ($aSelection->caption != null && !empty($aSelection->caption) )
				{
					$resourceHtml[$numberOfResources] = $resourceHtml[$numberOfResources] . "<br/><br/><div align='left'>" . $aSelection->caption . "</div>";
				}
				//$resourceHtml[$numberOfResources] = str_replace("\"", "\\\"", $resourceHtml[$numberOfResources]);
				$numberOfResources = $numberOfResources + 1;
				break;
			case Selection::LINKS:
				$linksSelection = $selectionIndex;
				// Don't know if we need these replacements ...
				$aSelection->article = str_replace("@BLOGINFO", get_bloginfo('url'), $aSelection->article);
				$aSelection->article = str_replace("@PROFILE", "profile=" . $fullProfile->shortName, $aSelection->article);
				break;
			default:
				break;
		}
		$selectionIndex = $selectionIndex + 1;
	}
}

mysqli_close($con);
