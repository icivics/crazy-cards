<?php

require_once(ABSPATH . 'wp-content/php/db/dbdefs.php');
require_once(ABSPATH . 'wp-content/php/dao/songlistmethods.php');
require_once(ABSPATH . 'wp-content/php/dao/dbfunctions2.php');
require_once(ABSPATH . 'wp-content/php/dao/MediaItemDao.php');
require_once(ABSPATH . 'wp-content/php/dao/MediaCategoryDao.php');
require_once(ABSPATH . 'wp-content/php/dao/PlaylistDao.php');
require_once(ABSPATH . 'wp-content/php/dao/PlaylistEntryDao.php');
require_once(ABSPATH . 'wp-content/php/dao/profilemethods.php');
require_once(ABSPATH . 'wp-content/php/dao/CategoryTopicDao.php');
require_once(ABSPATH . 'wp-content/php/dao/GameCardDao.php');
require_once(ABSPATH . 'wp-content/php/dao/GameResultDao.php');
require_once(ABSPATH . 'wp-content/php/dao/TileDao.php');

require_once(ABSPATH . 'wp-content/php/model/video.class.php');
require_once(ABSPATH . 'wp-content/php/model/songlistgrouping.class.php');
require_once(ABSPATH . 'wp-content/php/model/songbean.php');
require_once(ABSPATH . 'wp-content/php/model/mediacategory.class.php');
require_once(ABSPATH . 'wp-content/php/model/mediaentrybean.php');
require_once(ABSPATH . 'wp-content/php/model/member.class.php');
require_once(ABSPATH . 'wp-content/php/model/playlist.class.php');
require_once(ABSPATH . 'wp-content/php/model/playlistentry.class.php');
require_once(ABSPATH . 'wp-content/php/model/profile.class.php');
require_once(ABSPATH . 'wp-content/php/model/gamecard.class.php');
require_once(ABSPATH . 'wp-content/php/model/gamesession.class.php');
require_once(ABSPATH . 'wp-content/php/model/gameboardresult.class.php');
require_once(ABSPATH . 'wp-content/php/model/topicselection.class.php');
require_once(ABSPATH . 'wp-content/php/model/tile.class.php');
require_once(ABSPATH . 'wp-content/php/model/namevaluepair.class.php');
require_once(ABSPATH . 'wp-content/php/model/sessionstate.class.php');

require_once(ABSPATH . 'wp-content/php/pageservice/currentstate.php');
require_once(ABSPATH . 'wp-content/php/pageservice/insertpageview.php');
require_once(ABSPATH . 'wp-content/php/pageservice/songcontribution.php');

require_once(ABSPATH . 'wp-content/php/output/MediaOutputter.php');
require_once(ABSPATH . 'wp-content/php/output/PlaylistEntryOutputter.php');
require_once(ABSPATH . 'wp-content/php/output/DataTableOutputter.php');
require_once(ABSPATH . 'wp-content/php/output/TileOutputter.php');

require_once(ABSPATH . 'wp-content/php/util/contentfilter.php');
require_once(ABSPATH . 'wp-content/php/util/mysqliutil.php');
require_once(ABSPATH . 'wp-content/php/util/util.php');

$con = getMySqliDbConnection();

// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");

$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

insertPageView($con, get_the_title(), "TOP100AUTOPLAY", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$startAutoplay = $_POST["startautoplay"];
$shuffle = $_POST["shuffle"];
$command = $_GET["command"];
$autoplay = $_GET["autoplay"];
$topic = $_GET["topic"];

$seq = intval($_GET["seq"]);

if ($startAutoplay == "1" || !empty($command) ) {
	$songSelections = array();
	$allSongs = getAllAvailableSongs($con);
	if ($startAutoplay == "1") {
		for ($i=0;$i<count($allSongs);$i++) {
			$songSelections[$i] = $i;
		}
		if ($shuffle == "1") {
			shuffle($songSelections);
		}
		$strSelections = implode(',', $songSelections);
		mmc_getCurrentState()->setSongOrder($strSelections);
		//echo "Startautoplay: " . $startAutoplay . "  shuffle: " . $shuffle;
		$songToStart = $allSongs[$songSelections[0]];
		$url = get_bloginfo('url') . "/song?topic=" . $songToStart->shortName . "&seq=0&autoplay=1";
		//echo "going to redirect " . $url . "<br/>";
		redirect($url);
	} elseif (strcmp($command, "next") == 0) {
		$strSelections = mmc_getCurrentState()->getSongOrder();
		if (empty($strSelections)) {
			for ($i=0;$i<count($allSongs);$i++) {
				$songSelections[$i] = $i;
			}
			if ($shuffle == "1") {
				shuffle($songSelections);
			}
			$strSelections = implode(',', $songSelections);
			mmc_getCurrentState()->setSongOrder($strSelections);
		} else {
			$songSelections = explode(',', $strSelections);
		}
		$seq = $seq + 1;
		if ($seq < count($songSelections)) {
			$songToStart = $allSongs[$songSelections[$seq]];
			$url = PERMALINKBASE . "song?topic=" . $songToStart->shortName . "&seq=" . $seq . "&autoplay=1";
			//echo "going to redirect " . $url . "<br/>";
			redirect($url);
		}
	} elseif (strcmp($command, "nextmanual") == 0) {
		//echo "doing nextmanual";
		$current = findSongIndex($topic, $allSongs);
		$current = $current + 1;
		if ($current >= count($allSongs)) {
			$url = PERMALINKBASE . "songlist";
		} else {
			$songToStart = $allSongs[$current];
			$url = PERMALINKBASE . "song?topic=" . $songToStart->shortName;
		}
		redirect($url);
	}
}
mysqli_close($con);

?>