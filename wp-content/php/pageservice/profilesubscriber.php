<?php
	const SUBSCRIBER_NONE = 0;
	const SUBSCRIBER_VALID = 1;
	const SUBSCRIBER_INVALID = 2;
	
	const MODE_NO_SUBSCRIBER = 0;
	const MODE_LOGGEDIN_USER = 1;
	const MODE_VALID_SUBSCRIBER = 2;

	global $g_profileSubscriber;
	
	$g_profileSubscriber = new ProfileSubscriber();

	$subscriberId = $_GET["subscriberid"];
	if (!empty($subscriberId)) {
		$g_profileSubscriber->subscriberId = $subscriberId;
	}
?>