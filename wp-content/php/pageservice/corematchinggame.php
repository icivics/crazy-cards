<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/16/2019
 * Time: 11:38 AM
 */
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

function modifyGameCards($gameCards) {
    $songOfTheWeekToken = "@SONGOFTHEWEEK";
    foreach ($gameCards as $gameCard) {
        if (($position = strpos($gameCard->blurb, $songOfTheWeekToken)) == false) {
            continue;
        }
        $searchStr = substr($gameCard->blurb, $position, strlen($songOfTheWeekToken) + 11);
        $position += strlen($songOfTheWeekToken) + 1;
        // put divs and css in here to make a fancy song of the week link box
        $dateStr = substr($gameCard->blurb, $position, $position + 10);
        $ymd = DateTime::createFromFormat('Y-m-d', $dateStr)->format('F j, Y');
        $fullLink = "<a href='http://matts-top-100.com/musicblog/#" . $dateStr .
            "' target='_BLANK'>Song of the Week:  " . $ymd . "</a>";
        $gameCard->blurb = str_replace($searchStr, $fullLink, $gameCard->blurb);
        $gameCard->blurb = convertCRtoBR($gameCard->blurb);
    }
    return $gameCards;
}

function getGameTitle($category) {
    if (strcasecmp($category, "songoftheweek") == 0) {
        return "Song of the Week Game";
    } else if (strcasecmp($category, "icivicsgifs") == 0) {
        return "iCivics GIFs Game";
    } else if (strcasecmp($category, "trump") == 0) {
        return "Trump Game";
    } else {
        return "Disorder in the Court!";
    }
}


$current_user = wp_get_current_user();

$category = $_GET["category"];

if (empty($category)) {
    $category = (PRODUCT == "PoliticalFootball" ? "trump" : "songoftheweek");
}

$cheat = $_GET["cheat"];
if (empty($cheat) || $cheat != "1") {
    $cheat = "0";
}

$debug = $_GET["debug"];
if (empty($debug) || $debug != "1") {
    $debug = "0";
}

$gameSessionId = $_GET["gamesessionid"];
if (empty($gameSessionId)) {
    $gameSessionId = -1;
}
$newGame = ($gameSessionId == -1 ? 1 : 0);

$con = getMySqliDbConnection();
$gameCardDao = new GameCardDao($con);
$gameResultDao = new GameResultDao($con);

$categories = array();
array_push($categories, $category);
$arrGameCategoryMetadata = $gameCardDao->getCategoryMetadata($category);
$allGameCards = $gameCardDao->getAllGameCards($categories);

$allGameCards = modifyGameCards($allGameCards);

shuffle($allGameCards);
$bonusIndex = rand(0, 5);
$allGameCards[$bonusIndex]->bonus = 1;
$gameCardHolders = array();
for ($i=0;$i<6;$i++) {
    $card = $allGameCards[$i];
    $card->displayMode = 0;
    array_push($gameCardHolders, $card);
    $card2 = clone $card;
    $card2->displayMode = 1;
    array_push($gameCardHolders, $card2);
}

shuffle($gameCardHolders);
$gameBoardResult = new GameBoardResult();
$gameBoardResult->id = -1;
$gameBoardResult->gameSessionId = $gameSessionId;
$gameBoardResult->category = $category;
$gameBoardResult->points = 0;
$gameBoardResult->cheat = $cheat;
$gameBoardResult->username = $current_user->user_email;

$gameBoardResult = $gameResultDao->upsert($gameBoardResult);

$gameResultId = $gameBoardResult->id;
$gameSessionId = $gameBoardResult->gameSessionId;

$gameSession = $gameResultDao->fetchGameSession($gameSessionId);
$gameSession->recalc();
mmc_initCurrentStateAndProfile("CARDGAMES");

insertPageView($con, get_the_title(), "MATCHINGGAME", "PAGES");

$con->close();

$useMatchingGameEngine = 1;
$updaterURL = content_url() . '/php/pageservice/gameresultupdater.php';