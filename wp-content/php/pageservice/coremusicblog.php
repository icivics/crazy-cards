<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 3/16/2019
 * Time: 8:34 PM
 */

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

function findPastEntry($playlist, $monthsAgo) {
    $modifier = "-" . $monthsAgo . "months";
    $targetDate = date('Y-m-d', strtotime($modifier));
    //echo "Date for " . $monthsAgo . " months ago is " . date('Y-m-d', $targetDate) . "<br/>";
    for($i=0; $i < count($playlist->arrPlaylistEntries) - 1;$i++) {
        $entry = $playlist->arrPlaylistEntries[$i];
        if ($entry->postDate <= $targetDate) {
            return $entry->postDate;
        }
    }
    return null;
}

$con = getMySqliDbConnection();
$playlistEntryDao = new PlaylistEntryDao($con);
$playlistDao = new PlaylistDao($con);
$playlistEntryOutputter = new PlaylistEntryOutputter();
$id = $_GET['id'];
$playlist = $playlistDao->getPlaylistByName("Song of the Week");

$latestEntry = $playlist->arrPlaylistEntries[0]->postDate;
$entry3MonthsAgo = findPastEntry($playlist, 3);
$entry6MonthsAgo = findPastEntry($playlist, 6);
$entry1YearAgo = findPastEntry($playlist, 12);

mmc_initCurrentStateAndProfile("TOP100");
$secondaryTitle = "Song of the Week";

insertPageView($con, get_the_title(), "MUSICBLOG", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$con->close();
if ($playlist == null) {
    $redirectTo = "song-picker";
    header('Location: '.$redirectTo);
}
?>