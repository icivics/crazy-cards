<?php

require_once(ABSPATH . 'wp-content/php/dbdefs.php');
require_once(ABSPATH . 'wp-content/php/topicselection.class.php');


$topic = $_GET["topic"];

$con = getMySqliDbConnection();

$sql = "Select * from topic where topic.short_name = '".$topic ."'";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "fetching user grades");
$row = mysqli_fetch_assoc($result);

$title = $row['title'];
$authorId = $row['author'];
$authorDisplayName = get_the_author_meta( "display_name", $authorId );

$sql = "SELECT score.id, score.who, user_score.user_rating, user_score.description, user_score.approval_status, user_score.public, " .
	"user_score.user_id, mc_users.display_name " .
	"FROM (user_score INNER JOIN (score INNER JOIN (selection INNER JOIN topic ON selection.topic = topic.id) " .
	"ON score.selection = selection.id) ON user_score.score_id = score.id) INNER JOIN mc_users ON user_score.user_id = mc_users.ID " .
	"WHERE (((topic.short_name)='" . $topic . "')) ORDER BY user_score.user_id, score.id";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "fetching user grades");
$currentUserId = -1;
$currentUserScoreListItem = null;
$allUserScoreListItems = array();
while($row = mysqli_fetch_assoc($result))
{
	$userId = $row['user_id'];
	if ($userId != $currentUserId)
	{
		if ($currentUserScoreListItem != null)
			array_push($allUserScoreListItems, $currentUserScoreListItem);
		$currentUserScoreListItem = new UserScoreListItem();
		$currentUserScoreListItem->userId = $userId;
		$currentUserScoreListItem->userDisplayName = $row['display_name'];
		$currentUserScoreListItem->topicShortName = $topic;
		$currentUserId = $userId;
	}
	$scoreItem = new ScoreItem();
	$scoreItem->id = $row['id'];
	$scoreItem->who = $row['who'];
	$scoreItem->score = $row['user_rating'];
	$scoreItem->description = $row['description'];
	$scoreItem->ispublic = $row['public'];
	array_push($currentUserScoreListItem->scoreItems, $scoreItem);
}
if ($currentUserScoreListItem != null)
	array_push($allUserScoreListItems, $currentUserScoreListItem);
mysqli_close($con);
/*
foreach ($allUserScoreListItems as $userScoreListItem)
{
	echo "List item user id: " . $userScoreListItem->userId . "<br/>";
	echo "Display name: " . $userScoreListItem->userDisplayName . "<br/>";
	echo "Topic: " . $userScoreListItem->topicShortName . "<br/>";
	foreach ($userScoreListItem->scoreItems as $scoreItem)
	{
		echo "Who: " . $scoreItem->who . "<br/>";
		echo "Score: " . $scoreItem->score . "<br/>";
		echo "Description: " . $scoreItem->description . "<br/>";
	}
	echo "<br/>";
}
*/
?>