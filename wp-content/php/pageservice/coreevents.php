<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();


// initialize all of state managed variables
mmc_initCurrentStateWithCon($con);
// workaround
//mmc_initCurrentStateAndProfile("MYMORALCOMPASS");
//$fullProfile = mmc_getCurrentState()->getProfile();
$fullProfile = getCurrentFullProfile($con, mmc_getCurrentState()->getProfile()->shortName, null);

//$fullProfile = mmc_getCurrentState()->getProfile();

// ALL needs to be reevaluated and cleaned up
$theBackgroundImage = "";

$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;
$initial_state = $_GET["state"];
$submitMyScorecard = $_POST["submitMyScorecard"];
$submitMyWriting = $_POST["submitMyWriting"];
$submitTeacherOptionsForm = $_POST["submitteacheroptionsform"];
$configId = $_GET["configid"];

if (isset($_POST["student_user_id"]))
    $currentStudentId = $_POST["student_user_id"];
else
    $currentStudentId = -1;


$introSelection = -1;
$articleSelection = -1;
$scoreCardSelection = -1;
$linksSelection = -1;
$featuredGraphicSelection = -1;
$featuredGraphicHtml = "";
$numberOfResources = 0;
$numberOfAnalysis = 0;


if (mmc_getCurrentState()->getSubscribeCurrentUser()) {
    subscribeCurrentUser($con, $fullProfile);
}

mmc_getCurrentState()->setTopicAndConfig($con);


$topicConfiguration = fetchTopicConfiguration($con, mmc_getCurrentState()->getTopicConfigId(),
    mmc_getCurrentState()->getClassroomCode());

$g_visibleInstructions = createInstructions($topicConfiguration);


if ($submitMyWriting) {
    // I'd like to find another way other than hard-coding to 8
    for ($i = 1; $i <= 8; $i++) {
        $writingItem = new WritingItem();
        $writingItem->id = $_POST["mywriting_id" . $i];
        $writingItem->selectionId = $_POST["mywriting_selectionid" . $i];
        $writingItem->text = $_POST["mywriting_text" . $i];
        if (!empty($writingItem->id)) {
            if ($writingItem->id == -1) {
                $sql = "Insert into user_writing (user_id, selection_id, text, approval_status) values ('" . $wp_user_id . "','" . $writingItem->selectionId .
                    "', '" . my_escape($writingItem->text) . "', '1')";
            } else {
                $sql = "Update user_writing set text = '" . my_escape($writingItem->text) .
                    "' where id = " . $writingItem->id;

            }
            //echo $sql;
            $result = mySqli_query_wrapper($con, $sql, "update writing");
            if ($result != 1) {
                echo "Error inserting user writing --- Result = " . $result . "  error: " . mysql_error();
                echo "SQL is " . $sql;
                exit();
            }
        }
    }
    //exit();
}

if ($submitMyScorecard) {
    //foreach ($_POST as $key => $value){
    //	echo "key: " . $key . "   value: " . $value;
    //}

    $emailForActivity = 0;
    for ($i = 1; $i <= 8; $i++) {
        $scoreItem = new ScoreItem();
        $scoreItem->id = $_POST["myscorecard_record" . $i];
        $scoreItem->originalScoreId = $_POST["myscorecard_scoreid" . $i];
        $scoreItem->score = $_POST["myscorecard_userrating" . $i];
        $scoreItem->description = $_POST["myscorecard_description" . $i];
        if ($_POST["public"] == "public")
            $scoreItem->ispublic = 1;
        else
            $scoreItem->ispublic = 0;
        $arrScoreItems[$i - 1] = $scoreItem;
        //echo "Scorecard item " . $i . " is " . $arrScoreItems[$i-1]->id . " " . $arrScoreItems[$i-1]->originalScoreId . " " . $arrScoreItems[$i-1]->score . " " . $arrScoreItems[$i-1]->description . "<br/>";
        if (!empty($scoreItem->id)) {
            if ($wp_user_id <= 0) {
                $emailForActivity = 1;
            } else {
                if ($scoreItem->id == -1) {
                    $sql = "Insert into user_score (user_id, score_id, user_rating, description, approval_status, public) values ('" . $wp_user_id . "', '" . $scoreItem->originalScoreId .
                        "', '" . $scoreItem->score . "', '" . my_escape($scoreItem->description) . "', '1', '" . $scoreItem->ispublic . "')";
                } else {
                    $sql = "Update user_score set user_rating = '" . $scoreItem->score . "', description = '" . my_escape($scoreItem->description) .
                        "', public = '" . $scoreItem->ispublic . "' where id = " . $scoreItem->id;
                }
                $result = mySqli_query_wrapper($con, $sql, "update user score");
                if ($result != 1) {
                    echo "Error inserting user_score --- Result = " . $result . "  error: " . mysql_error();
                    echo "SQL is " . $sql;
                    exit();
                } else {
                    $emailForActivity = 2;
                }
            }
        }
    }
    if ($emailForActivity == 2) {
        $subject = 'User Score Activity';
        $message = "User scores saved for user " . $wp_user_id;
        //send email to admin
        wp_mail('mattosber@gmail.com', $subject, $message);
    } else if ($emailForActivity == 1) {
        $subject = 'User Score Activity';
        $message = "An attempt was made to save user scores with $wp_user <= 0";
        //send email to admin
        wp_mail('mattosber@gmail.com', $subject, $message);
    }
}
$topic = mmc_getCurrentState()->topicName;
$profileName = mmc_getCurrentState()->getProfileName();
insertPageView($con, $topic, "STORY", $topic, $profileName, mmc_getCurrentState()->getClassroomCode());

$topicDocumentsAll = fetchDocumentsForTopicConfiguration($con, mmc_getCurrentState()->topicConfigId,
    false, mmc_getCurrentState()->classroomCodeObject->profileSubscriberId);
$topicDocumentsVisible = array();
foreach ($topicDocumentsAll as $td) {
    if ($td->shared == 0) {
        $td->profile = $profileName;
    }
    if ($td->visible) {
        array_push($topicDocumentsVisible, $td);
    }
}
$sql = "Select * from topic where topic.short_name = '" . $topic . "'";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "select topic");
$row = mysqli_fetch_assoc($result);
$title = $row['title'];
$description = $row['description'];
$authorId = $row['author'];
$authorDisplayName = get_the_author_meta("display_name", $authorId);
$authorFirstName = get_the_author_meta("first_name", $authorId);
//echo "Title is " . $title;
$topicSelections = fetchTopicSelections($con, $topic);
$selectionIndex = 0;
$numberOfSelections = count($topicSelections);
while ($selectionIndex < count($topicSelections)) {
    $aSelection = $topicSelections[$selectionIndex];
    switch ($aSelection->type) {
        case Selection::INTRO:
            $introSelection = $selectionIndex;
            break;
        case Selection::FEATUREDGRAPHIC:
            $featuredGraphicSelection = $selectionIndex;
            $featuredGraphicHtml = "<img src='" . ABSURL . "wp-content/images/" . $topic . "/" . $aSelection->image . "' border='0' />";
            $featuredGraphicHtml = str_replace("\"", "\\\"", $featuredGraphicHtml);
            break;
        case Selection::ARTICLE:
            $articleSelection = $selectionIndex;
            break;
        case Selection::VIDEO:
            $resourceSelections[$numberOfResources] = $selectionIndex;
            $resourceHtml[$numberOfResources] = fixupVideoEmbed($aSelection->videoEmbedCode);
            if ($aSelection->caption != null && !empty($aSelection->caption)) {
                $resourceHtml[$numberOfResources] = $resourceHtml[$numberOfResources] . "<br/><br/><div align='left'>" . $aSelection->caption . "</div>";
            }
            //$resourceHtml[$numberOfResources] = str_replace("\"", "\\\"", $resourceHtml[$numberOfResources]);
            $numberOfResources = $numberOfResources + 1;
            break;
        case Selection::GRAPHIC:
            $resourceSelections[$numberOfResources] = $selectionIndex;
            $resourceHtml[$numberOfResources] = "<img src='" . ABSURL . "wp-content/images/" . $topic . "/" . $aSelection->image . "' border='0' />";
            $resourceHtml[$numberOfResources] = str_replace("\"", "\\\"", $resourceHtml[$numberOfResources]);
            if ($aSelection->caption != null && !empty($aSelection->caption)) {
                $resourceHtml[$numberOfResources] = $resourceHtml[$numberOfResources] . "<br/><br/><div align='left'>" . $aSelection->caption . "</div>";
            }
            $numberOfResources = $numberOfResources + 1;
            break;
        case Selection::SCORECARD:
            $scoreCardSelection = $selectionIndex;
            break;
        case Selection::LINKS:
            $linksSelection = $selectionIndex;
            break;
        case Selection::ANALYSIS:
            $analysisSelections[$numberOfAnalysis] = $selectionIndex;
            $numberOfAnalysis = $numberOfAnalysis + 1;
            break;
        case Selection::BACKGROUND:
            $theBackgroundImage = $aSelection->image;
            break;
        default:
            break;
    }
    $selectionIndex = $selectionIndex + 1;
}


$numberOfScores = 0;
$numberOfUserScores = 0;
$userScoresVisibleForTopic = 0;

/** TODO **/
// writing prompts code will need to change
$numberOfUserWritingItems = 0;

$numberOfWritingPrompts = $topicConfiguration->getNumberOfDbqs();

if ($numberOfWritingPrompts != 0) {
    if (loggedInAsTeacher())
        $userWritingId = $currentStudentId;
    else
        $userWritingId = $wp_user_id;
}

if ($scoreCardSelection != -1 && $topicSelects[$scoreCardSelection] != null) {
    $sql = "SELECT * from score where score.selection = " . $topicSelections[$scoreCardSelection]->id;
    //echo "Scorecard items: " . $sql . "<br/";
    $result = mySqli_query_wrapper($con, $sql, "score selection");
    while ($row = mysqli_fetch_assoc($result)) {
        $aScore = new ScoreItem();
        $aScore->id = $row['id'];
        $aScore->who = $row['who'];
        $aScore->score = $row['score'];
        $aScore->description = convertUtf8($row['description']);
        $scoreCardItems[$numberOfScores] = $aScore;
        $numberOfScores = $numberOfScores + 1;
    }

    //echo "Number of scores is " . $numberOfScores;

    if (isset($wp_user_id) && $wp_user_id > 0) {
        $sql = "select us.id, sc.id, us.user_id, sc.who, us.user_rating, us.description, us.approval_status, us.public from user_score us " .
            "inner join score sc on us.score_id=sc.id where us.user_id=" . $wp_user_id . " and sc.selection= " . $topicSelections[$scoreCardSelection]->id;
        //echo $sql;
        $result = mySqli_query_wrapper($con, $sql, "scores");
        //echo "<br/>After sql error is " . mysql_error();
        while ($row = mysqli_fetch_assoc($result)) {
            $aScore = new ScoreItem();
            $aScore->id = $row[0];
            $aScore->originalScoreId = $row[1];
            $aScore->who = $row['who'];
            $aScore->score = $row['user_rating'];
            $aScore->description = $row['description'];
            $aScore->ispublic = $row['public'];
            $userScoreCardItems[$numberOfUserScores] = $aScore;
            $numberOfUserScores = $numberOfUserScores + 1;
        }
        //echo "Number of user scores is " . $numberOfUserScores;
        //if ($numberOfUserScores > 0)
        //	echo " user score card item 0 id is: " . $userScoreCardItems[0]->id;

        $sql = "SELECT user_score.user_id FROM (user_score INNER JOIN (score INNER JOIN (selection INNER JOIN topic ON selection.topic = topic.id) " .
            "ON score.selection = selection.id) ON user_score.score_id = score.id) INNER JOIN mc_users ON user_score.user_id = mc_users.ID " .
            "WHERE (((user_score.approval_status)=1) AND ((user_score.public)=1) AND ((topic.short_name)='" . $topic . "'))" .
            "GROUP BY user_score.user_id";
        $result = mySqli_query_wrapper($con, $sql, "scores");
        //while($row = mysql_fetch_array($result))
        //{
        //	$userScoresVisibleForTopic = $userScoresVisibleForTopic + 1;
        //}
        //echo "<br/>After sql error is " . mysql_error();
        $userScoresVisibleForTopic = mysql_num_rows($result);
    }
    // set up user scores but have them as blank
    // system will still see them as not entered
    if ($numberOfUserScores == 0) {
        while ($numberOfUserScores < $numberOfScores) {
            $aScore = new ScoreItem();
            $aScore->id = -1;
            $aScore->originalScoreId = $scoreCardItems[$numberOfUserScores]->id;
            $aScore->who = $scoreCardItems[$numberOfUserScores]->who;
            $aScore->score = -1;
            $aScore->description = "";
            $aScore->ispublic = 1;
            $userScoreCardItems[$numberOfUserScores] = $aScore;
            $numberOfUserScores = $numberOfUserScores + 1;
        }
    }
    //echo "Number of user scores is " . $numberOfUserScores;
    //if ($numberOfUserScores > 0)
    //{
    //	echo " user score card item 0 id is: " . $userScoreCardItems[0]->id;
    //	echo " originalScoreId: " . $userScoreCardItems[0]->originalScoreId;
    //	echo " scoreCardItems id is " . $scoreCardItems[0]->id;
    //}
    //exit();
}


$con->close();

function userScoresAreEntered() {
    global $userScoreCardItems, $numberOfUserScores;
    //echo "numberOfUserScores is " . $numberOfUserScores;
    //echo "userScoreCardItems[0]->id is " . $userScoreCardItems[0]->id;
    if ($numberOfUserScores == 0 || $userScoreCardItems[0]->id == -1) {
        return false;
    } else {
        return true;
    }
}

function findUserWritingIndexForSelection($selectionId) {
    global $userWritingItems, $numberOfUserWritingItems;
    for ($i = 0; $i < $numberOfUserWritingItems; $i++) {
        if ($userWritingItems[$i]->selectionId == $selectionId)
            return $i;
    }
    return -1;
}

function loggedInAsTeacher() {
    global $current_user;
    get_currentuserinfo();
    if ($current_user->user_login == "Teacher")
        return true;
    else
        return false;
}


?>
