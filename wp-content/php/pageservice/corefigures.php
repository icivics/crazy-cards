<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
mmc_initCurrentStateWithCon($con);

//$fullProfile = mmc_getCurrentState()->getProfile();
$fullProfile = getCurrentFullProfile($con, mmc_getCurrentState()->getProfile()->shortName, null);

$topic=$_GET["topic"];

$featuredGraphicSelection = -1;
$featuredGraphicHtml = "";

insertPageView($con, get_the_title(), "FIGURE", $topic, mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$sql = "Select * from topic where topic.topic_type='FIGURE' and topic.short_name = '".$topic ."'";
//echo $sql;
$result = mySqli_query_wrapper($con, $sql, "fetch topic type figure");
$row = mysqli_fetch_assoc($result);
$title = $row['title'];
$description = $row['description'];
$authorId = $row['author'];
//echo "title: " . $title . "  description: " . $description;
if (!empty($title)) {
	$authorDisplayName = get_the_author_meta( "display_name", $authorId );
	$authorFirstName = get_the_author_meta("first_name", $authorId);
	//echo "Title is " . $title;
	$topicSelections = fetchTopicSelections($con, $topic);
	$selectionIndex = 0;
	$numberOfSelections = count($topicSelections);
	$numberOfResources = 0;
	$articleSelection = -1;
	while ($selectionIndex < count($topicSelections))
	{
		$aSelection = $topicSelections[$selectionIndex];
		switch ($aSelection->type)
		{
			case Selection::FEATUREDGRAPHIC:
				$featuredGraphicSelection = $selectionIndex;
				$featuredGraphicHtml = "<img src='" . ABSURL . "wp-content/images/" . $topic . "/" . 
					$aSelection->image . "' border='0' width='300' height='300' />";
				$featuredGraphicHtml = str_replace("\"", "\\\"", $featuredGraphicHtml);
				break;
			case Selection::ARTICLE:
				$articleSelection = $selectionIndex;
				break;
			case Selection::VIDEO:
				$resourceSelections[$numberOfResources] = $selectionIndex;
				$resourceHtml[$numberOfResources] = fixupVideoEmbed($aSelection->videoEmbedCode);
				if ($aSelection->caption != null && !empty($aSelection->caption) )
				{
					$resourceHtml[$numberOfResources] = $resourceHtml[$numberOfResources] . "<br/><br/><div align='left'>" . $aSelection->caption . "</div>";
				}
				//$resourceHtml[$numberOfResources] = str_replace("\"", "\\\"", $resourceHtml[$numberOfResources]);
				$numberOfResources = $numberOfResources + 1;
				break;
			case Selection::LINKS:
				$linksSelection = $selectionIndex;
				$aSelection->article = str_replace("@BLOGINFO", get_bloginfo('url'), $aSelection->article);
				$aSelection->article = str_replace("@PROFILE", "profile=" . $fullProfile->shortName, $aSelection->article);
				break;
			default:
				break;
		}
		$selectionIndex = $selectionIndex + 1;
	}
}

$con->close();
?>