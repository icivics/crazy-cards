<?php

require_once '../db/dbdefs.php';
/* Database connection start */
$conn = mysqli_connect(DBHOST, DBUSER, DBPASSWORD, DBSCHEMA) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */


// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;


$columns = array( 
// datatable column index  => database column name
	0 =>'title',
	1 => 'artist',
    2=> 'album',
    3=> 'genre',
    4=> 'released'
);

// getting total number records without any search
$sql = "SELECT id, title, artist, album, genre, released ";
$sql.=" FROM media_item";
$query=mysqli_query($conn, $sql) or die("song-grid-data.php: get songs");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "SELECT id, title, artist, album, genre, released ";
$sql.=" FROM media_item WHERE 1=1";
if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
	$sql.=" AND ( title LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR artist LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR album LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR genre LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR released LIKE '".$requestData['search']['value']."%' )";
}
$query=mysqli_query($conn, $sql) or die("song-grid-data.php: get songs");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result. 
$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
$query=mysqli_query($conn, $sql) or die("song-grid-data.php: get songs");

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$nestedData=array(); 

	$nestedData[] = $row["id"] . "|" . $row["title"];
	$nestedData[] = $row["artist"];
    $nestedData[] = $row["album"];
    $nestedData[] = $row["genre"];
    $nestedData[] = $row["released"];

	$data[] = $nestedData;
}
mysqli_close($conn);


$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data);  // send data as json format

?>
