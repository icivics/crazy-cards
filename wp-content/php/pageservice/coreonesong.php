<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$mediaItemDao = new MediaItemDao($con);
$playlistDao = new PlaylistDao($con);
$playlistEntryOutputter = new PlaylistEntryOutputter();
// initialize all of state managed variables
mmc_initCurrentStateAndProfile(PRODUCT);
$mediaId = $_GET['id'];
$guid = $_GET['guid'];

if ($mediaId == 'RANDOM') {
    $arrMediaItemIds = $mediaItemDao->getRandomPool();
    $upperLimit = count($arrMediaItemIds) - 1;
    $idx = rand(0, $upperLimit);
    $mediaId = $arrMediaItemIds[$idx];
}

$featuredGraphicSelection = -1;
$featuredGraphicHtml = "";

$mediaEntryBean = $mediaItemDao->getMediaInfoById($mediaId);
$playlist = null;
$playlistEntry = null;
if (!empty($guid)) {
    $playlist = $playlistDao->getPlaylistByGuid($guid);
    // only look for the entry if the mediaEntryBean and mediaId is valid
    if ($playlist != null && $mediaEntryBean != null) {
        foreach ($playlist->arrPlaylistEntries as $entry) {
            if ($entry->mediaItemId == $mediaId) {
                $playlistEntry = $entry;
                $playlistEntry->arrMediaCategories = $mediaEntryBean->arrMediaCategories;
                $playlistEntryAnchor = "#MEDIA_ID_" . $mediaId;
                break;
            }
        }
    }
}
$category = $_GET['category'];
if (empty($category)) {
    $category = "MUSIC";
}
if ($mediaEntryBean != null) {
    $title = $mediaEntryBean->title;
    insertPageView($con, get_the_title(), "SONG", sql_quotes($mediaEntryBean->title), mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());
} else {
    insertPageView($con, get_the_title(), "SONG", "INVALID", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());
}

mysqli_close($con);

$redirectTo = null;
// invalid media id
if ($mediaEntryBean == null) {
    // invalid or no playlist - go to song-picker
    if ($playlist == null) {
        $redirectTo = "song-picker";
    } else {
        // valid playlist - go to playlist page
        $redirectTo = "playlist?guid=" . $playlist->guid;
    }
}
if (!empty($redirectTo)) {
    header('Location: '.$redirectTo);
}