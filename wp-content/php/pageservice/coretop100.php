<?php

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$playlistDao = new PlaylistDao($con);
$tileDao = new TileDao($con);
$playlistEntryOutputter = new PlaylistEntryOutputter();
// initialize all of state managed variables
mmc_initCurrentStateAndProfile("TOP100");
$current_user = wp_get_current_user();
$wp_user_id = $current_user->ID;

$tiles = $tileDao->getTilesForPage("top100");
$songOfTheWeekPlaylist = $playlistDao->getPlaylistByName("Song of the week");
$songOfTheWeekEntry = null;
foreach ($songOfTheWeekPlaylist->arrPlaylistEntries as $entry) {
    if ($songOfTheWeekEntry == null || $songOfTheWeekEntry->sequence < $entry->sequence) {
        $songOfTheWeekEntry = $entry;
    }
}
$randomTop100Grouping = new SongListGrouping();
$randomTop100Grouping->first = 1;
$randomTop100Grouping->last = 100;
$htmlForRandomSongTeasers = createHtmlForSongTeasers($con, $randomTop100Grouping);

$tileOutputter = new TileOutputter($tiles);
$tileOutputter->setTop100Data($htmlForRandomSongTeasers, $songOfTheWeekEntry, $songOfTheWeekPlaylist, $playlistEntryOutputter);


// everything after is questionable

// Invisible groupings only visible by editor
$includeNotVisibleGroupings = loggedInUserCanEdit();

$allSongListGroupings = getAllSongListGroupings($con);
$afterTop10Grouping = new SongListGrouping();
$afterTop10Grouping->first = 11;
$afterTop10Grouping->last = 100;

$top3Grouping = new SongListGrouping();
$top3Grouping->first = 1;
$top3Grouping->last = 3;

$currentSongListGrouping = findCurrentSongListGrouping($allSongListGroupings);
$htmlForSongTeasers = createHtmlForSongTeasers($con, $currentSongListGrouping);
$htmlForAfterTop10Teasers = createHtmlForSongTeasers($con, $afterTop10Grouping);
$htmlForTop3Teasers = createHtmlForSongTeasers($con, $top3Grouping);
$htmlForCurrentSongGroupingLink = createHtmlForSongGroupingLink($currentSongListGrouping);
$htmlForAllSongListLink = createHtmlForAllSongListLink();
$htmlForNonCurrentVisibleSongGroupingLinks = 
	createHtmlForNonCurrentSongGroupingLinks($allSongListGroupings, $currentSongListGrouping, true);
if (!empty($htmlForNonCurrentVisibleSongGroupingLinks)) {
	$htmlForNonCurrentVisibleSongGroupingLinks = "Here are the previous batches:<br/>" . $htmlForNonCurrentVisibleSongGroupingLinks;
}
if ($includeNotVisibleGroupings) {
	$htmlForNonCurrentNonVisibleSongGroupingLinks =
	"Authoring tool only<br/>" .
	createHtmlForNonCurrentSongGroupingLinks($allSongListGroupings, $currentSongListGrouping, false);
} else {
	$htmlForNonCurrentNonVisibleSongGroupingLinks = "";
}
insertPageView($con, get_the_title(), "TOP100MAIN", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$title = "The Top 100";

mysqli_close($con);

?>