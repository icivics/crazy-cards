<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/6/2019
 * Time: 7:32 AM
 */
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$current_user = wp_get_current_user();
$secondaryTitle = "The Political Football";

$con = getMySqliDbConnection();
mmc_initCurrentStateAndProfile(PRODUCT);

// set up board in two divs - one to show their score, and one to wrap up and market
insertPageView($con, get_the_title(), "GAMEINTRO", "PAGES", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$con->close();
?>