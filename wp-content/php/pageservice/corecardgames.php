<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');


$con = getMySqliDbConnection();
$tileDao = new TileDao($con);
$tiles = $tileDao->getTilesForPage("cardgames");
//connect to the SQL database, and get the tile information from the tile table

$tileOutputter = new TileOutputter($tiles);
//the tile outputter creates the HTML for the tiles

mmc_initCurrentStateAndProfile("CARDGAMES");
insertPageView($con, get_the_title(), "MATCHINGGAME", "PAGES");
//initialize the page

$con->close();


