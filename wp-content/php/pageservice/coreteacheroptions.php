<?php 
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();


// initialize all of state managed variables
mmc_initCurrentState();
$fullProfile = mmc_getCurrentState()->getProfile();

$submitTeacherOptionsForm = $_POST["submitteacheroptionsform"];
$submitTeacherProfileForm = $_POST["submitteacherprofileform"];


echo "coreteacheroptions fullProfile id: " . $fullProfile->id . "<br/>";
$profileSubscriberId = mmc_getCurrentState()->classroomCodeObject->profileSubscriberId;
$associatedUserId = mmc_getCurrentState()->getAssociatedUserId();
echo "going to update profile messaging, profileSubscriberid: " . $profileSubscriberId . "<br/>";

$studentMessaging = findMessageOfType($fullProfile->arrMessaging, STUDENT);
if ($submitTeacherProfileForm) {
	$profileMessaging = new ProfileMessaging();
	$profileMessaging->userType = STUDENT;
	$profileMessaging->profileId = $fullProfile->id;
	$profileMessaging->profileSubscriberId = $profileSubscriberId;
	$profileMessaging->heading1 = $_POST["heading1"];
	$profileMessaging->heading2 = $_POST["heading2"];
	$profileMessaging->welcomeMsg = $_POST["welcomemsg"];
	echo "going to update profile messaging, profileSubscriberid: " . $profileSubscriberId . "<br/>";
	updateProfileMessaging($con, $profileMessaging, $profileSubscriberId);
	//exit();
}

if ($submitTeacherOptionsForm) {
	$configId = $_POST["configid"];
	
	echo "going to update teacher options form, configid: " . $configId . "<br/>";
	$topicConfiguration = fetchTopicConfiguration($con, $configId, $subscriberId);
	echo "topic configuration id: " . $topicConfiguration->id . "<br/>";
	//exit();
	
	$topicDocumentsAll = fetchDocumentsForTopicConfiguration($con, $configId, false, $profileSubscriberId);
	$topicDocumentsShared = filterOnlySharedDocs($topicDocumentsAll);
	$topicDocumentsPrivate = filterOnlyPrivateDocs($topicDocumentsAll);
	

	$lstTcMeta = array();

	$tcMeta0 = new tcMeta();
	$tcMeta0->field = "show_instructions";
	$tcMeta0->fieldType = "boolean";
	$tcMeta0->fieldValue = $_POST["showinstructions"];
	array_push($lstTcMeta, $tcMeta0);

	$tcMeta1 = new tcMeta();
	$tcMeta1->field = "instructions";
	$tcMeta1->fieldType = "text";
	$tcMeta1->fieldValue = convertCRtoBR($_POST["instructions"]);
	array_push($lstTcMeta, $tcMeta1);

	$tcMeta2 = new tcMeta();
	$tcMeta2->field = "show_author_analysis";
	$tcMeta2->fieldType = "boolean";
	$tcMeta2->fieldValue = $_POST["authoranalysis"];
	array_push($lstTcMeta, $tcMeta2);

	$tcMeta3 = new tcMeta();
	$tcMeta3->field = "show_author_scorecard";
	$tcMeta3->fieldType = "boolean";
	$tcMeta3->fieldValue = $_POST["authorscorecard"];
	array_push($lstTcMeta, $tcMeta3);

	$tcMeta4 = new tcMeta();
	$tcMeta4->field = "show_generic_scorecard";
	$tcMeta4->fieldType = "boolean";
	$tcMeta4->fieldValue = $_POST["genericscorecard"];
	array_push($lstTcMeta, $tcMeta4);

	updateTopicConfiguration($con, $configId, $lstTcMeta, $profileSubscriberId);

	// set visibility for shared documents
	foreach ($topicDocumentsShared as $td) {
		$postId = "doc_" . $td->id;
		if ($_POST[$postId] != null) {
			$td->visible = ($_POST[$postId] == "on" ? 1 : 0);
			updateTopicConfigurationDocument($con, $configId, $td->id, $td->visible, $profileSubscriberId);
		}
		else {
			$td->visible = 0;
			updateTopicConfigurationDocument($con, $configId, $td->id, $td->visible, $profileSubscriberId);
		}
	}
	$privateDocumentIdsToDelete = array();
	foreach ($topicDocumentsPrivate as $td) {
		array_push($privateDocumentIdsToDelete, $td->id);
	}
	deleteTopicDocumentReferences($con, $privateDocumentIdsToDelete);

	for ($i=0;$i<3;$i++) {
		$postIdName = "privatedocname" . $i;
		$postIdURL = "privatedocurl" . $i;
		$postIdVisible = "privatedocvisible" . $i;
		$name = $_POST[$postIdName];
		$url = $_POST[$postIdURL];
		$visible = $_POST[$postIdVisible];
		if (!empty($name) && !empty($url)) {
			$topicDocument = new TopicDocument(null);
			$topicDocument->name = $name;
			$topicDocument->url = $url;
			$topicDocument->visible = $visible;
			$topicDocument->subscriberId = $profileSubscriberId;
			$topicDocument->profile = $profileSubscriber->profileId;
			// this probably isn't going to work
			insertPrivateDocument($con, $topicDocument, $topicConfiguration, $profileSubscriberId, $associatedUserId);
		}
	}
}

mysqli_close($con);
?>