<?php 

require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$con = getMySqliDbConnection();
$categoryTopicDao = new CategoryTopicDao($con);
// initialize all of state managed variables
mmc_initCurrentStateWithCon($con);
// workaround
//mmc_initCurrentStateAndProfile("MYMORALCOMPASS");
//$fullProfile = mmc_getCurrentState()->getProfile();
$fullProfile = getCurrentFullProfile($con, mmc_getCurrentState()->getProfile()->shortName, null);
//echo "Full profile name: " . $fullProfile->shortName . "<br/>";
//echo "Full profile display name: " . $fullProfile->displayName . "<br/>";
//echo "Messaging count: " . count($fullProfile->arrMessaging) . "<br/>";

$quoteSetting = $_GET['randomQuote'];
if ($quoteSetting != "") {
	$quoteoftheday = selectRandomQuote($con);
} else {
	$quoteoftheday = selectQuoteOfTheDay($con);
}


$lstAllCategories = array();
$lstFeaturedTopics = array();
$numberOfTimePeriods = 0;

if (mmc_getCurrentState()->getSubscribeCurrentUser()) {
	subscribeCurrentUser($con, $fullProfile);
}

insertPageView($con,"Menu", "MENU", "Menu", mmc_getCurrentState()->getProfileName(), mmc_getCurrentState()->getClassroomCode());

$current_user = wp_get_current_user();

$lstAllCategories = $categoryTopicDao->fetchCategoriesAndTopics($fullProfile->id);
//echo "full profile id: " . $fullProfile->id . "<br/>";
foreach ($lstAllCategories as $category)
{
	foreach ($category->topics as $topic)
	{		
		if ($topic->featured == 1)
			array_push($lstFeaturedTopics, $topic);
	}
}
if (isset($_GET["featuredtopic"])) {
	$featuredTitle = $_GET['featuredtopic'];
	$featuredTopic = fetchTopic($featuredTitle);
}
else {
	$featuredTopic = findTodaysFeaturedTopic($lstFeaturedTopics, -1);
	if ($featuredTopic != null) {
		$featuredTitle = $featuredTopic->shortName;
	}
}

if ($featuredTopic != null) {
	$featuredSelections = $categoryTopicDao->fetchSelections($featuredTitle);
	
	foreach ($featuredSelections as $aSelection)
	{
		if ($aSelection->type == Selection::FEATUREDGRAPHIC)
		{
			$featuredImage = $aSelection->image;
			$featuredCaption = $aSelection->article;
			break;
		}
	}
}
$con->close();


	function getCategoryOpenState($categoryIndex) {
		$openCloseState = mmc_getCurrentState()->getMenuOpenCloseState();
		if (!empty($openCloseState)) {
			//echo "pow(2, " . $categoryIndex . "): " . pow(2, $categoryIndex) . "<br/>";
			//echo "Cookievalue is : " . $cookieValue . " categoryIndex: " . $categoryIndex . "<br/>";
			//echo "Testing " . pow(2, intval($categoryIndex)) . " and " . $cookieValue . "<br/>";
			$retvalue = (pow(2, intval($categoryIndex)) & $openCloseState);
			//echo "Returning " . $retvalue . "<br/>";
			return $retvalue;
		} else {
			//echo "menu open close state is empty";
			return $categoryIndex == 0;
		}
	}


?>