<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/28/2019
 * Time: 10:43 PM
 */

require_once('../db/dbdefs.php');
require_once('../dao/GameResultDao.php');
require_once('../model/gameboardresult.class.php');
require_once('../util/mysqliutil.php');

$con = getMySqliDbConnection();
$gameResultDao = new GameResultDao($con);

$gameResultId = $_GET['gameresultid'];

$points = $_GET['points'];
$complete = $_GET['complete'];

$gameBoardResult = new GameBoardResult();
$gameBoardResult->id = $gameResultId;
$gameBoardResult->points = $points;
$gameBoardResult->complete = $complete;
$gameResultDao->upsert($gameBoardResult);

$con->close();