<?php
require_once(ABSPATH . 'wp-content/php/standardincludes.php');

$returnToPage = $_POST["returntopage"];
$returnToPageTitle = $_POST["returntopagetitle"];
$numberOfResources = 0;
$resourceSelections = array();
$resourceHtml = array();

$con = getDbConnection();

// initialize all of state managed variables
mmc_initCurrentState();
$fullProfile = mmc_getCurrentState()->getProfile();

$category = $_POST['categoryEntry'];
$keywords = $_POST['keywordsEntry'];
$breadcrumbs = $_POST['breadcrumbsEntry'];

$sql = "Select * from video";
$whereOrAnd = " where ";
if (!empty($category)) {
	$sql .= $whereOrAnd . " category = '" . $category . "'";
	$whereOrAnd = " and ";
}
echo $sql;
$result = my_mysql_query($sql, "Error fetching videos");

while ($row = mysql_fetch_array($result)) {
	$aVideo = new Video();
	$aVideo->id = $row['id'];
	$aVideo->selectionId = $row['selection_id'];
	$aVideo->topicShortName = $row['topic_short_name'];
	$aVideo->videoId = $row['video_id'];
	$aVideo->name = $row['name'];
	$aVideo->category = $row['category'];
	$aVideo->keywords = $row['keywords'];
	$aVideo->url = $row['url'];
	$aVideo->breadcrumb = $row['breadcrumb'];
	$aVideo->videoEmbedCode = $row['video_embed_code'];
	$aVideo->thumbnailImage = $row['thumbnail_image'];
	$aVideo->description = $row['description'];
	array_push($resourceSelections, $aVideo);
	array_push($resourceHtml, $aVideo->videoEmbedCode);
	$numberOfResources++;
	if ($numberOfResources >= 10) {
		break;
	}
}

mysql_close($con);


?>