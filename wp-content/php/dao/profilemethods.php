<?php
    const VISITOR = "VISITOR";
    const LOGGEDINUSER = "LOGGEDINUSER";
    const STUDENT = "STUDENT";
    const TEACHER = "TEACHER";
    const HOMEPAGE = "HOMEPAGE";	// profile

    function getProfileId($con, $profilename) {
		$profileId = "0";
		$sql = "Select * from profile where shortname='" . $profilename . "'";
		$result = mysqli_query_wrapper($con, $sql, "getProfileId");
		if ($row = mysqli_fetch_assoc($result))
			$profileId = $row['id'];
		return $profileId;		
	}
	
	function getProfileNameFromClassroomCode($con, $classroomCode) {
		$sql = "Select p.shortname from profile p " .
			"join profile_subscriber ps on ps.profile_id = p.id " .
			"where ps.subscriber_id='" . $classroomCode . "'";
		//echo "getProfileNameFromClassroomCode sql: " . $sql;
		$profileName = null;
		$result = mysqli_query_wrapper($con, $sql, "getProfileNameFromClassroomCode");
		if ($row = mysqli_fetch_assoc($result)) {
			$profileName = $row['shortname'];
		}
		return $profileName;
	}
	
	function getIdFromClassroomCode($con, $classroomCode) {
		$sql = "Select id from profile_subscriber where subscriber_id='" . $classroomCode . "'";
		$id = null;
		$result = mysqli_query_wrapper($con, $sql, "getIdFromClassroomCode");
		if ($row = mysqli_fetch_assoc($result)) {
			$id = $row['id'];
		}
		return $id;
	}
	
	function getCurrentFullProfileForTopic($profilename, $classroomCode, $configId) {
		
	}
	
	function getCurrentFullProfile($con, $profilename, $classroomCode) {
		//echo "getCurrentFullProfile -- profilename: " . $profilename . "  classroom code: " . $classroomCode . "<br/>";
		global $current_user;
		if (empty($classroomCode) && is_user_logged_in() && !mmc_getCurrentState()->isMyMoralCompass()) {
			$classroomCodeObject = getClassroomCodeForUserId($con, $profilename, $current_user->ID);
			mmc_getCurrentState()->classroomCodeObject = $classroomCodeObject;
		}
		$fullProfile = getFullProfile($con, $profilename, $classroomCode);
		mmc_getCurrentState()->setFullProfile($fullProfile);
		//echo "after getFullProfile id: " . mmc_getCurrentState()->getProfile()->id . "<br/>";
		//dump("End of getCurrentFullProfile: ", $fullProfile, $fullProfile->arrMessaging);
		return $fullProfile;
	}
	
	
	
	// profile
	function getFullProfile($con, $profilename, $classroomCode) {
		return getFullProfileForClassroomCode($con, $profilename, $classroomCode);
	}
	
	function findMessageOfType($arrMessages, $userType) {
		//echo "findMessageOfType: usertype: " . $userType . "<br/>";
		$profileMessaging = $arrMessages[$userType];
		//echo "profileMessaging heading1: " . $profileMessaging->heading1 . "<br/>";
		//echo "profileMessaging heading2: " . $profileMessaging->heading2 . "<br/>";
		//echo "profileMessaging welcome: " . $profileMessaging->welcomeMsg . "<br/>";
		return $profileMessaging;
	}
	
	function addToMessagingIfNecessary($arrMessaging, $profileMessaging) {
		//echo "addToMessagingIfNecessary user type is: " . $profileMessaging->userType . " <br/>";
		//echo "addToMessagingIfNecessary start profileid: " . 
		//	$profileMessaging->profileId . "<br/>";
		$existingMessaging = findMessageOfType($arrMessaging, $profileMessaging->userType);
		if ($existingMessaging == null) {
			//echo "addToMessagingIfNecessary Existing message null <br/>";
			$arrMessaging[$profileMessaging->userType] = $profileMessaging;
			//echo "arrMessaging [" . $profileMessaging->userType . "] = " . $arrMessaging[$profileMessaging->userType]->heading1 . "<br/>";
			//echo "profileMessaging = " . $profileMessaging->heading1 . "<br/>";
		} else {
			$replace = false;
			//echo "addToMessagingIfNecessary Existing message not null profileid: " . 
			//	$profileMessaging->profileId . "<br/>";
			if (!empty($profileMessaging->profileSubscriberId)) {
				$replace = true;
			} elseif (!empty($profileMessaging->profileId)) {
				$replace = true;
			}
			if ($replace == true) {
				$arrMessaging[$profileMessaging->userType] = $profileMessaging;
			}
		}
		return $arrMessaging;
	}
	
	function getDefaultProfileMessaging($con, $profile, $subscriberId) {
		if (empty($subscriberId)) {
			$sql = "Select * from profile_messaging where (profile_id is null or profile_id='" . $profile->id . "') " .
				"and profile_subscriber_id is null";
		} else {
			$sql = "Select pm.id, pm.profile_id, pm.profile_subscriber_id, pm.user_type, " .
				"pm.heading1, pm.heading2, pm.welcome_message from profile_messaging pm " .
				"left outer join profile_subscriber ps on pm.profile_subscriber_id = ps.id " .
				"where (pm.profile_id is null or pm.profile_id='" . $profile->id . "') " .
				"and (ps.subscriber_id = '" . $subscriberId . "' or ps.subscriber_id is null)";
		}
		//echo $sql . "<br/>";
		$result = mysqli_query_wrapper($con, $sql, "getDefaultProfileMessaging");
		while ($row = mysqli_fetch_assoc($result)) {
			$profileMessaging = new ProfileMessaging();
			$profileMessaging->id = $row['id'];
			$profileMessaging->profileId = $row['profile_id'];
			$profileMessaging->profileSubscriberId = $row['profile_subscriber_id'];
			$profileMessaging->userType = $row['user_type'];
			$profileMessaging->heading1 = $row['heading1'];
			if (empty($profileMessaging->heading1)) {
				$profileMessaging->heading1 = "My Moral Compass";
			}
			$profileMessaging->heading2 = $row['heading2'];
			if (empty($profileMessaging->heading2)) {
				$profileMessaging->heading2 = $profile->displayName;
			}
			$profileMessaging->welcomeMsg = $row['welcome_message'];
			$arrMessaging = addToMessagingIfNecessary($arrMessaging, $profileMessaging);
		}
		$profile->arrMessaging = $arrMessaging;
		// all good at this point
		//dumpMessaging("Right after getDefaultProfileMessaging", $arrMessaging);
	}
	
	// TODO these two need to be changed
	function getFullProfileForClassroomCode($con, $profilename, $classroomCode) {
		//echo "getFullProfileForClassroomCode -- profilename: " . $profilename . "  classroom code: " . $classroomCode . "<br/>";

		$sql = "Select p.id as profileid, p.shortname, p.displayname, p.owner " .
			"from profile p where p.shortname='" . $profilename . "'";
		//echo "getDefaultFullProfile sql: " . $sql;
		$profile = null;
		$result = mysqli_query_wrapper($con, $sql, "getFullProfileForClassroomCode");
		if ($row = mysqli_fetch_assoc($result)) {
			$profile = new Profile();
			$profile->id = $row['profileid'];
			$profile->shortName = $row['shortname'];
			$profile->displayName = $row['displayname'];
			$profile->owner = $row['owner'];
			getDefaultProfileMessaging($con, $profile, $classroomCode);
			// good at this point
			//dumpMessaging("Right after getFullProfileForClassroomCode", $profile->arrMessaging);
		}
		return $profile;
	}
	
	function getProfileWelcomeMessage($fullProfile, $persona, $crConvert) {
		$profileMessaging = findMessageOfType($fullProfile->arrMessaging, $persona);
		//echo "getProfileWelcomeMessage value is " . $profileMessaging->welcomeMsg . "<br/>";
		if ($crConvert) {
			return convertCRtoBR($profileMessaging->welcomeMsg);
		} else {
			return $profileMessaging->welcomeMsg;
		}
	}
	
	function getProfileHeading($headingNumber, $fullProfile, $persona) {
		// for these purposes, persona from TEACHER inherits STUDENT
		if ($persona == TEACHER) {
			$persona = STUDENT;
		}
		//echo "getProfileHeading persona: " . $profileSubscriber->persona . "<br/>";
		$profileMessaging = findMessageOfType($fullProfile->arrMessaging, $persona);
		//echo "getProfileHeading welcome: " . $profileMessaging->welcomeMsg . "<br/>";
		if ($headingNumber == 1) {
			//echo "1 getProfileHeading returning " . $profileMessaging->heading1;
			return $profileMessaging->heading1;
		} else if ($headingNumber == 2) {
			//echo "2 getProfileHeading returning " . $profileMessaging->heading2;
			return $profileMessaging->heading2;
		} else {
			//echo "3 getProfileHeading returning null";
			return null;
		}
	}
	
	
	function getProfileMessaging($con, $profileId, $profileSubscriberId, $userType) {
		$priorityMessaging = null;
		if (!empty($profileSubscriberId)) {
			$profileSql = "profile_id = " . $profileId;
			$profileSubscriberSql = "profile_subscriber_id = " . $profileSubscriberId;
		} else {
			$profileSql = "(profile_id = " . $profileId . " or profile_id is null)";
			$profileSubscriberSql = "profile_subscriber_id is null";
		}
		$sql = "Select * from profile_messaging " .
			" where " . $profileSql . " and " . $profileSubscriberSql .
			" and user_type = '" . $userType . "'";
		$result = mySqli_query_wrapper($con, $sql, "getProfileMessaging");
		while ($row = mysql_fetch_array($result)) {
			$profileMessaging = new ProfileMessaging();
			$profileMessaging->userType = $userType;
			$profileMessaging->profileId = $row['profile_id'];
			$profileMessaging->profileSubscriberId = $row['profile_subscriber_id'];
			$profileMessaging->heading1 = $row['heading1'];
			$profileMessaging->heading2 = $row['heading2'];
			$profileMessaging->welcomeMsg = convertCRtoBR($row['welcome_message']);
			if ($priorityMessaging == null) {
				$priorityMessaging = $profileMessaging;
			} else if (!empty($profileMessaging->profileSubscriberId) && empty($priorityMessaging->profileSubscriberId)) {
				$priorityMessaging = $profileMessaging;
			} else if (!empty($profileMessaging->profileId) && empty($priorityMessaging->profileId)) {
				$priorityMessaging = $profileMessaging;
			}
		}
		return $priorityMessaging;
	}
	
	function updateProfile($con, $profile) {
		if ($profile->id != -1) {
			$sql = "Update profile set shortname='" . $profile->shortName .
			"', owner='" . $profile->owner .
			"', displayname='" . $profile->displayName .
			"' where id=" . $profile->id;
			//echo $sql;
			$result = mySqli_query_wrapper($con, $sql, "updateProfile");
		}
		else {
			$sql = "Insert into profile (shortname, displayname, owner) values('" .
					$profile->shortName . "', '" .
					$profile->displayName . "', '" .
					$profile->owner . "')";
			$result = mySqli_query_wrapper($con, $sql, "updateProfile");
			$profile->id = mysqli_insert_id();
		}
		return $profile;
	}
	
	function updateProfileMessaging($con, $profileMessaging, $profileSubscriberId) {
		$unmagic = getUnmagicValue();
		$existingProfile = getProfileMessaging($con, $profileMessaging->profileId, $profileSubscriberId, $profileMessaging->userType);
		if (!empty($existingProfile)) {
			if (!empty($profileSubscriberId)) {
				$profileSubscriberSql = " and profile_subscriber_id = " . $profileSubscriberId;
			} else {
				$profileSubscriberSql = " and profile_subscriber_id is null";
			}
			$sql = "Update profile_messaging set welcome_message='" . my_escape($profileMessaging->welcomeMsg, $unmagic) .
				"', heading1='" . my_escape($profileMessaging->heading1, $unmagic) .
				"', heading2='" . my_escape($profileMessaging->heading2, $unmagic) .
				"' where profile_id=" . $profileMessaging->profileId . " and user_type='" . 
				$profileMessaging->userType . "' " . $profileSubscriberSql;
			$result = mySqli_query_wrapper($con, $sql, "Updating profile_messaging");
		} else {
			$sql = "Insert into profile_messaging (profile_id, profile_subscriber_id, user_type, heading1, heading2, welcome_message) " .
					"values (" . $profileMessaging->profileId . ", " . $profileSubscriberId . ", '" .  
					$profileMessaging->userType . "', '" .
					my_escape($profileMessaging->heading1, $unmagic) .
					"', '" . my_escape($profileMessaging->heading2, $unmagic) .
					"', '" . my_escape($profileMessaging->welcomeMsg, $unmagic) . "')";
            $result = mySqli_query_wrapper($con, $sql, "Updating profile_messaging");
		}
	}
	
	// profile
	function getProfileNameForTopicConfiguration($con, $tcId) {
		$profileName = "MYMORALCOMPASS";
		$sql = "SELECT p.shortname from profile p inner join topic_configuration tc on p.id=tc.profile_id where tc.id='" . $tcId . "'";
        $result = mySqli_query_wrapper($con, $sql, "getProfileNameForTopicConfiguration");
		if ($row = mysqli_fetch_assoc($result))
			$profileName = $row['shortname'];
		return $profileName;		
	}
	
	// profile subscriber
	function getProfileSubscriberByUserId($con, $profileName, $userId) {
		$sql = "Select profile_id, subscriber_id, user_id from profile_subscriber ps join profile p on ps.profile_id=p.id " . 
				"where p.shortname = '" .
				$profileName . "' and user_id = '" . $userId . "'";
		//echo $sql;
        $result = mySqli_query_wrapper($con, $sql, "getProfileSubscriberByUserId");
		if ($result == false) {
			return null;
		}
		if ($row = mysqli_fetch_assoc($result)) {
			$profileSubscriber = new ProfileSubscriber();
			$profileSubscriber->profileId = $row['profile_id'];
			$profileSubscriber->subscriberId = $row['subscriber_id'];
			$profileSubscriber->userId = $row['user_id'];
			return $profileSubscriber;
		}
		return null;				
	}
	
	function getAllClassroomCodes($con) {
		$allClassroomCodes = array();
		
		$sql = "select ps.id as profilesubscriberid, ps.subscriber_id as classroomcode, u.user_nicename, p.shortname, p.displayname " . 
			"from profile_subscriber ps join profile p on ps.profile_id = p.id " .
			"join mc_users u on ps.user_id=u.ID order by u.user_nicename, classroomcode";
		//echo $sql;
		$result = mySqli_query_wrapper($con, $sql, "getAllClassroomCodes");
		if ($result == false) {
			return null;
		}
		while ($row = mysqli_fetch_assoc($result)) {
			$classroomdodebean = new ClassroomCodeBean();
			$classroomdodebean->profileSubscriberId = $row['profilesubscriberid'];
			$classroomdodebean->classroomCode = $row['classroomcode'];
			$classroomdodebean->userNiceName = $row['user_nicename'];
			$classroomdodebean->profileShortName = $row['shortname'];
			$classroomdodebean->profileDisplayName = $row['displayname'];
			array_push($allClassroomCodes, $classroomdodebean);
		}
		return $allClassroomCodes;
	}

	function deleteReferencesToProfileSubscriber($profileSubscriberToDelete) {
		$sql = "delete from topic_configuration_meta where profile_subscriber_id='" . $profileSubscriberToDelete . "'";
		$result = my_mysql_query($sql, "Error deleting classroom code 1");
		$sql = "delete from topic_configuration_document where profile_subscriber_id='" . $profileSubscriberToDelete . "'";
		$result = my_mysql_query($sql, "Error deleting classroom code 2");
		$sql = "delete from profile_messaging where profile_subscriber_id='" . $profileSubscriberToDelete . "'";
		$result = my_mysql_query($sql, "Error deleting classroom code 3");
		$sql = "delete from profile_subscriber where id='" . $profileSubscriberToDelete . "'";
		$result = my_mysql_query($sql, "Error deleting classroom code 4");
	}
	function getClassroomCodeForUserId($con, $profilename, $userId) {
		$sql = "Select ps.id, ps.user_id, ps.subscriber_id from " .
			"profile p join profile_subscriber ps on ps.profile_id=p.id " .
			"where p.shortname = '" .  $profilename . "' and ps.user_id = " . $userId;
		//echo $sql;
		$result = mySqli_query_wrapper($con, $sql, "getClassroomCodeForUserId");
		if ($result == true) {
			if ($row = mysqli_fetch_assoc($result)) {
				$classroomCodeObject = new ClassroomCodeObject();
				$classroomCodeObject->profileSubscriberId = $row['id'];
				$classroomCodeObject->classroomCode = $row['subscriber_id'];
				$classroomCodeObject->associatedUser = get_userdata($row['user_id']);
				$classroomCodeObject->status = SUBSCRIBER_VALID;
				return $classroomCodeObject;
			}
		}
		return null;
	}
	
	// profile subscriber
	function checkSubscriberId($profileSubscriber, $currentUserSubscriber) {
		if (!empty($profileSubscriber->subscriberId)) {
			$associatedProfileSubscriber = getProfileSubscriberBySubscriberId($profileSubscriber->profileId, $profileSubscriber->subscriberId);
			if ($associatedProfileSubscriber == null) {
				$profileSubscriber->subscriberStatus = SUBSCRIBER_INVALID;
				if (is_user_logged_in()) {
					$profileSubscriber->mode = MODE_LOGGEDIN_USER;
					$profileSubscriber->persona = LOGGEDINUSER;
				} else {
					$profileSubscriber->mode = MODE_NO_SUBSCRIBER;
					$profileSubscriber->persona = VISITOR;
				}
				$profileSubscriber->userId = -1;
				$profileSubscriber->teacherOptions = false;
			} else {
				$profileSubscriber->id = $associatedProfileSubscriber->id;
				$profileSubscriber->subscriberStatus = SUBSCRIBER_VALID;
				$profileSubscriber->mode = MODE_VALID_SUBSCRIBER;
				$profileSubscriber->userId = $associatedProfileSubscriber->userId;
				$profileSubscriber->userInfo = get_userdata($profileSubscriber->userId);
				if ($currentUserSubscriber->userId == $profileSubscriber->userId) {
					$profileSubscriber->teacherOptions = true;
					$profileSubscriber->persona = TEACHER;
				} else {
					$profileSubscriber->persona = STUDENT;
				}
			}
		} else if ($profileSubscriber != null) {
			$profileSubscriber->subscriberStatus = SUBSCRIBER_NONE;
			if ($currentUserSubscriber != null) {
				$profileSubscriber->mode = MODE_VALID_SUBSCRIBER;
				$profileSubscriber->subscriberStatus = SUBSCRIBER_VALID;
				$profileSubscriber->subscriberId = $currentUserSubscriber->subscriberId;
				$profileSubscriber->userId = $currentUserSubscriber->userId;
				$profileSubscriber->userInfo = get_userdata($profileSubscriber->userId);
				$profileSubscriber->teacherOptions = true;
				$profileSubscriber->persona = TEACHER;
			} else if (is_user_logged_in()) {
				$profileSubscriber->mode = MODE_LOGGEDIN_USER;
				$profileSubscriber->teacherOptions = false;
				$profileSubscriber->persona = LOGGEDINUSER;
			} else {
				$profileSubscriber->mode = MODE_NO_SUBSCRIBER;
				$profileSubscriber->teacherOptions = false;
				$profileSubscriber->persona = VISITOR;
			}
		}
		return $profileSubscriber;
	}
	
	// profile subscriber
	function getProfileSubscriberBySubscriberId($con, $profileId, $subscriberId) {
		if (empty($subscriberId)) {
			return null;
		}
		$sql = "Select id, profile_id, subscriber_id, user_id from profile_subscriber where profile_id=" .
		$profileId . " and subscriber_id = " . $subscriberId;
		//echo $sql;
        $result = mySqli_query_wrapper($con, $sql, "getProfileSubscriberBySubscriberId");
		if ($result == true) {
			if ($row = mysqli_fetch_assoc($result)) {
				$profileSubscriber = new ProfileSubscriber();
				$profileSubscriber->id = $row['id'];
				$profileSubscriber->profileId = $row['profile_id'];
				$profileSubscriber->subscriberId = $row['subscriber_id'];
				$profileSubscriber->userId = $row['user_id'];
				return $profileSubscriber;
			}
		}
		return null;
	}
	
	function getAssociatedUserFromClassroomCode($con, $classroomCode) {
		if (empty($classroomCode)) {
			return null;
		}
		$sql = "Select id, user_id from profile_subscriber where subscriber_id = " . $classroomCode;
		//echo $sql;
        $result = mySqli_query_wrapper($con, $sql, "getAssociatedUserFromClassroomCode");
		if ($result == true) {
			if ($row = mysqli_fetch_assoc($result)) {
				$userId = $row['user_id'];
				return $userId;
			}
		}
		return null;
	}
	
	function subscribeCurrentUser($con, $fullProfile) {
		//echo "subscribeCurrentUser profileid: " . $fullProfile->id . "<br/>";
		if (is_user_logged_in()) {
			$current_user = wp_get_current_user();
			$existingClassroomCodeObject = getClassroomCodeForUserId($con, $fullProfile->shortName, $current_user->ID);
			if (empty($existingClassroomCodeObject)) {
				$ok = false;
				// make sure classroom code is unique
				while (!$ok) {
					$classroomCode = rand(100000, 999999);
					if (getIdFromClassroomCode($classroomCode) == null) {
						$ok = true;
					} else {
						//echo "Rejecting code: " . $classroomCode . "<br/>";
					}
				}
				$sql = "Insert into profile_subscriber (profile_id, subscriber_id, user_id) " .
						"values (" . $fullProfile->id . ", " . $classroomCode . ", " . $current_user->ID . ")";
				//echo $sql;
				$result = mySqli_query_wrapper($con, $sql, "subscribeCurrentUser");
				mmc_getCurrentState()->classroomCodeObject = getClassroomCodeForUserId($con, $fullProfile->shortName, $current_user->ID);
				mmc_getCurrentState()->setPersona();
			}
		}
	}
	
	function dump($message, $fullProfile, $arrMessaging) {
		echo $message . "<br/>Full profile<br/>short name: " . $fullProfile->shortName . "<br/>";
		echo "display name: " . $fullProfile->displayName . "<br/>";
		echo "VISITOR<br/>heading 1: " . $arrMessaging[VISITOR]->heading1 . "<br/>";
		echo "VISITOR<br/>heading 2: " . $arrMessaging[VISITOR]->heading2 . "<br/>";
		echo "VISITOR<br/>welcome: " . $arrMessaging[VISITOR]->welcomeMsg . "<br/>";
		echo "LOGGEDINUSER<br/>heading 1: " . $arrMessaging[LOGGEDINUSER]->heading1 . "<br/>";
		echo "LOGGEDINUSER<br/>heading 2: " . $arrMessaging[LOGGEDINUSER]->heading2 . "<br/>";
		echo "LOGGEDINUSER<br/>welcome: " . $arrMessaging[LOGGEDINUSER]->welcomeMsg . "<br/>";
		echo "STUDENT<br/>heading 1: " . $arrMessaging[STUDENT]->heading1 . "<br/>";
		echo "STUDENT<br/>heading 2: " . $arrMessaging[STUDENT]->heading2 . "<br/>";
		echo "STUDENT<br/>welcome: " . $arrMessaging[STUDENT]->welcomeMsg . "<br/>";
		echo "TEACHER<br/>heading 1: " . $arrMessaging[TEACHER]->heading1 . "<br/>";
		echo "TEACHER<br/>heading 2: " . $arrMessaging[TEACHER]->heading2 . "<br/>";
		echo "TEACHER<br/>welcome: " . $arrMessaging[TEACHER]->welcomeMsg . "<br/>";
	}
	
	function dumpMessaging($message, $arrMessaging) {
		echo $message . "<br/>";
		echo "VISITOR<br/>heading 1: " . $arrMessaging[VISITOR]->heading1 . "<br/>";
		echo "VISITOR<br/>heading 2: " . $arrMessaging[VISITOR]->heading2 . "<br/>";
		echo "VISITOR<br/>welcome: " . $arrMessaging[VISITOR]->welcomeMsg . "<br/>";
		echo "LOGGEDINUSER<br/>heading 1: " . $arrMessaging[LOGGEDINUSER]->heading1 . "<br/>";
		echo "LOGGEDINUSER<br/>heading 2: " . $arrMessaging[LOGGEDINUSER]->heading2 . "<br/>";
		echo "LOGGEDINUSER<br/>welcome: " . $arrMessaging[LOGGEDINUSER]->welcomeMsg . "<br/>";
		echo "STUDENT<br/>heading 1: " . $arrMessaging[STUDENT]->heading1 . "<br/>";
		echo "STUDENT<br/>heading 2: " . $arrMessaging[STUDENT]->heading2 . "<br/>";
		echo "STUDENT<br/>welcome: " . $arrMessaging[STUDENT]->welcomeMsg . "<br/>";
		echo "TEACHER<br/>heading 1: " . $arrMessaging[TEACHER]->heading1 . "<br/>";
		echo "TEACHER<br/>heading 2: " . $arrMessaging[TEACHER]->heading2 . "<br/>";
		echo "TEACHER<br/>welcome: " . $arrMessaging[TEACHER]->welcomeMsg . "<br/>";
	}
	function dumpMessagingIds($message, $arrMessaging) {
		echo $message . "<br/>";
		echo "VISITOR id: " . $arrMessaging[VISITOR]->id . "<br/>";
		echo "LOGGEDINUSER id: " . $arrMessaging[LOGGEDINUSER]->id . "<br/>";
		echo "STUDENT id: " . $arrMessaging[STUDENT]->id . "<br/>";
		echo "TEACHER id: " . $arrMessaging[TEACHER]->id . "<br/>";
	}
						
	?>