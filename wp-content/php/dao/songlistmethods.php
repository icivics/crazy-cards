<?php 

const NUM_SONG_TEASERS = 2;

function getSongContributions($con, $emailAddress) {
	$existingSongs = array();
	$sql = "Select s.id, s.title, s.artist, s.video_url from songcontribution s join subscription_new n on s.member_id=n.id where n.email='" .
		$emailAddress . "'"; 
	//echo $sql;
	$result = mySqli_query_wrapper($con, $sql, "getSongContributions");
	while($row = mysqli_fetch_assoc($result)) {
		$songContribution = new SongContribution();
		$songContribution->id = $row['id'];
		$songContribution->title = $row['title'];
		$songContribution->artist = $row['artist'];
		$songContribution->videoURL = $row['video_url'];
		array_push($existingSongs, $songContribution);
	}
	return $existingSongs;
}

function getMember($con, $emailAddress) {
	$sql = "select * from subscription_new where email='" . $emailAddress . "' and category='TOP100'";
	//echo $sql;
	$result = mySqli_query_wrapper($con, $sql, "getMember");
	if ($row = mysqli_fetch_assoc($result) ) {
		$member = new Member();
		$member->id = $row['id'];
		$member->firstName = $row['first_name'];
		$member->lastName = $row['last_name'];
		$member->category = $row['category'];
		$member->emailAddress = $row['email'];
		$member->comments = $row['comments'];
	} else {
		$member = null;
	}
	return $member;
}

function createMemberIfNeeded($con, $member) {
	$existingMember = getMember($con, $member->emailAddress);
	if ($existingMember == null) {
		$sql = "insert into subscription_new (email, first_name, last_name, comments, category) values ('" . $member->emailAddress . "','" .
			$member->firstName . "','" . $member->lastName . "','" . $member->comments . "','" . "TOP100" . "')";
		$result = mySqli_query_wrapper($con, $sql, "createMemberIfNeeded");
		$subject = 'New Top 100 signup';

		$message = "A new user " . $email . " is now signed up for top 100.";

		//send email to admin
		//wp_mail( 'mattosber@gmail.com', $subject, $message);


		$result = wp_mail( 'mattosber@gmail.com', $subject, $message);
		
		$existingMember = getMember($con, $member->emailAddress);
	}
	return $existingMember;
}

function updateSongContributions($con, $member, $songContributions) {
	$member = createMemberIfNeeded($con, $member);
	$sql = "delete from songcontribution where member_id = '" . $member->id . "'";
	$result = mySqli_query_wrapper($con, $sql, "updateSongContributions");
	foreach ($songContributions as $songContribution){
		$sql = "insert into songcontribution (member_id, title, artist, video_url, creation_date) values (" .
				$member->id . ", '" . $songContribution->title . "', '" . $songContribution->artist . "', '" . $songContribution->videoURL .
				"', now() )";
		//echo $sql;
        $result = mySqli_query_wrapper($con, $sql, "updateSongContributions");
	}
}

function getAllSonglistGroupings($con) {
	$allSongListGroupings = array();
	$sql = "Select * from songlist_grouping";
	//echo $sql;
	$result = mySqli_query_wrapper($con, $sql, "getAllSonglistGroupings");
	while($row = mysqli_fetch_assoc($result)) {
		$grouping = new SongListGrouping();
		$grouping->id = $row['id'];
		$grouping->first = $row['first'];
		$grouping->last = $row['last'];
		$grouping->current = $row['current'];
		$grouping->visible = $row['visible'];
		array_push($allSongListGroupings, $grouping);
	}
	return $allSongListGroupings;
}

function findCurrentSongListGrouping($allSongListGroupings) {
	$currentSongListGrouping = null;
	foreach ($allSongListGroupings as $grouping) {
		if ($grouping->current == 1) {
			$currentSongListGrouping = $grouping;
			break;
		}
	}
	return $currentSongListGrouping;
}

function findSongListGroupingById($groupingId, $allSongListGroupings) {
	$songListGrouping = null;
	foreach ($allSongListGroupings as $grouping) {
		if ($grouping->id == $groupingId) {
			$songListGrouping = $grouping;
			break;
		}
	}
	return $songListGrouping;
}

function getAllAvailableSongs($con) {
	$allSongListGroupings = getAllSonglistGroupings($con);
	$min = -1;
	$max = -1;
	foreach ($allSongListGroupings as $grouping) {
		if ($grouping->visible == "1" && ($grouping->first < $min || $min == -1)) {
			$min = $grouping->first;
		}
		if ($grouping->visible == "1" && ($grouping->last > $max || $max == -1)) {
			$max = $grouping->last;
		}
	}
	return getSongsAndFeaturedGraphics($con, $min, $max, false, false);
}
function findSongListGroupingByNumber($numberToLookFor, $allSongListGroupings) {
	$songListGrouping = null;
	foreach ($allSongListGroupings as $grouping) {
		if ($numberToLookFor >= $grouping->first && $numberToLookFor <= $grouping->last) {
			$songListGrouping = $grouping;
			break;
		}
	}
	return $songListGrouping;
}

function createHtmlForSongGroupingLink($songListGrouping) {
	if ($songListGrouping->first == 1) {
		$html = "<a href='" . PERMALINKBASE . "/songlist?grouping=" . $songListGrouping->id .
		"'>Go to the top 10</a>";
	} else {
		$html = "<a href='" . PERMALINKBASE . "/songlist?grouping=" . $songListGrouping->id .
			"'>Go to songs " . $songListGrouping->first . " - " . $songListGrouping->last . "</a>";
	}
	return $html;
}

function createHtmlForAllSongListLink() {
	$html = "<a href='" . PERMALINKBASE . "/songlist?grouping=ALL'>See the entire top 100</a>";
	return $html;
}

function createHtmlForNonCurrentSongGroupingLinks($allSongListGroupings, $currentSongListGrouping, $visibleSetting) {
	// $visibleSetting - 0, means only want invisible
	// $visibleSetting - 1, means only want visible
	$html = "";
	foreach ($allSongListGroupings as $songListGrouping) {
		// skip over the current one
		if ($songListGrouping->id == $currentSongListGrouping->id) {
			continue;
		}
		// not visible, we're looking for visible, skip
			if (!$songListGrouping->visible && $visibleSetting) {
			continue;
		}
		// visible, we're looking for invisible, skip
		if ($songListGrouping->visible && !$visibleSetting) {
			continue;
		}
		$html .= createHtmlForSongGroupingLink($songListGrouping) . "<br/>";
	}
	return $html;
}

function getSongsAndFeaturedGraphics($con, $first, $last, $others, $onlyWithFeaturedGraphic) {
	$songsForGroup = array();
	$first += 100;
	$last += 100;
	$whereClause = " where t.topic_type='SONG' and t.sequence >= " . $first . " and t.sequence <= " . $last;
	if ($others) {
		$whereClause .= " or t.sequence = -2";
	}
	$orderBy = " order by t.sequence";
	$sql = "Select t.*, ts.graphic_image, ts2.article_text from topic t left outer join (select topic, type, graphic_image from selection where type=9) ts" .
			" on t.id=ts.topic " .
			" left outer join (select topic, type, article_text from selection where type=1) ts2 " .
			" on t.id=ts2.topic " . $whereClause . $orderBy;
	$result = mySqli_query_wrapper($con, $sql, "getSongsAndFeaturedGraphics");
	while($row = mysqli_fetch_assoc($result)) {
		$songBean = new SongBean();
		$songBean->id = $row['id'];
		$songBean->shortName = $row['short_name'];
		$songBean->name = $row['title'];
		$songBean->sequence = $row['sequence'] - 100;
		$songBean->featuredGraphic = $row['graphic_image'];
		$songBean->metadata = convertCRtoBR($row['article_text']);
		if (!$onlyWithFeaturedGraphic || !empty($songBean->featuredGraphic)) {
 			array_push($songsForGroup, $songBean);
		}
	}
	if ($others) {
		shuffle($songsForGroup);
	}
	return $songsForGroup;
}

function createHtmlForSongTeasers($con, $songListGrouping) {
	$songsForGroup = getSongsAndFeaturedGraphics($con, $songListGrouping->first, $songListGrouping->last, false, true);
	for ($i=0;$i<count($songsForGroup);$i++) {
		$resourceSelections[$i] = $i;
	}
	shuffle($resourceSelections);
	
	$html = "<center><table border='2'><tr>";
	for ($i=0;$i<NUM_SONG_TEASERS;$i++) {
		$song = $songsForGroup[$resourceSelections[$i]];
		$html .= "<td align='center' width='140'>";
		$html .= "<a href='" . PERMALINKBASE . "/song?topic=" . $song->shortName . "'>";
		$html .= "<img src='" . content_url() . "/images/" . $song->shortName . "/" . $song->featuredGraphic . "' width='120' height='90' />";
		$html .= "<br/><center>" .  $song->sequence . "<br/>" . $song->name . "</center>";
		$html .= "</a></td>";
	}
	$html .= "</tr></table></center>";
	return $html;
}

function createNextURL($autoplay, $seq, $topic) {
	if ($autoplay == 1) {
		$url = PERMALINKBASE . "/top-100-autoplay?command=next&autoplay=1&seq=" . $seq;
	} else {
		$url = PERMALINKBASE . "/top-100-autoplay?command=nextmanual&topic=" . $topic;
	}
	return $url;
}

function findSongIndex($topic, $allSongs) {
	for ($i=0;$i<count($allSongs);$i++) {
		$song = $allSongs[$i];
		if ($topic == $song->shortName) {
			return $i;
		}
	}
	return -1;
}
?>