<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/16/2019
 * Time: 11:30 AM
 */
class GameCardDao {
    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }

    function getAllGameCards($arrCategories) {
        $gameCards = array();
        $inPart = "'" . implode(", ", $arrCategories) . "'";
        $whereStatement = " where category in (" . $inPart . ")";
        $sql = "select * from " . centralTable("game_card") . $whereStatement;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "getAllGameCards");
        while ($row = mysqli_fetch_assoc($result)) {
            $gameCard = new GameCard();
            $gameCard->id = $row['id'];
            $gameCard->category = $row['category'];
            $gameCard->img = $row['image'];
            $gameCard->audio = $row['audio'];
            $gameCard->breadcrumb = $row['breadcrumb'];
            $gameCard->blurb = $row['blurb'];
            $gameCard->description = $row['description'];
            $gameCard->bonus = 0;
            array_push($gameCards, $gameCard);
        }
        return $gameCards;
    }


    function getGameCard($id) {
        $gameCard = null;
        $sql = "select * from " . centralTable("game_card") . " where id=" . $id;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "getGameCard");
        if ($row = mysqli_fetch_assoc($result)) {
            $gameCard = new GameCard();
            $gameCard->id = $row['id'];
            $gameCard->category = $row['category'];
            $gameCard->img = $row['image'];
            $gameCard->audio = $row['audio'];
            $gameCard->breadcrumb = $row['breadcrumb'];
            $gameCard->blurb = $row['blurb'];
            $gameCard->description = $row['description'];
        }
        return $gameCard;
    }

    function getCategoryMetadata($category) {
        $arrMetadata = array();
        $sql = "select * from " . centralTable("game_category_metadata") . " where category = '" . $category . "'";
        //echo $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "getCategoryMetadata");
        while ($row = mysqli_fetch_assoc($result)) {
            $nmvaluepair = new NameValuePair();
            $nmvaluepair->name = $row['name'];
            $nmvaluepair->value = $row['value'];
            array_push($arrMetadata, $nmvaluepair);
        }
        return $arrMetadata;
    }

    function getAllCategories() {
        $arrCategories = array();
        $sql = "select category from " . centralTable("game_card") . " group by category";
        //echo $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "getAllCategories");
        while ($row = mysqli_fetch_assoc($result)) {
            $category = $row['category'];
            array_push($arrCategories, $category);
        }
        return $arrCategories;

    }

    function upsert($gameCard) {
        $table = centralTable("game_card");
        if ($gameCard->id == -1) {
            // insert
            $doInsert = true;
            $sql = "insert into " . $table . " (category, image, audio, breadcrumb, blurb, description) " .
                "values ('" . $gameCard->category . "', '" .
                $gameCard->img . "', '" .
                $gameCard->audio . "', '" .
                $gameCard->breadcrumb . "', '" .
                $gameCard->blurb . "', '" .
                $gameCard->description . "')";

        } else {
            // update
            $doInsert = false;
            $sql = "update " . $table .
                " set category='" . $gameCard->category . "', " .
                "image='" . $gameCard->img . "', " .
                "audio='" . $gameCard->audio . "', " .
                "breadcrumb='" . $gameCard->breadcrumb . "', " .
                "blurb='" . $gameCard->blurb . "', " .
                "description='" . $gameCard->description . "' " .
                "where id = " . $gameCard->id;
        }
        //echo $sql;
        $result = mysqli_query($this->DbConnection, $sql) or die (mysqli_error($this->DbConnection));
        if ($doInsert) {
            $gameCard->id = mysqli_insert_id($this->DbConnection);
        }
        return $gameCard;
    }
}