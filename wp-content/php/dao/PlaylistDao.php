<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:24 AM
 */

class PlaylistDao {

    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }

    function getPlaylistsForOwner($ownerId) {
        $sql = "select id, playlist_guid, name, owner_id from " . centralTable("playlist");
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Error fetching playlist");
        $playlists = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $playlist = new Playlist();
            $playlist->id = $row['id'];
            $playlist->guid = $row['playlist_guid'];
            $playlist->name = $row['name'];
            $playlist->ownerId = $row['owner_id'];
            array_push($playlists, $playlist);
        }
        return $playlists;
    }

    function getPlaylistById($id) {
        // invalid playlist id
        if ($id <= 0) {
            return null;
        }
        // valid id
        $whereClause = "where p.id='" . $id . "'";
        return $this->getPlaylist($whereClause);
    }

    function getPlaylistByName($name) {
        $whereClause = "where p.name='" . $name . "'";
        return $this->getPlaylist($whereClause);
    }

    function getPlaylistByGuid($guid) {
        $whereClause = "where p.playlist_guid='" . $guid . "'";
        return $this->getPlaylist($whereClause);
    }

    function getPlaylist($whereClause) {
        $playlist = null;
        $sql = "select p.id, p.playlist_guid, p.name, p.owner_id, p.blurb, mi.id as mi_id, " .
            "pm.id as plmi_id, pm.blurb as pmblurb, pm.tagline, pm.post_date, pm.sequence, mi.title, " .
            "mi.artist, mi.album, mi.genre, mi.released from " . centralTable("playlist") .
            " p left outer join " . centralTable("playlist_media_item") . " pm on p.id=pm.playlist_id " .
            "left outer join " . centralTable("media_item") . " mi on pm.media_item_id=mi.id " .
            $whereClause . " order by pm.sequence desc";
        echo $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistDao getPlaylist");
        while ($row = mysqli_fetch_assoc($result)) {
            if ($playlist == null) {
                $playlist = new Playlist();
                $playlist->id = $row['id'];
                $playlist->guid = $row['playlist_guid'];
                $playlist->name = $row['name'];
                $playlist->ownerId = $row['owner_id'];
                $playlist->blurb = $row['blurb'];
            }
            if (!empty($row['plmi_id'])) {
                $playlistEntry = new PlaylistEntry();
                $playlistEntry->id = $row['plmi_id'];
                $playlistEntry->sequence = $row['sequence'];
                $playlistEntry->postDate = $row['post_date'];
                $playlistEntry->mediaItemId = $row['mi_id'];
                $playlistEntry->tagline = $row['tagline'];
                $playlistEntry->blurb = $row['pmblurb'];
                $playlistEntry->title = $row['title'];
                $playlistEntry->artist = $row['artist'];
                $playlistEntry->album = $row['album'];
                $playlistEntry->genre = $row['genre'];
                $playlistEntry->released = $row['released'];

                array_push($playlist->arrPlaylistEntries, $playlistEntry);
            }
        }
        if ($playlist != null) {
            // fetch associated playlist metadata
            $sql = "Select plmeta.property_name, plmeta.property_value from " .
                centralTable("playlist_metadata") . " plmeta where plmeta.playlist_id='" .
                $playlist->id . "'";
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistDao getPlaylist");
            while ($row = mysqli_fetch_assoc($result)) {
                $propertyName = $row['property_name'];
                $propertyValue = $row['property_value'];
                switch ($propertyName) {
                    case "playlist_page_invisible":
                        $playlist->playlistPageInvisible = ($propertyValue == '1');
                        break;
                    default:
                        // unknown property name, how to log it
                        break;
                }
            }
            // fetch metadata for the playlist items
            $sql = "Select mi.id, meta.plmi_id, meta.property_name, meta.property_value from " .
                centralTable("playlist_media_item") . " mi join " .
                centralTable("plmi_metadata") . " meta on mi.id=meta.plmi_id " .
                "where mi.playlist_id=" . $playlist->id;
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistDao getPlaylist");
            while ($row = mysqli_fetch_assoc($result)) {
                $plmiId = $row['plmi_id'];
                $playlistEntry = $this->findPlaylistEntry($plmiId, $playlist);
                if ($playlistEntry != null) {
                    $propertyName = $row['property_name'];
                    switch ($propertyName) {
                        case "video_id":
                            $playlistEntry->videoId = $row['property_value'];
                            break;
                        case "thumbnail_image":
                            $playlistEntry->thumbnailImage = $row['property_value'];
                            break;
                        case "blog_image":
                            $playlistEntry->blogImage = $row['property_value'];
                            break;
                        default:
                            // unknown property name, how to log it
                            break;
                    }
                }
            }
        }
        return $playlist;
    }

    function save($playlist) {
        if ($playlist->id == -1) {
            return $this->insert($playlist);
        } else {
            return $this->update($playlist);
        }
    }

    function insert($playlist) {
        $playlist->guid = $this->GUID ();
        $loggedInUser = wp_get_current_user();
        $sql = "insert into " . centralTable("playlist") .
            "(playlist_guid, owner_id, name, blurb, createdon, createdby) values (" .
                    "'" . $playlist->guid . "'," .
                    "'" . $loggedInUser->ID . "'," .
                    "'" . $playlist->name . "'," .
                    "'" . $playlist->blurb . "'," .
                    "'" . date("Y-m-d H:i:s") . "'," .
                    "'" . $loggedInUser->user_email . "'" .
                    ")";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "inserting playlist");
        $playlist->id = mysqli_insert_id($this->DbConnection);
        $this->updatePlaylistMetadata($playlist);
        return $playlist;
    }
    function update($playlist) {
        $loggedInUser = wp_get_current_user();
        $sql = "update " . centralTable("playlist") .
            " set name = '" . $playlist->name .
            "', blurb='" . $playlist->blurb .
            "', updatedon='" . date("Y-m-d H:i:s") .
            "', updatedby='" . $loggedInUser->user_email .
            "' where id=" . $playlist->id;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "updating playlist");
        $this->updatePlaylistMetadata($playlist);
        return $playlist;
    }

    function updatePlaylistMetadata($playlist) {
        $sql = "delete from " . centralTable("playlist_metadata") . " where playlist_id=" . $playlist->id;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Playlist metadata delete");
        $sql = "insert into " . centralTable("playlist_metadata") .
            " (playlist_id, property_name, property_value) values (" . $playlist->id .
            ",'playlist_page_invisible', '" . $playlist->playlistPageInvisible . "')";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Playlist metadata insert");
    }

    private function findPlaylistEntry($plmiId, $playlist) {
        foreach ($playlist->arrPlaylistEntries as $playlistEntry) {
            if ($playlistEntry->id == $plmiId) {
                return $playlistEntry;
            }
        }
        return null;
    }

    function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}
?>