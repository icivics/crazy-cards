<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 11/23/2017
 * Time: 8:28 AM
 */

class MediaCategoryDao {

    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }

    function getAllMediaCategories() {
        $mediaCategories = array();
        $sql = "select mc.id, mc.name, mc.description, mcf.field_name from " .
            centralTable("media_category") . " mc join " .
            centralTable("media_category_field") . " mcf on mc.id=mcf.category_id " .
            "order by mc.id, mcf.id";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaCategoryDao getAllMediaCategories");
        $thisCategory = null;
        while ($row = mysqli_fetch_assoc($result)) {
            $id = $row['id'];
            if ($thisCategory == null || $thisCategory->id != $id) {
                $category = new MediaCategory();
                $category->id = $row['id'];
                $category->name = $row['name'];
                $category->description = $row['description'];
                $thisCategory = $category;
                array_push($mediaCategories, $thisCategory);
            }
            array_push($thisCategory->arrFields, $row['field_name']);
        }
        return $mediaCategories;
    }

    function findMediaCategoryByName($mediaCategories, $name) {
        foreach ($mediaCategories as $mediaCategory) {
            if (strcasecmp($mediaCategory->name, $name) == 0) {
                return $mediaCategory;
            }
        }
        return null;
    }

}