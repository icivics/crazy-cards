<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/7/2017
 * Time: 8:09 PM
 */

class MediaItemDao {

    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }
    function saveMediaItem($mediaEntryBean) {
        $id = $mediaEntryBean->id;
        $doInsert = false;
        $loggedInUser = wp_get_current_user();
        date_default_timezone_set('America/New_York');
        if (empty($id)) {
            $sql = "Insert into " . centralTable("media_item") .
                " (title, artist, album, genre, released, tv_show, movie, event, product, " .
                "subject, categories, createdon, createdby) values ('" .
                $mediaEntryBean->title . "', '" .
                $mediaEntryBean->artist . "', '" .
                $mediaEntryBean->album . "', '" .
                $mediaEntryBean->genre . "', '" .
                $mediaEntryBean->released . "', '" .
                $mediaEntryBean->tvShow . "', '" .
                $mediaEntryBean->movie . "', '" .
                $mediaEntryBean->event . "', '" .
                $mediaEntryBean->product . "', '" .
                $mediaEntryBean->subject . "', '" .
                $this->concatMediaCategories($mediaEntryBean) . "', '" .
                date("Y-m-d H:i:s") . "','" .
                $loggedInUser->user_email . "')";
            $doInsert = true;
        } else {
            $sql = "Update " . centralTable("media_item") . " set title='" . $mediaEntryBean->title .
                "', artist='" . $mediaEntryBean->artist .
                "', album='" . $mediaEntryBean->album .
                "', genre='" . $mediaEntryBean->genre .
                "', released='" . $mediaEntryBean->released .
                "', tv_show='" . $mediaEntryBean->tvShow .
                "', movie='" . $mediaEntryBean->movie .
                "', event='" . $mediaEntryBean->event .
                "', product='" . $mediaEntryBean->product .
                "', subject='" . $mediaEntryBean->subject .
                "', categories='" . $this->concatMediaCategories($mediaEntryBean) .
                "', updatedon='" . date("Y-m-d H:i:s") .
                "', updatedby='" . $loggedInUser->user_email .
                "' where id=" . $id;
        }
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao saveMediaItem");
        if ($doInsert) {
            $mediaEntryBean->id = mysqli_insert_id($this->DbConnection);
        }
        $this->saveMediaCategories($mediaEntryBean);
        return $mediaEntryBean;
    }

    function concatMediaCategories($mediaEntryBean) {
        $sOutput = "";
        for ($i=0;$i<count($mediaEntryBean->arrMediaCategories);$i++) {
            $sOutput .= $mediaEntryBean->arrMediaCategories[$i]->name;
            if ($i < count($mediaEntryBean->arrMediaCategories) - 1) {
                $sOutput .= ", ";
            }
        }
        return $sOutput;
    }

    function saveMediaCategories($mediaEntryBean) {
        $sql = "delete from " . centralTable("media_item_category_xref") .
            " where media_item_id=" . $mediaEntryBean->id;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Deleting media_item_category_xref");
        foreach ($mediaEntryBean->arrMediaCategories as $mediaCategory) {
            $sql = "Insert into " . centralTable("media_item_category_xref") .
                " (media_item_id, media_category_id) values (" . $mediaEntryBean->id . ", " .
                $mediaCategory->id . ")";
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao saveMediaCategories");
        }
    }

    function updateVideoXrefs($mediaEntryBean, $videos) {
        $sql = "Delete from " . centralTable("media_item_video_xref") .
            " where media_item_id = " . $mediaEntryBean->id;
        $result = mysqli_query($this->DbConnection, $sql) or die (mysqli_error($this->DbConnection));
        foreach ($videos as $video) {
            $sql = "Insert into " . centralTable("media_item_video_xref") .
                " (media_item_id, video_id) values (" . $mediaEntryBean->id . ", " . $video->id . ")";
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao updateVideoXrefs");
        }
    }

    function findAndSaveIfNecessary($video) {
        $loggedInUser = wp_get_current_user();
        $sql = "Select * from " . centralTable("video") . " where video_id='" . $video->videoId . "'";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao findAndSaveIfNecessary");
        if ($row = mysqli_fetch_assoc($result)) {
            $video->id = $row['id'];
            $sql = "Update " . centralTable("video") . " set url = '" . $video->url .
                "', breadcrumb='" . $video->breadcrumb .
                "', updatedon='" . date("Y-m-d H:i:s") .
                "', updatedby='" . $loggedInUser->user_email .
                "' where id=" . $video->id;
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao findAndSaveVideoIfNecessary");
        } else {
            $sql = "Insert into " . centralTable("video") . " (video_id, url, category, breadcrumb, createdon, createdby) " .
                "values ('" .
                $video->videoId . "', '" .
                $video->url . "', '" .
                $video->category . "', '" .
                $video->breadcrumb . "', '" .
                date("Y-m-d H:i:s") . "','" .
                $loggedInUser->user_email . "')";
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao findAndSaveVideoIfNecessary");
            $id = mysqli_insert_id($this->DbConnection);
            $video->id = $id;
        }
        return $video;
    }

    function mediaItemExists($mediaItemId) {
        $sql = "select mi.id from " . centralTable("media_item") . " where id=" . $mediaItemId;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao mediaItemExists");
        if ( ($result =mysqli_fetch_assoc($result)) != null) {
            return true;
        } else {
            return false;
        }
    }

    function mediaItemCount($category) {
        $resultCount = 0;
        $mediaItemTable = centralTable("media_item");
        $xrefTable = centralTable("media_item_category_xref");
        $categoryTable = centralTable("media_category");
        if ($category == "ALL") {
            $sql = "select count(id) as countofid from " . $mediaItemTable;
        } else {
            $sql = "select count(mi.id) as countofid from " .
                        $mediaItemTable . " mi join " . $xrefTable . " x on mi.id=x.media_item_id " .
                        "join " . $categoryTable . " c on x.media_category_id=c.id where c.name='" .
                        $category . "'";
        }
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao mediaItemCount");
        if ($row = mysqli_fetch_assoc($result)) {
            $resultCount = $row['countofid'];
        }
        return $resultCount;
    }

    function getMediaInfoById($id) {
        $mediaEntryBean = null;
        if (empty($id) || $id <= 0) {
            return;
        }
        $sql = "select s.id, s.title, s.artist, s.album, s.genre, s.released, s.tv_show, s.movie, s.event, " .
            "s.product, s.subject, v.video_id, v.url, v.breadcrumb from " . centralTable("media_item") .
            " s left outer join " . centralTable("media_item_video_xref") . " x on s.id=x.media_item_id " .
            " left outer join " . centralTable("video") . " v on x.video_id=v.id" .
            " where s.id='" . $id . "'";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao getMediaInfoById");
        while ($row = mysqli_fetch_assoc($result)) {
            if ($mediaEntryBean == null) {
                $mediaEntryBean = new MediaEntryBean();
                $mediaEntryBean->id = $row['id'];
                $mediaEntryBean->title = $row['title'];
                $mediaEntryBean->artist = $row['artist'];
                $mediaEntryBean->album = $row['album'];
                $mediaEntryBean->genre = $row['genre'];
                $mediaEntryBean->released = $row['released'];
                $mediaEntryBean->event = $row['event'];
                $mediaEntryBean->tvShow = $row['tv_show'];
                $mediaEntryBean->movie = $row['movie'];
                $mediaEntryBean->product = $row['product'];
                $mediaEntryBean->subject = $row['subject'];
            }
            $video = new Video();
            $video->videoId = $row['video_id'];
            $video->url = $row['url'];
            $video->breadcrumb = $row['breadcrumb'];
            array_push($mediaEntryBean->arrVideos, $video);
        }
        $this->fetchMediaCategories($mediaEntryBean);
        return $mediaEntryBean;
    }

    function getRandomPool() {
        $arrItemId = array();
        $sql = "Select media_item_id from " . centralTable("media_item_category_xref") .
            " where media_category_id != 1";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao getRandomPool");
        while ($row = mysqli_fetch_assoc($result)) {
            $mediaItemId = $row['media_item_id'];
            array_push($arrItemId, $mediaItemId);
        }
        return $arrItemId;
    }

    function fetchMediaCategories($mediaEntryBean) {
        if ($mediaEntryBean == null) {
            return;
        }
        $mediaEntryBean->arrMediaCategories = array();
        $sql = "select c.id, c.name, c.description, mcf.field_name from " . centralTable("media_category") .
            " c join " . centralTable("media_item_category_xref") . " x on c.id=x.media_category_id " .
            " join " . centralTable("media_category_field") . " mcf on c.id=mcf.category_id" .
            " where x.media_item_id=" . $mediaEntryBean->id;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "MediaItemDao fetchMediaCategories");
        $thisCategory = null;
        while ($row = mysqli_fetch_assoc($result)) {
            $id = $row['id'];
            if ($thisCategory == null || $thisCategory->id != $id) {
                $mediaCategory = new MediaCategory();
                $mediaCategory->id = $row['id'];
                $mediaCategory->name = $row['name'];
                $mediaCategory->description = $row['description'];
                $thisCategory = $mediaCategory;
                array_push($mediaEntryBean->arrMediaCategories, $thisCategory);
            }
            array_push($thisCategory->arrFields, $row['field_name']);
        }
    }
}

?>