<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 11:24 AM
 */

class PlaylistEntryDao {

    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }

    function update($playlistEntry) {
        if (empty($playlistEntry->id)) {
            return;
        }
        $sql = "update " . centralTable("playlist_media_item") .
            " set blurb='" . $playlistEntry->blurb . "', post_date='" . $playlistEntry->postDate .
            "', tagline='" . $playlistEntry->tagline . "' where playlist_media_item.id=" . $playlistEntry->id;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntry update");
        $blogImageName = $playlistEntry->blogImage;
        if (!empty($blogImageName) ) {
            $sql = "insert into " . centralTable("plmi_metadata") .
                " (plmi_id, property_name, property_value) values (" . $playlistEntry->id . ",'blog_image', '" .
                $blogImageName . "')";
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntry metadata insert");
        }
    }

    function insert($mediaItemId, $playlist) {
        $mediaItemDao = new MediaItemDao($this->DbConnection);
        $mediaEntryBean = $mediaItemDao->getMediaInfoById($mediaItemId);
        if (empty($mediaEntryBean)) {
            // media item doesn't exist
            return;
        }
        if (count($mediaEntryBean->arrVideos) == 0) {
            // no videos
            return;
        }
        foreach ($playlist->arrPlaylistEntries as $playlistEntry) {
            if ($playlistEntry->mediaItemId == $mediaItemId) {
                // media item already in playlist
                return;
            }
        }
        $sequenceNumber = count($playlist->arrPlaylistEntries) + 1;
        $sql = "insert into ". centralTable("playlist_media_item") .
            " (playlist_id, media_item_id, sequence) " .
          "values (" . $playlist->id . "," . $mediaItemId . "," . $sequenceNumber . ")";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntry insert");
        $plmiId = $this->DbConnection->insert_id;
        $videoId = $mediaEntryBean->arrVideos[0]->videoId;
        $sql = "insert into " . centralTable("plmi_metadata") .
            " (plmi_id, property_name, property_value) values (" . $plmiId . ",'video_id', '" . $videoId . "')";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntry metadata insert");
        $sql = "insert into " . centralTable("plmi_metadata") .
            " (plmi_id, property_name, property_value) values (" . $plmiId . ",'thumbnail_image', '1.jpg')";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntry metadata insert");
    }

    function getPlaylistEntry($playlistEntryId) {
        $playlistEntry = null;
        $sql = "select plmi.id as plmi_id, plmi.sequence, mi.id as mi_id, plmi.tagline, plmi.post_date, " .
            "plmi.blurb as pmblurb, " .
            "mi.title, mi.artist, mi.album, mi.released from " .
            centralTable("playlist_media_item") . " plmi " .
            "join " . centralTable("media_item") . " mi on plmi.media_item_id=mi.id " .
            "where plmi.id=" . $playlistEntryId;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntryDao getPlaylistEntry");
        if ($row = mysqli_fetch_assoc($result)) {
            $playlistEntry = new PlaylistEntry();
            $playlistEntry->id = $row['plmi_id'];
            $playlistEntry->sequence = $row['sequence'];
            $playlistEntry->mediaItemId = $row['mi_id'];
            $playlistEntry->tagline = $row['tagline'];
            $playlistEntry->blurb = $row['pmblurb'];
            $playlistEntry->postDate = $row['post_date'];
            $playlistEntry->title = $row['title'];
            $playlistEntry->artist = $row['artist'];
            $playlistEntry->album = $row['album'];
            $playlistEntry->released = $row['released'];
        }
        if ($playlistEntry != null) {
            $sql = "select * from " . centralTable("plmi_metadata") .
                " meta where meta.plmi_id=" . $playlistEntryId;
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "PlaylistEntryDao getPlaylistEntry");
            while ($row = mysqli_fetch_assoc($result)) {
                $propertyName = $row['property_name'];
                switch ($propertyName) {
                    case "video_id":
                        $playlistEntry->videoId = $row['property_value'];
                        break;
                    case "thumbnail_image":
                        $playlistEntry->thumbnailImage = $row['property_value'];
                        break;
                    case "blog_image":
                        $playlistEntry->blogImage = $row['property_value'];
                        break;
                    default:
                        // unknown property name, how to log it
                        break;
                }
            }
        }
        return $playlistEntry;
    }

    function playlistEntryExists($playlistId, $mediaItemId) {
        $sql = "select mi.id from " . centralTable("media_item") .
            " mi join " . centralTable("playlist_media_item") .
            " plmi on mi.id=plmi.media_item_id where plmi.playlist_id=" . $playlistId . " and mi.id=" . $mediaItemId;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "playlist entry");
        if ($row = mysqli_fetch_assoc($result)) {
            return true;
        }
        return false;

    }
}
?>