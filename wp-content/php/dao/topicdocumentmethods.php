<?php

// topic configuration
function fetchDocumentsForTopicConfiguration($con, $configId, $visibleOnly, $subscriberId) {
	//echo "fetchDocumentsForTopicCOnfiguration subscriberid: " . $subscriberId . "<br/>";
	if (empty($subscriberId)) {
		$subscriberSql = "ps.id is null";
	} else {
		$subscriberSql = "(ps.id is null or ps.id='" . $subscriberId . "') order by ps.id";
	}

	$sql = "select td.id, td.shared, td.document_name, td.document_filename, td.document_url, tcd.profile_subscriber_id, tcd.visible " .
			"from topic_document td inner join topic_configuration_document tcd on tcd.topic_document_id=td.id " .
			"left outer join profile_subscriber ps on tcd.profile_subscriber_id=ps.id " .
			"where tcd.topic_configuration_id='" . $configId . "' and " .
			$subscriberSql;
			if ($visibleOnly) {
				$sql = $sql . " and tcd.visible=1";
			}
			//echo $sql;
			return fetchDocuments($con, $sql);
}

// topic configuration
function fetchUnlinkedSharedDocuments($con, $configId, $topicId) {
	$sql = "select td.id, td.shared, td.document_name, td.document_filename, document_url, '0' from topic_document td " .
			"where td.topic_id=" . $topicId . " and td.id not in (select tcd.topic_document_id from topic_configuration_document tcd " .
			"where tcd.topic_configuration_id=" . $configId . ")";
	return fetchDocuments($con, $sql);
}

// topic configuration
function getSubsetOfDocuments($docs, $shared) {
	$subsetOfDocuments = array();
	foreach ($docs as $doc) {
		if ($doc->shared == $shared) {
			array_push($subsetOfDocuments, $doc);
		}
	}
	return $subsetOfDocuments;
}

// topic configuration
function fetchSelectionDocument($con, $topicSelectionId) {
	$sql = "select id, shared, document_name, document_filename, document_url, '1' as visible from topic_document where selection_id=" . $topicSelectionId;
	$arrDocuments = fetchDocuments($con, $sql);
	if (count($arrDocuments) > 0) {
		return $arrDocuments[0];
	} else {
		return null;
	}
}

// topic configuration
function fetchDocuments($con, $sql) {
	$topicDocuments = array();
	//echo "<br/>fetchDocuments: <br/>" . $sql;
    $result = mySqli_query_wrapper($con, $sql, "fetchDocuments");
	while($row = mysqli_fetch_assoc($result))
	{
		$topicDocument = new TopicDocument("");
		$topicDocument->id = $row['id'];
		$topicDocument->name = $row['document_name'];
		$topicDocument->file = $row['document_filename'];
		$topicDocument->url = $row['document_url'];
		$topicDocument->shared = $row['shared'];
		$topicDocument->visible = ($row['visible'] == null ? 0 : $row["visible"]);
		$subscriberid = isset($row['profile_subscriber_id']) ? $row['profile_subscriber_id'] : "";
		$topicDocument->subscriberId = $subscriberid;
		$topicDocument->priority = (empty($subscriberid) ? 0 : 1);
		$existingDocument = findDocumentById($topicDocuments, $topicDocument->id);
		if (!empty($existingDocument)) {
			if ($existingDocument->priority < $topicDocument->priority) {
				$existingDocument->visible = $topicDocument->visible;
			}
		} else {
			array_push($topicDocuments, $topicDocument);
		}
		//echo "topicDocument name is: " . $topicDocument->name . "<br/>";
		
	}
	return $topicDocuments;

}


function filterOnlySharedDocs($allDocs) {
	$topicDocumentsShared = array();
	foreach ($allDocs as $td) {
		if (empty($td->subscriberId)) {
			array_push($topicDocumentsShared, $td);
		}
	}
	return $topicDocumentsShared;
}

function filterOnlyPrivateDocs($allDocs) {
	$topicDocumentsPrivate = array();
	foreach ($allDocs as $td) {
		if (!empty($td->subscriberId)) {
			array_push($topicDocumentsPrivate, $td);
		}
	}
	return $topicDocumentsPrivate;
}

function deleteTopicDocumentReferences($con, $privateDocumentIdsToDelete) {
	if (count($privateDocumentIdsToDelete) > 0) {
		$inclause = "(" . implode(",", $privateDocumentIdsToDelete) . ")";
		$sql = "delete from topic_configuration_document where topic_document_id in " . $inclause;
		$result = mysql_query($sql);
		if ($result != 1)
		{
			echo "Error deleting topic_configuration_docuemnts --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
		$sql = "delete from topic_document where id in " . $inclause;
        $result = mySqli_query_wrapper($con, $sql, "deleteTopicDocumentReferences");
		if ($result != 1)
		{
			echo "Error deleting topic_docuemnts --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
	}
}

function findDocumentById($documents, $id) {
	foreach ($documents as $topicDocument) {
		if ($topicDocument->id == $id)
			return $topicDocument;
	}
	return null;
}
// update topic configuration
function updateTopicConfigurationDocument($con, $tcRecord, $docRecord, $visible, $profileSubscriberId) {
	if ($profileSubscriberId == null) {
		$extraWhere = " profile_subscriber_id is NULL";
	} else {
		$extraWhere = " profile_subscriber_id = '" . $profileSubscriberId . "'";
	}
	//echo "Ready to updateTopicConfigurationDocument tcrecord: " . $tcRecord . "  docRecord: " . $docRecord . "  visible: " . $visible . "<br/>";
	$topicConfigurationDocument = findTopicConfigurationDocument($tcRecord, $docRecord, $profileSubscriberId);
	if ($topicConfigurationDocument == null) {
		if ($profileSubscriberId == null) {
			// don't include profile_subscriber_id if its value is null
			$sql = "insert into topic_configuration_document (topic_configuration_id, topic_document_id, visible) " .
					"values (" . $tcRecord . ", " . $docRecord . ", " . $visible . ")";
		} else {
			$sql = "insert into topic_configuration_document (profile_subscriber_id, topic_configuration_id, topic_document_id, visible) " .
				"values (" . $profileSubscriberId . ", " . $tcRecord . ", " . $docRecord . ", " . $visible . ")";
		}
	} else  {
		$sql = "update topic_configuration_document set visible=" . $visible .
		" where topic_configuration_id=" . $tcRecord . " and topic_document_id=" . $docRecord . " and " . $extraWhere;
	}
	//echo "UpdateTopicConfigurationDocument -- " . $sql . "<br/>";
    $result = mySqli_query_wrapper($con, $sql, "updateTopicConfigurationDocument");
	if ($result != 1)
	{
		echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
		exit();
	}
}

// insert a new private document
function insertPrivateDocument($con, $topicDocument, $topicConfiguration, $profileSubscriberId, $userId) {
	$unmagic = getUnmagicValue();
	$sql = "insert into topic_document (topic_id, shared, document_name, document_url, user_id, selection_id) " .
		"values (" . $topicConfiguration->topicId . ", 0, '" . 
		my_escape($topicDocument->name, $unmagic) . "', '" . my_escape($topicDocument->url, $unmagic) .
		"', " . $userId . ", -1)";
	//echo $sql . "<br/>";
    $result = mySqli_query_wrapper($con, $sql, "insertPrivateDocument");
	if ($result != 1)
	{
		echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
		exit();
	}
	$topicDocumentId = mysqli_insert_id();
	$sql = "insert into topic_configuration_document (profile_subscriber_id, topic_configuration_id, topic_document_id, visible) " .
		"values (" . $profileSubscriberId . ", " . $topicConfiguration->id . ", " . $topicDocumentId . ", " . 
		($topicDocument->visible ? 1 : 0) . ")";
	//echo $sql . "<br/>";
    $result = mySqli_query_wrapper($con, $sql, "insertPrivateDocument");
	if ($result != 1)
	{
		echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
		echo "SQL is " . $sql;
		exit();
	}
	//exit();
}

// topic configuration
function findTopicConfigurationDocument($con, $tcRecord, $docRecord, $profileSubscriberId) {
	if ($profileSubscriberId == null) {
		$extraWhere = " profile_subscriber_id is NULL";
	} else {
		$extraWhere = " profile_subscriber_id = '" . $profileSubscriberId . "'";
	}
	$tcd = new TopicConfigurationDocument();
	$sql = "select id, topic_configuration_id, topic_document_id, visible from topic_configuration_document where " .
			"topic_configuration_id=" . $tcRecord . " and topic_document_id=" . $docRecord . " and " . $extraWhere;
    $result = mySqli_query_wrapper($con, $sql, "findTopicConfigurationDocument");
	if ($row = mysqli_fetch_assoc($result))
    {
		$tcd = new TopicConfigurationDocument();
		$tcd->id = $row["id"];
		$tcd->topicConfigurationId = $row["topic_configuration_id"];
		$tcd->topicDocumentId = $row["topic_document_id"];
		$tcd->visible = $row["visible"];
		return $tcd;
	}
	return null;
}

// topic document configuration
function updateTopicDocument($con, $doc, $topicId, $tcRecord) {
	$topicDocument = findTopicDocument($topicId, $tcRecord, $doc->file, $doc->name);
	if ($topicDocument == null) {
		$sql = "insert into topic_document (topic_id, shared, document_name, document_filename, document_url, user_id, selection_id) " .
				"values (" . $topicId . ", '0', '" . $doc->name . "', '" . $doc->file .
				"', '" . $doc->url . "', '0', '-1')";
		//echo "updateTopicDocument -- " . $sql . "<br/>";
        $result = mySqli_query_wrapper($con, $sql, "updateTopicDocument");
		if ($result != 1)
		{
			echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
		$insertId = mysqli_insert_id();
		//echo "InsertId: " . $insertId;
		$topicDocument = new TopicDocument("");
		$topicDocument->id = $insertId;
		$topicDocument->name = $doc->name;
		$topicDocument->file = $doc->file;
		$topicDocument->url = $doc->url;
		$topicDocument->shared = 0;
		return $topicDocument;
	}
	else {
		$topicDocument->name = $doc->name;
		$topicDocument->file = $doc->file;
		$topicDocument->url = $doc->url;
		$sql = "update topic_document set document_name='" . $doc->name . "', document_filename='" . $doc->file .
		"', document_url='" . $doc->url . "'" .
		" where id=" . $topicDocument->id;
		//echo "updateTopicDocument -- " . $sql . "<br/>";
        $result = mySqli_query_wrapper($con, $sql, "updateTopicDocument");
		return $topicDocument;
	}
}

function findTopicDocument($con, $topicId, $tcRecord, $filename, $docname) {
	$sql = "select td.id, td.topic_id, td.shared, td.document_name, td.document_filename from topic_document td inner join " .
			"topic_configuration_document tcd on td.id=tcd.topic_document_id where td.document_filename='" . $filename .
			"' and td.document_name='" . $docname .
			"' and tcd.topic_configuration_id=" . $tcRecord;
	//echo "findTopicDocument -- " . $sql . "<br/>";
    $result = mySqli_query_wrapper($con, $sql, "fetchProfileForConfigId");
    if ($row = mysqli_fetch_assoc($result)) {
    	$topicDocument = new TopicDocument("");
		$topicDocument->id = $row['id'];
		$topicDocument->name = $row['document_name'];
		$topicDocument->file = $row['document_filename'];
		$topicDocument->shared = $row['shared'];
		//$topicDocument->dump();
		return $topicDocument;
	}
	return null;
}

function generateHtmlForDocuments($topicDocumentsAll) {
	$fullHtml = "";
	$shareddoccount = 0;
	$topicDocumentsShared = filterOnlySharedDocs($topicDocumentsAll);
	$topicDocumentsPrivate = filterOnlyPrivateDocs($topicDocumentsAll);

	// shared documents
	$fullHtml .= "Shared documents:<br/><br/>";
	$fullHtml .= "<table cellspacing=2 width='550'>";
	foreach ($topicDocumentsShared as $td) {
		$fullHtml .= "<tr><td rowspan='2'>";
		$fullHtml .= "<input type='checkbox' name='doc_" . $td->id . "' ";
		if ($td->visible) {
			$fullHtml .= " CHECKED";
		}
		$fullHtml .= " />";
		$fullHtml .= "</td><td>Name:</td>";
		$fullHtml .= "<td>" . $td->name . "</td>";
		$fullHtml .= "</tr><tr><td>URL:</td><td style='max-width:475px; word-wrap:break-word;'>" . $td->url . "</td></tr>";
		$shareddoccount++;
	}
	$fullHtml .= "</table>";
	// private documents, including empty text boxes up to 3
	$fullHtml .= "Private documents:<br/><br/>";
	$fullHtml .= "<table cellspacing=2>";
	$privatedoccount = 0;
	foreach ($topicDocumentsPrivate as $td) {
		$fullHtml .= "<tr><td rowspan='2'>";
		$fullHtml .= "<input type='checkbox' name='privatedocvisible" . $privatedoccount . "' ";
		if ($td->visible) {
			$fullHtml .= " CHECKED";
		}
		$fullHtml .= " />";
		$fullHtml .= "</td><td>Name:</td>";
		$fullHtml .= "<td><input type='text' name='privatedocname" . $privatedoccount . "' ";
		$fullHtml .= "value='" . fixQuotes($td->name) . "' maxlength='70' size='70' /></td>";
		$fullHtml .= "</tr><tr><td>URL:</td>";
		//$fullHtml .= "<td><textarea name='privatedocurl" . $privatedoccount . "' ";
		//$fullHtml .= "rows='2' cols='70' maxlength='150'>" . $td->url . "</textarea></td></tr>";
		$fullHtml .= "<td><input type='text' name='privatedocurl" . $privatedoccount . "' ";
		$fullHtml .= "size='70' maxlength='150' value='" . $td->url . "' /></td></tr>";
		$privatedoccount++;
	}
	$openspaces = 3 - $privatedoccount;
	if ($openspaces > 0) {
		for ($i=0;$i<$openspaces;$i++) {
			$fullHtml .= "<tr><td rowspan='2'>";
			$fullHtml .= "<input type='checkbox' name='privatedocvisible" . $privatedoccount . "' ";
			$fullHtml .= " />";
			$fullHtml .= "</td><td>Name:</td>";
			$fullHtml .= "<td><input type='text' name='privatedocname" . $privatedoccount . "' ";
			$fullHtml .= "value='' maxlength='70' size='70' /></td>";
			$fullHtml .= "</tr><tr><td>URL: </td>";
			//$fullHtml .= "<td><textarea name='privatedocurl" . $privatedoccount . "' ";
			//$fullHtml .= "rows='2' cols='70' maxlength='150'></textarea></td></tr>";
			$fullHtml .= "<td><input type='text' name='privatedocurl" . $privatedoccount . "' ";
			$fullHtml .= "size='70' maxlength='150' value='' /></td></tr>";
			$privatedoccount++;
		}
	}
	$fullHtml .= "</table>";
	return $fullHtml;
}


function generateHtmlForDocumentsMainPage($topicDocumentsVisible, $persona) {
	$fullHtml = "";
	$fullHtml .= "<section class=scorecard>";
	$fullHtml .= "<h6 class=padded-title>DOCUMENTS (" . count($topicDocumentsVisible) . ")</h6>";
	$fullHtml .= "<div id='documents' class=scores>";
	if ($persona == TEACHER || $persona == LOGGEDINUSER) {
		$fullHtml .= "Documents will appear at the bottom of the page in the student view<br/><br/>";
	}
	for ($i=0; $i<count($topicDocumentsVisible); $i++) {
		$aDocument = $topicDocumentsVisible[$i];
		$fullHtml .= "<a href='" . $aDocument->url . "' target='_new'>";
		$fullHtml .="<img src='" . ABSURL . "wp-content/images/Small-Document-gray-icon.jpg" . "' />&nbsp;";
		$fullHtml .= $aDocument->name . "</a><br/>";
	}
	$fullHtml .= "</div></section>";
	return $fullHtml;
}
?>
