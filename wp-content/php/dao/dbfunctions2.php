<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/10/2017
 * Time: 10:12 PM
 */

/* --------------------------------- */
/** Custom Instructions **/

function getCustomInstructions($con, $profileSubscriber, $topicConfiguration) {
    if (empty($profileSubscriber->subscriberId)) {
        return null;
    }
    $sql = "select * from topic_configuration_meta tcm where tcm.topic_configuration_id = " . $topicConfiguration->id .
        " and tcm.profile_subscriber_id=" . $profileSubscriber->id . " and tcm.meta_key='instructions'";
    //echo $sql;
    $result = mySqli_query_wrapper($con, $sql, "getCustomInstructions");
    if ($result == true) {
        if ($row = mysqli_fetch_assoc($result)) {
            $customInstructions = new CustomInstructions();
            $customInstructions->id = $row['id'];
            $customInstructions->profileSubscriberId = $row['profile_subscriber_id'];
            $customInstructions->topicConfigurationId = $row['topic_configuration_id'];
            $customInstructions->instructions = $row['meta_value'];
            return $customInstructions;
        }
    }
    return null;
}


function createGenericInstructions($topicConfiguration) {
    // won't be shown for visitor
    if (mmc_getCurrentState()->persona == LOGGEDINUSER) {
        $visibleInstructionsHtml = "When you get a classroom code, this area will contain instructions for your students.";
    } else if (mmc_getCurrentState()->persona == TEACHER) {
        $visibleInstructions = $topicConfiguration->instructions;
        $visibleInstructionsHtml = $visibleInstructions;
        $visibleInstructionsHtml .= "\n\nWhen you've completed the assignment, share the link with <b>" .
            mmc_getCurrentState()->classroomCodeObject->associatedUser->user_email . "</b>";
    } else if (mmc_getCurrentState()->persona == STUDENT) {
        $visibleInstructions = $topicConfiguration->instructions;
        $visibleInstructionsHtml = $visibleInstructions;
        $visibleInstructionsHtml .= "\n\nWhen you've completed the assignment, share the link with <b>" .
            mmc_getCurrentState()->classroomCodeObject->associatedUser->user_email . "</b>";
    }
    $visibleInstructionsHtml = convertCRtoBR($visibleInstructionsHtml);
    return $visibleInstructionsHtml;
}

function createInstructions($topicConfiguration) {
    $visibleInstructionsHtml = "";
    if ($topicConfiguration->instructionsCustomized == false) {
        //echo "instructions customized is FALSE";
        $visibleInstructionsHtml = createGenericInstructions($topicConfiguration);
    } else  {
        //echo "instructions customized is TRUE";
        $visibleInstructions = $topicConfiguration->instructions;
        $visibleInstructionsHtml = $visibleInstructions;
    }

    return $visibleInstructionsHtml;
}

function updateCustomInstructions($con, $profileSubscriber, $topicConfiguration, $instructions) {
    $customInstructions = getCustomInstructions($profileSubscriber, $topicConfiguration);
    if ($customInstructions == null) {
        $sql = "Insert into topic_configuration_meta (profile_subscriber_id, topic_configuration_id, meta_key, meta_value) " .
            "values (" . $profileSubscriber->id . ", " . $topicConfiguration->id . ", 'instructions', '" . $instructions . "')";
    } else {
        $sql = "Update topic_configuration_meta set meta_value = '" . $instructions . "' where profile_subscriber_id=" .
            $profileSubscriber->id . " and topic_configuration_id=" . $topicConfiguration->id .
            " and meta_key='instructions'";
    }
    //echo $sql;
    $result = mySqli_query_wrapper($con, $sql, "updateCustomInstructions");
    if ($result != 1) {
        echo "Error updating custom instructions --- Result = " . $result . "  error: " . mysql_error();
        echo "SQL is " . $sql;
        exit();
    }
}






/* --------------------------------- */
/** quotes **/

function fetchquotes($con) {
    $quotes = array();
    $sql = "Select * from quotes order by id ";
    $result = mySqli_query_wrapper($con, $sql, "quotes");
    while($row = mysqli_fetch_assoc($result))
    {
        $quote = new quote("");
        $quote->id = $row['id'];
        $quote->quotetext = $row['quotetext'];
        $quote->author = $row['author'];
        $quote->source = $row['source'];
        array_push($quotes, $quote);
    }
    return $quotes;
}

// quotes
function selectRandomQuote($con) {
    $arrQuotes = fetchQuotes($con);
    $arrLength = count($arrQuotes);
    $arrIndex = rand(0, $arrLength - 1);
    return $arrQuotes[$arrIndex];
}

function selectQuoteOfTheDay($con) {
    $arrQuotes = fetchQuotes($con);
    $arrLength = count($arrQuotes);
    $now = time(); // or your date as well
    $your_date = strtotime("2012-10-01 00:00:00");
    $datediff = floor(($now - $your_date) / (60*60*24));
    $arrIndex = $datediff % $arrLength;
    return $arrQuotes[$arrIndex];
}

// topic selections
function fetchTopicSelections($con, $topicShortName) {
    $sql="SELECT * FROM selection sl inner join topic tp on sl.topic=tp.id WHERE tp.short_name = '".$topicShortName ."' and sl.sequence <= 8 order by sl.sequence";
    //echo $sql . "<br/>";
    $result = mySqli_query_wrapper($con, $sql, "fetchTopicSelections");
    //echo "MYSQL Error code: " . mysql_errno() . ": " . mysql_error() . "<br/>";
    $selectionIndex = 0;
    $topicSelections = array();
    while($row = mysqli_fetch_assoc($result)) {
        $aSelection = new Selection();
        // row[0] yields id of selection
        $aSelection->id = $row[0];
        $aSelection->type = $row['type'];
        $aSelection->article = convertUtf8(nl2br($row['article_text']));
        $aSelection->thumbnail = $row['thumbnail_image'];
        $aSelection->image = $row['graphic_image'];
        $aSelection->breadcrumb = convertUtf8(nl2br($row['breadcrumb']));
        $aSelection->videoEmbedCode = $row['video_embed_code'];
        $aSelection->videoURL = $row['video_url'];
        $aSelection->caption = $row['caption'];
        $aSelection->size = $row['size'];
        $topicSelections[$selectionIndex] = $aSelection;
        $selectionIndex++;
    }
    return $topicSelections;
}

// topic configuration
function fetchDefaultConfigId($con, $topic) {
    $profileId = getProfileId($con, "MYMORALCOMPASS");
    return fetchConfigIdForProfileId($con, $topic, $profileId);
}

// topic configuration
function fetchConfigIdForProfileId($con, $topic, $profileId) {
    $configId = "";
    $sql = "select tc.id from topic inner join topic_configuration tc on topic.id = tc.topic_id where topic.short_name='" . $topic .
        "' and tc.profile_id=" . $profileId;
    //echo "FetchDefaultConfigId sql: <br/>" . $sql;
    $result = mySqli_query_wrapper($con, $sql, "fetchConfigIdForProfileId");
    if ($row = mysqli_fetch_assoc($result)) {
        $configId = $row['id'];
        //echo "config id: " . $configId . "<br/>";
    }
    //else echo "found no rows<br/>";
    return $configId;
}

// topic configuration
function fetchProfileForConfigId($con, $configId) {
    $profile = "";
    $sql = "select p.shortname from topic_configuration tc join profile p on tc.profile_id=p.id where tc.id=" . $configId;
    $result = mySqli_query_wrapper($con, $sql, "fetchProfileForConfigId");
    if ($row = mysqli_fetch_assoc($result)) {
        $profile = $row['shortname'];
    }
    //echo "fetchProfileForCOnfigId configId: " . $configId . "  profilename: " . $profile . "<br/>";
    return $profile;
}

// topic configuration
function fetchTopicConfiguration($con, $configId, $subscriberId) {
    //echo "config id: " . $configId;
    $topicConfiguration = new TopicConfiguration();
    $topicConfiguration->id = -1;
    $topicConfiguration->topicId = -1;
    $topicConfiguration->showInstructions = false;
    $topicConfiguration->showAuthorAnalysis = true;
    $topicConfiguration->showAuthorScorecard = true;
    $topicConfiguration->showUserScorecard = true;
    $topicConfiguration->showGenericScorecard = false;
    $topicConfiguration->showComments = true;
    $topicConfiguration->showDbqs = false;
    $topicConfiguration->showLoginControls = false;
    $topicConfiguration->showTopicDocuments = false;
    $topicConfiguration->showConfigDocuments = false;
    $topicConfiguration->instructions = "";
    $topicConfiguration->instructionsCustomized = false;
    $topicConfiguration->dbq1 = "";
    $topicConfiguration->dbq2 = "";
    $topicConfiguration->dbq3 = "";
    $topicConfiguration->doc1 = "";
    $topicConfiguration->doc2 = "";
    $topicConfiguration->doc3 = "";
    $topicShortName = "";

    if (!empty($configId)) {
        // add logic for subscriber id if null
        if (empty($subscriberId)) {
            $subscriberSql = "ps.id is null";
        } else {
            $subscriberSql = "(ps.id is null or ps.subscriber_id='" . $subscriberId . "')";
        }
        $sql = "select ps.id as profilesubscriberid, tc.id as topicconfigid, tc.*, tp.short_name, tcm.* from topic_configuration tc " .
            "inner join topic tp on tc.topic_id=tp.id " .
            "left outer join topic_configuration_meta tcm on tcm.topic_configuration_id=tc.id " .
            "left outer join profile_subscriber ps on tcm.profile_subscriber_id=ps.id " .
            "where tc.id='" . $configId . "' and " . $subscriberSql;

        //echo $sql . "<br/>";
        $result = mySqli_query_wrapper($con, $sql, "fetchTopicConfiguration");
        $numResults = mysqli_num_rows($result);
        //echo "\nNumber of results: " . $numResults;
        $myhashmap = array();
        if ($numResults > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                $topicConfiguration->topicId = $row['topic_id'];
                $metaKey = $row['meta_key'];
                $metaValue = $row['meta_value'];
                if ($metaKey == null) {
                    continue;
                }
                $subscriberIdFromDb = $row['profilesubscriberid'];
                if (!empty($subscriberIdFromDb)) {
                    $metaPriority = 1;
                    if ($metaKey == "instructions") {
                        $topicConfiguration->instructionsCustomized = true;
                    }
                } else {
                    $metaPriority = 0;
                }
                $useThisOne = false;
                if ($myhashmap[$metaKey] == null) {
                    $myhashmap[$metaKey] = $metaPriority;
                    $useThisOne = true;
                } else if ($metaPriority > $myhashmap[$metaKey]) {
                    $myhashmap[$metaKey] = $metaPriority;
                    $useThisOne = true;
                }
                //echo "metaKey: " . $metaKey . "<br/>";
                //echo "metaValue: " . $metaValue . "<br/>";
                //echo "useThisOne: " . $useThisOne . "<br/>";
                $topicConfiguration->id = $row["topicconfigid"];
                if ($topicConfiguration->topicId == -1) {
                    $topicConfiguration->topicId = $row["topic_id"];
                }
                if (empty($topicShortName))
                    $topicShortName = $row["short_name"];
                //echo "metaKey = " . $metaKey . " metaValue = " . $metaValue . "<br/>";
                if ($useThisOne) {
                    if ($metaKey == "show_instructions")
                        $topicConfiguration->showInstructions = strToBool($metaValue);
                    else if ($metaKey == "instructions") {
                        $topicConfiguration->instructions = $metaValue;
                        if ($metaPriority == 1) {
                            $topicConfiguration->instructionsCustomized = true;
                        }
                    }
                    else if ($metaKey == "show_author_analysis")
                        $topicConfiguration->showAuthorAnalysis = strToBool($metaValue);
                    else if ($metaKey == "show_author_scorecard")
                        $topicConfiguration->showAuthorScorecard = strToBool($metaValue);
                    else if ($metaKey == "show_user_scorecard")
                        $topicConfiguration->showUserScorecard = strToBool($metaValue);
                    else if ($metaKey == "show_generic_scorecard")
                        $topicConfiguration->showGenericScorecard = strToBool($metaValue);
                    else if ($metaKey == "show_comments")
                        $topicConfiguration->showComments = strToBool($metaValue);
                    else if ($metaKey == "show_dbqs")
                        $topicConfiguration->showDbqs = strToBool($metaValue);
                    else if ($metaKey == "show_login_controls")
                        $topicConfiguration->showLoginControls = strToBool($metaValue);
                    else if ($metaKey == "show_topic_documents")
                        $topicConfiguration->showTopicDocuments = strToBool($metaValue);
                    else if ($metaKey == "show_config_documents")
                        $topicConfiguration->showConfigDocuments = strToBool($metaValue);
                    else if ($metaKey == "dbq1")
                        $topicConfiguration->dbq1 = $metaValue;
                    else if ($metaKey == "dbq2")
                        $topicConfiguration->dbq2 = $metaValue;
                    else if ($metaKey == "dbq3")
                        $topicConfiguration->dbq3 = $metaValue;
                    else if ($metaKey == "doc1")
                        $topicConfiguration->doc1 = $metaValue;
                    else if ($metaKey == "doc2")
                        $topicConfiguration->doc2 = $metaValue;
                    else if ($metaKey == "doc3")
                        $topicConfiguration->doc3 = $metaValue;
                }
            }
        }

        $selectionArray = fetchTopicSelections($con, $topicShortName);
        $selectionIndex = 0;
        $dbqCount = 0;
        while ($selectionIndex < count($selectionArray)) {
            $aSelection = $selectionArray[$selectionIndex];
            switch ($aSelection->type)
            {
                case Selection::INSTRUCTIONS:
                    if (empty($topicConfiguration->instructions))
                        $topicConfiguration->instructions = $aSelection->article;
                    break;
                case Selection::WRITINGPROMPT:
                    if ($dbqCount == 0) {
                        if (empty($topicConfiguration->dbq1))
                            $topicConfiguration->dbq1 = $aSelection->article;
                    }
                    else if ($dbqCount == 1) {
                        if (empty($topicConfiguration->dbq2))
                            $topicConfiguration->dbq2 = $aSelection->article;
                    }
                    else if ($dbqCount == 2) {
                        if (empty($topicConfiguration->dbq3))
                            $topicConfiguration->dbq3 = $aSelection->article;
                    }
                    $dbqCount++;
                    break;
            }
            $selectionIndex++;
        }
    }
    /*
    echo "Topic configuration topicId: " . $topicConfiguration->topicId . "<br/>";
    echo "Show Instructions: " . boolToStr($topicConfiguration->showInstructions) . "<br/>";
    echo "Show Intro: " . boolToStr($topicConfiguration->showIntro) . "<br/>";
    echo "Show showAuthorAnalysis: " . boolToStr($topicConfiguration->showAuthorAnalysis) . "<br/>";
    echo "Show showAuthorScorecard: " . boolToStr($topicConfiguration->showAuthorScorecard) . "<br/>";
    echo "Show showUserScorecard: " . boolToStr($topicConfiguration->showUserScorecard) . "<br/>";
    echo "Show showComments: " . boolToStr($topicConfiguration->showComments) . "<br/>";
    echo "Show showDbqs: " . boolToStr($topicConfiguration->showDbqs) . "<br/";
    echo "Instructions: " . $topicConfiguration->instructions;
    */
    return $topicConfiguration;
}

function narrowDownHistoricalFiguresBasedOnProfile($con, $topicSelections, $fullProfile) {
    $newTopicSelections = array();
    $lstTopics = fetchTopicListForProfile($con, $fullProfile);

    $idx = 0;
    while ($idx < count($topicSelections)) {
        $topicSelection = $topicSelections[$idx];
        // remove element from list if topic isn't contained in list
        $topicsContained = topicsContainedIn($lstTopics, $topicSelection->breadcrumb);
        if (count($topicsContained) > 0) {
            array_push($newTopicSelections, $topicSelections[$idx]);
        }
        $idx++;
    }

    return $newTopicSelections;
}

function topicsContainedIn($lstTopics, $matchingTopicString) {
    $matchingTopicString = strtolower($matchingTopicString);
    $matchingTopics = explode(",", $matchingTopicString);

    $idx = count($matchingTopics) - 1;
    while ($idx >= 0) {
        $matchingTopics[$idx] = strtolower(trim($matchingTopics[$idx]));
        if (!in_array($matchingTopics[$idx], $lstTopics)) {
            //echo "in_array with " . $matchingTopics[$idx] . " returns FALSE<br/>";
            unset($matchingTopics[$idx]);
        } else  {
            //echo "in_array with " . $matchingTopics[$idx] . " returns TRUE<br/>";
        }
        $idx--;
    }
    return $matchingTopics;
}

function fetchTopicListForProfile($con, $profile) {
    $sql = "SELECT t.short_name FROM category cat join categorytopic ct on cat.id=ct.category_id " .
        "join topic_configuration tc on ct.topic_configuration_id = tc.id " .
        "join topic t on tc.topic_id=t.id " .
        "where cat.profile_id=" . $profile->id;
    //echo "fetchTopicListForProfile sql: " . $sql;
    $lstTopics = array();
    $result = mySqli_query_wrapper($con, $sql, "fetchTopicListForProfile");
    while($row = mysqli_fetch_assoc($result)) {
        $topicShortName = $row['short_name'];
        array_push($lstTopics, strtolower($topicShortName));
    }
    return $lstTopics;
}


?>