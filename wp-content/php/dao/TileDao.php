<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 9/1/2018
 * Time: 9:28 PM
 */
class TileDao {

    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }

    function getTilesForPage($page) {
        $sql = "select id, page, title, description, destination, image, sequence from " . centralTable( "tile") . " where page='" . $page . "' order by sequence";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Error fetching tiles");
        $tiles = array();
        //echo "getting tiles";
        while ($row = mysqli_fetch_assoc($result)) {
            //echo "tile processing";
            $tile = new Tile();
            $tile->id = $row['id'];
            $tile->page = $row['page'];
            $tile->title = $row['title'];
            $tile->description = $row['description'];
            $tile->destination = $row['destination'];
            $tile->image = $row['image'];
            $tile->sequence = $row['sequence'];
            array_push($tiles, $tile);
        }
        return $tiles;
    }

    function upsert($tile) {
        $table = centralTable("tile");
        if ($tile->id == -1) {
            // insert
            $doInsert = true;
            $sql = "insert into " . $table . " (page, title, description, destination, image, sequence) " .
                "values ('" . $tile->page . "', '" .
                $tile->title . "', '" .
                $tile->description . "', '" .
                $tile->destination . "', '" .
                $tile->image . "', '" .
                $tile->sequence . "')";

        } else {
            // update
            $doInsert = false;
            $sql = "update " . $table .
                " set page='" . $tile->page . "', " .
                "title='" . $tile->title . "', " .
                "description='" . $tile->description . "', " .
                "destination='" . $tile->destination . "', " .
                "image='" . $tile->image . "', " .
                "sequence='" . $tile->sequence . "' " .
                "where id = " . $tile->id;
        }
        //echo $sql;
        $result = mysqli_query($this->DbConnection, $sql) or die (mysqli_error($this->DbConnection));
        if ($doInsert) {
            $tile->id = mysqli_insert_id($this->DbConnection);
        }
        return $tile;
    }

    function getTile($id, $page) {
        $tile = null;
        $sql = "select * from " . centralTable("tile") . " where id=" . $id . " and page='" . $page . "'";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "getGameCard");
        if ($row = mysqli_fetch_assoc($result)) {
            $tile = new Tile();
            $tile->id = $row['id'];
            $tile->page = $row['page'];
            $tile->image = $row['image'];
            $tile->title = $row['title'];
            $tile->destination = $row['destination'];
            $tile->sequence = $row['sequence'];
            $tile->description = $row['description'];
        }
        return $tile;
    }

    function getTiles($page) {
        $tiles = array();
        $sql = "select * from " . centralTable("tile") . " where page='" . $page . "'";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "getTiles");
        while ($row = mysqli_fetch_assoc($result)) {
            $tile = new Tile();
            $tile->id = $row['id'];
            $tile->page = $page;
            $tile->image = $row['image'];
            $tile->destination = $row['destination'];
            $tile->title = $row['title'];
            $tile->sequence = $row['sequence'];
            $tile->description = $row['description'];
            array_push($tiles, $tile);
        }
        return $tiles;
    }

    function deleteTile($id) {
        $sql = "DELETE FROM " . centralTable('tile') . " WHERE id='" . $id . "'";
        //echo "SQL: " . $sql;
        return mySqli_query_wrapper($this->DbConnection, $sql, "getTiles");
    }
}

?>