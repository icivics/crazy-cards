<?php
	
	/* --------------------------------- */
	/** Custom Instructions **/
	
	function getCustomInstructions($profileSubscriber, $topicConfiguration) {
		if (empty($profileSubscriber->subscriberId)) {
			return null;
		}
		$sql = "select * from topic_configuration_meta tcm where tcm.topic_configuration_id = " . $topicConfiguration->id .
			" and tcm.profile_subscriber_id=" . $profileSubscriber->id . " and tcm.meta_key='instructions'";
		//echo $sql;
		$result = mysql_query($sql);
		if ($result == true) {
			if ($row = mysql_fetch_array($result)) {
				$customInstructions = new CustomInstructions();
				$customInstructions->id = $row['id'];
				$customInstructions->profileSubscriberId = $row['profile_subscriber_id'];
				$customInstructions->topicConfigurationId = $row['topic_configuration_id'];
				$customInstructions->instructions = $row['meta_value'];
				return $customInstructions;
			}
		}
		return null;
	}

	
	function createGenericInstructions($topicConfiguration) {
		// won't be shown for visitor
		if (mmc_getCurrentState()->persona == LOGGEDINUSER) {
			$visibleInstructionsHtml = "When you get a classroom code, this area will contain instructions for your students.";
		} else if (mmc_getCurrentState()->persona == TEACHER) {
			$visibleInstructions = $topicConfiguration->instructions;
			$visibleInstructionsHtml = $visibleInstructions;
			$visibleInstructionsHtml .= "\n\nWhen you've completed the assignment, share the link with <b>" .
					mmc_getCurrentState()->classroomCodeObject->associatedUser->user_email . "</b>";
		} else if (mmc_getCurrentState()->persona == STUDENT) {
			$visibleInstructions = $topicConfiguration->instructions;
			$visibleInstructionsHtml = $visibleInstructions;
			$visibleInstructionsHtml .= "\n\nWhen you've completed the assignment, share the link with <b>" .
					mmc_getCurrentState()->classroomCodeObject->associatedUser->user_email . "</b>";
		}
					$visibleInstructionsHtml = convertCRtoBR($visibleInstructionsHtml);
		return $visibleInstructionsHtml;
	}
	
	function createInstructions($topicConfiguration) {
		$visibleInstructionsHtml = "";
		if ($topicConfiguration->instructionsCustomized == false) {
			//echo "instructions customized is FALSE";
			$visibleInstructionsHtml = createGenericInstructions($topicConfiguration);
		} else  {
			//echo "instructions customized is TRUE";
			$visibleInstructions = $topicConfiguration->instructions;
			$visibleInstructionsHtml = $visibleInstructions; 
		}
		
		return $visibleInstructionsHtml;
	}
	
	function updateCustomInstructions($profileSubscriber, $topicConfiguration, $instructions) {
		$customInstructions = getCustomInstructions($profileSubscriber, $topicConfiguration);
		if ($customInstructions == null) {
			$sql = "Insert into topic_configuration_meta (profile_subscriber_id, topic_configuration_id, meta_key, meta_value) " . 
					"values (" . $profileSubscriber->id . ", " . $topicConfiguration->id . ", 'instructions', '" . $instructions . "')";
		} else {
			$sql = "Update topic_configuration_meta set meta_value = '" . $instructions . "' where profile_subscriber_id=" . 
					$profileSubscriber->id . " and topic_configuration_id=" . $topicConfiguration->id . 
					" and meta_key='instructions'";
		}
		//echo $sql;
		$result = mysql_query($sql);
		if ($result != 1) {
			echo "Error updating custom instructions --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
	}
	/* --------------------------------- */
	/** quotes **/

	function fetchquotes() {
		$quotes = array();
		$sql = "Select * from quotes order by id ";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
			$quote = new quote("");
			$quote->id = $row['id'];
			$quote->quotetext = $row['quotetext'];
			$quote->author = $row['author'];
			$quote->source = $row['source'];
			array_push($quotes, $quote);
		}
		return $quotes;
	}
		 	
	// quotes
	function selectRandomQuote() {
		$arrQuotes = fetchQuotes();
		$arrLength = count($arrQuotes);
		$arrIndex = rand(0, $arrLength - 1);
		return $arrQuotes[$arrIndex];
	}
	
	function selectQuoteOfTheDay() {
		$arrQuotes = fetchQuotes();
		$arrLength = count($arrQuotes);
		$now = time(); // or your date as well
		$your_date = strtotime("2012-10-01 00:00:00");
		$datediff = floor(($now - $your_date) / (60*60*24));
		$arrIndex = $datediff % $arrLength;
		return $arrQuotes[$arrIndex];
	}
	
	// topic configuration
	function getTopicNameFromTopicConfiguration($tcId) {
		$topicName = "";
		$sql = "SELECT t.short_name from topic t inner join topic_configuration tc on t.id=tc.topic_id where tc.id='" . $tcId . "'";
		$result = mysql_query($sql);
		if ($row = mysql_fetch_array($result))
			$topicName = $row['short_name'];
		return $topicName;
	}
	
	// topic configuration
	function getTopicIdFromTopicConfiguration($tcId) {
		$topicId = -1;
		$sql = "SELECT t.id from topic t inner join topic_configuration tc on t.id=tc.topic_id where tc.id='" . $tcId . "'";
		$result = mysql_query($sql);
		if ($row = mysql_fetch_array($result))
			$topicId = $row['id'];
		return $topicId;
	}
	
	// update topic configuration
	function updateTopicConfiguration($tcrecord, $lstTcMeta, $profileSubscriberId) {
		if (empty($profileSubscriberId)) {
			$profileSubscriberSql = " and profile_subscriber_id is null";
		} else {
			$profileSubscriberSql = " and profile_subscriber_id=" . $profileSubscriberId;
		}
		$topicId = getTopicIdFromTopicConfiguration($tcrecord);
		$sql = "delete from topic_configuration_meta where topic_configuration_id=" . $tcrecord .
		$sql .= $profileSubscriberSql;
		$sql .= " and (";
		for ($i=0;$i<count($lstTcMeta);$i++) {
			$sql = $sql . "meta_key='" . $lstTcMeta[$i]->field . "'";
			if ($i < count($lstTcMeta)-1)
				$sql = $sql . " OR ";
			else 
				$sql = $sql . ")";
		}
		//echo "Executing sql: " . $sql . "<br/>";
		$result = mysql_query($sql);
		if ($result != 1)
		{
			echo "Error delete topic configuration meta --- Result = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
		for ($i=0;$i<count($lstTcMeta);$i++)
		{
			$sql = "insert into topic_configuration_meta (topic_configuration_id, profile_subscriber_id, meta_key, meta_value) " .
				"values (" . $tcrecord . ", " . (empty($profileSubscriberId) ? "null" : $profileSubscriberId) . ", '" .
				$lstTcMeta[$i]->field . "', '";
			if ($lstTcMeta[$i]->fieldType == "text")
				$sql = $sql . $lstTcMeta[$i]->fieldValue . "')";
			else
				$sql = $sql . ($lstTcMeta[$i]->fieldValue == "on" ? "true" : "false") . "')";
			//echo "Executing sql: " . $sql . "<br/>";
			$result = mysql_query($sql);
			if ($result != 1)
			{
				echo "Error inserting topic_configuration_meta --- Result = " . $result . "  error: " . mysql_error();
					echo "SQL is " . $sql;
				exit();
			}
		}
		//$sql = "Update "
	}
	
	// update topic configuration
	function updateTopicsForProfile($profilename) 
	{
		// check to see which topics don't have a topic_configuration record for this profile
		// for the ones that don't, create one
		//echo "Checking topics for profile " . $profilename . "<br/>";
		$profileId = getProfileId($profilename);
		//echo "getProfileId returns " . $profileId;
		$topicsForProfile = array();
		$allTopics = array();
		$sql = "Select * from topic_configuration where profile_id='" . $profileId . "'";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
			$topic = new topicSpec();
			$topic->id = $row['topic_id'];
			$topic->configurationId = $row['id'];
			array_push($topicsForProfile, $topic);
		}
		$sql = "Select * from topic";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
			$topic = new topicSpec();
			$topic->id = $row['id'];
			array_push($allTopics, $topic);
		}
		foreach ($allTopics as $topic) 
		{
			if (!topicFoundInArray($topic->id, $topicsForProfile)) 
			{
				//echo "Inserting topic_configuration " . $topic->id . " for " . $profileId  . "<br/>";
				$sql = "Insert into topic_configuration (topic_id, profile_id) values ('" . $topic->id . "', '" . $profileId . "')";
				$result = mysql_query($sql);
				if ($result != 1)
				{
					echo "Error inserting --- Result = " . $result . "  error: " . mysql_error();
					echo "SQL is " . $sql;
					exit();
				}
				
			}
		}
	}
	
	
	function fetchTopicListForProfile($profile) {
		$sql = "SELECT t.short_name FROM category cat join categorytopic ct on cat.id=ct.category_id " .
			"join topic_configuration tc on ct.topic_configuration_id = tc.id " .
			"join topic t on tc.topic_id=t.id " .
			"where cat.profile_id=" . $profile->id;
		//echo "fetchTopicListForProfile sql: " . $sql;
		$lstTopics = array();
		$result = mysql_query($sql)  or die(mysql_error());
		while($row = mysql_fetch_array($result)) {
			$topicShortName = $row['short_name'];
			array_push($lstTopics, strtolower($topicShortName));
		}
		return $lstTopics;
	}
	
	function narrowDownHistoricalFiguresBasedOnProfile($topicSelections, $fullProfile) {
		$newTopicSelections = array();
		$lstTopics = fetchTopicListForProfile($fullProfile);

		$idx = 0;
		while ($idx < count($topicSelections)) {
			$topicSelection = $topicSelections[$idx];
			// remove element from list if topic isn't contained in list
			$topicsContained = topicsContainedIn($lstTopics, $topicSelection->breadcrumb);
			if (count($topicsContained) > 0) {
				array_push($newTopicSelections, $topicSelections[$idx]);
			}
			$idx++;
		}

		return $newTopicSelections;
	}
	
	function topicsContainedIn($lstTopics, $matchingTopicString) {
		$matchingTopicString = strtolower($matchingTopicString);
		$matchingTopics = explode(",", $matchingTopicString);

		$idx = count($matchingTopics) - 1;
		while ($idx >= 0) {
			$matchingTopics[$idx] = strtolower(trim($matchingTopics[$idx]));
			if (!in_array($matchingTopics[$idx], $lstTopics)) {
				//echo "in_array with " . $matchingTopics[$idx] . " returns FALSE<br/>";
				unset($matchingTopics[$idx]);
			} else  {
				//echo "in_array with " . $matchingTopics[$idx] . " returns TRUE<br/>";
			}
			$idx--;
		}
		return $matchingTopics;
	}
	
	// categories
	function findCategoryInList($categoryIdToMatch, $lstCategories) {
		foreach ($lstCategories as $category) {
			if ($category->id == $categoryIdToMatch)
				return $category;
		}
		return null;
	}

	// categories
	function fetchCategoriesAndTopics($profileId)
	{
		$lstCategories = array();
		
		$sql = "select * from category where profile_id='" . $profileId . "' order by sequence";
		//echo "fetchCategoriesAndTopics:<br/>" . $sql;
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
			$category = new Category();
			$category->id = $row['id'];;
			$category->categoryName = $row['name'];
			$category->categoryBreadcrumb = $row['breadcrumb'];
			$category->topics = array();
			array_push($lstCategories, $category);
		}
		
		$sql = "SELECT category.id as catid, category.name, category.breadcrumb, topic.id as topicid, topic.short_name,
			topic_configuration.id as tcid, topic.short_name, topic.featured, topic.title, topic.description, categorytopic.sequence
			FROM (categorytopic INNER JOIN category ON categorytopic.category_id = category.id)
			INNER JOIN (topic_configuration INNER JOIN topic ON topic_configuration.topic_id = topic.id)
			ON categorytopic.topic_configuration_id = topic_configuration.id
			WHERE topic_configuration.profile_id='" . $profileId . "' ORDER BY category.name, categorytopic.sequence";
		
		//$sql = "SELECT * from category1";
		
		//echo $sql;
		$result = mysql_query($sql);
		$count = mysql_num_rows($result);
		
		/*
		if ($result != 1)
		{
			echo "Error fetching categories = " . $result . "  error: " . mysql_error();
			echo "SQL is " . $sql;
			exit();
		}
		*/
		
		//echo "count: " . $count;
		
		$currentCategoryId = -1;
		while($row = mysql_fetch_array($result))
		{
			$categoryId = $row['catid'];
			$currentCategoryId = $categoryId;
			$category = findCategoryInList($categoryId, $lstCategories);
		
			$topic = new TopicSpec();
			$topic->id = $row['topicid'];
			$topic->configurationId = $row['tcid'];
			$topic->shortName = $row['short_name'];
			$topic->featured = $row['featured'];
			$topic->title = stripslashes($row['title']);
			$topic->description = stripslashes($row['description']);
			array_push($category->topics, $topic);
		}
		return $lstCategories;
	}
	
	// topic selections
	function fetchTopicSelections($topicShortName) {
		$sql="SELECT * FROM selection sl inner join topic tp on sl.topic=tp.id WHERE tp.short_name = '".$topicShortName ."' and sl.sequence <= 8 order by sl.sequence";
		//echo $sql . "<br/>";
		$result = mysql_query($sql);
		//echo "MYSQL Error code: " . mysql_errno() . ": " . mysql_error() . "<br/>";
		$selectionIndex = 0;
		$topicSelections = array();
		while($row = mysql_fetch_array($result)) {
			$aSelection = new Selection();
			// row[0] yields id of selection
			$aSelection->id = $row[0];
			$aSelection->type = $row['type'];
			$aSelection->article = convertUtf8(nl2br($row['article_text']));
			$aSelection->thumbnail = $row['thumbnail_image'];
			$aSelection->image = $row['graphic_image'];
			$aSelection->breadcrumb = convertUtf8(nl2br($row['breadcrumb']));
			$aSelection->videoEmbedCode = $row['video_embed_code'];
			$aSelection->videoURL = $row['video_url'];
			$aSelection->caption = $row['caption'];
			$aSelection->size = $row['size'];
			$topicSelections[$selectionIndex] = $aSelection;
			$selectionIndex++;
		}
		return $topicSelections;
	}
	
	// topic configuration
	function fetchDefaultConfigId($topic) {
		$profileId = getProfileId("MYMORALCOMPASS");
		return fetchConfigIdForProfileId($topic, $profileId);
	}
	
	// topic configuration
	function fetchConfigIdForProfileId($topic, $profileId) {
		$configId = "";
		$sql = "select tc.id from topic inner join topic_configuration tc on topic.id = tc.topic_id where topic.short_name='" . $topic .
		"' and tc.profile_id=" . $profileId;
		//echo "FetchDefaultConfigId sql: <br/>" . $sql;
		$result = mysql_query($sql);
		$error = mysql_error();
		//echo "\nerror is: " . $error;
		if ($row = mysql_fetch_array($result)) {
			$configId = $row['id'];
			//echo "config id: " . $configId . "<br/>";
		}
		//else echo "found no rows<br/>";
		return $configId;
	}
	
	// topic configuration
	function fetchProfileForConfigId($configId) {
		$profile = "";
		$sql = "select p.shortname from topic_configuration tc join profile p on tc.profile_id=p.id where tc.id=" . $configId;
		$result = mysql_query($sql);
		$error = mysql_error();
		if ($row = mysql_fetch_array($result)) {
			$profile = $row['shortname'];
		}
		//echo "fetchProfileForCOnfigId configId: " . $configId . "  profilename: " . $profile . "<br/>";
		return $profile;
	}

	// topic configuration
	function fetchTopicConfiguration($configId, $subscriberId) {
		//echo "config id: " . $configId;
		$topicConfiguration = new TopicConfiguration();
		$topicConfiguration->id = -1;
		$topicConfiguration->topicId = -1;
		$topicConfiguration->showInstructions = false;
		$topicConfiguration->showAuthorAnalysis = true;
		$topicConfiguration->showAuthorScorecard = true;
		$topicConfiguration->showUserScorecard = true;
		$topicConfiguration->showGenericScorecard = false;
		$topicConfiguration->showComments = true;
		$topicConfiguration->showDbqs = false;
		$topicConfiguration->showLoginControls = false;
		$topicConfiguration->showTopicDocuments = false;
		$topicConfiguration->showConfigDocuments = false;
		$topicConfiguration->instructions = "";
		$topicConfiguration->instructionsCustomized = false;
		$topicConfiguration->dbq1 = "";
		$topicConfiguration->dbq2 = "";
		$topicConfiguration->dbq3 = "";
		$topicConfiguration->doc1 = "";
		$topicConfiguration->doc2 = "";
		$topicConfiguration->doc3 = "";
		$topicShortName = "";
		
		if (!empty($configId)) {
			// add logic for subscriber id if null
			if (empty($subscriberId)) {
				$subscriberSql = "ps.id is null";
			} else {
				$subscriberSql = "(ps.id is null or ps.subscriber_id='" . $subscriberId . "')";
			}
			$sql = "select ps.id as profilesubscriberid, tc.id as topicconfigid, tc.*, tp.short_name, tcm.* from topic_configuration tc " .
				"inner join topic tp on tc.topic_id=tp.id " . 
				"left outer join topic_configuration_meta tcm on tcm.topic_configuration_id=tc.id " .
				"left outer join profile_subscriber ps on tcm.profile_subscriber_id=ps.id " .
				"where tc.id='" . $configId . "' and " . $subscriberSql;
	
			//echo $sql . "<br/>";
			$result = mysql_query($sql);
			$error = mysql_error();
			//echo "\nerror is: " . $error;
			$numResults = mysql_num_rows($result);
			//echo "\nNumber of results: " . $numResults;
			$myhashmap = array();
			if ($numResults > 0) {
				while($row = mysql_fetch_array($result)) {
					$topicConfiguration->topicId = $row['topic_id'];
					$metaKey = $row['meta_key'];
					$metaValue = $row['meta_value'];
					if ($metaKey == null) {
						continue;
					}
					$subscriberIdFromDb = $row['profilesubscriberid'];
					if (!empty($subscriberIdFromDb)) {
						$metaPriority = 1;
						if ($metaKey == "instructions") {
							$topicConfiguration->instructionsCustomized = true;
						}
					} else {
						$metaPriority = 0;
					}
					$useThisOne = false;
					if ($myhashmap[$metaKey] == null) {
						$myhashmap[$metaKey] = $metaPriority;
						$useThisOne = true;
					} else if ($metaPriority > $myhashmap[$metaKey]) {
						$myhashmap[$metaKey] = $metaPriority;
						$useThisOne = true;
					}
					//echo "metaKey: " . $metaKey . "<br/>";
					//echo "metaValue: " . $metaValue . "<br/>";
					//echo "useThisOne: " . $useThisOne . "<br/>";
					$topicConfiguration->id = $row["topicconfigid"];
					if ($topicConfiguration->topicId == -1) {
						$topicConfiguration->topicId = $row["topic_id"];
					}
					if (empty($topicShortName))
						$topicShortName = $row["short_name"];
					//echo "metaKey = " . $metaKey . " metaValue = " . $metaValue . "<br/>";
					if ($useThisOne) {
						if ($metaKey == "show_instructions")
							$topicConfiguration->showInstructions = strToBool($metaValue);
						else if ($metaKey == "instructions") {
							$topicConfiguration->instructions = $metaValue;
							if ($metaPriority == 1) {
								$topicConfiguration->instructionsCustomized = true;
							}
						}
						else if ($metaKey == "show_author_analysis")
							$topicConfiguration->showAuthorAnalysis = strToBool($metaValue);
						else if ($metaKey == "show_author_scorecard")
							$topicConfiguration->showAuthorScorecard = strToBool($metaValue);
						else if ($metaKey == "show_user_scorecard")
							$topicConfiguration->showUserScorecard = strToBool($metaValue);
						else if ($metaKey == "show_generic_scorecard")
							$topicConfiguration->showGenericScorecard = strToBool($metaValue);
						else if ($metaKey == "show_comments")
							$topicConfiguration->showComments = strToBool($metaValue);
						else if ($metaKey == "show_dbqs")
							$topicConfiguration->showDbqs = strToBool($metaValue);
						else if ($metaKey == "show_login_controls")
							$topicConfiguration->showLoginControls = strToBool($metaValue);
						else if ($metaKey == "show_topic_documents")
							$topicConfiguration->showTopicDocuments = strToBool($metaValue);
						else if ($metaKey == "show_config_documents")
							$topicConfiguration->showConfigDocuments = strToBool($metaValue);
						else if ($metaKey == "dbq1")
							$topicConfiguration->dbq1 = $metaValue;
						else if ($metaKey == "dbq2")
							$topicConfiguration->dbq2 = $metaValue;
						else if ($metaKey == "dbq3")
							$topicConfiguration->dbq3 = $metaValue;
						else if ($metaKey == "doc1")
							$topicConfiguration->doc1 = $metaValue;
						else if ($metaKey == "doc2")
							$topicConfiguration->doc2 = $metaValue;
						else if ($metaKey == "doc3")
							$topicConfiguration->doc3 = $metaValue;
					}
				}
			}
				
			$selectionArray = fetchTopicSelections($topicShortName);
			$selectionIndex = 0;
			$dbqCount = 0;
			while ($selectionIndex < count($selectionArray)) {
				$aSelection = $selectionArray[$selectionIndex];
				switch ($aSelection->type)
				{
					case Selection::INSTRUCTIONS:
						if (empty($topicConfiguration->instructions))
							$topicConfiguration->instructions = $aSelection->article;
						break;
					case Selection::WRITINGPROMPT:
						if ($dbqCount == 0) {
							if (empty($topicConfiguration->dbq1))
								$topicConfiguration->dbq1 = $aSelection->article;
						}
						else if ($dbqCount == 1) {
							if (empty($topicConfiguration->dbq2))
								$topicConfiguration->dbq2 = $aSelection->article;
						}
						else if ($dbqCount == 2) {
							if (empty($topicConfiguration->dbq3))
								$topicConfiguration->dbq3 = $aSelection->article;
						}
						$dbqCount++;
						break;
				}
				$selectionIndex++;
			}
		}
		/*
		echo "Topic configuration topicId: " . $topicConfiguration->topicId . "<br/>";
		echo "Show Instructions: " . boolToStr($topicConfiguration->showInstructions) . "<br/>";
		echo "Show Intro: " . boolToStr($topicConfiguration->showIntro) . "<br/>";
		echo "Show showAuthorAnalysis: " . boolToStr($topicConfiguration->showAuthorAnalysis) . "<br/>";
		echo "Show showAuthorScorecard: " . boolToStr($topicConfiguration->showAuthorScorecard) . "<br/>";
		echo "Show showUserScorecard: " . boolToStr($topicConfiguration->showUserScorecard) . "<br/>";
		echo "Show showComments: " . boolToStr($topicConfiguration->showComments) . "<br/>";
		echo "Show showDbqs: " . boolToStr($topicConfiguration->showDbqs) . "<br/";
		echo "Instructions: " . $topicConfiguration->instructions;
		*/
		return $topicConfiguration;
	}
	
	// utility
	function userCanEdit($currentUser, $profile) {
		$userId = $currentUser->ID;
		$profileId = $profile->id;
		$sql = "Select * from profile_user where user_id=" . $userId . " and profile_id=" . $profileId;
		//echo $sql;
		$result = mysql_query($sql);
		$error = mysql_error();
		//echo "\nerror is: " . $error;
		$numResults = mysql_num_rows($result);
		//echo "\nNumber of results: " . $numResults;
		return ($numResults > 0);
	}
	// utility
	function topicFoundInArray($id, $topicArray) {
		foreach ($topicArray as $topic) {
			if ($id == $topic->id)
				return true;
		}
		return false;
	}
	
	?>