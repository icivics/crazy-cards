<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/26/2019
 * Time: 9:43 AM
 */

class CategoryTopicDao {
    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }

    function fetchCategoriesAndTopics($profileId) {
        $lstCategories = array();

        $sql = "select * from category where profile_id='" . $profileId . "' order by sequence";
        //echo "fetchCategoriesAndTopics:<br/>" . $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Error fetching categories");
        while ($row = mysqli_fetch_assoc($result)) {
            $category = new Category();
            $category->id = $row['id'];;
            $category->categoryName = $row['name'];
            $category->categoryBreadcrumb = $row['breadcrumb'];
            $category->topics = array();
            array_push($lstCategories, $category);
        }

        $sql = "SELECT category.id as catid, category.name, category.breadcrumb, topic.id as topicid, topic.short_name,
                topic_configuration.id as tcid, topic.short_name, topic.featured, topic.title, topic.description, categorytopic.sequence
                FROM (categorytopic INNER JOIN category ON categorytopic.category_id = category.id)
                INNER JOIN (topic_configuration INNER JOIN topic ON topic_configuration.topic_id = topic.id)
                ON categorytopic.topic_configuration_id = topic_configuration.id
                WHERE topic_configuration.profile_id='" . $profileId . "' ORDER BY category.name, categorytopic.sequence";


        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Error fetching categories and topics");
        $count = mysqli_num_rows($result);
        $currentCategoryId = -1;
        while ($row = mysqli_fetch_assoc($result)) {
            $categoryId = $row['catid'];
            $currentCategoryId = $categoryId;
            $category = $this->findCategoryInList($categoryId, $lstCategories);

            $topic = new TopicSpec();
            $topic->id = $row['topicid'];
            $topic->configurationId = $row['tcid'];
            $topic->shortName = $row['short_name'];
            $topic->featured = $row['featured'];
            $topic->title = stripslashes($row['title']);
            $topic->description = stripslashes($row['description']);
            array_push($category->topics, $topic);
        }
        return $lstCategories;
    }

    // topic selections
    function fetchTopicSelections($topicShortName) {
        $sql = "SELECT * FROM selection sl inner join topic tp on sl.topic=tp.id WHERE tp.short_name = '" . $topicShortName . "' and sl.sequence <= 8 order by sl.sequence";
        //echo $sql . "<br/>";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Topic selections");
        //echo "MYSQL Error code: " . mysql_errno() . ": " . mysql_error() . "<br/>";
        $selectionIndex = 0;
        $topicSelections = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $aSelection = new Selection();
            // row[0] yields id of selection
            $aSelection->id = $row[0];
            $aSelection->type = $row['type'];
            $aSelection->article = convertUtf8(nl2br($row['article_text']));
            $aSelection->thumbnail = $row['thumbnail_image'];
            $aSelection->image = $row['graphic_image'];
            $aSelection->breadcrumb = convertUtf8(nl2br($row['breadcrumb']));
            $aSelection->videoEmbedCode = $row['video_embed_code'];
            $aSelection->videoURL = $row['video_url'];
            $aSelection->caption = $row['caption'];
            $aSelection->size = $row['size'];
            $topicSelections[$selectionIndex] = $aSelection;
            $selectionIndex++;
        }
        return $topicSelections;
    }

    // topic configuration
    function fetchDefaultConfigId($topic) {
        $profileId = getProfileId("MYMORALCOMPASS");
        return fetchConfigIdForProfileId($topic, $profileId);
    }

    // topic configuration
    function fetchConfigIdForProfileId($topic, $profileId) {
        $configId = "";
        $sql = "select tc.id from topic inner join topic_configuration tc on topic.id = tc.topic_id where topic.short_name='" . $topic .
            "' and tc.profile_id=" . $profileId;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Config for profile");
        if ($row = mysqli_fetch_assoc($result)) {
            $configId = $row['id'];
            //echo "config id: " . $configId . "<br/>";
        }
        //else echo "found no rows<br/>";
        return $configId;
    }

    // topic configuration
    function fetchProfileForConfigId($configId) {
        $profile = "";
        $sql = "select p.shortname from topic_configuration tc join profile p on tc.profile_id=p.id where tc.id=" . $configId;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "Config for profile");
        if ($row = mysqli_fetch_assoc($result)) {
            $profile = $row['shortname'];
        }
        return $profile;
    }

    // topic configuration
    function fetchTopicConfiguration($configId, $subscriberId) {
        //echo "config id: " . $configId;
        $topicConfiguration = new TopicConfiguration();
        $topicConfiguration->id = -1;
        $topicConfiguration->topicId = -1;
        $topicConfiguration->showInstructions = false;
        $topicConfiguration->showAuthorAnalysis = true;
        $topicConfiguration->showAuthorScorecard = true;
        $topicConfiguration->showUserScorecard = true;
        $topicConfiguration->showGenericScorecard = false;
        $topicConfiguration->showComments = true;
        $topicConfiguration->showDbqs = false;
        $topicConfiguration->showLoginControls = false;
        $topicConfiguration->showTopicDocuments = false;
        $topicConfiguration->showConfigDocuments = false;
        $topicConfiguration->instructions = "";
        $topicConfiguration->instructionsCustomized = false;
        $topicConfiguration->dbq1 = "";
        $topicConfiguration->dbq2 = "";
        $topicConfiguration->dbq3 = "";
        $topicConfiguration->doc1 = "";
        $topicConfiguration->doc2 = "";
        $topicConfiguration->doc3 = "";
        $topicShortName = "";

        if (!empty($configId)) {
            // add logic for subscriber id if null
            if (empty($subscriberId)) {
                $subscriberSql = "ps.id is null";
            } else {
                $subscriberSql = "(ps.id is null or ps.subscriber_id='" . $subscriberId . "')";
            }
            $sql = "select ps.id as profilesubscriberid, tc.id as topicconfigid, tc.*, tp.short_name, tcm.* from topic_configuration tc " .
                "inner join topic tp on tc.topic_id=tp.id " .
                "left outer join topic_configuration_meta tcm on tcm.topic_configuration_id=tc.id " .
                "left outer join profile_subscriber ps on tcm.profile_subscriber_id=ps.id " .
                "where tc.id='" . $configId . "' and " . $subscriberSql;

            //echo $sql . "<br/>";
            $result = mySqli_query_wrapper($this->DbConnection, $sql, "fetchTopicConfiguration");
            $numResults = mysqli_num_rows($result);
            //echo "\nNumber of results: " . $numResults;
            $myhashmap = array();
            if ($numResults > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $topicConfiguration->topicId = $row['topic_id'];
                    $metaKey = $row['meta_key'];
                    $metaValue = $row['meta_value'];
                    if ($metaKey == null) {
                        continue;
                    }
                    $subscriberIdFromDb = $row['profilesubscriberid'];
                    if (!empty($subscriberIdFromDb)) {
                        $metaPriority = 1;
                        if ($metaKey == "instructions") {
                            $topicConfiguration->instructionsCustomized = true;
                        }
                    } else {
                        $metaPriority = 0;
                    }
                    $useThisOne = false;
                    if ($myhashmap[$metaKey] == null) {
                        $myhashmap[$metaKey] = $metaPriority;
                        $useThisOne = true;
                    } else if ($metaPriority > $myhashmap[$metaKey]) {
                        $myhashmap[$metaKey] = $metaPriority;
                        $useThisOne = true;
                    }
                    //echo "metaKey: " . $metaKey . "<br/>";
                    //echo "metaValue: " . $metaValue . "<br/>";
                    //echo "useThisOne: " . $useThisOne . "<br/>";
                    $topicConfiguration->id = $row["topicconfigid"];
                    if ($topicConfiguration->topicId == -1) {
                        $topicConfiguration->topicId = $row["topic_id"];
                    }
                    if (empty($topicShortName))
                        $topicShortName = $row["short_name"];
                    //echo "metaKey = " . $metaKey . " metaValue = " . $metaValue . "<br/>";
                    if ($useThisOne) {
                        if ($metaKey == "show_instructions")
                            $topicConfiguration->showInstructions = strToBool($metaValue);
                        else if ($metaKey == "instructions") {
                            $topicConfiguration->instructions = $metaValue;
                            if ($metaPriority == 1) {
                                $topicConfiguration->instructionsCustomized = true;
                            }
                        } else if ($metaKey == "show_author_analysis")
                            $topicConfiguration->showAuthorAnalysis = strToBool($metaValue);
                        else if ($metaKey == "show_author_scorecard")
                            $topicConfiguration->showAuthorScorecard = strToBool($metaValue);
                        else if ($metaKey == "show_user_scorecard")
                            $topicConfiguration->showUserScorecard = strToBool($metaValue);
                        else if ($metaKey == "show_generic_scorecard")
                            $topicConfiguration->showGenericScorecard = strToBool($metaValue);
                        else if ($metaKey == "show_comments")
                            $topicConfiguration->showComments = strToBool($metaValue);
                        else if ($metaKey == "show_dbqs")
                            $topicConfiguration->showDbqs = strToBool($metaValue);
                        else if ($metaKey == "show_login_controls")
                            $topicConfiguration->showLoginControls = strToBool($metaValue);
                        else if ($metaKey == "show_topic_documents")
                            $topicConfiguration->showTopicDocuments = strToBool($metaValue);
                        else if ($metaKey == "show_config_documents")
                            $topicConfiguration->showConfigDocuments = strToBool($metaValue);
                        else if ($metaKey == "dbq1")
                            $topicConfiguration->dbq1 = $metaValue;
                        else if ($metaKey == "dbq2")
                            $topicConfiguration->dbq2 = $metaValue;
                        else if ($metaKey == "dbq3")
                            $topicConfiguration->dbq3 = $metaValue;
                        else if ($metaKey == "doc1")
                            $topicConfiguration->doc1 = $metaValue;
                        else if ($metaKey == "doc2")
                            $topicConfiguration->doc2 = $metaValue;
                        else if ($metaKey == "doc3")
                            $topicConfiguration->doc3 = $metaValue;
                    }
                }
            }

            $selectionArray = fetchTopicSelections($topicShortName);
            $selectionIndex = 0;
            $dbqCount = 0;
            while ($selectionIndex < count($selectionArray)) {
                $aSelection = $selectionArray[$selectionIndex];
                switch ($aSelection->type) {
                    case Selection::INSTRUCTIONS:
                        if (empty($topicConfiguration->instructions))
                            $topicConfiguration->instructions = $aSelection->article;
                        break;
                    case Selection::WRITINGPROMPT:
                        if ($dbqCount == 0) {
                            if (empty($topicConfiguration->dbq1))
                                $topicConfiguration->dbq1 = $aSelection->article;
                        } else if ($dbqCount == 1) {
                            if (empty($topicConfiguration->dbq2))
                                $topicConfiguration->dbq2 = $aSelection->article;
                        } else if ($dbqCount == 2) {
                            if (empty($topicConfiguration->dbq3))
                                $topicConfiguration->dbq3 = $aSelection->article;
                        }
                        $dbqCount++;
                        break;
                }
                $selectionIndex++;
            }
        }
        /*
        echo "Topic configuration topicId: " . $topicConfiguration->topicId . "<br/>";
        echo "Show Instructions: " . boolToStr($topicConfiguration->showInstructions) . "<br/>";
        echo "Show Intro: " . boolToStr($topicConfiguration->showIntro) . "<br/>";
        echo "Show showAuthorAnalysis: " . boolToStr($topicConfiguration->showAuthorAnalysis) . "<br/>";
        echo "Show showAuthorScorecard: " . boolToStr($topicConfiguration->showAuthorScorecard) . "<br/>";
        echo "Show showUserScorecard: " . boolToStr($topicConfiguration->showUserScorecard) . "<br/>";
        echo "Show showComments: " . boolToStr($topicConfiguration->showComments) . "<br/>";
        echo "Show showDbqs: " . boolToStr($topicConfiguration->showDbqs) . "<br/";
        echo "Instructions: " . $topicConfiguration->instructions;
        */
        return $topicConfiguration;
    }

    function fetchTopic($topicString)
    {
        $sql = "Select * from topic where topic.short_name = '".$topicString ."'";
        //echo $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "fetchTopic");
        $row = mysqli_fetch_assoc($result);
        $topic = new TopicSpec();
        $topic->id = $row[2];
        $topic->shortName = $row['short_name'];
        $topic->title = $row['title'];
        $topic->description = $row['description'];
        return $topic;
    }

    function fetchSelections($topicString)
    {
        $sql="SELECT * FROM selection sl inner join topic tp on sl.topic=tp.id WHERE tp.short_name = '".$topicString."' order by sl.sequence";
        //echo $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "fetchSelections");
        $topicSelections =  array();
        while($row = mysqli_fetch_assoc($result))
        {
            $aSelection = new Selection();
            // row[0] yields id of selection
            $aSelection->id = $row[0];
            $aSelection->type = $row['type'];
            $aSelection->article = convertUtf8(nl2br($row['article_text']));
            $aSelection->thumbnail = $row['thumbnail_image'];
            $aSelection->image = $row['graphic_image'];
            $aSelection->breadcrumb = convertUtf8(nl2br($row['breadcrumb']));
            $aSelection->videoEmbedCode = $row['video_embed_code'];
            $aSelection->videoURL = $row['video_url'];
            $aSelection->size = $row['size'];
            array_push($topicSelections, $aSelection);
        }
        return $topicSelections;
    }
    function findCategoryInList($categoryIdToMatch, $lstCategories) {
        foreach ($lstCategories as $category) {
            if ($category->id == $categoryIdToMatch)
                return $category;
        }
        return null;
    }


}