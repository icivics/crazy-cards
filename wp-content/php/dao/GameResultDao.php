<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 6/28/2019
 * Time: 10:27 PM
 */
class GameResultDao {
    private $DbConnection;

    function __construct($con) {
        $this->DbConnection = $con;
    }


    function upsert($gameBoardResult) {
        $table = centralTable("game_board_result");
        if ($gameBoardResult->id == -1) {
            if ($gameBoardResult->gameSessionId == -1) {
                $gameBoardResult->gameSessionId = $this->createGameSession($gameBoardResult);
            }
            // insert
            $doInsert = true;
            $sql = "insert into " . $table . " (game_session_id, category, time_started, points, cheat, complete) " .
                "values ('" . $gameBoardResult->gameSessionId . "', '" . $gameBoardResult->category . "', '" .
                date("Y-m-d H:i:s") . "', '" .
                $gameBoardResult->points . "', '" .
                $gameBoardResult->cheat . "', '" .
                "0')";
        } else {
            // update
            $doInsert = false;
            $sql = "update " . $table . " set " .
                "time_last_guess='" . date("Y-m-d H:i:s") . "', " .
                "points='" . $gameBoardResult->points . "', " .
                "complete='" . $gameBoardResult->complete . "' " .
                "where id = " . $gameBoardResult->id;
        }
        //echo $sql;
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "GameResultsDao upsert: " . $sql );
        if ($doInsert) {
            $gameBoardResult->id = mysqli_insert_id($this->DbConnection);
        }
        return $gameBoardResult;
    }

    function createGameSession($gameBoardResult) {
        $sql = "insert into " . centralTable("game_session") . " (username, time_started) " .
            "values ('" . $gameBoardResult->username . "', '" . date("Y-m-d H:i:s") . "')";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "GameResultDao createGameSession: " . $sql );
        $gameSessionId = mysqli_insert_id($this->DbConnection);
        return $gameSessionId;
    }

    function fetchGameSession($gameSessionId) {
        $gameSession = null;
        $first = true;
        $sql = "select gs.id as game_session_id, gb.id as game_board_id, gb.category, " .
            "gb.time_started, gb.time_last_guess, gb.points, gb.complete, gb.cheat " .
            "from " . centralTable("game_session") . " gs join " .
            centralTable("game_board_result") . " gb on" .
            " gs.id=gb.game_session_id " .
            "where gs.id='" . $gameSessionId . "'";
        $result = mySqli_query_wrapper($this->DbConnection, $sql, "GameResultsDao fetchGameSession: " . $sql );
        while ($row = mysqli_fetch_assoc($result)) {
            if ($first) {
                $gameSession = new GameSession();
                $gameSession->id = $row['game_session_id'];
                $gameSession->arrGameBoardResults = array();
                $first = false;
            }
            $gameBoardResult = new GameBoardResult();
            $gameBoardResult->id = $row['game_board_id'];
            $gameBoardResult->gameSessionId = $row['game_session_id'];
            $gameBoardResult->category = $row['category'];
            $gameBoardResult->timeStarted = $row['time_started'];
            $gameBoardResult->timeLastGuess = $row['time_last_guess'];
            $gameBoardResult->points = $row['points'];
            $gameBoardResult->complete = $row['complete'];
            $gameBoardResult->cheat = $row['cheat'];
            array_push($gameSession->arrGameBoardResults, $gameBoardResult);
        }
        return $gameSession;
    }
}
