<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 12/23/2017
 * Time: 6:44 PM
 */
class DataTableOutputter {
    function outputTableFields($mediaCategory) {
        $jsString = "fields: [";
        $count = 0;
        foreach ($mediaCategory->arrFields as $field) {
            $jsString .= "{label: '" . $field . "', name:'" . $field . "'}";
            if ($count < sizeof($mediaCategory->arrFields) - 1) {
                $jsString .= ",";
            }
            $count++;
        }
        $jsString .= "]";
        return $jsString;
    }

    function outputDataColumns($mediaCategory) {
        $jsString = "columns: [";
        $firstField = "id";
        $jsString .= "{data: '" . $firstField . "'}";
        $jsString .= ",";
        $count = 0;
        foreach ($mediaCategory->arrFields as $field) {
            $jsString .= "{data: '" . $field . "'}";
            if ($count < sizeof($mediaCategory->arrFields) - 1) {
                $jsString .= ",";
            }
            $count++;
        }
        $jsString .= "],";
        return $jsString;
    }

    function outputTheadHtml($mediaCategory) {
        $html = "<th>ID</th>";
        foreach ($mediaCategory->arrFields as $field) {
            $html .= "<th>" . $field . "</th>";
        }
        return $html;
    }
}