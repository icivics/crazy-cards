<?php 
	
	function findTodaysFeaturedTopic($featuredTopics, $initialTopicId)
	{
		if ($initialTopicId == -1) {
			$nTopics = count($featuredTopics);
			$now = time(); // or your date as well
			$your_date = strtotime("2012-10-01 00:00:00");
			$datediff = floor(($now - $your_date) / (60*60*24));
			$topicIndex = $datediff % $nTopics;
			//echo "Date diff: " . $datediff . "<br/>";
			//echo "nTopics: " . $nTopics . "<br/>";
			//echo "Topic index: " . $topicIndex . "<br/>";
			//echo "featuredtopic shortname: " . $featuredTopics[$topicIndex]->shortName;
			return $featuredTopics[$topicIndex]; 
		} else if ($initialTopicId != 0) {
			return findSpecificTopic($featuredTopics, $initialTopicId);
		} else {
			return null;
		}
	}
	
	function findSpecificTopic($featuredTopics, $initialTopicId) {
		foreach ($featuredTopics as $topic) {
			if ($topic->id == $initialTopicId) {
				return $topic;
			}
		}
		return null;
	}
	
	function featuredTopicHtml($featuredTopic, $featuredImage) {
		$featuredTopicHtml = "<h6 class=featured-title>Featured Article: <strong>" . $featuredTopic->title . "</strong></h6>";
		$featuredTopicHtml .= "<a href=\"" . get_bloginfo('url') . "/" . $featuredTopic->shortName . 
			"?configid=" . $featuredTopic->configurationId . "\">";
		$featuredTopicHtml .= "<img class='aligncenter wp-post-image' src='" . ABSURL . "wp-content/images/" .
				$featuredTopic->shortName . "/" . $featuredImage . "' alt='' /></a>";
		$featuredTopicHtml .= "<br/>";
		return $featuredTopicHtml;
	}

?>