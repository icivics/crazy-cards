<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 9/1/2018
 * Time: 9:56 PM
 */

class TileOutputter {

    private $tiles;
    private $htmlForRandomSongTeasers;
    private $songOfTheWeekEntry;
    private $songOfTheWeekPlaylist;
    private $playlistEntryOutputter;

    function __construct($tiles) {
        $this->tiles = $tiles;
    }

    function setTop100Data($htmlForRandomSongTeasers, $songOfTheWeekEntry,
                           $songOfTheWeekPlaylist, $playlistEntryOutputter) {
        $this->htmlForRandomSongTeasers = $htmlForRandomSongTeasers;
        $this->songOfTheWeekEntry = $songOfTheWeekEntry;
        $this->songOfTheWeekPlaylist = $songOfTheWeekPlaylist;
        $this->playlistEntryOutputter = $playlistEntryOutputter;

    }

    function createHtmlForTiles() {
        $html = "";
        foreach ($this->tiles as $tile) {
            $html .= "<div class='tile-wrapper col-md-6 col-sm-6 '>";
            $html .= "<div class='thumbnail tile-thumbnail'>";
            $html .= $this->createHtmlForOneTile($tile);
            $html .= "</div>";
            $html .= "</div>";
        }
        return $html;
    }

    function createHtmlForOneTile($tile) {
        $httpTest = (substr( $tile->destination, 0, 4 ) === "http");
        $fullDestination = ($httpTest == 1 ? $tile->destination : PERMALINKBASE . $tile->destination);
        $targetStr = ($httpTest == 1 ? " target='_NEW'" : "");
        if (strcasecmp($tile->destination, '@SONGOFTHEWEEK') == 0) {
            $href = $this->playlistEntryOutputter->outputMusicBlogTextLink($tile->title);
        } else {
            $href = "<a href='" . $fullDestination . "'" . $targetStr . ">" . $tile->title . "</a>";
        }

        $html = "";
        $html .= "<div class='tile-title'>" . $href . "</div>";
        $html .= "<br/>";
        if (strcasecmp($tile->image, '@SONGOFTHEWEEKIMAGE') == 0) {
            $html .= $this->playlistEntryOutputter->outputThumbnailHtml($this->songOfTheWeekEntry, $this->songOfTheWeekPlaylist);
        } else if (strcasecmp($tile->image, '@RANDOMTOP100') == 0) {
            $html .= $this->htmlForRandomSongTeasers;
        } else {
            $html .= "<div class='tile-image'><a href='" . $fullDestination . "'" . $targetStr . "><img src='" . ABSURL . "wp-content/themes/compass/img/" . $tile->image . "' class='img-responsive' style='height:55%;width:40%'/></a></div>";
        }
        $html .= "<br/>";
        $html .= "<div class='tile-description'>";
        if (!empty($tile->description)) {
            if (strcasecmp($tile->destination, '@SONGOFTHEWEEK') == 0) {
                $html .= $this->playlistEntryOutputter->outputMusicBlogTaglineLink($this->songOfTheWeekEntry);
            } else {
                $html .= $tile->description;
            }
        }
        $html .= "</div>";
        return $html;
    }
}
?>