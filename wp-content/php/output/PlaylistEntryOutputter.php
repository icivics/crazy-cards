<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/22/2017
 * Time: 10:16 PM
 */
class PlaylistEntryOutputter {
    function outputThumbnailHtml($playlistEntry, $playlist) {
        $youtubeImageTemplate =
            "<img src='https://img.youtube.com/vi/%s' border='0' />";

        $html = "<a href='" . PERMALINKBASE . "/musicblog'>";
        $videoReplace = $playlistEntry->videoId . "/" . $playlistEntry->thumbnailImage;
        $youtubeImageTemplate = str_replace("%s", $videoReplace, $youtubeImageTemplate);
        $html .= $youtubeImageTemplate . "</a>";
        return $html;
    }

    function outputPlaylistEntryInfo($playlistEntry, $playlist) {
        $tagline = $playlistEntry->tagline;
        if (empty($tagline)) {
            $tagline = "@TITLE - @ARTIST";
        }
        $html = "<a href='" . PERMALINKBASE . "/one-song?id=" . $playlistEntry->mediaItemId . "&guid=" . $playlist->guid . "'>";
        $html .= $tagline . "</a>";
        $html = str_replace("@ARTIST", $playlistEntry->artist, $html);
        $html = str_replace("@TITLE", $playlistEntry->title, $html);
        return $html;
    }

    function outputBlogEntryTagline($playlistEntry, $playlist) {
        $tagline = $playlistEntry->tagline;
        if (empty($tagline)) {
            $tagline = "@TITLE - @ARTIST";
        }
        $html = $tagline;
        $html = str_replace("<br/><br/>", ":  ", $html);
        $html = str_replace("@ARTIST", $playlistEntry->artist, $html);
        $html = str_replace("@TITLE", $playlistEntry->title, $html);
        return $html;
    }

    function outputBlogEntryBlurb($blurb) {
        $html = convertCRtoBR($blurb);
        return $html;
    }

    function outputBlogEntryImage($playlistEntry, $playlist) {
        $filePath = ABSURL . "wp-content/blogimages/" . $playlistEntry->blogImage;
        $html = "<a href=''><img src='" . $filePath . "' border='0'/></a>";
        return $html;
    }


    function outputPlaylistEntryTagline($playlistEntry, $playlist) {
        $tagline = $playlistEntry->tagline;
        $charIndex = strpos($tagline, "<br");
        if ($charIndex != false) {
            $tagline = substr($tagline, 0, $charIndex);
        }
        return $tagline;
    }

    function outputTextLink($playlistEntry, $playlist, $text) {
        $html = "<a href='" . PERMALINKBASE . "/one-song?id=" . $playlistEntry->mediaItemId . "&guid=" . $playlist->guid . "'>";
        $html .= $text;
        $html .= "</a>";
        return $html;
    }

    function outputPlaylistTextLink($playlist, $text) {
        $html = "<a href='" . PERMALINKBASE . "/playlist?guid=" . $playlist->guid . "'>";
        $html .= $text;
        $html .= "</a>";
        return $html;
    }

    function outputMusicBlogTextLink($text) {
        $html = "<a href='" . PERMALINKBASE . "/musicblog'>";
        $html .= $text;
        $html .= "</a>";
        return $html;
    }

    function outputMusicBlogTaglineLink($playlistEntry) {
        $tagline = $playlistEntry->tagline;
        if (empty($tagline)) {
            $tagline = "@TITLE - @ARTIST";
        }
        $html = "<a href='" . PERMALINKBASE . "/musicblog'>";
        $html .= $tagline . "</a>";
        $html = str_replace("@ARTIST", $playlistEntry->artist, $html);
        $html = str_replace("@TITLE", $playlistEntry->title, $html);
        return $html;
    }

    function outputPlaylistEntry($playlistEntry, $playlist) {
        //$html = "<a name='MEDIA_ID_" . $playlistEntry->mediaItemId . "'/>";
        $html = $this->outputThumbnailHtml($playlistEntry, $playlist);
        $html .= "<br/>";
        $html .= $this->outputPlaylistEntryInfo($playlistEntry, $playlist);
        return $html;
    }
}

?>