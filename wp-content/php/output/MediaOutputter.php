<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/21/2017
 * Time: 2:50 PM
 */

function htmlForMetadata($mediaEntryBean) {
    $str = "";
    if (count($mediaEntryBean->arrMediaCategories) == 0) {
        return $str;
    }
    foreach ($mediaEntryBean->arrMediaCategories[0]->arrFields as $field) {
        if ($field != "title") {
            $str .= ucfirst($field) . ": " . $mediaEntryBean->mapMediaEntryValue($field) . "<br/>";
        }
    }
    return $str;
}

function htmlForFeaturedThumbnail($mediaEntryBean) {
    if ($mediaEntryBean instanceof PlaylistEntry) {
        $thumbnail = $mediaEntryBean->thumbnailImage;
        $videoId = $mediaEntryBean->videoId;
    } else {
        $thumbnail = "1.jpg";
        if (count($mediaEntryBean->arrVideos) == 0) {
            return "";
        }
        $videoId = $mediaEntryBean->arrVideos[0]->videoId;
    }
    $youtubeImageTemplate =
        "<img src='https://img.youtube.com/vi/%s' border='0' />";

    $html = $youtubeImageTemplate;
    $html = str_replace("%s", $videoId . "/" . $thumbnail, $html);
    return $html;
}

function htmlForVideo($video, $autoplay) {
    return htmlForVideoId($video->videoId, $autoplay);
}

function htmlForVideoId($videoId, $autoplay) {
    if ($autoplay) {
        $youtubeTemplate =
            "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/%s?rel=0&autoplay=1\" frameborder=\"0\" allow='autoplay'" .
                "allowfullscreen ></iframe>";
    } else {
        $youtubeTemplate =
            "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/%s?rel=0\" frameborder=\"0\" " .
                "allowfullscreen ></iframe>";
    }
    $html = $youtubeTemplate;
    $html = str_replace("%s", $videoId, $html);
    return $html;
}

function htmlForVideoThumbnail($video, $videoIndex) {
    $imgSource = "<img src='https://img.youtube.com/vi/%s' border='0' />";
    $imgSource = str_replace("%s", $video->videoId . "/1.jpg", $imgSource);
    $html =
        "<div class='extra-resource'>" .
        "<a href='javascript:setResource(" . strval($videoIndex) . ", \"visible\")'>" . $imgSource . "</a>" .
        "<figcaption>" .
        $video->breadcrumb .
        "</figcaption></div>";
    return $html;
}
?>