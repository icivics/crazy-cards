<?php
global $unmagic;
function convertUtf8($text) {
	$quotes = array (
			"\xC2\xAB" => '"', // � (U+00AB) in UTF-8
			"\xC2\xBB" => '"', // � (U+00BB) in UTF-8
			"\xE2\x80\x98" => "'", // � (U+2018) in UTF-8
			"\xE2\x80\x99" => "'", // � (U+2019) in UTF-8
			"\xE2\x80\x9A" => "'", // � (U+201A) in UTF-8
			"\xE2\x80\x9B" => "'", // ? (U+201B) in UTF-8
			"\xE2\x80\x9C" => '"', // � (U+201C) in UTF-8
			"\xE2\x80\x9D" => '"', // � (U+201D) in UTF-8
			"\xE2\x80\x9E" => '"', // � (U+201E) in UTF-8
			"\xE2\x80\x9F" => '"', // ? (U+201F) in UTF-8
			"\xE2\x80\xB9" => "'", // � (U+2039) in UTF-8
			"\xE2\x80\xBA" => "'", // � (U+203A) in UTF-8
			"\xC3\xA2\xE2\x82\xAC\x22" => "-",
			"\xC3\xA2\xE2\x82\xAC\xE2\x84\xA2" => "'",
			"\xC3\xA2\xE2\x82\xAC\xC5\x93" => "\"",
			"\xC3\xA2\xE2\x82\xAC\xC2\x9D" => "\"",
			"\xC3\xA2\xE2\x82\xAC\xC2\xA6" => "-" 
	);
	// "\xC3\xA2\xE2\x82\xAC\xC2\xA8" => "\"",
	// "\xC3\xA2\xE2\x82\xAC\xC2\xA6" => "\"",
	
	$text = strtr ( $text, $quotes );
	// echo $str;
	// $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	// $text = str_replace(chr(130), ',', $text); // baseline single quote
	// $text = str_replace(chr(132), '"', $text); // baseline double quote
	// $text = str_replace(chr(133), '...', $text); // ellipsis
	// $text = str_replace(chr(145), "'", $text); // left single quote
	// $text = str_replace(chr(146), "'", $text); // right single quote
	// $text = str_replace(chr(147), '"', $text); // left double quote
	// $text = str_replace(chr(148), '"', $text); // right double quote
	
	// $text = mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8');
	
	// First, replace UTF-8 characters.
	// $text = str_replace(
	// array("’", "\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
	// array("'", "'", "'", '"', '"', '-', '--', '...'),
	// $text);
	return $text;
}
function strToHex($string) {
	$hex = '';
	for($i = 0; $i < strlen ( $string ); $i ++) {
		$hex .= " " . dechex ( ord ( $string [$i] ) );
	}
	return $hex;
}
function my_escape($str, $unmagic) {
	// return $str;
	// echo "Before mysql_real_escape_string str is " . $str . "<br/>";
	// echo "Unmagic is: " . $unmagic . "<br/>";
	// $str = convertUtf8($str);
	if ($unmagic == 0)
		$str = mysql_real_escape_string ( $str );
		// echo "After mysql_real_escape_string str is " . $str . "<br/>";
	return $str;
}

function sql_quotes($str) {
    $str = str_replace("'", "''", $str);
    return $str;
}

function fixQuotes($str) {
	$str = str_replace("'", "&apos;", $str);
	return $str;
}
function getUnmagicValue() {
	$unmagic = 0;
	if (UNMAGICQUOTES == '1') {
		$unmagic = 1;
	}
	// echo "getUnmagicValue Value of unmagic is: " . $unmagic;
	return $unmagic;
}
function convertCRtoBR($str) {
	$strHex = strToHex ( $str );
	//echo "Convert CR to BR string coming in: " . $strHex . "<br/>";
	
	$str = str_replace ( "\n", "<br/>", $str );
	// $str = str_replace("\x09", "", $str);
	// $str = preg_replace('/[\x00-\x09\x0B\x0C\x0E-\x1F\x80-\x9F]/u', '', $str);
	// $str = str_replace("\t", "", $str);
	// $strHex = strToHex($str);
	// echo "Convert CR to BR string leaving: " . $strHex . "<br/>";
	// exit();
	return $str;
}
function convertBRtoCR($str) {
	$strHex = strToHex ( $str );
	// echo "Convert BR to CR string coming in: " . $strHex . "<br/>";
	$str = str_replace ( "<br/>", "\n", $str );
	// $strHex = strToHex($str);
	// echo "Convert BR to CR string leaving: " . $strHex . "<br/>";
	return $str;
}
function fixupVideoEmbed($videoEmbedCode) {
	if (strpos ( $videoEmbedCode, "?enablejsapi=1" ) == false) {
		$srcposition = strpos ( $videoEmbedCode, "src=" );
		$firstquote = strpos ( $videoEmbedCode, "\"", $srcposition + 1 );
		$secondquote = strpos ( $videoEmbedCode, "\"", $firstquote + 1 );
		$qmark = strpos ( $videoEmbedCode, "?", $firstquote );
		if ($qmark != false && $qmark < $secondquote)
			$videoEmbedCode = substr_replace ( $videoEmbedCode, "&enablejsapi=1", $secondquote, 0 );
		else
			$videoEmbedCode = substr_replace ( $videoEmbedCode, "?enablejsapi=1", $secondquote, 0 );
	}
	return $videoEmbedCode;
}
function profileAndSubscriberString($profile, $subscriberId) {
	$s = "profile=" . $profile;
	if (! empty ( $subscriberId )) {
		$s .= "&subscriberid=" . $subscriberId;
	}
	return $s;
}
function homeURL($profile, $subscriberId) {
	if (strcasecmp($profile, "TOP100") == 0) {
		$url = PERMALINKBASE . "top100";
	} else {
		$url = PERMALINKBASE;
		$url .= "?profile=" . $profile;
		if (! empty ( $subscriberId )) {
			$url .= "&subscriberid=" . $subscriberId;
		}
	}
	return $url;
}
function subscriberString($subscriberId) {
	$s = "";
	if (! empty ( $subscriberId )) {
		$s .= "&subscriberid=" . $subscriberId;
	}
	return $s;
}
function sameURL($profile, $classroomCode, $configId) {
	$url = strtok ( $_SERVER ["REQUEST_URI"], '?' );
	$url .= "?profile=" . $profile;
	if (! empty ( $classroomCode )) {
		$url .= "&classroomCode=" . $classroomCode;
	} else {
		$url .= "&classroomCode=NONE";
	}
	if (! empty ( $configId )) {
		$url .= "&configid=" . $configId;
	}
	return $url;
}
function strToBool($str) {
	return ($str == 'true' ? true : false);
}
function boolToStr($bool) {
	return $bool == true ? 'true' : 'false';
}

function findValueInArray($arrNmVal, $target) {
    foreach ($arrNmVal as $nmval) {
        if ($nmval->name == $target) {
            return $nmval->value;
        }
    }
    return null;
}
/*
function getDbConnection() {
	$con = mysql_connect(DBHOST, DBUSER, DBPASSWORD);
	if (!$con)
	{
		die('Could not connect: ' . mysql_error());
	}
	mysql_select_db(DBSCHEMA, $con);
	return $con;
}
function my_mysql_query($sql, $message) {
	$result = mysql_query ( $sql );
	if ($result == false) {
		echo "<br/>" . $message . "<br/> --- Result = " . $result . "<br/>error: " . mysql_error ();
		echo "<br/>SQL is " . $sql;
		exit ();
	}
	return $result;
}
*/
function loggedInUserCanEdit() {
	return is_user_logged_in() && current_user_can( 'edit_pages' );
}

function redirect($url, $statusCode = 301)
{
	header('Location: ' . $url, true, $statusCode);
	die();
}

function assetPath() {
    return "http://54.226.201.211/wp-content/MatchingGame/";
}

