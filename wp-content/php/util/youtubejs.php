<script type="text/javascript">
<?php global $seq; ?>

/**
 * Determine the mobile operating system.
 * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
 *
 * @returns {String}
 */
function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "iOS";
    }

    return "unknown";
}

var player;
function onYouTubeIframeAPIReady() {
	
  player = new YT.Player('youtube-video', {
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

function onPlayerReady() {
	if (getMobileOperatingSystem() != "iOS") {
	  player.playVideo();
	}
}



function onPlayerStateChange(event) {
	var sequence = <?php echo $seq ?>;
	
	if (event.data === 0) {
		url = "<?php bloginfo('url')?>/top100-autoplay?command=next&seq=" + sequence;
		window.location =  url;
	}
}

</script>

<?php /*
function outputYoutubeJS($autoplay) {
	echo 'var player;' . '\n';
	echo 'function onYouTubeIframeAPIReady() {';
	echo 'player = new YT.Player(\'youtube-video\', {';
	echo 'events: {';
	echo ' \'onReady\': onPlayerReady,';
	echo '\'onStateChange\': onPlayerStateChange';		
	echo '}})';
	echo '}';
	echo 'function onPlayerReady() {';
	if ($autoplay == 1):
		echo 'player.playVideo();';
	endif;
	echo '}';
	echo 'function onPlayerStateChange(event) {';
	if ($autoplay == 1):
		echo 'if (event.data === 0) {';
		echo 'window.location = ' . get_bloginfo('url') . '/top100-autoplay?command=next';
	endif;
	echo '}';
}
*/
?>