<?php
/**
 * Created by PhpStorm.
 * User: matto
 * Date: 10/9/2017
 * Time: 4:50 PM
 */

function getMySqliDbConnection() {
    $con = mysqli_connect(DBHOST, DBUSER, DBPASSWORD, DBSCHEMA);
    if (mysqli_connect_errno($con)) {
        die('Could not connect: ' . mysqli_connect_error());
    }
    return $con;
}
function getOdbcConnection() {
    $con = mysqli_connect(DBHOST, DBUSER, DBPASSWORD, DBSCHEMA);
    if (mysqli_connect_errno($con)) {
        die('Could not connect: ' . mysqli_connect_error());
    }
    return $con;
}
function mySqli_query_wrapper($con, $sql, $message) {
    $result = mysqli_query ($con, $sql );
    if ($result == false) {
        echo "<br/>" . $message . "<br/> --- Result = " . $result . "<br/>error: " . mysqli_error ($con);
        echo "<br/>SQL is " . $sql;
        exit ();
    }
    return $result;
}

function centralTable($tableName) {
    return CENTRAL_DBSCHEMA . "." . $tableName;
}

?>